<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title> Lulusnegeri | Edulab</title>
    <link rel="icon" type="image/png" href="<?= base_url()?>xfile/background/identitas-logo.png" />
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
    <link href="<?= base_url()?>assets-landing/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets-landing/css/webfont.css" rel="stylesheet">
    <!-- Core CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets-landing/css/bulma.css">
    <link rel="stylesheet" href="<?= base_url()?>assets-landing/css/app.css">
    <link rel="stylesheet" href="<?= base_url()?>assets-landing/css/core.css">
</head>

<body class="is-white">

    <!-- Pageloader -->
    <div class="pageloader"></div>
    <div class="infraloader is-active"></div>
    <div class="navbar is-inline-flex is-transparent no-shadow no-background is-landing is-hidden-mobile">
        <div class="container is-fluid">
            <div class="navbar-brand">
                <a href="/" class="">
                    <img src="<?= base_url()?>xfile/background/logo.png" style="height:55px;margin-left:12px;margin-top:6px;" alt="">
                </a>
            </div>
            <div class="navbar-menu">
                <!-- <div class="navbar-start">
                    <div class="navbar-item">
                        <a href="#">-</a>
                    </div>
                    <div class="navbar-item">
                        <a href="#kl"></a>
                    </div>
                    <div class="navbar-item">
                        <a>kslb</a>
                    </div>
                </div> -->

                <div class="navbar-end">

                    <div class="navbar-item">
                        <div class="navbar-item">
                            <a href="<?= base_url() ?>login">Masuk</a>
                        </div>
                        <a id="signup-button" href="#" class="button is-cta is-solid accent-button raised">
                            Daftar
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>    <nav class="navbar mobile-navbar is-landing is-hidden-desktop" aria-label="main navigation">
        <!-- Brand -->
        <div class="navbar-brand">
            <a class="navbar-item" href="/">
                <img class="dark-mobile-logo" src="<?= base_url()?>assets-landing/logoedu.png" alt="">
                <img class="light-mobile-logo" src="<?= base_url()?>assets-landing/logoedu.png" alt="">
            </a>

            <!-- Mobile menu toggler icon -->
            <div class="navbar-burger">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- Navbar mobile menu -->
        <div class="navbar-menu">
            <!-- Account -->
            <div class="navbar-item has-dropdown">
                <div class="navbar-link">
                    <img src="https://via.placeholder.com/150x150" data-demo-src="<?= base_url()?>assets-landing/images/avatars/avatar-w.png" alt="">
                    <span class="is-heading">Lulusnegeri</span>
                </div>

                <!-- Mobile Dropdown -->
                <div class="navbar-dropdown">
                    <a href="<?= base_url() ?>login" class="navbar-item">Login</a>
                    <a href="<?= base_url() ?>login" class="button is-fullwidth is-solid accent-button">Sign Up</a>
                </div>
            </div>
        </div>
    </nav>
    <div class="landing-wrapper">

        <div class="hero is-fullheight landing-hero-wrapper">
            <div id="particles-js"></div>
            <div class="hero-body">
                <div class="container">
                    <div class="columns landing-caption is-vcentered">
                        <div class="column is-6">
                            <img src="<?= base_url()?>assets-landing/bg-01.svg" alt="">
                        </div>
                        <div class="column is-5 is-offset-1">
                            <h2>Lulusnegeri</h2>
                            <h3>by Edulab.</h3>
                            <div class="buttons">
                                <a href="<?= base_url() ?>tryout/regist" class="button">Try Out</a>
                                <a href="<?= base_url() ?>portal/dashboard" class="button">Battlefield</a>
                                <!-- <button id="tour-start" class="button is-hidden-mobile">Battlefield</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div id="buy" class="section cta-wrapper">
            <div class="container">
                <div class="header-logo">
                    <img src="<?= base_url()?>assets-landing/images/logo/friendkit-white.svg" alt="">
                </div>
                <div class="cta-title">
                    <h3>Leaderboard on progress..</h3>

                </div>
                <img class="people-img" src="<?= base_url()?>assets-landing/bg2-01.svg" alt="">
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <div class="columns">
                    <!-- Column -->
                    <div class="column is-4">
                        <div>
                            <img class="small-footer-logo" src="<?= base_url()?>xfile/background/logo.png" alt="">
                            <div class="footer-description pt-10 pb-10">
                              LulusNegeri is online website examination system to measure the knowledge of student in Edulab, student can do exam online in their own time and their own device.
                            </div>
                        </div>
                        <div>
                            <span class="moto"> <i data-feather="heart"></i> by Edulab Indonesia.</span>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="column is-6 is-offset-2">
                        <div class="columns">
                            <!-- Column -->
                            <div class="column">
                                <ul class="footer-column">
                                    <li class="column-header">
                                        Lulusnegeri
                                    </li>
                                    <li class="column-item"><a href="#">Home</a></li>
                                    <li class="column-item"><a href="#">Try Out</a></li>
                                    <li class="column-item"><a href="#">Battlefield</a></li>
                                    <li class="column-item"><a href="#">Help</a></li>
                                </ul>
                            </div>
                            <!-- Column -->
                            <div class="column">
                                <ul class="footer-column">
                                    <li class="column-header">
                                        Quick Access
                                    </li>
                                    <li class="column-item"><a href="#">Top Global</a></li>
                                    <li class="column-item"><a href="#">Top Sekolah</a></li>
                                    <li class="column-item"><a href="#">Top Local</a></li>
                                    <li class="column-item"><a href="#">Top Harian</a></li>
                                </ul>
                            </div>
                            <!-- Column -->
                            <div class="column">
                                <ul class="footer-column">
                                    <li class="column-header">
                                        Terms
                                    </li>
                                    <li class="column-item"><a href="#">Terms of Service</a></li>
                                    <li class="column-item"><a href="#">Privacy policy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="copyright">
            <div class="container">
                <div class="inner">
                    <div class="left is-hidden-mobile">
                        &copy; 2019 Kostlab. All Rights Reserved.
                    </div>
                    <div class="right">
                        <img src="<?= base_url()?>assets-landing/images/logo/cssninja-grayscale.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
      </div>

    <!-- Concatenated js plugins and jQuery -->
    <script src="<?= base_url()?>assets-landing/js/app.js"></script>
    <script src="<?= base_url()?>assets-landing/js/particles.min.js"></script>

    <!-- Core js -->
    <script src="<?= base_url()?>assets-landing/data/tipuedrop_content.js"></script>
    <script src="<?= base_url()?>assets-landing/js/global.js"></script>
    <script src="<?= base_url()?>assets-landing/js/main.js"></script>

    <!-- Page and UI related js -->
    <script src="<?= base_url()?>assets-landing/js/landing.js"></script>
    <script src="<?= base_url()?>assets-landing/js/tour.js"></script>
</body>

</html>
