<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Try Out Online by Edulab">
    <meta name="author" content="Ansonika">
    <title>Edulab | Try Out</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?= base_url()."assets-front/"?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url()."assets-front/"?>css/menu.css" rel="stylesheet">
    <link href="<?= base_url()."assets-front/"?>css/style.css" rel="stylesheet">
	<link href="<?= base_url()."assets-front/"?>css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?= base_url()."assets-front/"?>css/custom.css" rel="stylesheet">

	<!-- MODERNIZR MENU -->
	<script src="<?= base_url()."assets-front/"?>js/modernizr.js"></script>
 <style>
 body{
   background-image:url(<?php echo base_url()?>xfile/background/bg-awal.png);
   background-repeat: no-repeat;  background-size: cover;
 }
 .logo-identitas{
   background-image:url(<?php echo base_url()?>xfile/background/awal-anim.png);
   background-repeat: no-repeat;  background-size:contain, cover;
   padding: 100px 0 100px 100px;
   left:21%;
   top:23%;
   position: absolute;
 }
 @media (max-width: 991px) {
   body{
     background-size: cover;
   }
   .logo-identitas{
     top: 28%;
     left:1%;
   }
   .img-fluid{
     bottom: 9%;
     right: 19%;
     position: absolute;
   }
 }
 @media (max-width: 767px) {
   body{
     background-image:url(<?php echo base_url()?>xfile/background/bg-awal.png);
     background-repeat: no-repeat;
   }
   .logo-identitas{
     /* right:2000%; */
     /* left:0%; */
   }
 }
 </style>
 <?php if($this->session->flashdata('message')) {
   $flashMessage=$this->session->flashdata('message');
 echo "<script>alert('$flashMessage')</script>";
 } ?>
</head>

<body>

	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div><!-- /Preload -->

	<div id="loader_form">
		<div data-loader="circle-side-2"></div>
	</div><!-- /loader_form -->

	<nav>
		<ul class="cd-primary-nav">
			<li><a href="<?= base_url()."tryout/"?>" class="animated_link">Home</a></li>
		</ul>
	</nav>
	<!-- /menu -->

  <div class="container-fluid full-height">
		<div class="row row-height">
			<div class="col-lg-6 content-left" id="start">
				<div class="content-left-wrapper-2">
						<!-- <a href="index.html" id="logo"><img src="<?= base_url()?>xfile/background/logo.png" alt="" width="49" height="35"></a> -->
					<!-- /social -->
					<div>
            <div class="logo-identitas">
            </div>
            <div id="wizard_container">
              <!-- <div id="top-wizard">
    							<div id="progressbar"></div>
    						</div> -->
                <form id="wrapped" method="POST" >
                  <input id="website" name="website" type="text" value="">
                  <div id="middle-wizard">
                    <div class="step">
                      <figure><a href="#" class="btn_1 rounded" style="padding: 40px 80px;" onclick="jumpto(1)">Student</a></figure>
                      <figure><a href="#" class="btn_1 rounded" style="padding: 40px 80px;">Educator</a></figure>
                      <figure><a href="#" class="btn_1 rounded mobile_btn" style="padding: 40px 80px;" onclick="jumpto(1)">Student</a></figure>
                      <figure><a href="#" class="btn_1 rounded mobile_btn" style="padding: 40px 80px;">Educator</a></figure>
                    </div>
                    <div class="step">
                      <figure><a href="<?php echo base_url()?>tryout/regist" class="btn_1 rounded" style="padding: 40px 80px;">Try Out</a></figure>
                      <figure><a href="<?php echo base_url()?>siswa" class="btn_1 rounded" style="padding: 40px 70px;">Dashboard</a></figure>
                      <figure><a href="<?php echo base_url()?>tryout/regist" class="btn_1 rounded mobile_btn" style="padding: 40px 80px;">Try Out</a></figure>
                      <figure><a href="<?php echo base_url()?>siswa" class="btn_1 rounded mobile_btn" style="padding: 40px 70px;">Dashboard</a></figure>
                    </div>
                  </div>
                  <div id="bottom-wizard">
                    <center><button type="button" name="backward" class="backward" style="position:absolute">Kembali</button></center>
                    <!-- <button type="button" name="forward" class="forward">Next</button> -->
                    <button type="submit" name="process" class="submit">Submit</button>
                  </div>
                </form>
          </div>
          <br>
						<!-- <a href="#start" class="btn_1 rounded mobile_btn">Start Now!</a> -->
					</div>
					<div class="copy">© 2019 Edulab</div>
				</div>
				<!-- /content-left-wrapper -->
			</div>
			<!-- /content-left -->

			<div class="col-lg-6 content-right">
              <center><figure><img src="<?= base_url()?>xfile/background/logo.png" alt="" class="img-fluid" width="60%"></figure></center>
			</div>
		</div>
	</div>

	<div class="cd-overlay-nav">
		<span></span>
	</div>
	<!-- /cd-overlay-nav -->

	<div class="cd-overlay-content">
		<span></span>
	</div>
	<!-- /cd-overlay-content -->

	<a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>
	<!-- /menu button -->

	<!-- Modal terms -->
	<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="termsLabel">Terms and conditions</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<p>Lorem ipsum dolor sit amet, in porro albucius qui, in <strong>nec quod novum accumsan</strong>, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
					<p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus. Lorem ipsum dolor sit amet, <strong>in porro albucius qui</strong>, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
					<p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn_1" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<!-- COMMON SCRIPTS -->
	<script src="<?= base_url()."assets-front/"?>js/jquery-3.2.1.min.js"></script>
    <script src="<?= base_url()."assets-front/"?>js/common_scripts.min.js"></script>
	<script src="<?= base_url()."assets-front/"?>js/velocity.min.js"></script>
	<script src="<?= base_url()."assets-front/"?>js/functions.js"></script>
	<script src="<?= base_url()."assets-front/"?>js/pw_strenght.js"></script>
  <script type="text/javascript">
  var base_url="<?= base_url()?>";
  function jumpto(no){
    $("#wizard_container").wizard('select',no);
  }
  </script>
	<!-- Wizard script -->
	<script src="<?= base_url()."assets-front/"?>js/quotation_func.js"></script>

</body>
</html>
