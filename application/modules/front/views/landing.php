<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title> Lulusnegeri | Edulab</title>
    <link rel="icon" type="image/png" href="<?= base_url()?>xfile/background/identitas-logo.png" />
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
    <link href="<?= base_url()?>assets-landing/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets-landing/css/webfont.css" rel="stylesheet">
    <!-- Core CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets-landing/css/bulma.css">
    <link rel="stylesheet" href="<?= base_url()?>assets-landing/css/app.css">
    <link rel="stylesheet" href="<?= base_url()?>assets-landing/css/core.css">
</head>

<body class="is-white">

    <!-- Pageloader -->
    <div class="pageloader"></div>
    <div class="infraloader is-active"></div>
    <div class="navbar is-inline-flex is-transparent no-shadow no-background is-landing is-hidden-mobile">
        <div class="container is-fluid">
            <div class="navbar-brand">
                <a href="/" class="">
                    <img src="<?= base_url()?>xfile/background/logo.png" style="height:55px;margin-left:12px;margin-top:6px;" alt="">
                </a>
            </div>
            <div class="navbar-menu">
                <!-- <div class="navbar-start">
                    <div class="navbar-item">
                        <a href="#">-</a>
                    </div>
                    <div class="navbar-item">
                        <a href="#kl"></a>
                    </div>
                    <div class="navbar-item">
                        <a>kslb</a>
                    </div>
                </div> -->

                <div class="navbar-end">
                  <?php if($this->session->userdata('status')==''){ ?>
                    <div class="navbar-item">
                        <div class="navbar-item">
                            <a href="<?= base_url() ?>login">Masuk</a>
                        </div>
                        <!-- <a id="signup-button" href="#" class="button is-cta is-solid accent-button raised">
                            Daftar
                        </a> -->
                    </div>
                  <?php }else{ ?>
                    <div class="navbar-item">
                      <?php if($this->session->userdata('level')=='admin'){ ?>
                        <a id="signup-button" href="<?= base_url() ?>admin" class="button is-cta is-solid accent-button raised">
                            Dashboard
                        </a>
                      <?php }else{ ?>
                        <a id="signup-button" href="<?= base_url() ?>portal/dashboard" class="button is-cta is-solid accent-button raised">
                            Dashboard
                        </a>
                      <?php } ?>

                    </div>
                  <?php } ?>
                </div>

            </div>
        </div>
    </div>    <nav class="navbar mobile-navbar is-landing is-hidden-desktop" aria-label="main navigation">
        <!-- Brand -->
        <div class="navbar-brand">
            <a class="navbar-item" href="/">
                <img class="dark-mobile-logo" src="<?= base_url()?>assets-landing/logoedu.png" alt="">
                <img class="light-mobile-logo" src="<?= base_url()?>assets-landing/logoedu.png" alt="">
            </a>

            <!-- Mobile menu toggler icon -->
            <div class="navbar-burger">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- Navbar mobile menu -->
        <div class="navbar-menu">
            <!-- Account -->
            <div class="navbar-item has-dropdown">
                <div class="navbar-link">
                    <img src="https://via.placeholder.com/150x150" data-demo-src="<?= base_url()?>assets-landing/images/avatars/avatar-w.png" alt="">
                    <span class="is-heading">Lulusnegeri</span>
                </div>

                <!-- Mobile Dropdown -->
                <?php if($this->session->userdata('status')==''){ ?>
                  <div class="navbar-dropdown">
                      <a href="<?= base_url() ?>login" class="button is-fullwidth is-solid accent-button">Masuk</a>
                  </div>
                <?php }else{ ?>
                  <div class="navbar-dropdown">
                    <?php if($this->session->userdata('level')=='admin'){ ?>
                      <a href="<?= base_url() ?>admin" class="navbar-item">Dashboard</a>
                    <?php }else{ ?>
                      <a href="<?= base_url() ?>portal/dashboard" class="navbar-item">Dashboard</a>
                    <?php } ?>
                      <a href="<?= base_url() ?>login/logout" class="button is-fullwidth is-solid accent-button">Keluar</a>
                  </div>
                <?php } ?>

            </div>
        </div>
    </nav>
    <div class="landing-wrapper">

        <div class="hero is-fullheight landing-hero-wrapper">
            <div id="particles-js"></div>
            <div class="hero-body">
                <div class="container">
                    <div class="columns landing-caption is-vcentered">
                        <div class="column is-6">
                            <img src="<?= base_url()?>assets-landing/bg-01.svg" alt="">
                        </div>
                        <div class="column is-5 is-offset-1">
                            <h2>Lulusnegeri</h2>
                            <h3>by Edulab.</h3>
                            <div class="buttons">
                                <a href="<?= base_url() ?>login?ref=tryout" class="button">Try Out</a>
                                <a href="<?= base_url() ?>portal/dashboard" class="button">Battlefield</a>
                                <!-- <button id="tour-start" class="button is-hidden-mobile">Battlefield</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div id="buy" class="section cta-wrapper">
            <div class="container">
                <div class="header-logo">
                    <img src="<?= base_url()?>assets-landing/images/logo/friendkit-white.svg" alt="">
                </div>
                <div class="cta-title">
                    <h3>Top Point</h3>
                </div>
            </div>
        </div>
        <div class="section">
          <div class="container">
          <div class="columns">
            <div class="column is-3">
              <div class="card">
                            <div class="card-heading is-bordered">
                                <h4>Top Point Nasional</h4>
                            </div>
                            <div class="card-body no-padding">
                                <!-- Recommended Page -->
                                <?php if($topSiswa!=NULL){ ?>
                                <?php foreach ($topSiswa as $key): ?>
                                <div class="page-block transition-block">
                                    <img src="<?= base_url() ?>assets-landing/siswa.svg" data-demo-src="<?= base_url() ?>assets-landing/siswa.svg" data-page-popover="0" alt="">
                                    <div class="page-meta">
                                        <span><?= $key->nama ?></span>
                                        <span>Total Point : <?= $key->jumlah_point ?> <br><?= $key->sekolah ?></span>
                                    </div>
                                    <div class="add-page add-transition">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                              <?php }else{ ?>
                                <p>belum ada data</p>
                              <?php } ?>
                                <!-- Recommended Page -->

                            </div>
                        </div>
            </div>
            <div class="column is-3">
              <div class="card">
                            <div class="card-heading is-bordered">
                                <h4>Top Sekolah</h4>
                            </div>
                            <div class="card-body no-padding">
                                <!-- Recommended Page -->
                                <?php if($topSekolah!=NULL){ ?>
                                <?php foreach ($topSekolah as $key): ?>
                                <div class="page-block transition-block">
                                    <img src="<?= base_url() ?>assets-landing/sekolah.svg" data-demo-src="<?= base_url() ?>assets-landing/sekolah.svg" data-page-popover="0" alt="">
                                    <div class="page-meta">
                                        <span><?= $key->sekolah ?></span>
                                        <span>Total Point <?= $key->point ?> (<?= $key->jumlah_siswa ?> siswa)</span>
                                    </div>
                                    <div class="add-page add-transition">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                              <?php }else{ ?>
                                <p>belum ada data</p>
                              <?php } ?>
                                <!-- Recommended Page -->

                            </div>
                        </div>
            </div>
            <div class="column">
              <img class="people-img" src="<?= base_url()?>assets-landing/top.png" alt="">
            </div>
          </div>
        </div>

        </div>

        <footer class="footer">
            <div class="container">
                <div class="columns">
                    <div class="column is-4">
                        <div>
                            <img class="small-footer-logo" src="<?= base_url()?>xfile/background/logo.png" alt="">
                            <div class="footer-description pt-10 pb-10">
                              LulusNegeri is online website examination system to measure the knowledge of student in Edulab, student can do exam online in their own time and their own device.
                            </div>
                        </div>
                        <div>
                            <span class="moto"> <i data-feather="heart"></i> by Edulab Indonesia.</span>
                        </div>
                    </div>
                    <div class="column is-6 is-offset-2">
                        <div class="columns">
                            <div class="column">
                                <ul class="footer-column">
                                    <li class="column-header">
                                        Lulusnegeri
                                    </li>
                                    <li class="column-item"><a href="<?= base_url() ?>">Home</a></li>
                                    <li class="column-item"><a href="<?= base_url() ?>portal/tryout/go">Try Out</a></li>
                                    <li class="column-item"><a href="<?= base_url() ?>portal/dashboard">Battlefield</a></li>
                                </ul>
                            </div>
                            <div class="column">
                                <ul class="footer-column">
                                    <li class="column-header">
                                        Quick Access
                                    </li>
                                    <li class="column-item"><a href="#">Top Global</a></li>
                                    <li class="column-item"><a href="#">Top Sekolah</a></li>
                                    <li class="column-item"><a href="#">Top Local</a></li>
                                    <li class="column-item"><a href="#">Top Harian</a></li>
                                </ul>
                            </div>
                            <div class="column">
                                <ul class="footer-column">
                                    <li class="column-header">
                                        Sosial Media
                                    </li>
                                    <li class="column-item"><a href="#">Instagram</a></li>
                                    <li class="column-item"><a href="#">Facebook</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="copyright">
            <div class="container">
                <div class="inner">
                    <div class="left is-hidden-mobile">
                        &copy; 2019 Kostlab. All Rights Reserved.
                    </div>
                    <div class="right">
                    </div>
                </div>
            </div>
        </div>
      </div>

    <!-- Concatenated js plugins and jQuery -->
    <script src="<?= base_url()?>assets-landing/js/app.js"></script>
    <script src="<?= base_url()?>assets-landing/js/particles.min.js"></script>

    <!-- Core js -->
    <script src="<?= base_url()?>assets-landing/data/tipuedrop_content.js"></script>
    <script src="<?= base_url()?>assets-landing/js/global.js"></script>
    <script src="<?= base_url()?>assets-landing/js/main.js"></script>

    <!-- Page and UI related js -->
    <script src="<?= base_url()?>assets-landing/js/landing.js"></script>
    <script src="<?= base_url()?>assets-landing/js/tour.js"></script>
</body>

</html>
