<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbs extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function getJumlahPoint($table,$limit){
    $this->db->select('*');
    $this->db->select('(total_point_ipa+total_point_ips+total_point_tpa) as jumlah_point');
    $this->db->from($table);
    $this->db->order_by('jumlah_point','DESC');
    $this->db->limit($limit);
    return $this->db->get();
  }

  function getJumlahPointHariIni($table,$limit,$date){
    if ($table == 'siswa') {
      $this->db->select('siswa.*');
      $this->db->from('siswa');
      $this->db->join('list_jawaban_soal', 'siswa.nise = list_jawaban_soal.nise', 'left');
      $this->db->where('DATE(list_jawaban_soal.waktu_mulai)', $date);
      $this->db->group_by('siswa.nise');
    }else {
      $this->db->select('users.*');
      $this->db->from('users');
      $this->db->join('list_jawaban_soal_users', 'users.id = list_jawaban_soal_users.user_id', 'left');
      $this->db->where('DATE(list_jawaban_soal_users.waktu_mulai)', $date);
      $this->db->group_by('users.id');
    }
    $this->db->select('SUM(point_soal) as jumlah_point');
    $this->db->order_by('jumlah_point','DESC');
    $this->db->limit($limit);
    return $this->db->get();
  }

  function getPointRank($limit,$jurusan = NULL){
    //get table siswa
    $this->db->select('siswa.nise as identity, siswa.nama, siswa.sekolah, sekolah.nama as sekolah_mitra');
    $this->db->select('siswa.total_point_ipa, siswa.total_point_ips, siswa.total_point_tpa');
    $this->db->select('(siswa.total_point_ipa+siswa.total_point_ips+siswa.total_point_tpa)as jumlah_point');
    $this->db->from('siswa');
    $this->db->join('sekolah', 'siswa.id_sekolah_mitra = sekolah.id', 'left');
    if ($jurusan == 'IPS') {
      $this->db->order_by('total_point_ips','DESC');
      $this->db->where('total_point_ips !=','0');
    }elseif ($jurusan == 'IPA') {
      $this->db->order_by('total_point_ipa','DESC');
      $this->db->where('total_point_ipa !=','0');
    }elseif ($jurusan == 'TPA') {
      $this->db->order_by('total_point_tpa','DESC');
      $this->db->where('total_point_tpa !=','0');
    }else {
      $this->db->order_by('jumlah_point','DESC');
      $this->db->where('(siswa.total_point_ipa+siswa.total_point_ips+siswa.total_point_tpa) !=','0');
    }
    $this->db->limit($limit);
    return $this->db->get();
  }

  function getPointRankHariIni($limit,$date,$jurusan = NULL){
    //get data siswa
    $this->db->select('list_jawaban_soal.nise as identity, list_jawaban_soal.waktu_mulai, SUM(list_jawaban_soal.point_soal) as jumlah_point');
    $this->db->select('siswa.nama, siswa.sekolah, sekolah.nama as sekolah_mitra');
    $this->db->from('list_jawaban_soal');
    $this->db->join('siswa', 'list_jawaban_soal.nise = siswa.nise', 'left');
    $this->db->join('sekolah', 'siswa.id_sekolah_mitra = sekolah.id', 'left');
    $this->db->where('DATE(list_jawaban_soal.waktu_mulai)', $date);
    if ($jurusan != NULL) {
      $this->db->select('mata_pelajaran.jurusan');
      $this->db->join('soal', 'list_jawaban_soal.kode_soal = soal.kode', 'left');
      $this->db->join('mata_pelajaran', 'soal.mapel = mata_pelajaran.id', 'left');
      $this->db->where('mata_pelajaran.jurusan', $jurusan);
    }
    $this->db->group_by('list_jawaban_soal.nise');
    $this->db->order_by('jumlah_point','DESC');
    $this->db->having('jumlah_point !=','0');
    $this->db->limit($limit);
    return $this->db->get();;
  }

  function getTopSekolah($limit){
    $this->db->select('sekolah.*, COUNT(siswa.nise) as jumlah_siswa');
    $this->db->from('sekolah');
    $this->db->join('siswa', 'sekolah.id = siswa.id_sekolah_mitra', 'left');
    $this->db->group_by('sekolah.id');
    $this->db->order_by('jumlah_siswa','DESC');
    $this->db->limit($limit);
    return $this->db->get();
  }

}
