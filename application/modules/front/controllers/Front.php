<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Dbs');
    $this->load->helper(array('dbs'));
    //Codeigniter : Write Less Do More
  }

  function index()
  {
    $data['name']='Kostlab';
    // $topSiswa=getdata('temporary_table',array('kode'=>'top_nasional'))->row();
    // $topSekolah=getdata('temporary_table',array('kode'=>'top_sekolah'))->row();
    // if($topSiswa->deskripsi!=NULL){
    //   $data['topSiswa']=json_decode($topSiswa->deskripsi);
    // }else{
    //   $data['topSiswa']=NULL;
    // }
    // if($topSekolah->deskripsi!=NULL){
    //   $data['topSekolah']=json_decode($topSekolah->deskripsi);
    // }else{
    //   $data['topSekolah']=NULL;
    // }
    $this->load->view('landing',$data);
  }

  function datafront(){
    /* Top Nasional - UPDATE
       mendapatkan point ranking siswa
       parameter = getPointRank(limit,jurusan(IPA,IPS,TPA))*/

    // $listRank = $this->Dbs->getPointRank('10')->result(); //berdasarkan jumlah point (ipa+ips+tpa)
    // $listRank = $this->Dbs->getPointRank('10','IPA')->result(); //berdasarkan jumlah point ipa
    // $listRank = $this->Dbs->getPointRank('10','IPS')->result(); //berdasarkan jumlah point ips
    // $listRank = $this->Dbs->getPointRank('10','TPA')->result(); //berdasarkan jumlah point tpa
    // var_dump($listRank);


    /** ==================================================================== **/


    /* Top Hari Ini - UPDATE
       mendapatkan point ranking siswa dan users berdasarkan tanggal
       parameter = getPointRankHariIni(limit,tanggal_hari_ini)*/

    // date_default_timezone_set('Asia/Jakarta');
    // $date = date('Y-m-d');
    // $date = "2019-05-02";
    // $listRankHariIni = $this->Dbs->getPointRankHariIni('10',$date)->result();
    // var_dump($listRankHariIni);


    /** ==================================================================== **/


    /* Top sekolah (Nasional) - UPDATE
       mendapatkan urutan jumlah siswa persekolah
       parameter = getTopSekolah(limit)*/
    // $topSekolah = $this->Dbs->getTopSekolah('10')->result();
    // var_dump($topSekolah);


    /** ==================================================================== **/


    /* Tambahan - Tidak dipakai
       Menampilkan data jumlah point per table berdasarkan tabel siswa / users
       param = getJumlahPoint(table,limit)
    */
    // $listJumlahPoint = $this->Dbs->getJumlahPoint('siswa','10')->result();
    // $listJumlahPoint = $this->Dbs->getJumlahPoint('users','10')->result();
    // var_dump($listJumlahPoint);


    /** ==================================================================== **/


    /* Tambahan - Tidak Dipakai
       Menampilkan data jumlah point hari ini (berdasarkan table siswa/users)
       jumlah_point = (ipa + ips + tpa user)
       param = getJumlahPointHariIni(table,limit,tanggal_sekarang);
    */
    // date_default_timezone_set('Asia/Jakarta');
    // $date = date('Y-m-d');
    //$date = "2019-05-02";
    //$listJumlahPointHariIni = $this->Dbs->getJumlahPointHariIni('users','10',$date)->result();
    // $listJumlahPointHariIni = $this->Dbs->getJumlahPointHariIni('siswa','10',$date)->result();
    // var_dump($listJumlahPointHariIni);


    /** ==================================================================== **/
  }



}
