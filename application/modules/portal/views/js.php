<script src="<?= base_url() ?>assets-portal/js/vendor/chart.js/dist/Chart.min.js" charset="utf-8"></script>
<script>
var mapelRadar=<?= $mapelRadar ?>;
var totalPointRadar=<?= $totalPointRadar ?>;
var ctx = document.getElementById('myChart');
var marksData = {
  labels: mapelRadar,
  datasets: [{
    label: "Point",
    backgroundColor: "rgba(246,196,17,0.5)",
    data: totalPointRadar
  }],
};
var myChart = new Chart(ctx, {
    type: 'bar',
    data: marksData,
});


</script>
