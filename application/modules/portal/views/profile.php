<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
    <div class="row">

      <div class="col-md-9">

        <!-- Tabbable Widget -->
        <div class="tabbable paper-shadow relative" data-z="0.5">

          <!-- Tabs -->
          <ul class="nav nav-tabs">
            <li class="active"><a href="<?php echo base_url()."$module/profile" ?>"><i class="fa fa-fw fa-lock"></i> <span class="hidden-sm hidden-xs">Edit Profile</span></a></li>
          </ul>
          <!-- // END Tabs -->

          <!-- Panes -->
          <div class="tab-content">

            <div id="account" class="tab-pane active">
              <form class="form-horizontal" action="<?php echo base_url().$action ?>" method="post">
                <div class="form-group">
                  <label for="inputEmail3" class="col-md-2 control-label">Nise</label>
                  <div class="col-md-6">
                    <div class="form-control-material">
                      <input type="text" class="form-control" name="username" id="nise" value="<?php echo $data_user->nis ?>" placeholder="Nise anda" readonly>
                      <!-- <label for="username">username anda</label> -->
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-md-2 control-label">Nama Lengkap</label>
                  <div class="col-md-6">
                    <div class="form-control-material">
                      <input type="text" class="form-control" name="nama" id="fullname" value="<?php echo $data_user->nama ?>" placeholder="Nama Lengkap Anda" readonly>
                      <!-- <label for="fullname"></label> -->
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-md-2 control-label">Kelas</label>
                  <div class="col-md-6">
                    <div class="form-control-material">
                      <input type="text" class="form-control" name="kelas" id="kelas" value="<?php echo $data_user->nama_kelas ?>" placeholder="Kelas" readonly>
                      <!-- <label for="kelas">Kelas anda sekarang</label> -->
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-md-2 control-label">Alamat</label>
                  <div class="col-md-6">
                    <div class="form-control-material">
                      <input type="text" class="form-control" name="alamat" id="fullname" value="<?php echo $data_user->alamat ?>" placeholder="Nama Lengkap Anda">
                      <!-- <label for="fullname"></label> -->
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail3" class="col-md-2 control-label">Email</label>
                  <div class="col-md-6">
                    <div class="form-control-material">
                      <input type="text" class="form-control" name="email" id="fullname" value="<?php echo $data_user->email ?>" placeholder="Nama Lengkap Anda">
                      <!-- <label for="fullname"></label> -->
                    </div>
                  </div>
                </div>
                <input type="hidden" name="previous_nis" value="<?php echo $data_user->nis ?>">
                <div class="form-group margin-none">
                  <div class="col-md-offset-2 col-md-10">
                    <button type="submit" class="btn btn-primary paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated>Save Changes</button>
                  </div>
                </div>
              </form>

            </div>

          </div>
          <!-- // END Panes -->

        </div>
        <!-- // END Tabbable Widget -->

        <br/>
        <br/>

      </div>
      <div class="col-md-3">
        <?php $this->load->view($menu_right) ?>
      </div>

    </div>
