<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
    <div class="row">

      <div class="col-md-9">

        <div class="panel panel-default col-md-12">
          <div class="panel-body table-responsive">
            <h3>Data Latihan Soal</h3>
            <br>

            <!-- Data table -->
            <table data-toggle="data-table" class="table" cellspacing="0" >
                <thead>
                  <tr>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Sub Mapel</th>
                    <th class="text-center">Waktu</th>
                    <th class="text-center">Jawaban Siswa</th>
                    <th class="text-center">Level</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th class="text-center">Nomor</th>
                    <th class="text-center">Mata Pelajaran</th>
                    <th class="text-center">Kunci Jawaban</th>
                    <th class="text-center">Jawaban</th>
                    <th class="text-center">Level</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php foreach ($listSoal as $soal): ?>
                    <tr>
                      <td class="text-center"><?php echo $soal->kode ?></td>
                      <td class="text-center"><?php echo $soal->nama_sub ?></td>
                      <td class="text-center"><?php echo $soal->waktu_mulai ?></td>
                      <td class="text-center">
                        <?php echo strtoupper($soal->jawaban_siswa) ?>
                        <?php if (strtoupper($soal->jawaban_siswa) == strtoupper($soal->jawaban_soal)): ?>
                          <span class="fa fa-check-circle"><i></i></span>
                        <?php else: ?>
                          <span class="fa fa-times-circle"><i></i></span>
                        <?php endif; ?>
                      </td>
                      <td class="text-center"><?php echo $soal->level ?></td>
                      <td class="text-center">
                        <a href="<?php echo base_url()."$module/mycourses/detailsoal?id=".$soal->id_jawaban."&type=latihan" ?>">
                          <button class="btn btn-primary btn-stroke">Detail</button>
                        </a>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            <!-- // Data table -->

            <!-- // Data table -->
          </div>

        </div>

        <br/>
        <br/>

      </div>
      <div class="col-md-3">
        <?php $this->load->view($menu_right) ?>
      </div>

    </div>
