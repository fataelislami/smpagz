<div class="row">

  <div class="col-md-9">


    <div class="row" data-toggle="isotope">

      <div class="panel panel-default col-md-12">
        <div class="panel-body">
          <h3>Data Siswa</h3>
          <br>
          <div class="form-group form-control-material static">
            <input type="text" class="form-control" id="dataTryout1" value="<?php echo $dataTryout->nama ?>" placeholder="" readonly>
            <label for="dataTryout1">Nama</label>
          </div>
          <div class="form-group form-control-material static">
            <input type="text" class="form-control" id="dataTryout2" value="SMP Al-Ghozali Purwakarta" placeholder="" readonly>
            <label for="dataTryout2">Asal Sekolah</label>
          </div>
        </div>
      </div>

      <div class="panel panel-default col-md-12">
        <div class="panel-body table-responsive">
          <div class="row">
            <h3 class="col-md-6">Data Tryout</h3>
            <a href="<?php echo base_url().$module."/export/tryout?level=".$this->session->userdata('level')."&id="."$idTryout" ?>" class="col-md-6 text-right">
              <button class="btn btn-success"><i class="fa fa-check-circle"></i> Ekspor</button>
            </a>
          </div>
          <br>
          <div class="row">
            <div class="col-md-8">
              <div class="form-group form-control-material static">
                <input type="text" class="form-control" id="dataTryout1" value="<?php echo $dataTryout->nama_tryout_event ?>" placeholder="" readonly>
                <label for="dataTryout1">Event</label>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-control-material static">
                <input type="text" class="form-control" id="dataTryout1" value="<?php echo $dataTryout->kode_soal ?>" placeholder="" readonly>
                <label for="dataTryout1">Kode Soal</label>
              </div>
            </div>
          </div>

          <!-- Data table -->
          <table data-toggle="data-table" class="table" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="text-center">Nomor</th>
                <th class="text-center">Mata Pelajaran</th>
                <th class="text-center">Kunci Jawaban</th>
                <th class="text-center">Jawaban</th>
                <th class="text-center">Aksi</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th class="text-center">Nomor</th>
                <th class="text-center">Mata Pelajaran</th>
                <th class="text-center">Kunci Jawaban</th>
                <th class="text-center">Jawaban</th>
                <th class="text-center">Aksi</th>
              </tr>
            </tfoot>
            <tbody>
              <?php foreach ($dataJawaban as $jb): ?>
                <tr>
                  <td class="text-center"><?php echo $jb->nomor ?></td>
                  <td class="text-center"><?php echo $jb->nama ?></td>
                  <td class="text-center"><?php echo $jb->kunci_jawaban ?></td>
                  <td class="text-center"><?php echo $jb->jawaban_siswa ?></td>
                  <td class="text-center">
                    <a href="<?php echo base_url()."$module/mycourses/detailsoal?id=".$jb->kode ?>">
                      <button class="btn btn-primary btn-stroke">Detail</button>
                    </a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <!-- // Data table -->
          <br />
          <hr>
          <h3>Data Jawaban Berdasarkan Mata Pelajaran</h3>
          <br>
          <!-- Data table -->
          <table class="table panel panel-default table-pricing-2" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th class="text-center">Nama Mata Pelajaran</th>
                <th class="text-center">Benar</th>
                <th class="text-center">Salah</th>
                <th class="text-center">Kosong</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($dataJawabanMapel as $jm): ?>
                <tr>
                  <td class="text-center"><?php echo $jm['nama_mapel'] ?></td>
                  <td class="text-center"><?php echo $jm['benar'] ?></td>
                  <td class="text-center"><?php echo $jm['salah'] ?></td>
                  <td class="text-center"><?php echo $jm['kosong'] ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <!-- // Data table -->
        </div>

          </div>

    </div>


    <br/>
    <br/>
    <br>
    <br>
    <br>


  </div>
  <div class="col-md-3">

    <?php $this->load->view($menu_right) ?>

  </div>

</div>
