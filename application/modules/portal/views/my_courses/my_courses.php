<div class="row">

  <div class="col-md-9">

    <div class="panel panel-default curriculum paper-shadow" data-z="0.5">
      <div class="panel-heading panel-heading-gray" data-toggle="collapse" data-target="#curriculum-1">
        <div class="media">
          <div class="media-left">
            <span class="icon-block img-circle bg-indigo-300 half text-white"><i class="fa fa-graduation-cap"></i></span>
          </div>
          <div class="media-body">
            <h4 class="text-headline">Riwayat Tryout (<?php echo sizeof($listTryoutDashboard) ?>)</h4>
          </div>
        </div>
        <span class="collapse-status collapse-open">Open</span>
        <span class="collapse-status collapse-close">Close</span>
      </div>
      <div class="list-group collapse in" id="curriculum-1">
        <?php if (sizeof($listTryoutDashboard)>0):
            $num = 1; ?>
          <?php foreach ($listTryoutDashboard as $to): ?>
            <div class="list-group-item media" data-target="<?php echo base_url()."portal/mycourses/tryout/".$to->id ?>">
              <div class="media-left">
                <div class="text-crt"><?php echo "$num"; ?></div>
              </div>
              <div class="media-body">
                <i class="fa fa-fw fa-circle text-green-300"></i>
                <?php echo $to->nama ?>
                <div class="caption">
                  <span class="text-light"><?php echo formatharitanggal($to->tanggal) ?></span>

                </div>
              </div>
              <div class="media-right">
                <div class="width-100 text-right text-caption"><?php echo $to->skor ?></div>
              </div>
            </div>
          <?php $num++;
                endforeach; ?>
        <?php endif; ?>
      </div>
    </div>
    <br>
    <!-- <ul class="pagination margin-top-none">
      <li class="disabled"><a href="#">&laquo;</a></li>
      <li class="active"><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">&raquo;</a></li>
    </ul> -->
    <br/>
    <br/>

  </div>
  <div class="col-md-3">

    <?php $this->load->view($menu_right) ?>

  </div>

</div>
