<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
    <div class="row">

      <div class="col-md-9">

    <div class="panel panel-default paper-shadow" data-z="0.5">
      <div class="media v-middle">
        <div class="media-left" style="display:none">
            <div class="panel-body">
            </div>
        </div>
        <div class="media-body">
          <div class="panel-body">
            <h4 class="text-headline" style="font-size:20px">Soal</h4>

          </div>
        </div>
        <div class="media-right media-padding">
          <a class="btn btn-white paper-shadow relative" data-z="0.5" data-hover-z="1" data-animated href="<?= base_url() ?>portal/soal/<?= $datasoal->nomor ?>?sub=<?= $datasoal->nomor ?>">
          Kerjakan Soal <?= $datasoal->nama ?>
          </a>
        </div>
      </div>
      <div class="panel-body soal-body">
        <?php echo $datasoal->soal; ?>
        <br>
        <hr>
        <div class="text-subhead-2 text-light">Answer</div>
        <div class="checkbox checkbox-primary">
          <input id="checkbox1" type="checkbox" disabled="" <?php echo ($datasoal->jawaban=='A')?"checked=\"\"":""?>>
          <label for="checkbox1"><?php echo $datasoal->pil_a ?></label>
        </div>
        <div class="checkbox checkbox-primary">
          <input id="checkbox1" type="checkbox" disabled="" <?php echo ($datasoal->jawaban == 'B')? "checked=\"\"":"" ?>>
          <label for="checkbox1"><?php echo $datasoal->pil_b ?></label>
        </div>
        <div class="checkbox checkbox-primary">
          <input id="checkbox1" type="checkbox" disabled="" <?php echo ($datasoal->jawaban=='C')?"checked=\"\"":""?>>
          <label for="checkbox1"><?php echo $datasoal->pil_c ?></label>
        </div>
        <div class="checkbox checkbox-primary">
          <input id="checkbox1" type="checkbox" disabled="" <?php echo ($datasoal->jawaban=='D')?"checked=\"\"":""?>>
          <label for="checkbox1"><?php echo $datasoal->pil_d ?></label>
        </div>
        <div class="checkbox checkbox-primary">
          <input id="checkbox1" type="checkbox" disabled="" <?php echo ($datasoal->jawaban=='E')?"checked=\"\"":""?>>
          <label for="checkbox1"><?php echo $datasoal->pil_e ?></label>
        </div>
      </div>
      <div class="panel-footer">
        <div class="">
          <h4>Jawaban = <?php echo $datasoal->jawaban ?></h4>
        </div>
      </div>

    </div>

    <div class="panel panel-default paper-shadow" data-z="0.5">
      <div class="panel-body">
        <h4>Penjelasan</h4>
        <hr>
        <?php if ($datasoal->penjelasan != "") {
          echo $datasoal->penjelasan;
        }else {
          echo "Penjelasan soal belum tersedia";
        } ?>
      </div>
    </div>

        <br/>
        <br/>

      </div>
      <div class="col-md-3">
        <?php $this->load->view($menu_right) ?>
      </div>

    </div>
