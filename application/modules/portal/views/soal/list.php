<div class="row">
  <div class="col-md-8 col-lg-9">

    <?php if (isset($list_mapel_ipa)): ?>
      <div class="panel panel-default paper-shadow" data-z="0.5">
        <ul class="list-group">
          <li class="list-group-item">
            <div class="media v-middle">
              <div class="media-body">
                <h4 class="text-headline margin-none">IPA</h4>
                <p class="text-subhead text-light">Latihan Soal Jurusan Ilmu Pengetahuan Alam</p>
              </div>
              <div class="media-right">
                <!-- <a href="#" class="btn btn-white btn-flat"><i class="fa fa-fw fa-plus"></i> Lihat Lebih Banyak</a> -->
              </div>
            </div>
          </li>

          <?php foreach ($list_mapel_ipa as $mapel): ?>
            <li class="list-group-item media v-middle">
              <div class="media-left">
                <?php if ($mapel->image != NULL): ?>
                  <img src="<?= base_url() ?>xfile/mapel/<?php echo $mapel->image ?>" style="width:50px;height:50px;" alt="" class="img-circle" />
                <?php else: ?>
                  <div class="icon-block half img-circle bg-grey-300">
                    <i class="fa fa-file-text text-white"></i>
                  </div>
                <?php endif; ?>
              </div>
              <div class="media-body">
                <h4 class="text-subhead margin-none">
                  <a href="<?= base_url() ?>portal/soal/<?= $mapel->slug ?>" class="link-text-color"><?php echo $mapel->nama ?></a>
                </h4>
                <div class="text-light text-caption">
                  jumlah sub mapel  <?php echo $mapel->nama ?> : <?php echo $mapel->jumlah_submapel ?>
                </div>
              </div>
              <div class="media-right">
                <a href="<?= base_url() ?>portal/soal/<?= $mapel->slug ?>" class="btn btn-white text-light"><i class="fa fa-tasks fa-fw"></i> <?php echo ($mapel->jumlah_soal != "") ? $mapel->jumlah_soal : "0" ; ?></a>
              </div>
            </li>
          <?php endforeach; ?>

        </ul>
      </div>
    <?php endif; ?>

    <?php if (isset($list_mapel_ips)): ?>
      <div class="panel panel-default paper-shadow" data-z="0.5">
        <ul class="list-group">
          <li class="list-group-item">
            <div class="media v-middle">
              <div class="media-body">
                <h4 class="text-headline margin-none">IPS</h4>
                <p class="text-subhead text-light">Latihan Soal Jurusan Ilmu Pengetahuan Sosial</p>
              </div>
              <div class="media-right">
                <!-- <a href="#" class="btn btn-white btn-flat"><i class="fa fa-fw fa-plus"></i> Lihat Lebih Banyak</a> -->
              </div>
            </div>
          </li>

          <?php foreach ($list_mapel_ips as $mapel): ?>
            <li class="list-group-item media v-middle">
              <div class="media-left">
                <?php if ($mapel->image != NULL): ?>
                  <img src="<?= base_url() ?>xfile/mapel/<?php echo $mapel->image ?>" style="width:50px;height:50px;" alt="" class="img-circle" />
                <?php else: ?>
                  <div class="icon-block half img-circle bg-grey-300">
                    <i class="fa fa-file-text text-white"></i>
                  </div>
                <?php endif; ?>
              </div>
              <div class="media-body">
                <h4 class="text-subhead margin-none">
                  <a href="<?= base_url() ?>portal/soal/<?= $mapel->slug ?>" class="link-text-color"><?php echo $mapel->nama ?></a>
                </h4>
                <div class="text-light text-caption">
                  jumlah sub mapel  <?php echo $mapel->nama ?> : <?php echo $mapel->jumlah_submapel ?>
                </div>
              </div>
              <div class="media-right">
                <a href="<?= base_url() ?>portal/soal/<?= $mapel->slug ?>" class="btn btn-white text-light"><i class="fa fa-tasks fa-fw"></i> <?php echo ($mapel->jumlah_soal != "") ? $mapel->jumlah_soal : "0" ; ?></a>
              </div>
            </li>
          <?php endforeach; ?>

        </ul>
      </div>
    <?php endif; ?>

    <?php if (isset($list_mapel_ipc)): ?>
      <div class="panel panel-default paper-shadow" data-z="0.5">
        <ul class="list-group">
          <li class="list-group-item">
            <div class="media v-middle">
              <div class="media-body">
                <h4 class="text-headline margin-none">IPC</h4>
                <p class="text-subhead text-light">Latihan Soal Jurusan Ilmu Pengetahuan Campuran</p>
              </div>
              <div class="media-right">
                <!-- <a href="#" class="btn btn-white btn-flat"><i class="fa fa-fw fa-plus"></i> Lihat Lebih Banyak</a> -->
              </div>
            </div>
          </li>

          <?php foreach ($list_mapel_ipc as $mapel): ?>
            <li class="list-group-item media v-middle">
              <div class="media-left">
                <?php if ($mapel->image != NULL): ?>
                  <img src="<?= base_url() ?>xfile/mapel/<?php echo $mapel->image ?>" style="width:50px;height:50px;" alt="" class="img-circle" />
                <?php else: ?>
                  <div class="icon-block half img-circle bg-grey-300">
                    <i class="fa fa-file-text text-white"></i>
                  </div>
                <?php endif; ?>
              </div>
              <div class="media-body">
                <h4 class="text-subhead margin-none">
                  <a href="<?= base_url() ?>portal/soal/<?= $mapel->slug ?>" class="link-text-color"><?php echo $mapel->nama ?></a>
                </h4>
                <div class="text-light text-caption">
                  jumlah sub mapel  <?php echo $mapel->nama ?> : <?php echo $mapel->jumlah_submapel ?>
                </div>
              </div>
              <div class="media-right">
                <a href="<?= base_url() ?>portal/soal/<?= $mapel->slug ?>" class="btn btn-white text-light"><i class="fa fa-tasks fa-fw"></i> <?php echo ($mapel->jumlah_soal != "") ? $mapel->jumlah_soal : "0" ; ?></a>
              </div>
            </li>
          <?php endforeach; ?>

        </ul>
      </div>
    <?php endif; ?>


    <br/>

  </div>
  <div class="col-md-4 col-lg-3">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="form-group form-control-material">
          <input id="forumSearch" type="text" class="form-control" placeholder="Type to search" />
          <label for="forumSearch">Search ...</label>
        </div>
        <a href="#" class="btn btn-inverse paper-shadow relative" data-z="0.5" data-hover-z="1">Search</a>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title" style="margin-bottom:8px">Top Point Nasional</h4>
        <ul class="nav nav-tabs" tabindex="0" style="margin-left:-15px">
          <li class="active"><a href="#rank_today" data-toggle="tab" aria-expanded="true"><i class="fa fa-fw fa-clock-o"></i> Hari Ini</a></li>
          <li class=""><a href="#rank" data-toggle="tab" aria-expanded="false"><i class="fa fa-fw fa-calendar"></i> Sepanjang Waktu</a></li>
        </ul>
      </div>
      <div class="panel-body list-group">
        <div class="tabbable">
              <div class="tab-content" style="padding:0px">
                <div id="rank_today" class="tab-pane active">
                  <ul class="list-group">
                    <?php if($rank_today!=NULL){ ?>
                      <?php $i=1; ?>
                      <?php foreach ($rank_today as $key): ?>
                        <?php if($i==1){ ?>
                          <li class="list-group-item">
                            <div class="media v-middle">
                              <div class="media-left">
                                <div class="icon-block s30 bg-blue-400 text-white"><?= $i ?></div>
                              </div>
                              <div class="media-body">
                                <a href="#" class="link-text-color"><?= $key->nama?> (<?= $key->jumlah_point ?> point)<br><sekolah style="font-size:10px;"><?= $key->sekolah?></sekolah></a>
                              </div>
                            </div>
                          </li>
                        <?php }else{ ?>
                          <li class="list-group-item">
                            <div class="media v-middle">
                              <div class="media-left">
                                <div class="icon-block s30 bg-grey-400 text-white"><?= $i ?></div>
                              </div>
                              <div class="media-body">
                                <a href="#" class="link-text-color"><?= $key->nama?> (<?= $key->jumlah_point ?> point)<br><sekolah style="font-size:10px;"><?= $key->sekolah?></sekolah></a>
                              </div>
                            </div>
                          </li>
                        <?php } ?>

                        <?php $i++ ?>
                      <?php endforeach; ?>
                    <?php } ?>
                  </ul>
                </div>
                <div id="rank" class="tab-pane">
                  <ul class="list-group">
                    <?php if($rank!=NULL){ ?>
                      <?php $i=1; ?>
                      <?php foreach ($rank as $key): ?>
                        <?php if($i==1){ ?>
                          <li class="list-group-item">
                            <div class="media v-middle">
                              <div class="media-left">
                                <div class="icon-block s30 bg-blue-400 text-white"><?= $i ?></div>
                              </div>
                              <div class="media-body">
                                <a href="#" class="link-text-color"><?= $key->nama?> (<?= $key->jumlah_point ?> point)<br><sekolah style="font-size:10px;"><?= $key->sekolah?></sekolah></a>
                              </div>
                            </div>
                          </li>
                        <?php }else{ ?>
                          <li class="list-group-item">
                            <div class="media v-middle">
                              <div class="media-left">
                                <div class="icon-block s30 bg-grey-400 text-white"><?= $i ?></div>
                              </div>
                              <div class="media-body">
                                <a href="#" class="link-text-color"><?= $key->nama?> (<?= $key->jumlah_point ?> point)<br><sekolah style="font-size:10px;"><?= $key->sekolah?></sekolah></a>
                              </div>
                            </div>
                          </li>
                        <?php } ?>

                        <?php $i++ ?>
                      <?php endforeach; ?>
                    <?php } ?>
                  </ul>
                </div>
              </div>
              <!-- // END Panes -->

            </div>
      </div>
      <hr>
      <?php if(sizeof($currentRank)>0){ ?>
      <?php if ($currentRank->jumlah_point > 0): ?>
        <div class="panel-body list-group">
          <ul class="list-group">
            <li class="list-group-item">
              <div class="media v-middle">
                <div class="media-left">
                  <div class="icon-block s30 bg-green-400 text-white"><i class="fa fa-star"></i></div>
                </div>
                <div class="media-body">
                  <a href="#" class="link-text-color"><?= $currentRank->nama ?> (<?= $currentRank->jumlah_point ?> point)</a>
                </div>
              </div>
            </li>
          </ul>
        </div>
      <?php endif; ?>
    <?php } ?>
      <div class="panel-footer">
        <span class="fa fa-fw fa-circle text-green-400"></span> Point Saat Ini
        <br/>
        <span class="fa fa-fw fa-circle text-blue-400"></span> Top Point
      </div>
    </div>
  </div>
</div>
