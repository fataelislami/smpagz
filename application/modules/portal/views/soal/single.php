<div class="row">

  <div class="col-md-9">

    <h5 class="text-subhead-2 text-light">Silabus</h5>
    <div class="panel panel-default curriculum open paper-shadow" data-z="0.5">
      <div class="panel-heading panel-heading-gray" data-toggle="collapse" data-target="#curriculum-1">
        <div class="media">
          <div class="media-left">
            <span class="icon-block img-circle bg-indigo-300 half text-white"><i class="fa fa-graduation-cap"></i></span>
          </div>
          <div class="media-body">
            <h4 class="text-headline"><?= $getSubMataPelajaran->row()->nama_mapel ?></h4>
            <p><?= $getSubMataPelajaran->row()->durasi ?></p>
          </div>
        </div>
        <span class="collapse-status collapse-open">Show</span>
        <span class="collapse-status collapse-close">Hide</span>
      </div>
      <div class="list-group collapse in" id="curriculum-1">
        <?php $i=1; ?>
        <?php foreach ($getSubMataPelajaran->result() as $key): ?>
          <div class="list-group-item media" data-target="<?php echo base_url()."portal/soal/".$key->slug_mapel."?sub=".$key->slug_submapel ?>">
            <div class="media-left">
              <div class="text-crt"><?= $i ?>.</div>
            </div>
            <div class="media-body">
              <i class="fa fa-fw fa-circle text-green-300"></i> <?= $key->nama_sub_mapel ?>
            </div>
            <div class="media-right">
              <div class="width-100 text-right text-caption"><?= $key->jumlah_soal ?></div>
            </div>
          </div>
          <?php $i++ ?>
        <?php endforeach; ?>
      </div>
    </div>

    <br/>
    <br/>

  </div>
  <div class="col-md-3">

    <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
      <div class="panel-heading panel-collapse-trigger">
        <h4 class="panel-title">My Account</h4>
      </div>
      <div class="panel-body list-group">
        <ul class="list-group list-group-menu">
          <li class="list-group-item"><a class="link-text-color" href="<?php echo base_url().$module ?>/dashboard">Dashboard</a></li>
          <li class="list-group-item"><a class="link-text-color" href="<?php echo base_url().$module ?>/mycourses">Aktivitas Tryout</a></li>
          <li class="list-group-item"><a class="link-text-color" href="<?php echo base_url().$module ?>/profile">Profil</a></li>
          <li class="list-group-item"><a class="link-text-color" href="<?= base_url()?>login/logout"><span>Keluar</span></a></li>
        </ul>
      </div>
    </div>

    <div class="panel panel-default margin-none">
      <div class="panel-heading">
        <h4 class="panel-title">Top Point <?= $getSubMataPelajaran->row()->nama_mapel ?></h4>
      </div>
      <div class="panel-body list-group">
        <ul class="list-group">
          <?php if($rank!=NULL){ ?>
            <?php $i=1; ?>
            <?php foreach ($rank as $key): ?>
              <?php if($i==1){ ?>
                <li class="list-group-item">
                  <div class="media v-middle">
                    <div class="media-left">
                      <div class="icon-block s30 bg-blue-400 text-white"><?= $i ?></div>
                    </div>
                    <div class="media-body">
                      <a href="#" class="link-text-color"><?= $key->nama?> (<?= $key->jumlah_point ?> point)</a>
                    </div>
                  </div>
                </li>
              <?php }else{ ?>
                <li class="list-group-item">
                  <div class="media v-middle">
                    <div class="media-left">
                      <div class="icon-block s30 bg-grey-400 text-white"><?= $i ?></div>
                    </div>
                    <div class="media-body">
                      <a href="#" class="link-text-color"><?= $key->nama?> (<?= $key->jumlah_point ?> point)</a>
                    </div>
                  </div>
                </li>
              <?php } ?>

              <?php $i++ ?>
            <?php endforeach; ?>
          <?php } ?>
        </ul>
      </div>
      <hr>
      <div class="panel-footer">
        <span class="fa fa-fw fa-circle text-blue-400"></span> Top Point
      </div>
    </div>


  </div>

</div>
