<div class="row">

  <div class="col-md-9">

    <div class="text-subhead-2 text-light">Soal <?= $getSoal->nama_soal ?></div>
    <div class="panel panel-default paper-shadow" data-z="0.5">
      <!-- <div class="panel-heading">
        <h4 class="text-headline">Step by Step</h4>
      </div> -->
      <div class="panel-body soal-body">
        <?= $getSoal->soal ?>
      </div>
    </div>

    <div class="text-subhead-2 text-light">Pilih Jawaban</div>
    <div class="panel panel-default paper-shadow" data-z="0.5">
      <div class="panel-body">
            <table>
              <tr>
                <td>
                  <b>A</b>.
                </td>
                <td>
                  <div class="radio radio-primary">
                    <input type="radio" name="jawaban" id="radio11" value="A">
                    <label for="radio11"><?= $getSoal->pil_a ?></label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>B</b>.
                </td>
                <td>
                  <div class="radio radio-primary">
                    <input type="radio" name="jawaban" id="radio12" value="B">
                    <label for="radio12"><?= $getSoal->pil_b ?></label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>C</b>.
                </td>
                <td>
                  <div class="radio radio-warning">
                    <input type="radio" name="jawaban" id="radio13" value="C">
                    <label for="radio13"><?= $getSoal->pil_c ?></label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>D</b>.
                </td>
                <td>
                  <div class="radio radio-warning">
                    <input type="radio" name="jawaban" id="radio14" value="D">
                    <label for="radio14"><?= $getSoal->pil_d ?></label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>E</b>.
                </td>
                <td>
                  <div class="radio radio-warning">
                    <input type="radio" name="jawaban" id="radio15" value="E">
                    <label for="radio15"><?= $getSoal->pil_e ?></label>
                  </div>
                </td>
              </tr>
            </table>
      </div>
      <div class="panel-footer">
        <div class="text-right">
          <button class="btn btn-success" id="simpan"><i class="fa fa-save fa-fw"></i> Simpan Jawaban</button>
          <button class="btn btn-primary" id="next"><i class="fa fa-chevron-right fa-fw"></i> Lewati</button>
        </div>
      </div>
    </div>

    <br/>
    <br/>

  </div>
  <div class="col-md-3">

    <div class="s-container">
      <div class="text-subhead-2 text-light">Waktu</div>
      <div class="tk-countdown"></div>
    </div>

    <div class="panel panel-default margin-none">
      <div class="panel-heading">
        <h4 class="panel-title">Top Point <?php echo $mapel->nama ?> hari ini</h4>
      </div>
      <div class="panel-body list-group">
        <ul class="list-group">
          <?php if($rank!=NULL){ ?>
            <?php $i=1; ?>
            <?php foreach ($rank as $key): ?>
              <?php if($i==1){ ?>
                <li class="list-group-item">
                  <div class="media v-middle">
                    <div class="media-left">
                      <div class="icon-block s30 bg-blue-400 text-white"><?= $i ?></div>
                    </div>
                    <div class="media-body">
                      <a href="#" class="link-text-color"><?= $key->nama?> (<?= $key->jumlah_point ?> point)</a>
                    </div>
                  </div>
                </li>
              <?php }else{ ?>
                <li class="list-group-item">
                  <div class="media v-middle">
                    <div class="media-left">
                      <div class="icon-block s30 bg-grey-400 text-white"><?= $i ?></div>
                    </div>
                    <div class="media-body">
                      <a href="#" class="link-text-color"><?= $key->nama?> (<?= $key->jumlah_point ?> point)</a>
                    </div>
                  </div>
                </li>
              <?php } ?>

              <?php $i++ ?>
            <?php endforeach; ?>
          <?php } ?>
        </ul>
      </div>
      <hr>
      <div class="panel-footer">
        <span class="fa fa-fw fa-circle text-blue-400"></span> Top Point
      </div>
    </div>

  </div>

</div>
