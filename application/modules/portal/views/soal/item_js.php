<script src="<?= base_url() ?>assets-portal/js/vendor/countdown/all.js"></script>
<script type="text/javascript">
var today = new Date();
var dateFromPHP='<?= $waktu_mulai; ?>'//Ganti dengan waktu mulai
var now = new Date(dateFromPHP.replace(/-/g, '/'));
var id_jawaban='<?php if($getSoal->id_jawaban!=NULL){echo $getSoal->id_jawaban;}else{echo $id_jawaban;} ?>';
var menit=<?= $menit ?>;
// $('.tk-countdown').countdown({until: newYear});
$('.tk-countdown').countdown({since: '-'+menit+'M',
  format: 'HMS',onTick: watchCountUp});//,timezone: +7
  function watchCountUp(periods) {
    // console.log('Just ' + periods[5] + ' minutes and ' +
    //     periods[6] + ' seconds to go');
  // $('#monitor').text('Just ' + periods[5] + ' minutes and ' +
  //     periods[6] + ' seconds to go');
}
</script>
<script type="text/javascript">
  $(".radio").change(function(){
    var jawaban=$("input[name=jawaban]:checked").val();
  });
  $("#simpan").click(function(){
    var jawaban=$("input[name=jawaban]:checked").val();
    var levelUser='<?= $level ?>';
    var nise_id='<?= $nise_id ?>';
    var kode_soal='<?= $getSoal->kode ?>';
    var point_soal='<?= $getSoal->point_soal ?>';
    if(typeof jawaban==="undefined"){
      Swal.fire({
  type: 'error',
  title: 'Oops...',
  showConfirmButton: false,
  text: 'Kamu belum milih jawaban untuk disimpan',
  timer: 1000
})
    }else{
      answer(levelUser,nise_id,kode_soal,jawaban,point_soal);
    }
  });
  $("#next").click(function(){
    var jawaban=$("input[name=jawaban]:checked").val();
    var levelUser='<?= $level ?>';
    var nise_id='<?= $nise_id ?>';
    var kode_soal='<?= $getSoal->kode ?>';
    var point_soal='<?= $getSoal->point_soal ?>';
      Swal.fire({
        title: 'Lewati Soal',
        text: "Apakah kamu memilih untuk tidak menjawab terlebih dahulu?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',

      }).then((result) => {
        if (result.value) {
          skip(levelUser,nise_id,kode_soal)
        }
      })
  });
</script>
<script type="text/javascript">

function answer(levelUser,nise_id,kode_soal,jawaban,point_soal){
  var form = new FormData();
  form.append("level", levelUser);
  form.append("nise_id", nise_id);
  form.append("kode_soal", kode_soal);
  form.append("jawaban", jawaban);
  form.append("point_soal", point_soal);
  form.append("skip", '');
  var settings = {
  "async": true,
  "crossDomain": true,
  "url": "<?= base_url() ?>api/process/answer_soal",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "1925e906-e107-4ad7-ac17-3a2eae837509"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
  }

  $.ajax(settings).done(function (response) {
    var obj=JSON.parse(response);
    console.log(obj);
  if(obj.status_code==200){
    var message=obj.message;
    var correct=obj.correct;
    if(correct==1){
      var typeAlert='success';
      var animationAlert='animated tada';
    }else{
      var typeAlert='error';
      var animationAlert='animated shake';
    }
    Swal.fire({
    type: typeAlert,
    title: message,
    showConfirmButton: true,
    showCancelButton:true,
    cancelButtonText: 'Lihat Pembahasan',
    confirmButtonText: 'Lanjutkan',
    reverseButtons: true,
    animation: false,
    allowOutsideClick: false,
    customClass: {
      popup: animationAlert
    }
    // html:
    // 'You can use <b>bold text</b>, ' +
    // '<a href="//sweetalert2.github.io">links</a> ' +
    // 'and other HTML tags',
    // timer: 1200
  }).then((result) => {
    if(result.value){
      window.location.reload();//Redirect Jika selesai
    }else{
      var url='<?= base_url() ?>portal/mycourses/detailsoal?id='+id_jawaban+'&type=latihan';
      window.location.replace(url);
    }
  })

  }
  });
}

function skip(levelUser,nise_id,kode_soal){
  var form = new FormData();
  form.append("level", levelUser);
  form.append("nise_id", nise_id);
  form.append("kode_soal", kode_soal);
  form.append("jawaban", '');
  form.append("point_soal", '');
  form.append("skip", 1);

  var settings = {
  "async": true,
  "crossDomain": true,
  "url": "<?= base_url() ?>api/process/answer_soal",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "1925e906-e107-4ad7-ac17-3a2eae837509"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
  }

  $.ajax(settings).done(function (response) {
    var obj=JSON.parse(response);
    console.log(obj);
  if(obj.status_code==200){
    Swal.fire({
    type: 'success',
    title: 'Soal berhasil dilewati',
    text: "Menyiapkan soal selanjutnya.",
    showConfirmButton: false,
    timer: 1000
  }).then((result) => {
    window.location.reload();//Redirect Jika selesai
  })

  }
  });
}

</script>
