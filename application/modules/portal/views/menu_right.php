  <div class="panel panel-default" data-toggle="panel-collapse" data-open="true">
    <div class="panel-heading panel-collapse-trigger">
      <h4 class="panel-title">My Account</h4>
    </div>
    <div class="panel-body list-group">
      <ul class="list-group list-group-menu">
        <li class="list-group-item"><a class="link-text-color" href="<?php echo base_url().$module ?>/dashboard">Dashboard</a></li>
        <li class="list-group-item"><a class="link-text-color" href="<?php echo base_url().$module ?>/mycourses">Aktivitas Tryout</a></li>
        <li class="list-group-item"><a class="link-text-color" href="<?php echo base_url().$module ?>/profile">Profil</a></li>
        <li class="list-group-item"><a class="link-text-color" href="<?= base_url()?>login/logout"><span>Keluar</span></a></li>
      </ul>
    </div>
  </div>
