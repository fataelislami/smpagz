<div class="row">

  <div class="col-md-9">

    <div class="row" data-toggle="isotope">
      <!-- <div class="item col-xs-12 col-lg-6">
        <div class="panel panel-default paper-shadow" data-z="0.5">
          <div class="panel-heading">
            <h4 class="text-headline margin-none">Aktivitas</h4>
            <p class="text-subhead text-light">Soal yang baru saja kamu kerjakan</p>
          </div>
          <ul class="list-group">
            <?php if (sizeof($listMatkulDashboard) > 0): ?>
              <?php foreach ($listMatkulDashboard as $mat): ?>
                <li class="list-group-item media v-middle">
                  <div class="media-body">
                    <a href="<?php echo base_url().$module.'/mycourses/soal/'.$mat->slug ?>" class="text-subhead list-group-link"><?php echo $mat->mapel_nama ?></a>
                  </div>
                  <div class="media-right">
                    <div class="progress progress-mini width-100 margin-none">
                      <div class="progress-bar progress-bar-green-300" role="progressbar" aria-valuenow="<?php echo ($mat->total_soal_users/$mat->total_soal_all * 100) ?>" aria-valuemin="0" aria-valuemax="100" style="width:45%;">
                      </div>
                    </div>
                  </div>
                </li>
              <?php endforeach; ?>
            <?php else: ?>
              <li class="list-group-item media v-middle">
                <div class="media-body">
                  <p class="text-center">anda belum mengisi soal</p>
                </div>
              </li>
            <?php endif; ?>


          </ul>
          <div class="panel-footer text-right">
            <a href="<?php echo base_url().$module.'/mycourses' ?>" class="btn btn-white paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#"> View all</a>
          </div>
        </div>
      </div> -->


      <!-- <div class="item col-xs-12 col-lg-6">
        <div class="panel panel-default paper-shadow" data-z="0.5">
          <div class="panel-heading">
            <h4 class="text-headline">Level Kamu Saat ini
              <small>(4)</small>
            </h4>
          </div>
          <div class="panel-body">
            mendapatkan level
            <?php if(sizeof($dataLevelDashboard)>0){ ?>
            <?php if ($dataLevelDashboard[0]->total_point != "") {
              $level = ""; $point = "";
              foreach ($dataLevelDashboard as $pt) {
                if ($pt->total_point >= $pt->point) {
                  $level = $pt->nama; $point = $pt->total_point; $image=$pt->image;
                }
              }
            } ?>
          <?php }else{
            $level = ""; $point = "";
          } ?>

            <?php if ($level !=""): //jika ada level ?>
              <center>
              <img src="<?= base_url() ?>xfile/file/<?= $image ?>" width="150" alt="">
            </center>
            <?php else: //jika tidak ada level ?>
              <li class="list-group-item media v-middle">
                <div class="media-body">
                  <p class="text-center">anda belum memiliki level</p>
                </div>
              </li>
            <?php endif; ?>

          </div>
        </div>
        <div class="panel panel-default paper-shadow" data-z="0.5">
          <div class="panel-body">
            <h4 class="text-headline margin-none">Pencapaian</h4>
            <p class="text-subhead text-light">Riwayat Pencapaian</p>
            <?php if(sizeof($dataLevelDashboard)>0){ ?>
            <?php
              if ($dataLevelDashboard[0]->total_point != "") {
                foreach ($dataLevelDashboard as $lv) {
                  if ($lv->total_point >= $lv->point) { ?>
                    <img src="<?= base_url() ?>xfile/file/<?= $lv->image ?>" width="70" alt="<?= $lv->nama ?>">
                  <?php }
                }
              }
            ?>
          <?php } ?>
          </div>
        </div>

      </div> -->


      <div class="item col-xs-12 col-lg-12">
        <div class="panel panel-default paper-shadow" data-z="0.5">
          <div class="panel-heading">
            <h4 class="text-headline margin-none">Try Out</h4>
            <p class="text-subhead text-light">Riwayat Try Out</p>
          </div>
          <ul class="list-group">
            <?php if (sizeof($listTryoutDashboard)>0): ?>
              <?php foreach ($listTryoutDashboard as $to): ?>
                <li class="list-group-item media v-middle">
                  <div class="media-body">
                    <h4 class="text-subhead margin-none">
                      <a href="<?php echo base_url()."portal/mycourses/tryout/$to->id" ?>" class="list-group-link"><?php echo $to->nama ?></a>
                    </h4>
                    <div class="caption">
                      <span class="text-light"><?php echo formatharitanggal($to->tanggal) ?></span>
                      <a href="#" ></a>
                    </div>
                  </div>
                  <div class="media-right text-center">
                    <span class="caption text-light">Selesai</span>
                  </div>
                </li>
              <?php endforeach; ?>
            <?php else: ?>
              <li class="list-group-item media v-middle">
                <div class="media-body">
                  <p class="text-center">anda belum mengikuti tryout</p>
                </div>
              </li>
            <?php endif; ?>
          </ul>
          <div class="panel-footer">
            <a href="<?php echo base_url(),"portal/mycourses" ?>" class="btn btn-primary paper-shadow relative" data-z="0" data-hover-z="1" data-animated href="#"> Go to Results</a>
          </div>
        </div>
      </div>
      <div class="item col-xs-12 col-lg-12">
        <div class="panel panel-default paper-shadow" data-z="0.5">
          <div class="panel-heading">
            <h4 class="text-headline margin-none">Grafik</h4>
            <p class="text-subhead text-light">Point Mata Pelajaran</p>
          </div>
          <div class="panel-body">
            <canvas id="myChart" width="400" height="400"></canvas>
          </div>
        </div>
      </div>
    </div>

    <br/>
    <br/>

  </div>
  <div class="col-md-3">

    <?php $this->load->view($menu_right) ?>

  </div>

</div>
