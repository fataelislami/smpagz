<script src="<?= base_url() ?>assets-portal/js/vendor/countdown/all.js"></script>

<script type="text/javascript">
var today = new Date();
var dateFromPHP='<?php echo $waktu_mulai ?>';//Ganti dengan waktu mulai
var now = new Date(dateFromPHP.replace(/-/g, '/'));
var base_url='<?= base_url(); ?>';
var menit=<?= $menit ?>;
// $('.tk-countdown').countdown({until: newYear});
$('.tk-countdown').countdown({since: '-'+menit+'M',
  format: 'HMS',onTick: watchCountUp});//,timezone: +7
  function watchCountUp(periods) {
    var hours=periods[4]*60;
    var minutes=periods[5];
    var totalminute=Number(hours+minutes);
  // $('#monitor').text('Just ' + periods[5] + ' minutes and ' +
  //     periods[6] + ' seconds to go');
  var durasi=<?php echo $durasi ?>;
  var id_try_out=<?php echo "$id_try_out" ?>;
  var utbk=<?php echo "$utbk" ?>;
  // console.log(totalminute+'Jam '+hours+' Menit '+minutes);
  // console.log('durasi '+durasi);
  if(totalminute>=durasi){
    passingdata(id_try_out,'false',totalminute,durasi);
    console.log("Waktu Habis");
  }else{
    if(utbk!=false){
      var durasi_mapel=<?php echo $durasi_mapel ?>;
      // var durasi_now=minute+'.'+second;
      // console.log(durasi_now);
      // console.log(durasi_mapel);
      // if(totalminute>=durasi_mapel){
        // window.location.href = "portal/tryout/utbk";
      // }
    }
  }
}
</script>
<script type="text/javascript">
  function toast(){
    const Toast = Swal.mixin({
      toast: true,
      position: 'center',
      showConfirmButton: false,
      timer: 1000,
      timerProgressBar: true,
    })

    Toast.fire({
      icon: 'success',
      title: 'JAWABAN TERSIMPAN'
    })
  }
</script>
<script type="text/javascript">
jQuery('body').on('click','.next-tab', function(){
    var next = jQuery('.nav-tabs > .active').next('li');
    if(next.length){
      next.find('a').trigger('click');
      //CEK JAWABAN
      // var radioId=$(this).attr("radioID");
      // if($('input:radio[name='+radioId+']:checked').is(':checked')){
      //   var radioValue=$('input:radio[name='+radioId+']:checked').val();
      //   alert(radioValue)
      // }else{
      //   alert("NONE");
      // }
    }else{
      var page=<?= $page ?>;
      var total_page=<?= $total_page ?>;
      var utbk=<?= $utbk ?>;
      var urlNext='<?= $urlNext; ?>'
      if(utbk==false){
        if(page==total_page){
          Swal.fire({
        title: 'Akhiri Ujian?',
        text: "Pastikan semua soal sudah terisi dengan baik",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          var id_try_out=<?php echo "$id_try_out" ?>;
          passingdata(id_try_out,'true',null,null);
          Swal.fire(
            'Menyimpan Data!',
            'Anda akan dialihkan ke halaman final',
            'success'
          )
        }
      })
    }else{
      window.location.href = urlNext;
    }
      }
    }
});
jQuery('body').on('click','.previous-tab', function(){
      var prev = jQuery('.nav-tabs > .active').prev('li')
      if(prev.length){
        prev.find('a').trigger('click');
        //CEK JAWABAN
        // var radioId=$(this).attr("radioID");
        // if($('input:radio[name='+radioId+']:checked').is(':checked')){
        //   var radioValue=$('input:radio[name='+radioId+']:checked').val();
        //   alert(radioValue)
        // }else{
        //   alert("NONE");
        // }
      }else{
        var page=<?= $page ?>;
        var total_page=<?= $total_page ?>;
        var utbk=<?= $utbk ?>;
        var urlPrev='<?= $urlPrev; ?>'
        if(utbk==false){
          if(page!=1){
            window.location.href = urlPrev;
      }
        }
      }
});
var value;
var formControl2;
function getVals(formControl, controlType) {
		value = $(formControl).val();
    var jawaban=value;
    var extract_value=jawaban.split("#");
    var extract_nomor=controlType.split("_");
    var nomorSoal=extract_nomor[1];
    var jawaban=extract_value[0];
    var kode_list_soal=extract_value[1];
    var id_try_out=extract_value[2];
    answer(kode_list_soal,id_try_out,jawaban,nomorSoal);
		// console.log(value);
		// console.log(kode_list_soal);
		// console.log(id_try_out);
		// answer(kode_list_soal,id_try_out,jawaban,stepnow,formControl);
}
//JAWABAN
var value2;
function hapus(formControl,controlType){
  var radioId=$(formControl).attr("radioID");
  if($('input:radio[name='+radioId+']:checked').is(':checked')){
    var radioValue=$('input:radio[name='+radioId+']:checked').val();
    var extract_value=radioValue.split("#");
    var extract_nomor=radioId.split("_");
    var nomorSoal=extract_nomor[1];
    var jawaban=extract_value[0];
    var kode_list_soal=extract_value[1];
    var id_try_out=extract_value[2];
    Swal.fire({
  title: 'Hapus jawaban ini?',
  text: "Anda akan menghapus jawaban ini",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, Hapus Jawaban!',
  reverseButtons: true
}).then((result) => {
  if (result.value) {
    delete_answer(kode_list_soal,id_try_out,jawaban,nomorSoal,radioId);
    Swal.fire(
      'Deleted!',
      'Jawaban berhasil dihapus.',
      'success'
    )
  }
})
  }
}
</script>

<script type="text/javascript">
function answer(kode_list_soal,id_try_out,jawaban,nomorSoal){
  var form = new FormData();
form.append("kode_list_soal", kode_list_soal);
form.append("id_try_out", id_try_out);
form.append("jawaban", jawaban);

var settings = {
  "async": true,
  "crossDomain": true,
  "url": base_url+"api/process/answer",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "99683224-f1e7-455e-b8af-d790b9b274f6"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

$.ajax(settings).done(function (response) {
  var obj=JSON.parse(response);
  var status=obj.status;
  if(status=='updated' || status=='inserted'){
    toast();
    $("#btn_sel-"+nomorSoal).addClass('bg-green-400').removeClass('bg-grey-400');
    $("#BatalJawaban_"+nomorSoal).show();
  }
  // if(status=='not updated'){
  //
  // }
});
}

function delete_answer(kode_list_soal,id_try_out,jawaban,nomorSoal,radioId){
	var form = new FormData();
form.append("kode_list_soal", kode_list_soal);
form.append("id_try_out", id_try_out);
form.append("jawaban", jawaban);

var settings = {
  "async": true,
  "crossDomain": true,
  "url": base_url+"api/process/delete_answer",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "99683224-f1e7-455e-b8af-d790b9b274f6"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

$.ajax(settings).done(function (response) {
	$("#BatalJawaban_"+nomorSoal).hide();
	$('input:radio[name='+radioId+']').prop('checked',false);
  $("#btn_sel-"+nomorSoal).addClass('bg-grey-400').removeClass('bg-green-400');
// console.log(response);
});
}

function passingdata(id_try_out,param_submit,param_totalminute,param_durasi){
  var form = new FormData();
form.append("id_try_out_js", id_try_out);
form.append("submit", param_submit);
form.append("totalminute", param_totalminute);
form.append("durasi", param_durasi);

var settings = {
  "async": true,
  "crossDomain": true,
  "url": "<?php echo base_url();?>tryout/finish",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "771cf5d9-c563-453b-9652-8f74b31a288e"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

$.ajax(settings).done(function (response) {
  window.location.href = "<?php echo base_url()?>tryout/finish";
});
}
</script>
