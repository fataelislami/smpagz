
<div class="row">
  <div class="tabbable tabs-vertical tabs-right">
  <div class="col-md-9">

    <div class="tab-content" style="padding:0px;background:transparent">
      <?php $i=1; ?>
      <?php foreach ($soal->results as $oSoal): ?>
      <div id="tabs_<?= $i ?>" class="tab-pane <?php if($i==1){echo "active";} ?>">
        <div class="text-subhead-2 text-light">Soal <?= $oSoal->nomor ?></div>
    <div class="panel panel-default paper-shadow" data-z="0.5">
      <div class="panel-body soal-body">
        <?= $oSoal->soal; ?>
      </div>
    </div>
    <?php
      $pil_a=$oSoal->pil_a;
      $pil_b=$oSoal->pil_b;
      $pil_c=$oSoal->pil_c;
      $pil_d=$oSoal->pil_d;
      $pil_e=$oSoal->pil_e;
     ?>

    <div class="text-subhead-2 text-light">Pilih Jawaban</div>
    <div class="panel panel-default paper-shadow" data-z="0.5">
      <div class="panel-body">
            <table>
              <tr>
                <td>
                  <b>A</b>.
                </td>
                <td>
                  <div class="radio radio-primary">
                    <input type="radio" name="jawaban_<?= $oSoal->nomor ?>" id="radio_<?= $oSoal->nomor ?>_A" value="A#<?= $oSoal->kode; ?>#<?= $id_try_out ?>" onclick="getVals(this, '<?php echo "jawaban_$oSoal->nomor" ?>');">
                    <label for="radio_<?= $oSoal->nomor ?>_A"><?= $pil_a ?></label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>B</b>.
                </td>
                <td>
                  <div class="radio radio-primary">
                    <input type="radio" name="jawaban_<?= $oSoal->nomor ?>" id="radio_<?= $oSoal->nomor ?>_B" value="B#<?= $oSoal->kode; ?>#<?= $id_try_out ?>" onclick="getVals(this, '<?php echo "jawaban_$oSoal->nomor" ?>');">
                    <label for="radio_<?= $oSoal->nomor ?>_B"><?= $pil_b ?></label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>C</b>.
                </td>
                <td>
                  <div class="radio radio-warning">
                    <input type="radio" name="jawaban_<?= $oSoal->nomor ?>" id="radio_<?= $oSoal->nomor ?>_C" value="C#<?= $oSoal->kode; ?>#<?= $id_try_out ?>" onclick="getVals(this, '<?php echo "jawaban_$oSoal->nomor" ?>');">
                    <label for="radio_<?= $oSoal->nomor ?>_C"><?= $pil_c ?></label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>D</b>.
                </td>
                <td>
                  <div class="radio radio-warning">
                    <input type="radio" name="jawaban_<?= $oSoal->nomor ?>" id="radio_<?= $oSoal->nomor ?>_D" value="D#<?= $oSoal->kode; ?>#<?= $id_try_out ?>" onclick="getVals(this, '<?php echo "jawaban_$oSoal->nomor" ?>');">
                    <label for="radio_<?= $oSoal->nomor ?>_D"><?= $pil_d ?></label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>E</b>.
                </td>
                <td>
                  <div class="radio radio-warning">
                    <input type="radio" name="jawaban_<?= $oSoal->nomor ?>" id="radio_<?= $oSoal->nomor ?>_E" value="E#<?= $oSoal->kode; ?>#<?= $id_try_out ?>" onclick="getVals(this, '<?php echo "jawaban_$oSoal->nomor" ?>');">
                    <label for="radio_<?= $oSoal->nomor ?>_E"><?= $pil_e ?></label>
                  </div>
                </td>
              </tr>
            </table>
            <script>
            var $radios = $('input:radio[name=jawaban_<?= $oSoal->nomor ?>]');
              $radios.filter('[value="<?= $oSoal->jawaban_siswa;?>#<?= $oSoal->kode;?>#<?= $id_try_out;?>"]').prop('checked', true);
            </script>
      </div>
      <div class="panel-footer">
        <div class="text-right">
          <button class="btn btn-danger pull-left" onclick="hapus(this,'jawaban_<?= $oSoal->nomor ?>')" radioID="jawaban_<?= $oSoal->nomor ?>" id="BatalJawaban_<?= $oSoal->nomor ?>" style="display:none"><i class="fa fa-remove fa-fw"></i> HAPUS JAWABAN</button>
          <button class="btn btn-primary previous-tab" radioID="jawaban_<?= $oSoal->nomor ?>"><i class="fa fa-chevron-left fa-fw"></i> SEBELUMNYA</button>
          <button class="btn btn-primary next-tab" radioID="jawaban_<?= $oSoal->nomor ?>">SELANJUTNYA<i class="fa fa-chevron-right fa-fw"></i></button>
        </div>

      </div>
    </div>
    </div>
    <?php $i++; ?>
    <?php endforeach; ?>
    </div>

    <br/>
    <br/>

  </div>
  <div class="col-md-3">

    <div class="s-container">
      <div class="text-subhead-2 text-light">Waktu</div>
      <div class="tk-countdown"></div>
    </div>

    <div class="panel panel-default margin-none">
      <div class="panel-heading">
        <h4 class="panel-title">Nomor Soal</h4>
      </div>
      <div class="panel-body list-group">
        <ul class="list-group nav nav-tabs" tabindex="7" style="overflow: hidden; outline: none;">
          <?php $j=1; ?>
          <?php foreach ($soal->results as $oSoal): ?>
          <li class="list-group-item <?php if($j==1){echo "active";} ?>" radioID="jawaban_<?= $oSoal->nomor ?>">
            <div class="media">
              <a href="#tabs_<?= $j?>" data-toggle="tab" class="link-text-color" >
                <?php if($oSoal->jawaban_siswa!=null){ ?>
                  <script type="text/javascript">
                  $("#BatalJawaban_<?= $oSoal->nomor ?>").show();
                  </script>
                  <div class="icon-block s30 bg-green-400 text-white" id="btn_sel-<?= $oSoal->nomor ?>"><?= $oSoal->nomor ?></div>
                <?php }else{ ?>
                  <script type="text/javascript">
                    $("#BatalJawaban_<?= $oSoal->nomor ?>").hide();
                  </script>
                  <div class="icon-block s30 bg-grey-400 text-white" id="btn_sel-<?= $oSoal->nomor ?>"><?= $oSoal->nomor ?></div>
                <?php } ?>
              </a>
            </div>
          </li>
          <?php $j++ ?>
        <?php endforeach; ?>
        </ul>
      </div>
      <hr>
      <div class="panel-footer">
        <span class="fa fa-fw fa-circle text-green-400"></span> Sudah dikerjakan
        <br/>
        <span class="fa fa-fw fa-circle text-grey-400"></span> Belum dikerjakan
        <br/>
        <span class="fa fa-fw fa-circle text-blue-400"></span> Posisi saat ini
      </div>
      <?php if($utbk=='false'){ ?>
        <?php if($page!=1){ ?>
          <div class="panel-footer">
            <a href="<?php echo $urlPrev ?>"class="btn btn-success" style="width:100%"><i class="fa fa-chevron-left fa-fw"></i></a>
          </div>
        <?php } ?>
        <?php if ($page!=$total_page): ?>
          <div class="panel-footer">
            <a href="<?php echo $urlNext ?>" class="btn btn-success" style="width:100%"><i class="fa fa-chevron-right fa-fw"></i></a>
          </div>
        <?php endif; ?>
      <?php } ?>

    </div>

  </div>
</div>
</div>
