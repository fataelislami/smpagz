<div class="row">

  <div class="col-md-12">
    <div class="row" data-toggle="isotope">
      <div class="item col-xs-12 col-lg-12">
        <div class="panel panel-default paper-shadow" data-z="0.5">
          <div class="panel-heading">
            <h4 class="text-headline margin-none">Try Out</h4>
            <p class="text-subhead text-light">Event Try Out Saat Ini</p>
          </div>
          <ul class="list-group">
            <?php if ($listTryout->num_rows()>0): ?>
              <?php foreach ($listTryout->result() as $key): ?>
                <li class="list-group-item media v-middle">
                <div class="media-left">
                  <div class="icon-block half img-circle bg-grey-300">
                    <i class="fa fa-file-text text-white"></i>
                  </div>
                </div>
                <div class="media-body">
                  <h4 class="text-subhead margin-none">
                    <a href="#" class="link-text-color"><?= $key->nama ?></a>
                  </h4>
                  <div class="text-light text-caption">
                    Waktu Mulai  <a href="#"><?= $key->waktu_mulai ?></a> &nbsp; | <i class="fa fa-clock-o fa-fw"></i> <?= $key->durasi ?> menit
                  </div>
                </div>
                <div class="media-right">
                  <button class="btn btn-white text-light btn-mulai" idEvent="<?= $key->id ?>"><i class="fa fa-flag fa-fw"></i> MULAI</button>
                </div>
              </li>
              <?php endforeach; ?>
            <?php else: ?>
              <li class="list-group-item media v-middle">
                <div class="media-body">
                  <p class="text-center">Belum ada event try out saat ini</p>
                </div>
              </li>
            <?php endif; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
