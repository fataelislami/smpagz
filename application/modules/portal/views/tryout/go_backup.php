<div class="row">
  <div class="tabbable tabs-vertical tabs-right">
  <div class="col-md-9">

    <div class="tab-content" style="padding:0px;background:transparent">
      <?php $i=1; ?>
      <?php foreach ($soal->results as $oSoal): ?>
      <div id="tabs_<?= $i ?>" class="tab-pane <?php if($i==1){echo "active";} ?>">
        <div class="text-subhead-2 text-light">Soal 1</div>
    <div class="panel panel-default paper-shadow" data-z="0.5">
      <div class="panel-body">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </div>
    </div>

    <div class="text-subhead-2 text-light">Pilih Jawaban</div>
    <div class="panel panel-default paper-shadow" data-z="0.5">
      <div class="panel-body">
            <table>
              <tr>
                <td>
                  <b>A</b>.
                </td>
                <td>
                  <div class="radio radio-primary">
                    <input type="radio" name="jawaban" id="radio11" value="A">
                    <label for="radio11">CONTOH</label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>B</b>.
                </td>
                <td>
                  <div class="radio radio-primary">
                    <input type="radio" name="jawaban" id="radio12" value="B">
                    <label for="radio12">CONTOH</label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>C</b>.
                </td>
                <td>
                  <div class="radio radio-warning">
                    <input type="radio" name="jawaban" id="radio13" value="C">
                    <label for="radio13">CONTOH</label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>D</b>.
                </td>
                <td>
                  <div class="radio radio-warning">
                    <input type="radio" name="jawaban" id="radio14" value="D">
                    <label for="radio14">CONTOH</label>
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <b>E</b>.
                </td>
                <td>
                  <div class="radio radio-warning">
                    <input type="radio" name="jawaban" id="radio15" value="E">
                    <label for="radio15">CONTOH</label>
                  </div>
                </td>
              </tr>
            </table>
      </div>
      <div class="panel-footer">
        <div class="text-right">
          <button  class="btn btn-primary previous-tab"><i class="fa fa-chevron-left fa-fw"></i> SEBELUMNYA</button>
          <button  class="btn btn-primary next-tab">SELANJUTNYA<i class="fa fa-chevron-right fa-fw"></i></button>
        </div>
      </div>
    </div>
    </div>
    <?php $i++; ?>
    <?php endforeach; ?>
    </div>

    <br/>
    <br/>

  </div>
  <div class="col-md-3">

    <div class="s-container">
      <div class="text-subhead-2 text-light">Waktu</div>
      <div class="tk-countdown"></div>
    </div>

    <div class="panel panel-default margin-none">
      <div class="panel-heading">
        <h4 class="panel-title">List Soal</h4>
      </div>
      <div class="panel-body list-group">
        <ul class="list-group nav nav-tabs" tabindex="7" style="overflow: hidden; outline: none;">
          <?php $j=1; ?>
          <?php foreach ($soal->results as $oSoal): ?>
          <li class="list-group-item <?php if($j==1){echo "active";} ?>" style="float:left">
            <a href="#tabs_<?= $j?>" data-toggle="tab" class="link-text-color" style="display: block;">
            <div class="media v-middle">
              <div class="media-left">
                <div class="icon-block s30 bg-grey-400 text-white"><?= $j ?></div>
              </div>
              <div class="media-body" style="color:#000000">
                Soal No <?= $j ?>
              </div>
            </div>
            </a>
          </li>
          <?php $j++ ?>
        <?php endforeach; ?>
        </ul>
      </div>
      <hr>
      <div class="panel-footer">
        <span class="fa fa-fw fa-circle text-green-400"></span> Sudah dikerjakan
        <br/>
        <span class="fa fa-fw fa-circle text-grey-400"></span> Belum dikerjakan
        <br/>
        <span class="fa fa-fw fa-circle text-blue-400"></span> Posisi saat ini
      </div>
      <div class="panel-footer">
        <button class="btn btn-success" id="next" style="width:100%"><i class="fa fa-chevron-right fa-fw"></i>HALAMAN SELANJUTNYA</button>
      </div>
    </div>

  </div>
</div>
</div>
