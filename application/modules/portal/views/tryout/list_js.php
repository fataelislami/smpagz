<script type="text/javascript">
var base_url='<?= base_url() ?>';
// Swal.fire({
// type: 'error',
// title: 'Oops...',
// showConfirmButton: false,
// text: 'Kamu belum milih jawaban untuk disimpan',
// timer: 1000
// })
$(".btn-mulai").click(function(){
  var idEvent=$(this).attr('idEvent');
  Swal.fire({
    title: 'Masukan Password Event',
    input: 'password',
    inputPlaceholder: 'Contoh:123456',
    inputAttributes: {
      maxlength: 10,
      autocapitalize: 'off',
      autocorrect: 'off'
    }
  }).then((result) => {
    if (result.value) {
      var password=result.value;
      auth_event(idEvent,password);
    }
  })
});


function auth_event(id,password){
	var form = new FormData();
form.append("id", id);
form.append("password", password);

var settings = {
"async": true,
"crossDomain": true,
"url": base_url+"api/event/auth",
"method": "POST",
"headers": {
	"cache-control": "no-cache",
	"Postman-Token": "fb446c5c-1d09-4f99-9302-44ea7c2ee5cd"
},
"processData": false,
"contentType": false,
"mimeType": "multipart/form-data",
"data": form
}

$.ajax(settings).done(function (response) {
var obj=JSON.parse(response);
console.log(obj);
if(obj.total_result==1){
  Swal.fire('PASSWORD BENAR')
}else{
  Swal.fire('PASSWORD EVENT SALAH')
}
});
}

</script>
