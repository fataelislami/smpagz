<link rel="stylesheet" href="<?= base_url() ?>assets-portal/css/vendor/animate.css">
<style media="screen">
@media only screen and (max-width: 1200px) {
.list-group-item {
  padding: 21px;
}
}
@media only screen and (min-width: 1201px) {
.list-group-item {
  padding: 18.5px;
}
}
@media only screen and (max-width: 990px) {
.list-group {
  display: flex;
}
.soal-body img
{
  width:100%;
}
}
</style>
<script src="<?= base_url()."assets-front/"?>js/jquery-3.2.1.min.js"></script>
