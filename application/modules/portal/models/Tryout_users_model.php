<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tryout_users_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  //get by id
  function get_tryout_user_byId($id){
    $this->db->where('id', $id);
    return $this->db->get('try_out_users');
  }

  function get_tryout_by_id($id){
    $this->db->select('try_out_users.*');
    $this->db->select('users.*');
    $this->db->select('sekolah.nama as nama_sekolah');
    $this->db->select('event.nama as nama_event, event.soal_to');
    $this->db->join('users', 'try_out_users.$id_user = users.id', 'left');
    $this->db->join('sekolah', 'users.id_sekolah = sekolah.id', 'left');
    $this->db->join('event', 'try_out_users.id_event = event.id', 'left');
    $this->db->where('try_out_users.id', $id);
    return $this->db->get('try_out_users');
  }

  function get_Jawaban_To($id_to,$mapel){
    $this->db->select(' tb1.id, list_soal_to.kode, list_soal_to.nomor, mata_pelajaran.nama');
    $this->db->select('tb1.jawaban as jawaban_siswa');
    $this->db->select('list_soal_to.jawaban as kunci_jawaban, list_soal_to.sub_bab');
    $this->db->from('try_out_users');
    $this->db->join('list_soal_to', 'try_out_users.kode_soal = list_soal_to.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal_to.mapel = mata_pelajaran.id', 'left');
    $this->db->join("(SELECT * FROM list_jawaban_to_users WHERE list_jawaban_to_users.id_try_out_users = $id_to) AS tb1", 'list_soal_to.kode = tb1.kode_list_soal', 'left');
    if ($mapel != NULL) {
      $this->db->where('mata_pelajaran.id', $mapel);
    }
    $this->db->where('try_out_users.id', $id_to);
    $this->db->order_by('list_soal_to.nomor','ASC');
    return $this->db->get();
  }

  function getListMapelByTO($id_to){
    $this->db->select('mata_pelajaran.nama , mata_pelajaran.id');
    $this->db->from('try_out_users');
    $this->db->join('list_soal_to', 'try_out_users.kode_soal = list_soal_to.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal_to.mapel = mata_pelajaran.id', 'left');
    $this->db->where('try_out_users.id', $id_to);
    $this->db->group_by('mata_pelajaran.id');
    return $this->db->get();
  }

}
