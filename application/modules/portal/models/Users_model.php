<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  // update data
  function update($id, $data)
  {
      $this->db->where("id", $id);
      $this->db->update("users", $data);
  }

  function get_user_by_id($id_user){
    $this->db->select('users.*');
    $this->db->select('sekolah.nama as nama_sekolah');
    $this->db->join('sekolah', 'users.id_sekolah = sekolah.id', 'left');
    $this->db->where('users.id', $id_user);
    return $this->db->get('users');
  }

  function checkUsernameUsers($username){
    $this->db->where('username', $username);
    return $this->db->get('users');
  }

}
