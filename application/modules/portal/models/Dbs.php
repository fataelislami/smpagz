<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbs extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function check($table,$where){
    return $this->db->get_where($table,$where);
  }

  function insert($data,$table){
   $insert = $this->db->insert($table, $data);
   if ($this->db->affected_rows()>0) {
     return true;
     }else{
     return false;
     }
 }
 function delete($table,$where){
   $this->db->where($where);
   $this->db->delete($table);
 }

 function update($data,$table,$where){
    $this->db->where($where);
    $db=$this->db->update($table,$data);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
    }
  }

  function getMapelBySlugSubbab($slugSubbab){
    $this->db->select('mata_pelajaran.*');
    $this->db->from('sub_mapel');
    $this->db->join('mata_pelajaran', 'sub_mapel.id_mapel = mata_pelajaran.id', 'left');
    $this->db->where('sub_mapel.slug', $slugSubbab);
    return $this->db->get();
  }


  /*================Fungsi untuk mendapatkan ranking keseluruhan==============*/
  function getPointRank($limit=5,$result=NULL){
    //get table siswa
    $this->db->select('siswa.nise as identity, siswa.nama, siswa.sekolah');
    $this->db->select('siswa.total_point_ipa, siswa.total_point_ips, siswa.total_point_tpa');
    $this->db->select('(siswa.total_point_ipa+siswa.total_point_ips+siswa.total_point_tpa)as jumlah_point');
    $this->db->from('siswa');
    $table_siswa = $this->db->get_compiled_select();

    //get table users
    $this->db->select('users.id as identity, users.nama');
    $this->db->select('sekolah.nama as sekolah');
    $this->db->select('users.total_point_ipa, users.total_point_ips, users.total_point_tpa');
    $this->db->select('(users.total_point_ipa+users.total_point_ips+users.total_point_tpa)as jumlah_point');
    $this->db->from('users');
    $this->db->join('sekolah', 'users.id_sekolah = sekolah.id', 'left');
    $table_users = $this->db->get_compiled_select();

    //union table
    if ($result=="get_compiled_select") {
      $this->db->select("(@rownum:=@rownum+1)as rank,result_table.* FROM (".$table_siswa." UNION ".$table_users."ORDER BY `jumlah_point` DESC) AS result_table,(SELECT @rownum := 0) r");
      return $this->db->get_compiled_select();
    }else {
      $this->db->select("(@rownum:=@rownum+1)as rank,result_table.* FROM (".$table_siswa." UNION ".$table_users."ORDER BY `jumlah_point` DESC LIMIT "."$limit".") AS result_table,(SELECT @rownum := 0) r");
      $this->db->where('jumlah_point > 0');
      $this->db->limit($limit);
      return $this->db->get();
    }
  }
  function getPointRankIndividu($id){
    $this->db->select('*');
    $this->db->select('(siswa.total_point_ipa+siswa.total_point_ips+siswa.total_point_tpa)as jumlah_point');
    $this->db->from("siswa");
    $this->db->where('nise', $id);
    return $this->db->get();
  }
  /*===========End Fungsi untuk mendapatkan ranking keseluruhan================*/


  /*=============Fungsi untuk mendapatkan ranking by tanggal dan mapel=========*/
  function getPointRankByDateAndMapel($date,$mapel,$limit=5,$result=NULL){
    //get data siswa
    $this->db->select('siswa.nise as identity, siswa.nama, siswa.sekolah');
    $this->db->select('SUM(list_jawaban_soal.point_soal) as jumlah_point');
    $this->db->from('list_jawaban_soal');
    $this->db->join('siswa','list_jawaban_soal.nise = siswa.nise', 'left');
    $this->db->join('soal','list_jawaban_soal.kode_soal = soal.kode', 'left');
    $this->db->join('mata_pelajaran','soal.mapel = mata_pelajaran.id', 'left');
    $this->db->where('DATE(list_jawaban_soal.waktu_mulai)', $date);
    $this->db->where('mata_pelajaran.slug',$mapel);
    $this->db->group_by('list_jawaban_soal.nise');
    $table_siswa = $this->db->get_compiled_select();

    //get table users
    $this->db->select('users.id as identity, users.nama, sekolah.nama');
    $this->db->select('SUM(list_jawaban_soal_users.point_soal) as jumlah_point');
    $this->db->from('list_jawaban_soal_users');
    $this->db->join('users','list_jawaban_soal_users.user_id = users.id', 'left');
    $this->db->join('sekolah', 'users.id_sekolah = sekolah.id', 'left');
    $this->db->join('soal','list_jawaban_soal_users.kode_soal = soal.kode', 'left');
    $this->db->join('mata_pelajaran','soal.mapel = mata_pelajaran.id', 'left');
    $this->db->where('DATE(list_jawaban_soal_users.waktu_mulai)', $date);
    $this->db->where('mata_pelajaran.slug',$mapel);
    $this->db->group_by('list_jawaban_soal_users.user_id');
    $table_users = $this->db->get_compiled_select();

    //union table
    if ($result=="get_compiled_select") {
      $this->db->select("(@rownum:=@rownum+1)as rank,result_table.* FROM (".$table_siswa." UNION ".$table_users."ORDER BY `jumlah_point` DESC) AS result_table,(SELECT @rownum := 0) r");
      return $this->db->get_compiled_select();
    }else {
      $this->db->select("(@rownum:=@rownum+1)as rank,result_table.* FROM (".$table_siswa." UNION ".$table_users."ORDER BY `jumlah_point` DESC LIMIT "."$limit".") AS result_table,(SELECT @rownum := 0) r");
      $this->db->limit($limit);
      return $this->db->get();
    }
  }
  function getPointRankIndividuByDateAndMapel($date,$mapel,$id){
    $table = $this->getPointRankByDateAndMapel($date,$mapel,NULL,'get_compiled_select');
    $this->db->select('*');
    $this->db->from("($table) as table_res");
    $this->db->where('table_res.identity', $id);
    return $this->db->get();
  }

  function radarAllMapel($id){
    $this->db->select('mata_pelajaran.nama');
    $this->db->select('COALESCE(SUM(soal_jawaban.point),0) as total_point');
    $this->db->from('mata_pelajaran');
      $this->db->join("(SELECT soal.mapel as id_mapel, SUM(list_jawaban_soal.point_soal) AS point
                       FROM list_jawaban_soal
                       LEFT JOIN soal ON list_jawaban_soal.kode_soal = soal.kode
                       WHERE list_jawaban_soal.nise = $id
                       GROUP BY soal.mapel) as soal_jawaban",
                      'mata_pelajaran.id = soal_jawaban.id_mapel'
                      );
    $this->db->group_by('mata_pelajaran.id');
    return $this->db->get();
  }
  /*===========END Fungsi untuk mendapatkan ranking by tanggal dan mapel=======*/


}
