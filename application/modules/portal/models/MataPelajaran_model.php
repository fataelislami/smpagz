<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MataPelajaran_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function getMapelBySlug($slug){
    $this->db->select('*');
    $this->db->where('slug', $slug);
    return $this->db->get('mata_pelajaran');
  }

  function getListMapel($jurusan=NULL){
    $this->db->select('mata_pelajaran.*');
    $this->db->select('count(sub_mapel.id) as jumlah_submapel');
    $this->db->select('jmlh_soal.jumlah_soal');
    $this->db->from('mata_pelajaran');
    $this->db->join('sub_mapel', 'mata_pelajaran.id = sub_mapel.id_mapel', 'left');
    $this->db->join('(select mata_pelajaran.id, count(soal.kode) as jumlah_soal
                      from mata_pelajaran
                      join soal on mata_pelajaran.id = soal.mapel
                      group by mata_pelajaran.id) as jmlh_soal',
                    'mata_pelajaran.id = jmlh_soal.id',
                    'left');
    $this->db->group_by('mata_pelajaran.id');
    $this->db->order_by("jmlh_soal.jumlah_soal", "desc");
    $this->db->where('mata_pelajaran.jurusan', $jurusan);
    return $this->db->get();
  }

  function getListSubMapel($slug){
    $this->db->select('mata_pelajaran.nama as nama_mapel,mata_pelajaran.durasi as durasi, mata_pelajaran.slug as slug_mapel');
    $this->db->select('sub_mapel.nama_sub as nama_sub_mapel, sub_mapel.slug as slug_submapel');
    $this->db->select('count(soal.kode) as jumlah_soal');
    $this->db->from('mata_pelajaran');
    $this->db->where('mata_pelajaran.slug', $slug);
    $this->db->join('sub_mapel', 'sub_mapel.id_mapel = mata_pelajaran.id');
    $this->db->join('soal', 'sub_mapel.id = soal.sub_bab');
    $this->db->group_by('sub_mapel.id');
    return $this->db->get();
  }

  function getMapelCountByUser($user_id,$limit=NULL){
    $this->db->select('mata_pelajaran.id as mapel_id, mata_pelajaran.nama as mapel_nama, mata_pelajaran.slug');
    $this->db->select('COUNT(soal.kode) as total_soal_users');
    $this->db->select('table_total_soal.total_soal_all');
    $this->db->from('list_jawaban_soal_users');
    $this->db->join('soal', 'list_jawaban_soal_users.kode_soal = soal.kode', 'left');
    $this->db->join('mata_pelajaran', 'soal.mapel = mata_pelajaran.id', 'left');
    $this->db->join("(select mata_pelajaran.id, COUNT(soal.kode) as total_soal_all
                      from mata_pelajaran
                      join soal on mata_pelajaran.id = soal.mapel
                      group by mata_pelajaran.id
                      ) as table_total_soal",
                    'mata_pelajaran.id = table_total_soal.id',
                    'left');
    $this->db->where('list_jawaban_soal_users.user_id', $user_id);
    $this->db->where('list_jawaban_soal_users.waktu_selesai <>', "NULL");
    $this->db->where('list_jawaban_soal_users.jawaban <>', "NULL");
    $this->db->group_by('soal.mapel');
    if ($limit != NULL) {
      $this->db->limit($limit);
    }
    return $this->db->get();
  }

  function getMapelCountBySiswa($nis,$limit=NULL){
    $this->db->select('mata_pelajaran.id as mapel_id, mata_pelajaran.nama as mapel_nama, mata_pelajaran.slug');
    $this->db->select('COUNT(soal.kode) as total_soal_users');
    $this->db->select('table_total_soal.total_soal_all');
    $this->db->select('IFNULL(`jumlah_benar`,0) as jumlah_benar, IFNULL(`jumlah_salah`,0) as jumlah_salah');
    $this->db->from('list_jawaban_soal');
    $this->db->join('soal', 'list_jawaban_soal.kode_soal = soal.kode', 'left');
    $this->db->join('mata_pelajaran', 'soal.mapel = mata_pelajaran.id', 'left');
    $this->db->join("(select mata_pelajaran.id, COUNT(soal.kode) as total_soal_all
                      from mata_pelajaran
                      join soal on mata_pelajaran.id = soal.mapel
                      group by mata_pelajaran.id
                      ) as table_total_soal",
                    'mata_pelajaran.id = table_total_soal.id',
                    'left');
    $this->db->join("(SELECT soal.mapel as id,COUNT(id) as jumlah_benar
                      FROM list_jawaban_soal
                      LEFT JOIN soal ON list_jawaban_soal.kode_soal = soal.kode
                      WHERE list_jawaban_soal.nis = '$nis' AND list_jawaban_soal.jawaban = soal.jawaban
                      GROUP BY soal.mapel
                      ) as table_soal_benar",
                    'mata_pelajaran.id = table_soal_benar.id',
                    'left');
    $this->db->join("(SELECT soal.mapel as id,COUNT(id) as jumlah_salah
                      FROM list_jawaban_soal
                      LEFT JOIN soal ON list_jawaban_soal.kode_soal = soal.kode
                      WHERE list_jawaban_soal.nis = '$nis' AND list_jawaban_soal.jawaban != soal.jawaban
                      GROUP BY soal.mapel
                      ) as table_soal_salah",
                    'mata_pelajaran.id = table_soal_salah.id',
                    'left');
    $this->db->where('list_jawaban_soal.nis', $nis);
    $this->db->where('list_jawaban_soal.waktu_selesai <>', "NULL");
    $this->db->where('list_jawaban_soal.jawaban <>', "NULL");
    $this->db->group_by('soal.mapel');
    if ($limit != NULL) {
      $this->db->limit($limit);
    }
    return $this->db->get();
  }

  function getTryoutDashboardUsers($user_id,$limit=NULL){
    date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('Y-m-d H:i:s');

    $this->db->select('tryout_users.id, tryout_users.skor');
    $this->db->select('tryout_event.nama, tryout_event.tanggal');
    $this->db->from('tryout_users');
    $this->db->join('tryout_event', 'tryout_users.id_event = tryout_event.id', 'left');
    $this->db->where('tryout_users.id_user', $user_id);
    $this->db->where("tryout_event.waktu_selesai <=", $tanggal);
    if ($limit!=null) {
      $this->db->limit($limit);
    }
    return $this->db->get();
  }

  function getTryoutDashboardSiswa($nis,$limit=NULL){
    date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('Y-m-d H:i:s');

    $this->db->select('tryout.id, tryout.skor');
    $this->db->select('tryout_event.nama, tryout_event.tanggal');
    $this->db->from('tryout');
    $this->db->join('tryout_event', 'tryout.id_event = tryout_event.id', 'left');
    $this->db->where('tryout.nis', $nis);
    $this->db->where("tryout_event.waktu_selesai <=", $tanggal);
    if ($limit!=null) {
      $this->db->limit($limit);
    }
    return $this->db->get();
  }

  function getDatalevelDashboardUsers($user_id){
    $this->db->select('badge.id, badge.nama, badge.point,badge.image');
    $this->db->select('(users.total_point_ipa+users.total_point_ips+users.total_point_tpa) as total_point');
    $this->db->where('users.id', $user_id);
    $this->db->order_by('badge.point','asc');
    return $this->db->get(['users','badge']);
  }

  function getDatalevelDashboardSiswa($nis){
    $this->db->select('badge.id, badge.nama, badge.point,badge.image');
    $this->db->select('(siswa.total_point_ipa+siswa.total_point_ips+siswa.total_point_tpa) as total_point');
    $this->db->where('siswa.nis', $nis);
    $this->db->order_by('badge.point','asc');
    return $this->db->get(['siswa','badge']);
  }

}
