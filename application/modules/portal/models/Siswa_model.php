<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  // update data
  function update($nis, $data)
  {
      $this->db->where("nis", $nis);
      $this->db->update("siswa", $data);
  }

  function get_siswa_by_nis($nis){
    $this->db->select('siswa.*');
    $this->db->select('kelas.nama as nama_kelas');
    $this->db->join('kelas', 'siswa.id_kelas = kelas.kode', 'left');
    $this->db->where('siswa.nis', $nis);
    return $this->db->get('siswa');
  }

  // function checkUsernameUsers($username){
  //   $this->db->where('username', $username);
  //   return $this->db->get('users');
  // }

}
