<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soal_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function update($data,$table,$where){
     $this->db->where($where);
     $db=$this->db->update($table,$data);
     if ($this->db->affected_rows()>0) {
       return true;
       }else{
       return false;
       }
   }

  // get soal to by kode
  function get_soalTo_by_kode($kode){
    $this->db->select('list_soal.*');
    $this->db->select('mata_pelajaran.nama');
    $this->db->where('kode', $kode);
    $this->db->join('mata_pelajaran', 'list_soal.id_mata_pelajaran = mata_pelajaran.id', 'left');
    return $this->db->get('list_soal');
  }

  // get soal by kode
  function get_soal_by_kode($kode){
    $this->db->select('soal.*');
    $this->db->select('mata_pelajaran.nama,mata_pelajaran.slug as slug_mapel');
    $this->db->select('sub_mapel.nama_sub as sub_bab,sub_mapel.slug as sub_bab_slug');
    if ($this->session->userdata('level')=='users') {
      $this->db->from('list_jawaban_soal_users');
      $this->db->join('soal', 'list_jawaban_soal_users.kode_soal = soal.kode', 'left');
      $this->db->where('list_jawaban_soal_users.id', $kode);
    }elseif ($this->session->userdata('level')=='siswa') {
      $this->db->from('list_jawaban_soal');
      $this->db->join('soal', 'list_jawaban_soal.kode_soal = soal.kode', 'left');
      $this->db->where('list_jawaban_soal.id', $kode);
    }else {
      return 0;
    }
    $this->db->join('mata_pelajaran', 'soal.mapel = mata_pelajaran.id', 'left');
    $this->db->join('sub_mapel', 'soal.sub_bab = sub_mapel.id', 'left');


    return $this->db->get();
  }

  function getListSoalUser($slug,$user_id){
    $this->db->select("list_jawaban_soal_users.id as id_jawaban, list_jawaban_soal_users.jawaban as jawaban_siswa");
    $this->db->select('list_jawaban_soal_users.user_id, list_jawaban_soal_users.waktu_mulai');
    $this->db->select('soal.kode, soal.jawaban as jawaban_soal');
    $this->db->select('sub_mapel.nama_sub');
    $this->db->select('soal_level.nama as level');
    $this->db->from('list_jawaban_soal_users');
    $this->db->join('soal', 'list_jawaban_soal_users.kode_soal = soal.kode', 'left');
    $this->db->join('mata_pelajaran', 'soal.mapel = mata_pelajaran.id', 'left');
    $this->db->join('sub_mapel', 'soal.sub_bab = sub_mapel.id', 'left');
    $this->db->join('soal_level', 'soal.level = soal_level.id', 'left');
    $this->db->where('list_jawaban_soal_users.user_id', $user_id);
    $this->db->where('list_jawaban_soal_users.jawaban <>', "NULL");
    $this->db->where('list_jawaban_soal_users.waktu_selesai <>', "NULL");
    $this->db->where('mata_pelajaran.slug', $slug);
    $this->db->order_by('waktu_mulai','DESC');
    return $this->db->get();
  }

  function getListSoalSiswa($slug,$nise){
    $this->db->select("list_jawaban_soal.id as id_jawaban, list_jawaban_soal.jawaban as jawaban_siswa");
    $this->db->select('list_jawaban_soal.nise, list_jawaban_soal.waktu_mulai');
    $this->db->select('soal.kode, soal.jawaban as jawaban_soal');
    $this->db->select('sub_mapel.nama_sub');
    $this->db->select('soal_level.nama as level');
    $this->db->from('list_jawaban_soal');
    $this->db->join('soal', 'list_jawaban_soal.kode_soal = soal.kode', 'left');
    $this->db->join('mata_pelajaran', 'soal.mapel = mata_pelajaran.id', 'left');
    $this->db->join('sub_mapel', 'soal.sub_bab = sub_mapel.id', 'left');
    $this->db->join('soal_level', 'soal.level = soal_level.id', 'left');
    $this->db->where('list_jawaban_soal.nise', $nise);
    $this->db->where('list_jawaban_soal.jawaban <>', "NULL");
    $this->db->where('list_jawaban_soal.waktu_selesai <>', "NULL");
    $this->db->where('mata_pelajaran.slug', $slug);
    $this->db->order_by('waktu_mulai','DESC');
    return $this->db->get();
  }

  function getSoal($id,$slug,$level){
    $this->db->select('soal.*');
    $this->db->select('mapel.nama as nama_soal');
    $this->db->select('jawaban.id as id_jawaban');
    $this->db->select('soal_level.point as point_soal');
    $this->db->from('soal');
    $this->db->join('soal_level', 'soal_level.id = soal.level');
    $this->db->join('sub_mapel', 'soal.sub_bab = sub_mapel.id', 'left');
    $this->db->join('mata_pelajaran mapel', 'mapel.id = soal.mapel', 'left');
    if ($level == 'users') {
      $this->db->join("(SELECT * FROM `list_jawaban_soal_users` WHERE user_id = '$id')AS jawaban",
                      'soal.kode = jawaban.kode_soal',
                      'left');
    }else {
      $this->db->join("(SELECT * FROM `list_jawaban_soal` WHERE nise = '$id')AS jawaban",
                      'soal.kode = jawaban.kode_soal',
                      'left');
    }
    $this->db->where("(sub_mapel.slug = '$slug') AND
                      (
                        (jawaban.id IS NULL) OR
                        (jawaban.jumlah_skip <= 5 AND
                         jawaban.view = 0)
                      )");
    $this->db->order_by('RAND()');
    $this->db->limit('1');
    return $this->db->get();
  }

  function getResumeSoal($id,$slug,$level){//query untuk mengambil soal belum ada perubahan sama sekali
    $this->db->select('soal.*');
    $this->db->select('mapel.nama as nama_soal');
    $this->db->select('jawaban.id as id_jawaban');
    $this->db->select('jawaban.jumlah_skip as jumlah_skip');
    $this->db->select('jawaban.jawaban as jawaban_user');
    $this->db->select('jawaban.waktu_mulai as waktu_mulai');
    $this->db->select('soal_level.point as point_soal');
    $this->db->from('soal');
    $this->db->join('soal_level', 'soal_level.id = soal.level');
    $this->db->join('sub_mapel', 'soal.sub_bab = sub_mapel.id', 'left');
    $this->db->join('mata_pelajaran mapel', 'mapel.id = soal.mapel', 'left');
    if ($level == 'users') {
      $this->db->join("(SELECT * FROM `list_jawaban_soal_users` WHERE user_id = '$id')AS jawaban",
                      'soal.kode = jawaban.kode_soal',
                      'left');
                      $this->db->where('jawaban.user_id', $id);
    }else {
      $this->db->join("(SELECT * FROM `list_jawaban_soal` WHERE nise = '$id')AS jawaban",
                      'soal.kode = jawaban.kode_soal',
                      'left');
                      $this->db->where('jawaban.nise', $id);
    }
    $this->db->where('sub_mapel.slug', $slug);
    $this->db->where('jawaban.waktu_selesai', NULL);
    $this->db->limit('1');
    return $this->db->get();
  }

//TRY OUT
  function getSoalByTo($id_try_out,$kodematpel){
    // $this->db->cache_on();
  $this->db->select("list_soal.kode,list_soal.nomor,list_soal.soal,list_soal.pil_a,list_soal.pil_b,list_soal.pil_c,list_soal.pil_d,list_soal.pil_e,list_soal.jawaban,list_soal.id_mata_pelajaran,list_soal.kode_soal");
  // $this->db->select("mata_pelajaran.nama as nama_mapel");
  $this->db->select("tryout.*");
  $this->db->select("tryout_event.durasi as durasi");
  $this->db->select('tb1.jawaban as jawaban_siswa');
  $this->db->from('tryout');
  $this->db->join('tryout_event','tryout_event.id=tryout.id_event');
  $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
  // $this->db->join('mata_pelajaran', 'list_soal.mapel = mata_pelajaran.id', 'left');
  $this->db->join("(SELECT * FROM jawaban_tryout WHERE jawaban_tryout.id_try_out = $id_try_out) AS tb1", 'list_soal.kode = tb1.id_list_soal', 'left');
  if($kodematpel!=null){
    $this->db->where('list_soal.mapel', $kodematpel);
  }
  $this->db->where('tryout.id', $id_try_out);
  return $this->db->get();

}

function getUtbkData($id_try_out){
$this->db->select("mata_pelajaran.nama as nama_mapel,mata_pelajaran.durasi as durasi_mapel,mata_pelajaran.id");
$this->db->select("list_soal.nomor");
$this->db->select("tryout.waktu_mulai,tryout.waktu_selesai,tryout.id_event");
$this->db->select("event.durasi as durasi");
$this->db->from('tryout');
$this->db->join('tryout_event','tryout_event.id=tryout.id_event');
$this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
$this->db->join('mata_pelajaran', 'list_soal.mapel = mata_pelajaran.id', 'left');
$this->db->group_by('list_soal.mapel');
$this->db->where('tryout.id', $id_try_out);
$this->db->order_by('list_soal.nomor', 'asc');


return $this->db->get();

}

function getJumlahSoal($id_try_out){
  $this->db->cache_on();
$this->db->select("list_soal.nomor");
// $this->db->select("mata_pelajaran.nama as nama_mapel");
$this->db->from('tryout');
$this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
// $this->db->join('mata_pelajaran', 'list_soal.mapel = mata_pelajaran.id', 'left');
$this->db->where('tryout.id', $id_try_out);
return $this->db->get();

}
function check($table,$where){
  return $this->db->get_where($table,$where);
}

}
