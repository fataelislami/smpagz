<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function list($tipe,$date,$kelas=null,$jurusan=null){
    $this->db->select("*");
    $this->db->from("event");
    // if($tipe=="QUIZ" && $kelas==null && $jurusan==null){
    //   $this->db->where("'$date' BETWEEN `waktu_mulai` AND `waktu_selesai` AND `tipe`='$tipe'");
    // }
    if($tipe=="TO"){
      $this->db->where("'$date' BETWEEN `waktu_mulai` AND `waktu_selesai`");
    }
    if($kelas==0 || $kelas==7 || $kelas==8 || $kelas==9){
      $this->db->where('kelas', $kelas);
    }else{
      $this->db->where('kelas', $kelas);
      $this->db->where('jurusan', $jurusan);
    }
    return $this->db->get();
  }

}
