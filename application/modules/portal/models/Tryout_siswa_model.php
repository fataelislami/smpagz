<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tryout_siswa_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  //get by id
  function get_tryout_siswa_byId($id){
    $this->db->where('id', $id);
    return $this->db->get('tryout');
  }

  function get_tryout_by_id($id){
    $this->db->select('tryout.*');
    $this->db->select('siswa.*');
    $this->db->select('tryout_event.nama as nama_tryout_event, tryout_event.kode_soal');
    $this->db->join('siswa', 'tryout.nis = siswa.nis', 'left');
    $this->db->join('tryout_event', 'tryout.id_event = tryout_event.id', 'left');
    $this->db->where('tryout.id', $id);
    return $this->db->get('tryout');
  }

  function get_Jawaban_To($id_to,$mapel){
    $this->db->select(' tb1.id, list_soal.kode, list_soal.nomor, mata_pelajaran.nama');
    $this->db->select('tb1.jawaban as jawaban_siswa');
    $this->db->select('list_soal.jawaban as kunci_jawaban');
    $this->db->from('tryout');
    $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal.id_mata_pelajaran = mata_pelajaran.id', 'left');
    $this->db->join("(SELECT * FROM jawaban_tryout WHERE jawaban_tryout.id_try_out = $id_to) AS tb1", 'list_soal.kode = tb1.id_list_soal', 'left');
    if ($mapel != NULL) {
      $this->db->where('mata_pelajaran.id', $mapel);
    }
    $this->db->where('tryout.id', $id_to);
    $this->db->order_by('list_soal.nomor','ASC');
    return $this->db->get();
  }

  function getListMapelByTO($id_to){
    $this->db->select('mata_pelajaran.nama , mata_pelajaran.id');
    $this->db->from('tryout');
    $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal.id_mata_pelajaran = mata_pelajaran.id', 'left');
    $this->db->where('tryout.id', $id_to);
    $this->db->group_by('mata_pelajaran.id');
    return $this->db->get();
  }

}
