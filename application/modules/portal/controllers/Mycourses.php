<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mycourses extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('MataPelajaran_model','mapel');
    $this->load->model('Tryout_users_model','Mtryout_users');
    $this->load->model('Tryout_siswa_model','Mtryout_siswa');
    $this->load->model('Soal_model','Msoal');
    $this->load->model(array('Dbs'));
    $this->load->helper(array('dbs','tanggal'));
    //Codeigniter : Write Less Do More
    if($this->session->userdata('status')!='login'){
      redirect(base_url('login'));
    }
    if(($this->session->userdata('level')!='users')AND($this->session->userdata('level')!='siswa')){
      $this->session->set_flashdata('message', 'Silahkan Login Terlebih Dahulu');
      redirect(redirect($_SERVER['HTTP_REFERER']));
    }
  }

  function index()
  {
    $id_user = $this->session->userdata('id'); //isi id user disini
    $nise = $this->session->userdata('nis');

    $listTryoutDashboard = $this->mapel->getTryoutDashboardSiswa($nise)->result();

    $data=array(
      'content'=>'portal/my_courses/my_courses',
      'menu_right'=>'portal/menu_right',
      'module'=>'portal',
      'listTryoutDashboard'=>$listTryoutDashboard,
    );
    $this->template->load_front($data);
  }

  function tryout($id){
    if ($this->session->userdata("level")=="siswa") { //jika yang login siswa alihkan ke fungsi tryout siswa
      $this->tryout_siswa($id);
    }elseif ($this->session->userdata("level")=="users") { //jika yang tryout user alihkan ke fungsi tryout users
      $this->tryout_users($id);
    }else {
      redirect(base_url('portal/dashboard'));
    }
  }

  function tryout_siswa($id){
    if ($this->Mtryout_siswa->get_tryout_by_id($id)->num_rows() > 0) {
      $dataTryout = $this->Mtryout_siswa->get_tryout_by_id($id)->row(); // mengambil data tryout
      $dataJawaban = $this->Mtryout_siswa->get_Jawaban_To($id,NULL)->result(); // mengambil data jawaban tryout

      //mengambil data jawaban tryout per mapel
      $listMapel = $this->Mtryout_siswa->getListMapelByTO($id)->result(); //mengambil list mapel
      $dataJawabanMapel = []; // array penampung

      foreach ($listMapel as $r) { // ulangi sebanyak banyaknya mapel
        $listJawabanPerMapel = $this->Mtryout_siswa->get_Jawaban_To($id,$r->id)->result();
        $benar = 0; $salah = 0; $null = 0;
        //menghitung jumlah benar salah kosong
        foreach ($listJawabanPerMapel as $a) {
          if ($a->jawaban_siswa == NULL) {
            $null++;
          }elseif ($a->jawaban_siswa == $a->kunci_jawaban) {
            $benar++;
          }else {
            $salah++;
          }
        }
        //mentimpan hasil perhitungan
        $hasil = array(
          'nama_mapel' => $r->nama,
          'benar' => $benar,
          'salah' => $salah,
          'kosong' => $null,
        );
        array_push($dataJawabanMapel,$hasil);
      }
      //end mengambil data jawaban tryout per mapel

      $data=array(
        'content'=>'portal/my_courses/tryout',
        'menu_right'=>'portal/menu_right',
        'module'=>'portal',
        'dataTryout'=>$dataTryout,
        'dataJawaban'=>$dataJawaban,
        'dataJawabanMapel'=>$dataJawabanMapel,
        'idTryout'=>$id,
      );

      // var_dump($dataTryout);die;
      // $rank = getdata('temporary_table',array('kode'=>'top_nasional'))->row();
      // if($rank->deskripsi!=NULL){
      //   $data['rank']=json_decode($rank->deskripsi);
      // }else{
      //   $data['rank']=NULL;
      // }
      // $rank_today = getdata('temporary_table',array('kode'=>'top_nasional_now'))->row();
      // if($rank_today->deskripsi!=NULL){
      //   $data['rank_today']=json_decode($rank_today->deskripsi);
      // }else{
      //   $data['rank_today']=NULL;
      // }
      // $currentRank = $this->Dbs->getPointRankIndividu($this->session->userdata('id'))->row();
      // $data['currentRank']=$currentRank;
      $this->template->load_front($data);
    }else {
      redirect(base_url('portal/dashboard'));
    }
  }

  function tryout_users($id){
    if ($this->Mtryout_users->get_tryout_by_id($id)->num_rows() > 0) {
      $dataTryout = $this->Mtryout_users->get_tryout_by_id($id)->row(); // mengambil data tryout
      $dataJawaban = $this->Mtryout_users->get_Jawaban_To($id,NULL)->result(); // mengambil data jawaban tryout

      //mengambil data jawaban tryout per mapel
      $listMapel = $this->Mtryout_users->getListMapelByTO($id)->result(); //mengambil list mapel
      $dataJawabanMapel = []; // array penampung

      foreach ($listMapel as $r) { // ulangi sebanyak banyaknya mapel
        $listJawabanPerMapel = $this->Mtryout_users->get_Jawaban_To($id,$r->id)->result();
        $benar = 0; $salah = 0; $null = 0;
        //menghitung jumlah benar salah kosong
        foreach ($listJawabanPerMapel as $a) {
          if ($a->jawaban_siswa == NULL) {
            $null++;
          }elseif ($a->jawaban_siswa == $a->kunci_jawaban) {
            $benar++;
          }else {
            $salah++;
          }
        }
        //mentimpan hasil perhitungan
        $hasil = array(
          'nama_mapel' => $r->nama,
          'benar' => $benar,
          'salah' => $salah,
          'kosong' => $null,
        );
        array_push($dataJawabanMapel,$hasil);
      }
      //end mengambil data jawaban tryout per mapel

      $data=array(
        'content'=>'portal/my_courses/tryout',
        'menu_right'=>'portal/menu_right',
        'module'=>'portal',
        'dataTryout'=>$dataTryout,
        'dataJawaban'=>$dataJawaban,
        'dataJawabanMapel'=>$dataJawabanMapel,
        'idTryout'=>$id,
      );
      $rank = getdata('temporary_table',array('kode'=>'top_nasional'))->row();
      if($rank->deskripsi!=NULL){
        $data['rank']=json_decode($rank->deskripsi);
      }else{
        $data['rank']=NULL;
      }
      $rank_today = getdata('temporary_table',array('kode'=>'top_nasional_now'))->row();
      if($rank_today->deskripsi!=NULL){
        $data['rank_today']=json_decode($rank_today->deskripsi);
      }else{
        $data['rank_today']=NULL;
      }
      $currentRank = $this->Dbs->getPointRankIndividu($this->session->userdata('id'))->row();
      $data['currentRank']=$currentRank;
      $this->template->load_front($data);
    }else {
      redirect(base_url('portal/dashboard'));
    }
  }

  function soal($slug){
    //cek apakah slug ada atau tidak
    if ($this->mapel->getMapelBySlug($slug)->num_rows() > 0) {
      $id_user = $this->session->userdata('id');
      $nise = $this->session->userdata('nise');

      if ($this->session->userdata('level')=='users') { //model untuk users
        $listSoal = $this->Msoal->getListSoalUser($slug,$id_user)->result();
      }else { //model untuk siswa
        $listSoal = $this->Msoal->getListSoalSiswa($slug,$nise)->result();
      }

      //var_dump($listSoal);die;
      $data=array(
        'content'=>'portal/my_courses/soal',
        'menu_right'=>'portal/menu_right',
        'module'=>'portal',
        'listSoal'=>$listSoal,
      );
      $rank = getdata('temporary_table',array('kode'=>'top_nasional'))->row();
      if($rank->deskripsi!=NULL){
        $data['rank']=json_decode($rank->deskripsi);
      }else{
        $data['rank']=NULL;
      }
      $rank_today = getdata('temporary_table',array('kode'=>'top_nasional_now'))->row();
      if($rank_today->deskripsi!=NULL){
        $data['rank_today']=json_decode($rank_today->deskripsi);
      }else{
        $data['rank_today']=NULL;
      }
      $currentRank = $this->Dbs->getPointRankIndividu($this->session->userdata('id'))->row();
      $data['currentRank']=$currentRank;
      $this->template->load_front($data);
    }else {
      redirect(base_url('portal/mycourses'));
    }
  }

  function detailsoal(){
    $idJawaban = $this->input->get('id');
    if ($idJawaban != "") {
      if ($this->input->get('type')=="latihan") {
        $dataSoal = $this->Msoal->get_soal_by_kode($idJawaban); //ambil data soal
        $update = array('view' => '1');
        $where = array('id' => $idJawaban);
        if ($this->session->userdata('level')=='users') {
          $table = "list_jawaban_soal_users";
        }else {
          $table = "list_jawaban_soal";
        }
        $this->Msoal->update($update,$table,$where);
      }else {
        $dataSoal = $this->Msoal->get_soalTo_by_kode($idJawaban); //ambil data soal to
      }

      if ($dataSoal->num_rows() > 0) { //cek apakah soal ada di database
        $dataSoal = $dataSoal->row();

        $data=array(
          'content'=>'portal/my_courses/detail-soal',
          'menu_right'=>'portal/menu_right',
          'module'=>'portal',
          'datasoal'=>$dataSoal,
        );
        $data['css']='portal/soal/item_css';
        // $rank = getdata('temporary_table',array('kode'=>'top_nasional'))->row();
        // if($rank->deskripsi!=NULL){
        //   $data['rank']=json_decode($rank->deskripsi);
        // }else{
        //   $data['rank']=NULL;
        // }
        // $rank_today = getdata('temporary_table',array('kode'=>'top_nasional_now'))->row();
        // if($rank_today->deskripsi!=NULL){
        //   $data['rank_today']=json_decode($rank_today->deskripsi);
        // }else{
        //   $data['rank_today']=NULL;
        // }
        // $currentRank = $this->Dbs->getPointRankIndividu($this->session->userdata('id'))->row();
        // $data['currentRank']=$currentRank;
        $this->template->load_front($data);

      }else {
        redirect(base_url('portal/dashboard'));
      }
    }else {
      redirect(base_url('portal/dashboard'));
    }
  }



}
