<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tryout extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Event_model','Soal_model'));
  }

  function index()
  {

  }

  function go(){
    if($this->session->userdata('id_try_out')){
      if($this->session->userdata('utbk')!='false'){
        redirect(base_url('portal/tryout/utbk'));
        die;
      }
      $id_try_out=$this->session->userdata('id_try_out');//Dari Session
      $check_try_out = $this->Soal_model->check("tryout",array('id'=>$id_try_out));
      if($check_try_out->num_rows()>0){
      }else{
        redirect(base_url('tryout/regist'));
      }
    }else{
      redirect(base_url('tryout/regist'));//redirect ke halaman .
      die;
    }
		$check_try_out = $this->Soal_model->check("tryout",array('id'=>$id_try_out,'waktu_selesai!='=>null));
    if($check_try_out->num_rows()>0){
      redirect(base_url('tryout/finish'));//redirect ke halaman yang menyatakan anda sudah selesai mengerjakan TO Sesi Ini.
    }

    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $jamsekarang=$dt->format('Y-m-d H:i:s');
    //END TIMEZONE
    $jumlah=$this->Soal_model->getJumlahSoal($id_try_out)->num_rows();
    $total_page=ceil($jumlah/12);
    // var_dump($total_page);
    // die;
        //StartPagination
    if(isset($_GET['page'])){//cek parameter page
      $page=$_GET['page'];
    }else{
      $page=1;//default jika parameter page tidak diload
    }
    $limitDb=12;
    $offsetDb=0;
    if($page!=1 and $page!=0){
      $offsetDb=$limitDb*($page-1);
    }
    //End Pagination
    if($page<=$total_page and $page>=1){
      //edit ini
      $this->db->limit($limitDb,$offsetDb);
      $nextPage='true';
      $page=$page+1;
      //sama ini
    }else{
      redirect(site_url('portal/tryout/go'));
    }
      $json=$this->soal_by_to($id_try_out);
    //set session continue
    $dataSession = array(
      'continue' => 'true',
      'id_try_out'=>$id_try_out
    );
    $this->session->set_userdata($dataSession);
    //set session continue
    $obj=(object)$json;
    //Cek Durasi
    $menit = round(abs(strtotime($jamsekarang) - strtotime($obj->waktu_mulai)) / 60,0);
    if((int)$menit>=(int)$obj->durasi){//ambil dari database
      $this->session->set_flashdata('id_try_out', $id_try_out);
      log_message('error', 'C1:Waktu Mulai '.$obj->waktu_mulai." durasi".$obj->durasi." menit".$menit."Id Try Out ".$id_try_out);
      redirect(site_url('tryout/finish'));
      // redirect(base_url());//redirect ke halaman yang menyatakan waktu habis.
    }
    //Cek Durasi
    $data['soal']=$obj;
    $data['id_try_out']=$id_try_out;
    $data['waktu_mulai']=$obj->waktu_mulai;
    $data['menit']=$menit;
    $data['durasi']=$obj->durasi;
    $data['utbk']='false';
    $data['durasi_mapel']=$obj->durasi;//karena bukan utbk, jadi durasi mapel disamakan
    //tambah ini
    $data['nextPage']=$nextPage;
    $data['urlNext']=base_url().'portal/tryout/go?page='.$page;
    $data['urlPrev']=base_url().'portal/tryout/go?page='.($page-2);
    $data['page']=$page-1;
    $data['total_page']=$total_page;
    //sampe ini
    $data['content']='portal/tryout/go';
    $data['js']='portal/tryout/go_js';
    $data['css']='portal/tryout/go_css';
    $data['menu_right']='portal/menu_right';
    $this->template->load_front($data);
  }

  function utbk(){
    if($this->session->userdata('id_try_out')){
      if($this->session->userdata('utbk')!='true'){
        redirect(base_url('tryout/go'));
        die;
      }
      $id_try_out=$this->session->userdata('id_try_out');//Dari Session
      $check_try_out = $this->Soal_model->check("try_out",array('id'=>$id_try_out));
      if($check_try_out->num_rows()>0){
      }else{
        redirect(base_url('tryout/regist'));
      }
    }else{
      redirect(base_url('tryout/regist'));//redirect ke halaman .
      die;
    }
    //UPDATE 3 APRIL
    $check_try_out = $this->Soal_model->check("try_out",array('id'=>$id_try_out,'waktu_selesai!='=>null));
    if($check_try_out->num_rows()>0){
      redirect(base_url('tryout/finish'));//redirect ke halaman yang menyatakan anda sudah selesai mengerjakan TO Sesi Ini.
    }

    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $jamsekarang=$dt->format('Y-m-d H:i:s');
    //END TIMEZONE
      $json=$this->list_soal_utbk($id_try_out);
    //set session continue
    $dataSession = array(
      'continue' => 'true',
      'id_try_out'=>$id_try_out
    );
    $this->session->set_userdata($dataSession);
    //set session continue
    $obj=(object)$json;
    $menit = round(abs(strtotime($jamsekarang) - strtotime($obj->waktu_mulai)) / 60,0);
    if((int)$menit>=(int)$obj->durasi){//ambil dari database
      $this->session->set_flashdata('id_try_out', $id_try_out);
      log_message('error', 'C1:Waktu Mulai '.$obj->waktu_mulai." durasi".$obj->durasi." menit".$menit."Id Try Out ".$id_try_out);
      redirect(site_url('tryout/finish'));
      // redirect(base_url());//redirect ke halaman yang menyatakan waktu habis.
    }else{
      $durasi_mapel=0;
      $id_mapel='';
      // $menit=25;
      $i=1;
      $numItem=$obj->total_result;
      foreach ($obj->results as $o) {
        $durasi_mapel=$durasi_mapel+$o->durasi_mapel;
        if($menit<$durasi_mapel){
          $id_mapel=$o->id;//CEK ID MAPEL
          $jsonsoal=$this->soal_by_to($id_try_out,$id_mapel);
          $objsoal=(object)$jsonsoal;
          if($i==$numItem){
            $data['isLast']='true';
          }
          break;
        }
        $i++;
      }
    }
    //Cek AUTO REFRESH SERVERSIDE
    $refresh_on=(($durasi_mapel-$menit)*60);
    header("Refresh: $refresh_on;");
    //Init
    $data['nextPage']=0;
    $data['urlNext']=0;
    $data['urlPrev']=0;
    $data['page']=0;
    $data['total_page']=0;
    //
    $data['soal']=$objsoal;
    $data['id_try_out']=$id_try_out;
    $data['menit']=$menit;
    $data['waktu_mulai']=$obj->waktu_mulai;
    $data['durasi']=(int)$obj->durasi;
    $data['durasi_mapel']=$durasi_mapel;
    $data['utbk']='true';

    $data['content']='portal/tryout/go';
    $data['js']='portal/tryout/go_js';
    $data['css']='portal/tryout/go_css';
    $data['menu_right']='portal/menu_right';
    $this->template->load_front($data);
  }


  function list(){
    //Get List EVENT
    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $now=$dt->format('Y-m-d H:i:s');
    //END TIMEZONE
    $nise=$this->session->userdata('id');
    $kelas=substr($nise,6,1);
    $jurusan=substr($nise,7,1);
    $tipe="TO";
    $res=$this->Event_model->list($tipe,$now,$kelas,$jurusan);
    $data['content']='portal/tryout/list';
    $data['js']='portal/tryout/list_js';
    $data['menu_right']='portal/menu_right';

    //initData
    $data['listTryout']=$res;
    $this->template->load_front($data);
  }


  //SOAL
  function soal_by_to($id_try_out,$kodematpel=null){
      $res=$this->Soal_model->getSoalByTo($id_try_out,$kodematpel);
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'waktu_mulai'=>$result[0]->waktu_mulai,
          'waktu_selesai'=>$result[0]->waktu_selesai,
          'id_event'=>$result[0]->id_event,
          'durasi'=>$result[0]->durasi,
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    return $data;
  }

  function list_soal_utbk($id_try_out){
      $res=$this->Soal_model->getUtbkData($id_try_out);
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'waktu_mulai'=>$result[0]->waktu_mulai,
          'waktu_selesai'=>$result[0]->waktu_selesai,
          'id_event'=>$result[0]->id_event,
          'durasi'=>$result[0]->durasi,
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    return $data;
  }

}
