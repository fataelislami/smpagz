<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soal extends MY_Controller{

 //Route :
 // -lulusnegeri.com/soal/matematika
 // -lulusnegeri.com/soal/matematika?no=2
  public function __construct()
  {
    parent::__construct();
    //$this->load->model('NamaModel','Alias');
    $this->load->model('MataPelajaran_model','mapel'); //diubah pakai alias jadi $this->mapel->fungsi
    $this->load->model('Soal_model','soal');
    $this->load->model('Dbs');
    $this->load->helper(array('dbs'));
    //Codeigniter : Write Less Do More
    if($this->session->userdata('edulab')=='2'){
      redirect(base_url('portal/dashboard'));
    }
  }

  function _remap($param) {//Agar index bisa diberi parameter
      $this->index($param);
  }

  function index($param=null)
  {
    $data = array();
    if($param!='index'){//ketika parameter url terisi (Task : belum dikasih pengecekan by slug)
      $sub=$this->input->get('sub');
      if($sub!=''){
        //$data['content']='portal/soal/item';
        $this->item($sub);
      }else{
        //$data['content']='portal/soal/single';
        $this->single($param);
      }
    }else{
      //$data['content']='portal/soal/list';
      $this->list();
    }
    //$this->template->load_front($data);

  }

  function item($sub){
    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $now=$dt->format('Y-m-d H:i:s');
    $dateNow=$dt->format('Y-m-d');
    //END TIMEZONE

    $id = $this->session->userdata('id'); //isi berupa id atau nise
    $slug = $sub; //isikan slug sub mata pelajaran
    $level = $this->session->userdata('level'); //isikan level (users atau siswa)

    $getSoalResume = $this->soal->getResumeSoal($id,$slug,$level);

    if($getSoalResume->num_rows()>0){//jika ada soal yang belum selesai
      $getSoal=$getSoalResume->row();
      $menit = round(abs(strtotime($now) - strtotime($getSoal->waktu_mulai)) / 60,0);
      $data['menit']=$menit;
      $data['waktu_mulai']=$getSoal->waktu_mulai;
    }else{//jika tidak ada soal, lakukan pengecekan soal baru
      $getSoalNew = $this->soal->getsoal($id,$slug,$level);//ambil soal berdasarkan kondisi query {cek model}
      if($getSoalNew->num_rows()>0){
        $getSoal=$getSoalNew->row();
        $kode_soal=$getSoal->kode;
        //Jika soal belum ada dalam table list_jawaban_soal atau list_jawaban_soal_users
        if($level=='users'){
          $table='list_jawaban_soal_users';
          $dataInsert=array('user_id'=>$id,'kode_soal'=>$kode_soal);
        }else{
          $table='list_jawaban_soal';
          $dataInsert=array('nise'=>$id,'kode_soal'=>$kode_soal);
        }
        $waktu_mulai=$now;
        $menit = round(abs(strtotime($now) - strtotime($waktu_mulai)) / 60,0);
        $data['menit']=$menit;
        $data['waktu_mulai']=$waktu_mulai;
        $dataInsert['waktu_mulai']=$waktu_mulai;
        $sql=$this->Dbs->insert($dataInsert,$table);
        $data['id_jawaban']=$this->db->insert_id();
      }else{
        redirect(base_url('portal/soal'));
      }
    }
    // var_dump($getSoal);die;
    $data['getSoal']=$getSoal;
    //Init Data user untuk dikirim ke JS
    $data['nise_id']=$id;
    $data['level']=$level;
    //get slug mapel
    $mapel=$this->Dbs->getMapelBySlugSubbab($sub)->row();
    //Init Data Rank
    $rank = getdata('temporary_table',array('kode'=>'mapel_'.$mapel->slug))->row();
    if($rank->deskripsi!=NULL){
      $data['rank']=json_decode($rank->deskripsi);
    }else{
      $data['rank']=NULL;
    }
    // $currentRank = $this->Dbs->getPointRankIndividuByDateAndMapel($dateNow,$mapel->slug,$this->session->userdata('id'));
    // $data['currentRank']=$currentRank;
    $data['mapel']=$mapel;
    $data['content']='portal/soal/item';
    $data['js']='portal/soal/item_js';
    $data['css']='portal/soal/item_css';
    $this->template->load_front($data);
  }

  function single($slug){
    if($this->session->userdata('status')!='login'){
      redirect(base_url('login'));
    }
    if(($this->session->userdata('level')!='users')AND($this->session->userdata('level')!='siswa')){
      $this->session->set_flashdata('message', 'Silahkan Login Terlebih Dahulu');
      redirect(redirect($_SERVER['HTTP_REFERER']));
    }
    $loadSubMataPelajaran=$this->mapel->getListSubMapel($slug);
    if($loadSubMataPelajaran->num_rows()==0){
      $this->session->set_flashdata('message', 'Belum ada data soal');
      redirect(base_url('portal/soal'));
    }
    $getSubMataPelajaran=$loadSubMataPelajaran;
    $data['content']='portal/soal/single';
    $data['module']='portal';
    $rank = getdata('temporary_table',array('kode'=>'mapel_'.$slug))->row();
    if($rank->deskripsi!=NULL){
      $data['rank']=json_decode($rank->deskripsi);
    }else{
      $data['rank']=NULL;
    }
    // $currentRank = $this->Dbs->getPointRankIndividu($this->session->userdata('id'))->row();
    // $data['currentRank']=$currentRank;
    //initData
    $data['getSubMataPelajaran']=$getSubMataPelajaran;
    $this->template->load_front($data);
  }

  function list(){
    $kelas=substr($this->session->userdata('id'),6,1);
    $list_mapel_ipa = json_decode(getdata('temporary_table',array('kode'=>'list_mapel_ipa'))->row()->deskripsi);
    $list_mapel_ips = json_decode(getdata('temporary_table',array('kode'=>'list_mapel_ips'))->row()->deskripsi);
    $list_mapel_ipc = json_decode(getdata('temporary_table',array('kode'=>'list_mapel_ipc'))->row()->deskripsi);

    $data = array(
      'list_mapel_ipa' => $list_mapel_ipa,
      'list_mapel_ips' => $list_mapel_ips,
      'list_mapel_ipc' => $list_mapel_ipc,
    );

    $data['content']='portal/soal/list';
    $rank = getdata('temporary_table',array('kode'=>'top_nasional'))->row();
    if($rank->deskripsi!=NULL){
      $data['rank']=json_decode($rank->deskripsi);
    }else{
      $data['rank']=NULL;
    }
    $rank_today = getdata('temporary_table',array('kode'=>'top_nasional_now'))->row();
    if($rank_today->deskripsi!=NULL){
      $data['rank_today']=json_decode($rank_today->deskripsi);
    }else{
      $data['rank_today']=NULL;
    }
    $currentRank = $this->Dbs->getPointRankIndividu($this->session->userdata('id'))->row();
    $data['currentRank']=$currentRank;
    $this->template->load_front($data);
  }

  function imam(){
    // $id = '6'; //isikan id atau nise
    // $slug = 'manusia-purba-di-indonesia-dan-dunia'; //isikan slug sub mata pelajaran
    // $level = 'users'; //isikan level (users atau siswa)

    $id = $this->session->userdata('id');; //isikan id atau nise
    $slug = 'test-subab'; //isikan slug sub mata pelajaran
    $level = $this->session->userdata('level'); //isikan level (users atau siswa)

    $data = $this->soal->getsoal($id,$slug,$level)->result();
    var_dump($this->session->userdata());
    var_dump($data);
  }

  function get_rank(){
    $this->load->model('Dbs');

    //mendapatkan list level user dan siswa
    // parameter = getPointRank(limit data)
    $data = $this->Dbs->getPointRank(5)->result();
    var_dump($data);

    //mendapatkan level perorang
    // parameter = getPointRankIndividu(nise atau id_users);
    $data = $this->Dbs->getPointRankIndividu("19000000002")->row();
    var_dump($data);
  }

  /*
  Fungsi getRankByDateAndMapel
  untuk mendapatkan fungsi rangking berdasarkan tanggal($date) dan matapelajaran($mapel),
  Isikan $id dengan nise atau id user untuk mendapatkan info rangking satu user tersebut,
  Kosongkan $id untuk mendatkan seluru list rangking
  */
  function getRankByDateAndMapel($date,$mapel,$id=NULL){
    if ($id==NULL) {
      return $this->Dbs->getPointRankByDateAndMapel($date,$mapel,$limit=5);
    }else{
      return $this->Dbs->getPointRankIndividuByDateAndMapel($date,$mapel,$id);
    }

  }


}
