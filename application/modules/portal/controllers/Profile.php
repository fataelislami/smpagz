nis<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('Users_model','Musers');
    $this->load->model('Siswa_model','Msiswa');
    $this->load->model('Dbs');
    $this->load->helper(array('dbs'));
    //Codeigniter : Write Less Do More
    if($this->session->userdata('status')!='login'){
      redirect(base_url('login'));
    }
    if(($this->session->userdata('level')!='users')AND($this->session->userdata('level')!='siswa')){
      $this->session->set_flashdata('message', 'Silahkan Login Terlebih Dahulu');
      redirect(redirect($_SERVER['HTTP_REFERER']));
    }
  }

  function index()
  {
    $nis = $this->session->userdata('nis');
    $data_user = $this->Msiswa->get_siswa_by_nis($nis)->row();
    
    $data=array(
      'content'=>'portal/profile',
      'menu_right'=>'portal/menu_right',
      'module'=>'portal',
      'action'=>'portal/profile/update_action',
      'data_user'=>$data_user,
    );

    $this->template->load_front($data);
  }

  public function update_action()
  {
    $data = array(
      'email' => $this->input->post('email',TRUE),
      'alamat' => $this->input->post('alamat',TRUE),
    );

    $this->Msiswa->update($this->session->userdata('id'), $data);
    $this->session->set_flashdata('message', 'Update Record Success');
    redirect(site_url('portal/profile'));
  }

  function get_rank(){
    $this->load->model('Dbs');

    //mendapatkan list level user dan siswa
    // parameter = getPointRank(limit data)
    $data = $this->Dbs->getPointRank(5)->result();
    var_dump($data);

    //mendapatkan level perorang
    // parameter = getPointRankIndividu(nis atau id_users);
    $data = $this->Dbs->getPointRankIndividu($this->session->userdata('id'))->row();
    var_dump($data);
  }

}
