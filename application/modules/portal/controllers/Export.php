<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Export extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tryout_users_model','Mtryout_users');
        $this->load->model('Tryout_siswa_model','Mtryout_siswa');
        if($this->session->userdata('status')!='login'){
          redirect(base_url('login'));
        }
    }

    public function index()
    {
      redirect(base_url());
    }

    function tryout(){
      $level = $this->input->get('level');
      $idTryout = $this->input->get('id');

      if ($level == "users") {
        $this->tryout_users($idTryout);
      }elseif ($level == "siswa") {
        $this->tryout_siswa($idTryout);
      }else {
        redirect(base_url('portal/mycourses'));
      }
    }

    function tryout_siswa($id){
      if ($this->Mtryout_siswa->get_tryout_by_id($id)->num_rows() > 0) {
        $dataTryout = $this->Mtryout_siswa->get_tryout_by_id($id)->row(); // mengambil data tryout
        $dataJawaban = $this->Mtryout_siswa->get_Jawaban_To($id,NULL)->result(); // mengambil data jawaban tryout
        //var_dump($dataTryout);die;
        //mengambil data jawaban tryout per mapel
        $listMapel = $this->Mtryout_siswa->getListMapelByTO($id)->result(); //mengambil list mapel
        $dataJawabanMapel = []; // array penampung

        foreach ($listMapel as $r) { // ulangi sebanyak banyaknya mapel
          $listJawabanPerMapel = $this->Mtryout_siswa->get_Jawaban_To($id,$r->id)->result();
          $benar = 0; $salah = 0; $null = 0;
          //menghitung jumlah benar salah kosong
          foreach ($listJawabanPerMapel as $a) {
            if ($a->jawaban_siswa == NULL) {
              $null++;
            }elseif ($a->jawaban_siswa == $a->kunci_jawaban) {
              $benar++;
            }else {
              $salah++;
            }
          }
          //mentimpan hasil perhitungan
          $hasil = array(
            'nama_mapel' => $r->nama,
            'benar' => $benar,
            'salah' => $salah,
            'kosong' => $null,
          );
          array_push($dataJawabanMapel,$hasil);
        }
        //end mengambil data jawaban tryout per mapel

        //mulai ekspor data
        include_once("PHP_XLSX/xlsxwriter.class.php");

        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename("Laporan.xlsx").'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $style_header = array( 'font'=>'Arial','font-size'=>10,'font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center', 'border'=>'left,right,top,bottom' ,'widths'=>[5,10,12,30,30,10,10,18,30,40,5,30,40,5] );
        $style_row = array( 'font'=>'Arial','font-size'=>10, 'halign'=>'center', 'border'=>'left,right,top,bottom');

        $writer = new XLSXWriter();
        $writer->setAuthor('Kostlab');

        $sheet1 = "Tryout - $dataTryout->nama";
        $header = array("string","string","string","string","string");
        $writer->writeSheetHeader($sheet1, $header, $col_options = $arrayName = array('suppress_row'=>true,'widths'=>[5,20,60,20,20,10,10,10,12,11] ));

        // setting style kolom
        $style_header0 = array( 'font'=>'Arial','font-size'=>10,'font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'left', 'border'=>'left,right,top,bottom' );
        $style_header = array( 'font'=>'Arial','font-size'=>10,'font-style'=>'bold', 'halign'=>'center', 'fill'=>'#eee', 'border'=>'left,right,top,bottom' );
        $style_row = array( 'font'=>'Arial','font-size'=>10, 'halign'=>'center', 'border'=>'left,right,top,bottom');
        $styles1 = array([],$style_header0,$style_row);
        $styles2 = array( 'font'=>'Arial','font-size'=>16,'font-style'=>'bold', 'halign'=>'center' );
        $styles3 = array([],[],[],[],[],$style_header);

        // isi data tabel 1
        $jurusan1 = "";
        $jurusan2 = "";
        // if ($dataTryout->jurusan_univ1 != NULL) {
        //   $jurusan1 = "$dataTryout->nama_univ1 - $dataTryout->jurusan_univ1 [$dataTryout->pg_univ1]";
        // }
        // if ($dataTryout->jurusan_univ2 != NULL) {
        //   $jurusan2 = "$dataTryout->nama_univ2 - $dataTryout->jurusan_univ2 [$dataTryout->pg_univ2]";
        // }
        $writer->writeSheetRow($sheet1, $rowdata = array("Data Tryout Siswa"), $styles2 );
        $writer->writeSheetRow($sheet1, $rowdata = array("") );
        $writer->writeSheetRow($sheet1, $rowdata = array("","Nama","$dataTryout->nama"), $styles1 );
        $writer->writeSheetRow($sheet1, $rowdata = array("","Sekolah","SMP Al-Ghozali Purwakarta"), $styles1 );
        $writer->writeSheetRow($sheet1, $rowdata = array("","Nama Event","$dataTryout->nama_tryout_event"), $styles1 );
        $writer->writeSheetRow($sheet1, $rowdata = array("","Kode Soal","$dataTryout->kode_soal"), $styles1 );
        $writer->writeSheetRow($sheet1, $rowdata = array("","Tanggal TO","$dataTryout->tanggal"), $styles1 );
        // $writer->writeSheetRow($sheet1, $rowdata = array("","Jurusan 1","$jurusan1"), $styles1 );
        // $writer->writeSheetRow($sheet1, $rowdata = array("","Jurusan 2","$jurusan2"), $styles1 );
        $writer->writeSheetRow($sheet1, $rowdata = array());

        //isi data tabel 2
        $writer->writeSheetRow($sheet1, $rowdata = array("No","Mata Pelajaran","Kunci Jawaban","Jawaban"), $style_header );
        foreach ($dataJawaban as $jb) {
          $writer->writeSheetRow($sheet1, $rowdata = array("$jb->nomor","$jb->nama","$jb->kunci_jawaban","$jb->jawaban_siswa"), $style_row );
        }

        //isi data tabel 3
        $writer->writeSheetRow($sheet1, $rowdata = array());
        $writer->writeSheetRow($sheet1, $rowdata = array("No","Mata Pelajaran","Benar","Salah","Kosong"), $style_header );
        $num = 1;
        foreach ($dataJawabanMapel as $jm) {
          $writer->writeSheetRow($sheet1, $rowdata = array("$num",$jm['nama_mapel'],$jm['benar'],$jm['salah'],$jm['kosong']), $style_row );
          $num++;
        }

        // setting merge kolom
        $writer->markMergedCell($sheet1, $start_row=0, $start_col=0, $end_row=0, $end_col=4);

        $writer->writeToStdOut();
      }else {
        echo "Tryout tidak terdaftar";
      }
    }

    function tryout_users($id){
      if ($this->Mtryout_users->get_tryout_by_id($id)->num_rows() > 0) {
        $dataTryout = $this->Mtryout_users->get_tryout_by_id($id)->row(); // mengambil data tryout
        $dataJawaban = $this->Mtryout_users->get_Jawaban_To($id,NULL)->result(); // mengambil data jawaban tryout

        //mengambil data jawaban tryout per mapel
        $listMapel = $this->Mtryout_users->getListMapelByTO($id)->result(); //mengambil list mapel
        $dataJawabanMapel = []; // array penampung

        foreach ($listMapel as $r) { // ulangi sebanyak banyaknya mapel
          $listJawabanPerMapel = $this->Mtryout_users->get_Jawaban_To($id,$r->id)->result();
          $benar = 0; $salah = 0; $null = 0;
          //menghitung jumlah benar salah kosong
          foreach ($listJawabanPerMapel as $a) {
            if ($a->jawaban_siswa == NULL) {
              $null++;
            }elseif ($a->jawaban_siswa == $a->kunci_jawaban) {
              $benar++;
            }else {
              $salah++;
            }
          }
          //mentimpan hasil perhitungan
          $hasil = array(
            'nama_mapel' => $r->nama,
            'benar' => $benar,
            'salah' => $salah,
            'kosong' => $null,
          );
          array_push($dataJawabanMapel,$hasil);
        }
        //end mengambil data jawaban tryout per mapel

        //mulai ekspor data
        include_once("PHP_XLSX/xlsxwriter.class.php");

        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename("Laporan.xlsx").'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $style_header = array( 'font'=>'Arial','font-size'=>10,'font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center', 'border'=>'left,right,top,bottom' ,'widths'=>[5,10,12,30,30,10,10,18,30,40,5,30,40,5] );
        $style_row = array( 'font'=>'Arial','font-size'=>10, 'halign'=>'center', 'border'=>'left,right,top,bottom');

        $writer = new XLSXWriter();
        $writer->setAuthor('Kostlab');

        $sheet1 = "Tryout - $dataTryout->nama";
        $header = array("string","string","string","string","string");
        $writer->writeSheetHeader($sheet1, $header, $col_options = $arrayName = array('suppress_row'=>true,'widths'=>[5,20,40,20,20,10,10,10,12,11] ));

        // setting style kolom
        $style_header0 = array( 'font'=>'Arial','font-size'=>10,'font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'left', 'border'=>'left,right,top,bottom' );
        $style_header = array( 'font'=>'Arial','font-size'=>10,'font-style'=>'bold', 'halign'=>'center', 'fill'=>'#eee', 'border'=>'left,right,top,bottom' );
        $style_row = array( 'font'=>'Arial','font-size'=>10, 'halign'=>'center', 'border'=>'left,right,top,bottom');
        $styles1 = array([],$style_header0,$style_row);
        $styles2 = array( 'font'=>'Arial','font-size'=>16,'font-style'=>'bold', 'halign'=>'center' );
        $styles3 = array([],[],[],[],[],$style_header);

        // isi data tabel 1
        $writer->writeSheetRow($sheet1, $rowdata = array("Data Tryout Siswa"), $styles2 );
        $writer->writeSheetRow($sheet1, $rowdata = array("") );
        $writer->writeSheetRow($sheet1, $rowdata = array("","Nama","$dataTryout->nama"), $styles1 );
        $writer->writeSheetRow($sheet1, $rowdata = array("","Sekolah","$dataTryout->nama_sekolah"), $styles1 );
        $writer->writeSheetRow($sheet1, $rowdata = array());

        //isi data tabel 2
        $writer->writeSheetRow($sheet1, $rowdata = array("No","Mata Pelajaran","Subbab","Kunci Jawaban","Jawaban"), $style_header );
        foreach ($dataJawaban as $jb) {
          $writer->writeSheetRow($sheet1, $rowdata = array("$jb->nomor","$jb->nama","$jb->sub_bab","$jb->kunci_jawaban","$jb->jawaban_siswa"), $style_row );
        }

        //isi data tabel 3
        $writer->writeSheetRow($sheet1, $rowdata = array());
        $writer->writeSheetRow($sheet1, $rowdata = array("No","Mata Pelajaran","Benar","Salah","Kosong"), $style_header );
        $num = 1;
        foreach ($dataJawabanMapel as $jm) {
          $writer->writeSheetRow($sheet1, $rowdata = array("$num",$jm['nama_mapel'],$jm['benar'],$jm['salah'],$jm['kosong']), $style_row );
          $num++;
        }

        // setting merge kolom
        $writer->markMergedCell($sheet1, $start_row=0, $start_col=0, $end_row=0, $end_col=4);

        $writer->writeToStdOut();
      }else {
        echo "Tryout Tidak terdaftar";
      }
    }

    function export($header,$data,$filename){
      include_once("PHP_XLSX/xlsxwriter.class.php");

      header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
      header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      header('Content-Transfer-Encoding: binary');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');

      $style_header = array( 'font'=>'Arial','font-size'=>10,'font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center', 'border'=>'left,right,top,bottom' ,'widths'=>[5,10,12,30,30,10,10,18,30,40,5,30,40,5] );
      $style_row = array( 'font'=>'Arial','font-size'=>10, 'halign'=>'center', 'border'=>'left,right,top,bottom');

      $writer = new XLSXWriter();
      $writer->setAuthor('Kostlab');
      $writer->writeSheetHeader('Sheet1', $header, $style_header);
      foreach($data as $row)
      	$writer->writeSheetRow('Sheet1', $row , $style_row);
      $writer->writeToStdOut();
    }

}
