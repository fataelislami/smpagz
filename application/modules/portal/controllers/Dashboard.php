<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('MataPelajaran_model','mapel');
    $this->load->model(array('Dbs'));
    $this->load->helper(array('dbs','tanggal'));
    //Codeigniter : Write Less Do More
    if($this->session->userdata('status')!='login'){
      redirect(base_url('login'));
    }
    if(($this->session->userdata('level')!='users')AND($this->session->userdata('level')!='siswa')){
      $this->session->set_flashdata('message', 'Silahkan Login Terlebih Dahulu');
      redirect(redirect($_SERVER['HTTP_REFERER']));
    }
  }

  function index()
  {
    $id_user = $this->session->userdata('id'); //isi id user disini
    $nise = $this->session->userdata('nis');
    // var_dump($radarMapel);die;


    // $listMatkulDashboard = $this->mapel->getMapelCountBySiswa($nise,"3")->result();
    $listTryoutDashboard = $this->mapel->getTryoutDashboardSiswa($nise,"3")->result();
    // $dataLevelDashboard = $this->mapel->getDatalevelDashboardSiswa($nise)->result();



    $data=array(
      'content'=>'portal/dashboard',
      'menu_right'=>'portal/menu_right',
      'module'=>'portal',
      // 'listMatkulDashboard'=>$listMatkulDashboard,
      'listTryoutDashboard'=>$listTryoutDashboard,
      // 'dataLevelDashboard'=>$dataLevelDashboard,
      'css'=>'portal/css',
      'js'=>'portal/js'
    );
    // $rank = getdata('temporary_table',array('kode'=>'top_nasional'))->row();
    // if($rank->deskripsi!=NULL){
    //   $data['rank']=json_decode($rank->deskripsi);
    // }else{
    //   $data['rank']=NULL;
    // }
    // $rank_today = getdata('temporary_table',array('kode'=>'top_nasional_now'))->row();
    // if($rank_today->deskripsi!=NULL){
    //   $data['rank_today']=json_decode($rank_today->deskripsi);
    // }else{
    //   $data['rank_today']=NULL;
    // }
    // $currentRank = $this->Dbs->getPointRankIndividu($this->session->userdata('id'))->row();
    // $data['currentRank']=$currentRank;
    // //Init Radar
    // $radarMapel = $this->Dbs->radarAllMapel($nise)->result();
    // $mapel=[];
    // $total_point=[];
    // if(sizeof($radarMapel)!=0){
    //   foreach ($radarMapel as $key) {
    //     array_push($mapel,$key->nama);
    //     array_push($total_point,$key->total_point);
    //   }
    //   $data['mapelRadar']=json_encode($mapel);
    //   $data['totalPointRadar']=json_encode($total_point);
    // }else{
    //   $data['mapelRadar']='NULL';
    //   $data['totalPointRadar']='NULL';
    // }
    $this->template->load_front($data);
  }

}
