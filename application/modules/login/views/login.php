<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title> SMPI Al-Ghozali</title>
    <link rel="icon" type="image/png" href="<?= base_url()?>xfile/logo.png" />
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
    <link href="<?= base_url()?>assets-landing/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="<?= base_url()?>assets-landing/css/webfont.css" rel="stylesheet">
    <!-- Core CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets-landing/css/bulma.css">
    <link rel="stylesheet" href="<?= base_url()?>assets-landing/css/app.css">
    <link rel="stylesheet" href="<?= base_url()?>assets-landing/css/core.css">
    <?php if($this->session->flashdata('flashMessage')) {
      $flashMessage=$this->session->flashdata('flashMessage');
    echo "<script>alert('$flashMessage')</script>";
     } ?>
</head>

<body class="is-white">
  <div class="pageloader"></div>
      <div class="infraloader is-active"></div>
      <div class="login-wrapper">

          <!-- Main Wrapper -->
          <div class="login-wrapper columns is-gapless">
              <!--Left Side (Desktop Only)-->
              <div class="column is-6 is-hidden-mobile hero-banner">
                  <div class="hero is-fullheight is-login">
                      <div class="hero-body">
                          <div class="container">
                              <div class="left-caption">
                                  <h2>SMP - AGZ</h2>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <!--Right Side-->
              <div class="column is-6">
                  <div class="hero form-hero is-fullheight">
                      <!--Logo-->
                      <div class="logo-wrap">
                          <div class="wrap-inner">
                              <img src="#" alt="">
                          </div>
                      </div>
                      <!--Login Form-->
                      <div class="hero-body">
                          <div class="form-wrapper">
                              <!--Avatar-->
                              <div class="avatar">
                                  <div class="badge">
                                      <i data-feather="check"></i>
                                  </div>
                                  <img src="<?= base_url() ?>assets-landing/bg-01.svg" data-demo-src="<?= base_url() ?>assets-landing/bg-01.svg" alt="">
                              </div>
                              <!--Form-->
                              <form  action="login/auth" method="post">
                              <div class="login-form">
                                  <div class="field">
                                      <div class="control">
                                          <input class="input" name="username" type="text" placeholder="NISE">
                                          <div class="input-icon">
                                              <i data-feather="user"></i>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="field">
                                      <div class="control">
                                          <input class="input password-input" name="password" type="password" placeholder="●●●●●●●">
                                          <input type="hidden" name="ref" value="<?= $this->input->get('ref'); ?>">
                                          <div class="input-icon">
                                              <i data-feather="lock"></i>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="field">
                                      <div class="control">
                                          <input type="submit" class="button is-solid primary-button raised is-rounded is-fullwidth" value="Masuk">
                                      </div>
                                  </div>
                              </div>
                            </form>

                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

    <!-- Concatenated js plugins and jQuery -->
    <script src="<?= base_url()?>assets-landing/js/app.js"></script>
    <script src="<?= base_url()?>assets-landing/js/particles.min.js"></script>

    <!-- Core js -->
    <script src="<?= base_url()?>assets-landing/data/tipuedrop_content.js"></script>
    <script src="<?= base_url()?>assets-landing/js/global.js"></script>
    <script src="<?= base_url()?>assets-landing/js/main.js"></script>

    <!-- Page and UI related js -->
    <script src="<?= base_url()?>assets-landing/js/landing.js"></script>
    <script src="<?= base_url()?>assets-landing/js/tour.js"></script>
</body>

</html>
