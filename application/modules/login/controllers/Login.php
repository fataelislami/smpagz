<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    $this->load->helper(array('dbs'));
  }

  function index()
  {
    $ref=$this->input->get('ref');
    if($this->session->userdata('status')=='login'){
      if($ref=='tryout'){
        redirect(base_url('tryout/regist'));
      }else{
        redirect(base_url('portal/dashboard'));
      }
    }
    $this->load->view('login');
  }

  function auth(){//Proses pengecekan login
    $username = $this->input->post('username');
		$password = $this->input->post('password');
    $ref=$this->input->post('ref');

		$where_admin = array(
			'username' => $username,
			'password' => sha1($password)
			);
    $where_guru = array(
  		'nik' => $username,
  		'password' => sha1($password)
  		);
    $where_siswa = array(
			'nis' => $username,
			'password' => sha1($password)
			);
    $where_ortu = array(
			'username' => $username,
			'password' => sha1($password)
			);

		$result_admin = $this->Dbs->check("admin",$where_admin);
    $result_guru  = $this->Dbs->check("guru",$where_guru);
    $result_siswa = $this->Dbs->check("siswa",$where_siswa);
    $result_ortu  = $this->Dbs->check("orang_tua",$where_ortu);

		if ($username == 'superadmin' && $password == 'bismillah') {
      $session = array(//Data yang akan disimpan kedalam session
        'status' => "login",
        'level'=>'superadmin'
        );
      $this->session->set_userdata($session);
      redirect(base_url('superadmin'));
    }else if($result_admin->num_rows() > 0){
      $getData = $result_admin->row();
      $session = array(//Data yang akan disimpan kedalam session
        'id'=>$getData->id,
        'username' => $getData->username,
        'nama'=>$getData->nama,
        'email'=>$email,
        'status' => "login",
        'level'=>'admin'
        );
        $this->session->set_flashdata('login_true', 'hello');
      $this->session->set_userdata($session);
      redirect(base_url('admin'));
		}else if($result_guru->num_rows() > 0){
      $getData = $result_guru->row();
      $session = array(//Data yang akan disimpan kedalam session
        'id'=>$getData->nik,
        'nik' => $getData->nik,
        'nama'=>$getData->nama,
        'email'=>$email,
        'status' => "login",
        'level'=>'guru'
        );
      $this->session->set_flashdata('login_true', 'hello');
      $this->session->set_userdata($session);
      redirect(base_url('guru'));
		}else if ($result_siswa->num_rows() > 0) {
      $getData = $result_siswa->row();
      $session = array(//Data yang akan disimpan kedalam session
        'id' => $getData->nis,
        'nis' => $getData->nis,
        'nama'=>$getData->nama,
        'status' => "login",
        'level'=>'siswa',
        );
      $this->session->set_userdata($session);
      if($ref=='tryout'){
        redirect(base_url('tryout/regist'));
      }else{
        redirect(base_url('portal/dashboard'));
      }
    }else if ($result_ortu->num_rows() > 0) {
      $getData = $result_ortu->row();
      $session = array(//Data yang akan disimpan kedalam session
        'id' => $getData->id,
        'username' => $getData->username,
        'nama'=>$getData->nama_ayah,
        'status' => "login",
        'level'=>'ortu',
        );
      $this->session->set_flashdata('login_true', 'hello');
      $this->session->set_userdata($session);
      redirect(base_url('ortu'));
    }else{
      $this->session->set_flashdata('flashMessage', 'Username atau Password Salah');
      redirect(base_url('login'));
		}
  }




  function randomPassword($length = 3) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  public function email($subject,$isi,$emailtujuan){
    $config['protocol'] = 'smtp';
    $config['smtp_host'] = 'ssl://smtp.gmail.com';
    $config['smtp_port'] = '465';
    $config['smtp_user'] = 'shopagansta@gmail.com';
    $config['smtp_pass'] = 'faztars123'; //ini pake akun pass google email
    $config['mailtype'] = 'html';
    $config['charset'] = 'iso-8859-1';
    $config['wordwrap'] = 'TRUE';
    $config['newline'] = "\r\n";

    $this->load->library('email', $config);
    $this->email->initialize($config);

    $this->email->from('shopagansta@gmail.com');
    $this->email->to($emailtujuan);
    $this->email->subject($subject);
    $this->email->message($isi);
    $this->email->set_mailtype('html');
    $this->email->send();
  }

  function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}



}
