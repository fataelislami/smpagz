<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbs extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function check($table,$where){
		return $this->db->get_where($table,$where);
	}

  function check_to($id){
		$this->db->select('utbk');
    $this->db->from('tryout');
    $this->db->join('event', 'tryout.id_event = event.id', 'left');
    $this->db->where('tryout.id', $id);
    return $this->db->get();
	}

  function getdata($from,$where=null,$limit=9,$offset=0){
    if($where!=null){
      $this->db->where($where);
    }
    $this->db->limit($limit, $offset);
    $db=$this->db->get($from);
    return $db;
  }

  function insert($data,$table){
   $insert = $this->db->insert($table, $data);
   if ($this->db->affected_rows()>0) {
     return true;
     }else{
     return false;
     }
 }
 function CheckData($field,$value,$table){
   $this->db->select('*');
   $this->db->where($field, $value);
   $result = $this->db->get($table,1,0)->num_rows();
   if ($result > 0) {
     return true;
   }else {
     return false;
   }
 }
 function getScoring($id_to){
   $this->db->select(' tb1.id, tb1.jawaban as jawaban_siswa');
   $this->db->select('list_soal.jawaban as kunci_jawaban');
   $this->db->from('tryout');
   $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
   $this->db->join("(SELECT * FROM jawaban_tryout WHERE jawaban_tryout.id_try_out = $id_to) AS tb1", 'list_soal.kode = tb1.id_list_soal', 'left');
   $this->db->where('tryout.id', $id_to);
   return $this->db->get();
 }
 function update($data,$table,$where){
    $this->db->where($where);
    $db=$this->db->update($table,$data);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
  }
    function getSoalByTo($id_try_out,$kodematpel){
      // $this->db->cache_on();
    $this->db->select("list_soal.kode,list_soal.nomor,list_soal.image,list_soal.soal,list_soal.tipe_pil,list_soal.pil_a,list_soal.pil_b,list_soal.pil_c,list_soal.pil_d,list_soal.pil_e,list_soal.jawaban,list_soal.mapel,list_soal.kode_soal");
    // $this->db->select("mata_pelajaran.nama as nama_mapel");
    $this->db->select("tryout.*");
    $this->db->select("event.durasi as durasi");
    $this->db->select('tb1.jawaban as jawaban_siswa');
    $this->db->from('tryout');
    $this->db->join('event','event.id=tryout.id_event');
    $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
    // $this->db->join('mata_pelajaran', 'list_soal.mapel = mata_pelajaran.id', 'left');
    $this->db->join("(SELECT * FROM jawaban_tryout WHERE jawaban_tryout.id_try_out = $id_try_out) AS tb1", 'list_soal.kode = tb1.id_list_soal', 'left');
    if($kodematpel!=null){
      $this->db->where('list_soal.mapel', $kodematpel);
    }
    $this->db->where('tryout.id', $id_try_out);
    return $this->db->get();

  }

  function getJumlahSoal($id_try_out){
    $this->db->cache_on();
  $this->db->select("list_soal.nomor");
  // $this->db->select("mata_pelajaran.nama as nama_mapel");
  $this->db->from('tryout');
  $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
  // $this->db->join('mata_pelajaran', 'list_soal.mapel = mata_pelajaran.id', 'left');
  $this->db->where('tryout.id', $id_try_out);
  return $this->db->get();

}

  function getUtbkData($id_try_out){
  $this->db->select("mata_pelajaran.nama as nama_mapel,mata_pelajaran.durasi as durasi_mapel,mata_pelajaran.id");
  $this->db->select("list_soal.nomor");
  $this->db->select("tryout.waktu_mulai,tryout.waktu_selesai,tryout.id_event");
  $this->db->select("event.durasi as durasi");
  $this->db->from('tryout');
  $this->db->join('event','event.id=tryout.id_event');
  $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
  $this->db->join('mata_pelajaran', 'list_soal.mapel = mata_pelajaran.id', 'left');
  $this->db->group_by('list_soal.mapel');
  $this->db->where('tryout.id', $id_try_out);
  $this->db->order_by('list_soal.nomor', 'asc');


  return $this->db->get();

}

  function getsoalto($where){
    $this->db->select("soal_to.kode as kode");
    $this->db->from("soal_to");
    $this->db->join("list_soal","list_soal.kode_soal=soal_to.kode");
    $this->db->where("list_soal.mapel!=",NULL);
    $this->db->group_by("soal_to.kode");
    $this->db->where($where);
    return $this->db->get();
  }
}
