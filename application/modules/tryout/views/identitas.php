<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Try Out Online by SMPAGZ.">
    <meta name="author" content="Ansonika">
    <title>SMPAGZ | Isi Identitas Try Out</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?= base_url()."assets-front/"?>css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url()."assets-front/"?>css/menu.css" rel="stylesheet">
    <link href="<?= base_url()."assets-front/"?>css/style.css" rel="stylesheet">
  <link href="<?= base_url()."assets-front/"?>css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?= base_url()."assets-front/"?>css/custom.css" rel="stylesheet">

  <!-- MODERNIZR MENU -->
  <script src="<?= base_url()."assets-front/"?>js/modernizr.js"></script>
 <style>
 body{
   background-image:url(<?php echo base_url()?>xfile/background/bg-identitas.png);
   background-repeat: no-repeat;  background-size: cover;
 }
 .logo-identitas{
   background-image:url(<?php echo base_url()?>xfile/background/identitas-logo.png);
   background-repeat: no-repeat;  background-size:contain, cover;
   padding: 200px;
   left:140%;
   top:40px;
   position: absolute;
 }
 </style>
</head>

<body oncontextmenu="return false;">

  <div id="preloader">
    <div data-loader="circle-side"></div>
  </div><!-- /Preload -->

  <div id="loader_form">
    <div data-loader="circle-side-2"></div>
  </div><!-- /loader_form -->

  <nav>
    <ul class="cd-primary-nav">
      <li><a href="<?= base_url()?>" class="animated_link">Home</a></li>
    </ul>
  </nav>
  <!-- /menu -->

  <div class="container-fluid full-height">
    <div class="row row-height">
      <!-- /content-left -->

      <div class="col-lg-12 content-right" id="start">
        <div id="wizard_container">
          <div id="top-wizard">
              <div id="progressbar"></div>
            </div>
            <!-- /top-wizard -->
            <form id="wrapped" method="POST">
              <input id="website" name="website" type="text" value="">
              <!-- Leave for security protection, read docs for details -->
              <div id="middle-wizard">
                  <div class="step col-md-6">
                    <div class="logo-identitas">
                    </div>
                    <h3 class="main_question"><strong>1/3</strong>Identitas Diri</h3>
                    <div class="form-group">
                      <input type="text" name="nise" class="form-control required" id="nise" placeholder="NISE" onchange="getVals(this, 'NISE');" value="<?= $nise; ?>" readonly>
                    </div>
                    <div class="form-group">
                      <input type="text" name="nama" class="form-control required" id="nama" placeholder="Nama" value="<?= $nama; ?>" readonly>
                    </div>
                    <!-- <div class="form-group" id="form-password">
                      <input type="password" name="password" class="form-control required" id="password" placeholder="Password" onchange="getVals(this, 'PASSWORD');" required>
                    </div> -->
                    <div id="pass-info" class="clearfix"></div>
                    <div id="jurusan">
                      <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                            <div class="styled-select clearfix">
                              <select class="wide" id="pil_univ_1" name="pil_univ_1" onchange="getVals(this, 'pil_univ_1');">

                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group">
                            <div class="styled-select clearfix">
                              <select class="wide" id="pil_1" name="pil_1" onchange="getVals(this, 'pil_1');">

                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                            <div class="styled-select clearfix">
                              <select class="wide" id="pil_univ_2" name="pil_univ_2" onchange="getVals(this, 'pil_univ_2');">

                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group">
                            <div class="styled-select clearfix">
                              <select class="wide" id="pil_2" name="pil_2" onchange="getVals(this, 'pil_2');">

                              </select>
                            </div>
                          </div>
                        </div>
                      </div>



                    </div>

                    <div id="select_event" style="display:none">
                      <div class="form-group">
                        <div class="styled-select clearfix">
                          <select class="wide required event_option" name="event">
                          </select>
                        </div>
                      </div>
                    </div>
                    <div id="event_false" style="display:none">
                      <div class="form-group">
                        <input id="event_false" type="text" class="form-control" value="Belum Ada Event Terdaftar" readonly>
                      </div>
                    </div>
                    <div class="form-group" id="form-password-event">
                      <input type="password" name="password_event" class="form-control required" id="password_event" placeholder="Password Event" onchange="getVals(this, 'PASSWORD_EVENT');" required>
                    </div>
                    <div id="pass-info-event" class="clearfix"></div>


                  </div>

                <!-- /step-->
                <div class="step">
                  <h3 class="main_question"><strong>2/3</strong>Petunjuk Umum</h3>
                  <p><span style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;">1. Bacalah dengan cermat aturan dan tata cara menjawab setiap tipe soal!</span><br style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;"><span style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;">2. Selama ujian berlangsung, Anda tidak diperkenankan menggunakan alat hitung dalam segala bentuk.</span><br style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;"><span style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;">3. Selama ujian berlangsung, Anda tidak diperkenankan menggunakan alat komunikasi.</span><br style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;"><span style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;">4. Selama ujian berlangsung, Anda tidak diperkenankan untuk bertanya atau meminta penjelasan kepada siapapun tentang soal-soal ujian, termasuk kepada pengawas ujian.</span><br style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;"><span style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;">5. Selama ujian berlangsung, Anda tidak diperkenankan keluar masuk ruang ujian</span><br style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;"><span style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;">6. Setelah ujian selesai. Anda diminta tetap duduk sampai mendapat isyarat dari pengawas untuk meninggalkan ruang.</span><br style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;"><span style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;">7. Jawaban yang salah dan kosong diberi skor 0, sedangkan jawaban benar mendapat skor 1.</span><br style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;"><span style="color: rgb(32, 33, 36); font-family: Roboto, Arial, sans-serif; font-variant-ligatures: none; letter-spacing: 0.1px; white-space: pre-wrap;">8. Untuk keperluan coret-mencoret pergunakanlah 1 lembar A4 yang disediakan oleh pengawas.</span><br></p>
                  <div class="form-group terms">
                    <label class="container_check">Saya Mengerti
                      <input type="checkbox" name="terms" value="Yes" class="required">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
                <!-- /step-->
                <div class="submit step">
                  <h3 class="main_question"><strong>3/3</strong>Pastikan Berdo'a sebelum memulai</h3>
                  Bismillah..

                  <div class="form-group terms">
                    <label class="container_check">Saya Mengerti
                      <input type="checkbox" name="terms2" value="Yes" class="required">
                      <span class="checkmark"></span>
                    </label>
                  </div>
                </div>
              </div>
              <!-- /middle-wizard -->
              <div id="bottom-wizard">
                <button type="button" name="backward" class="backward">Sebelumnya</button>
                <button type="button" name="forward" class="forward">Selanjutnya</button>
                <button type="submit" name="process" class="submit">Mulai</button>
              </div>
              <!-- /bottom-wizard -->
            </form>
          </div>
          <!-- /Wizard container -->
      </div>
      <!-- /content-right-->
    </div>
    <!-- /row-->
  </div>
  <!-- /container-fluid -->

  <div class="cd-overlay-nav">
    <span></span>
  </div>
  <!-- /cd-overlay-nav -->

  <div class="cd-overlay-content">
    <span></span>
  </div>
  <!-- /cd-overlay-content -->

  <a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>
  <!-- /menu button -->


  <!-- COMMON SCRIPTS -->
  <script src="<?= base_url()."assets-front/"?>js/jquery-3.2.1.min.js"></script>
    <script src="<?= base_url()."assets-front/"?>js/common_scripts.min.js"></script>
  <script src="<?= base_url()."assets-front/"?>js/velocity.min.js"></script>
  <script src="<?= base_url()."assets-front/"?>js/functions.js"></script>
  <script src="<?= base_url()."assets-front/"?>js/pw_strenght.js"></script>
  <script type="text/javascript">
  document.addEventListener("contextmenu", function(e){
    e.preventDefault();
}, false);
document.onkeydown = function(e) {
  if(event.keyCode == 123) {
     return false;
  }
  if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
     return false;
  }
  if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
     return false;
  }
  if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
     return false;
  }
  if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
     return false;
  }
}
  $("#pil_1").attr('class','wide');
  $("#pil_2").attr('class','wide');
  $("#jurusan").hide();
  var base_url="<?= base_url()?>";
  var nise="<?= $nise; ?>";
  </script>
  <!-- Wizard script -->
  <script src="<?= base_url()."assets-front/"?>js/registration_func.js"></script>

</body>
</html>
