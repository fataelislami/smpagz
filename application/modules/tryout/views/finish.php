<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Try Out Online by Edulab">
    <meta name="author" content="Ansonika">
    <title>Edulab | Try Out Finish</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?= base_url()."assets-front/"?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url()."assets-front/"?>css/menu.css" rel="stylesheet">
    <link href="<?= base_url()."assets-front/"?>css/style.css" rel="stylesheet">
	<link href="<?= base_url()."assets-front/"?>css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?= base_url()."assets-front/"?>css/custom.css" rel="stylesheet">

	<!-- MODERNIZR MENU -->
	<script src="<?= base_url()."assets-front/"?>js/modernizr.js"></script>
 <style>
 body{
   background-image:url(<?php echo base_url()?>xfile/background/bg-identitas.png);
   background-repeat: no-repeat;  background-size: cover;
 }
 .logo-identitas{
   background-image:url(<?php echo base_url()?>xfile/background/identitas-logo.png);
   background-repeat: no-repeat;  background-size:contain, cover;
   padding: 200px;
   left:140%;
   top:40px;
   position: absolute;
 }
 </style>
</head>

<body>
  <div id="success">
      <div class="icon icon--order-success svg">
           <svg xmlns="http://www.w3.org/2000/svg" width="100px" height="100px">
            <g fill="none" stroke="#8EC343" stroke-width="2">
               <circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
               <path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
            </g>
           </svg>
       </div>
  	<h4><span>Try Out Selesai! <br></span></h4>
  	<small>Terimakasih Telah Mengerjakan Try Out Dengan Jujur</small>
    <br>
    <br>
    <!-- <a href="<?php echo base_url() ?>"><button type="button" name="forward" class="forward">HOME</button></a> -->
    <a href="<?php echo base_url() ?>portal/dashboard"><button type="button" name="forward" class="forward">DASHBOARD</button></a>


  </div>
</body>
</html>
