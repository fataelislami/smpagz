<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Try Out Online by Edulab">
    <meta name="author" content="Edulab">
    <title>Edulab | Try Out</title>

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?= base_url()."assets-front/"?>css/bootstrap.min.css" rel="stylesheet">
	  <link href="<?= base_url()."assets-front/"?>css/menu.css" rel="stylesheet">
    <link href="<?= base_url()."assets-front/"?>css/style.css" rel="stylesheet">
	  <!-- <link href="<?= base_url()."assets-front/"?>css/vendors.css" rel="stylesheet"> -->
    <!-- YOUR CUSTOM CSS -->
    <link href="<?= base_url()."assets-front/"?>css/custom.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url()?>assets/plugins/html5-editor/bootstrap-wysihtml5.css" />

	<!-- MODERNIZR MENU -->
	<script src="<?= base_url()."assets-front/"?>js/modernizr.js"></script>
  <script src="<?= base_url()."assets-front/"?>js/jquery-3.2.1.min.js"></script>
  <style>
  body{
    background-image:url(<?php echo base_url()?>xfile/background/bg-soal.png);
    background-repeat: no-repeat;  background-size: cover;
  }
  .content-right{
display:inline;
  }
  @media (max-width: 991px) {
    .content-right {
      display:flex;
    }
.content-left-wrapper figure img {
    height: 15%;
    width: auto;
}
  }
  #snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #333;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    left: 50%;
    bottom: 30px;
    font-size: 17px;
  }

  #snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
  }

  @-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
  }

  @-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }

  @keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
  }
  </style>
</head>

<body>

	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div><!-- /Preload -->

	<div id="loader_form">
		<div data-loader="circle-side-2"></div>
	</div><!-- /loader_form -->

	<nav>
		<ul class="cd-primary-nav">
			<li><a href="<?php echo base_url() ?>" class="animated_link">Home</a></li>
		</ul>
	</nav>
	<!-- /menu -->

	<div class="container-fluid full-height">
		<div class="row row-height">
			<div class="col-lg-8 content-right" id="start">
				<div id="wizard_container">
					<div id="top-wizard">
							<div id="progressbar"></div>
						</div>
						<!-- /top-wizard -->
						<form id="wrapped" method="POST">
							<input id="website" name="website" type="text" value="">
							<!-- Leave for security protection, read docs for details -->
							<div id="middle-wizard">
                <?php $i=1; ?>
                <?php foreach ($soal->results as $oSoal): ?>
                  <div class="step">
                    <div class="" style="">
                      <h3 class="main_question"><strong>Soal No <?= $oSoal->nomor; ?></strong></h3>
                        <?php if ($oSoal->image != ""): ?>
                          <img style="max-height:200px" src="<?php echo base_url()."xfile/images/soal/$oSoal->image"; ?>" alt="">
                        <?php endif; ?>
                      <?= $oSoal->soal; ?>
                    </div>
                    <br>
                    <?php if ($oSoal->tipe_pil=='text'){ ?>
                        <?php
                          $pil_a=$oSoal->pil_a;
                          $pil_b=$oSoal->pil_b;
                          $pil_c=$oSoal->pil_c;
                          $pil_d=$oSoal->pil_d;
                          $pil_e=$oSoal->pil_e;
                         ?>
                    <?php }else{ ?>

                          <?php
                          $base_url=base_url().'xfile/images/soal';
                          $pil_a="<img style='max-height:150px' src='$base_url/$oSoal->pil_a' alt=''>";
                          $pil_b="<img style='max-height:150px' src='$base_url/$oSoal->pil_b' alt=''>";
                          $pil_c="<img style='max-height:150px' src='$base_url/$oSoal->pil_c' alt=''>";
                          $pil_d="<img style='max-height:150px' src='$base_url/$oSoal->pil_d' alt=''>";
                          $pil_e="<img style='max-height:150px' src='$base_url/$oSoal->pil_e' alt=''>";
                           ?>
                    <?php } ?>
                    <div class="form-group">
                      <label class="container_radio version_2"> <?= $pil_a; ?>
                        <input type="radio" name="<?php echo "jawaban_$i";?>" value="A#<?= $oSoal->kode; ?>#<?= $id_try_out ?>" class="<?php echo "jawaban_$i";?>" onclick="getVals(this, '<?php echo "jawaban_$i" ?>');">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                    <div class="form-group">
                      <label class="container_radio version_2"><?= $pil_b; ?>
                        <input type="radio" name="<?php echo "jawaban_$i";?>" value="B#<?= $oSoal->kode; ?>#<?= $id_try_out ?>" class="<?php echo "jawaban_$i";?>" onclick="getVals(this, '<?php echo "jawaban_$i" ?>');">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                    <div class="form-group">
                      <label class="container_radio version_2"><?= $pil_c; ?>
                        <input type="radio" name="<?php echo "jawaban_$i";?>" value="C#<?= $oSoal->kode; ?>#<?= $id_try_out ?>" class="<?php echo "jawaban_$i";?>" onclick="getVals(this, '<?php echo "jawaban_$i" ?>');">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                    <div class="form-group">
                      <label class="container_radio version_2"><?= $pil_d; ?>
                        <input type="radio" name="<?php echo "jawaban_$i";?>" value="D#<?= $oSoal->kode; ?>#<?= $id_try_out ?>" class="<?php echo "jawaban_$i";?>" onclick="getVals(this, '<?php echo "jawaban_$i" ?>');">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                    <div class="form-group">
                      <label class="container_radio version_2"><?= $pil_e; ?>
                        <input type="radio" name="<?php echo "jawaban_$i";?>" value="E#<?= $oSoal->kode; ?>#<?= $id_try_out ?>" class="<?php echo "jawaban_$i";?>" onclick="getVals(this, '<?php echo "jawaban_$i" ?>');">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                  </div>
                  <script>
                  var $radios = $('input:radio[name=jawaban_<?= $i?>]');
                  // if($radios.is(':checked') === false) {
                    $radios.filter('[value="<?= $oSoal->jawaban_siswa;?>#<?= $oSoal->kode;?>#<?= $id_try_out;?>"]').prop('checked', true);
                 // }
                  </script>
                  <?php $i++; ?>
                <?php endforeach; ?>
                <?php if (($utbk=='false') and $page==$total_page){ ?>
                  <div class="submit step">
                    <h3 class="main_question">Selesai Ujian?</h3>
                    <div class="summary">
                      <h5>Untuk memastikan jawaban telah <b>tersimpan</b>,pastikan semua indikator nomor soal berwarna <b>HIJAU</b>.</h5>
                    </div>
                    <div style="overflow-x:auto;">
                      <table border="1" class="table">
                        <tr class="tdNomor">
                        </tr>
                        <tr class="tdJawaban">
                        </tr>
                      </table>
                    </div>
                    <div class="form-group terms">
                      <label class="container_check">Dengan ini saya menyatakan bahwa data yang diisikan pada formulir ini adalah benar.
                        <input type="checkbox" name="terms3" value="Yes" class="required">
                        <input type="hidden" name="id_try_out" value="<?php echo $id_try_out; ?>">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                  </div>
                <?php }else { ?>
                    <?php if(isset($isLast)){ ?>
                      <div class="submit step">
                        <h3 class="main_question">Selesai Ujian?</h3>
                        <div class="summary">
                          <h5>Untuk memastikan jawaban telah <b>tersimpan</b>,pastikan semua indikator nomor soal berwarna <b>HIJAU</b>.</h5>
                        </div>
                        <div style="overflow-x:auto;">
                          <table border="1" class="table">
                            <tr class="tdNomor">
                            </tr>
                            <tr class="tdJawaban">
                            </tr>
                          </table>
                        </div>
                        <div class="form-group terms">
                          <label class="container_check">Dengan ini saya menyatakan bahwa data yang diisikan pada formulir ini adalah benar.
                            <input type="checkbox" name="terms3" value="Yes" class="required">
                            <input type="hidden" name="id_try_out" value="<?php echo $id_try_out; ?>">
                            <span class="checkmark"></span>
                          </label>
                        </div>
                      </div>
                    <?php }else{ ?>
                      <div class="step">
                        <h3 class="main_question">Silakan cek kembali soal yang sudah dikerjakan</h3>
                        <div class="summary">
                          <h5>Untuk memastikan jawaban telah <b>tersimpan</b>,pastikan semua indikator nomor soal berwarna <b>HIJAU</b>.</h5>
                        </div>
                        <div style="overflow-x:auto;">
                          <table border="1" class="table">
                            <tr class="tdNomor">
                            </tr>
                            <tr class="tdJawaban">
                            </tr>
                          </table>
                        </div>

                      </div>
                    <?php } ?>
                <?php } ?>

								<!-- /step-->
							</div>
							<!-- /middle-wizard -->
              <button type="button" name="forward" class="btn btn-info PilihJawaban">Pilih Jawaban</button>
              <button type="button" name="forward" class="btn btn-danger BatalJawaban" style="display:none">Hapus Jawaban</button>
							<div id="bottom-wizard">

								<button type="button" name="backward" class="backward">Prev</button>
								<button type="button" name="forward" class="forward">Next</button>
								<button type="submit" name="process" class="submit">Submit</button>
							</div>
							<!-- /bottom-wizard -->
						</form>
					</div>
					<!-- /Wizard container -->
			</div>
			<!-- /content-right-->
      <div class="col-lg-4 content-left">
				<div class="content-left-wrapper scrollbar">
					<div style="margin-top:50px">
						<figure><img src="<?php echo base_url()?>xfile/background/soal-logo.png" style="margin-bottom: -20;" alt="" width="100px"></figure>
            <h2><div class="timer" style="color:#f9b019;"></div></h2>
						<p style="color:#f9b019">Edulab Try Out</p>
            <?php if($soal->total_result<=26){ ?>
              <div class="row scrollbar">
            <?php }else{ ?>
              <div class="row scrollbar" style="height:85%">
            <?php } ?>
                <?php $j=1; ?>
                <?php foreach ($soal->results as $oSoal): ?>
                  <div class="col-md-3" style="padding-left: 0;padding-right:0;">
                    <?php if($oSoal->jawaban_siswa!=null){ ?>
                    <a href="#<?php echo $j; ?>" id="btn_sel-<?php echo $j; ?>" class="btn_soal btn_isi" onclick="jumpto(<?php echo $j-1; ?>)"><?php echo $oSoal->nomor; ?></a>
                  <?php }else{ ?>
                    <a href="#<?php echo $j; ?>" id="btn_sel-<?php echo $j; ?>" class="btn_soal" onclick="jumpto(<?php echo $j-1; ?>)"><?php echo $oSoal->nomor; ?></a>
                  <?php } ?>
                  </div>

                  <?php $j++; ?>
                <?php endforeach; ?>
                <?php if($utbk=='false'){ ?>
                  <?php if($page!=1){ ?>
                    <a href="<?php echo $urlPrev ?>" class="btn_soal" style="width:47.5%">PREV</a>
                  <?php } ?>
                  <?php if ($page!=$total_page): ?>
                    <a href="<?php echo $urlNext ?>" class="btn_soal" style="width:47.5%">NEXT</a>
                  <?php endif; ?>
                <?php } ?>
              </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="cd-overlay-nav">
		<span></span>
	</div>
	<div class="cd-overlay-content">
		<span></span>
	</div>
  <div id="snackbar">Jawaban Tersimpan..</div>


	<a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>
	<!-- COMMON SCRIPTS -->
  <script src="<?= base_url()."assets-front/"?>js/moment.min.js"></script>
  <script src="<?= base_url()."assets-front/"?>js/common_scripts.js"></script>
	<script src="<?= base_url()."assets-front/"?>js/velocity.min.js"></script>
  <script src="<?= base_url()."assets-front/"?>js/ez.countimer.js"></script>
	<script src="<?= base_url()."assets-front/"?>js/functions.js"></script>
  <script type="text/javascript">
  $('.timer').countimer({
  // Enable the timer events
  enableEvents: true,

  // Display the milliseconds next to the seconds in the full view
  displayMillis: false,

  // whether to remove the HTML element from the DOM after destroy
  destroyDOMElement: false,

  // Auto start on inti
  autoStart : true,

  // Show hours
  useHours : false,

  // Custom indicator for minutes
  minuteIndicator: '',

  // Custom indicator for seconds
  secondIndicator: '',

  // Separator between each time block
  separator : ':',

  // Number of leading zeros
  leadingZeros: 2,

  // Initial time
  initHours : 0,
  initMinutes : <?php echo $menit ?>,
  initSeconds: <?php echo $detik ?>

}).on('minute', function(evt, time){
  // every second
  var minute=time.displayedMode.unformatted.minutes;
  // var second=time.displayedMode.unformatted.seconds;
  var durasi=<?php echo $durasi ?>;
  console.log(durasi);
  var id_try_out=<?php echo "$id_try_out" ?>;
  var utbk=<?php echo "$utbk" ?>;
  if(minute>=durasi){
    passingdata(id_try_out);
    console.log("Waktu Habis");
  }else{
    if(utbk!='false'){
      var durasi_mapel=<?php echo $durasi_mapel ?>;
      // var durasi_now=minute+'.'+second;
      // console.log(durasi_now);
      // console.log(durasi_mapel);
      console.log(durasi_mapel);
      if(minute>=durasi_mapel){
        window.location.href = "<?php echo base_url()?>tryout/utbk";
      }
    }
  }
});

function passingdata(id_try_out){
  var form = new FormData();
form.append("id_try_out_js", id_try_out);

var settings = {
  "async": true,
  "crossDomain": true,
  "url": "<?php echo base_url();?>tryout/finish",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "771cf5d9-c563-453b-9652-8f74b31a288e"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

$.ajax(settings).done(function (response) {
  window.location.href = "<?php echo base_url()?>tryout/finish";
});
}
  </script>
  <script type="text/javascript">
  function jumpto(no){
    $("#wizard_container").wizard('select',no);
  }
  // $("#q1").click(function(){
  //   $("#wizard_container").wizard('select',0); //SELECT KE STEP BERAPA
  // });
  // $("#q2").click(function(){
  //   $("#wizard_container").wizard('select',1); //SELECT KE STEP BERAPA
  // });
  </script>
  <script type="text/javascript">
  var base_url="<?= base_url()?>";
  var id_to="<?php echo $id_try_out ?>";
  console.log(id_to);

  </script>
	<!-- Wizard script -->
	<script src="<?= base_url()."assets-front/"?>js/survey_func.js"></script>

</body>
</html>
