<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tryout extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    if($this->session->userdata('status')!='login'){
      redirect(base_url('login'));
    }
    if(($this->session->userdata('level')!='users')AND($this->session->userdata('level')!='siswa')){
      $this->session->set_flashdata('message', 'Silahkan Login Terlebih Dahulu');
      redirect(redirect($_SERVER['HTTP_REFERER']));
    }
  }

  function index()
  {
    redirect(base_url());
  }

  function regist(){
    $data['nama']=$this->session->userdata('nama');
    $data['nise']=$this->session->userdata('id');
    $this->load->view('identitas',$data);
  }

  function proses(){
    $nise=$_POST['nise'];
    // $pil_1='NULL';
    // $pil_2='NULL';
    $event=$_POST['event'];
    $loadDb=$this->Dbs->getdata('tryout',array('nis'=>$nise,'id_event'=>$event));
    if($loadDb->num_rows()>0){
      $getData=$loadDb->row();
      $dataSession = array(
        'continue' => 'true',
        'id_try_out'=>$getData->id
      );
      $loadEvent=$this->Dbs->getdata('tryout_event',array('id'=>$event));
      if($loadEvent->num_rows()>0){
        $get=$loadEvent->row();
        $utbk=$get->utbk;
      }
        $dataSession['utbk']='false';
        $this->session->set_userdata($dataSession);
        redirect(base_url('portal/tryout/go'));
      // echo "User Ini Sedang Ikut Try Out dan akan melanjutkan id_tryout = ".$getData->id;
    }else{
      //TIMEZON php
      $tz = 'Asia/Jakarta';
      $timestamp = time();
      $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
      $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
      $waktu_mulai=$dt->format('Y-m-d H:i:s');
      $tanggal=$dt->format('Y-m-d');
      //END TIMEZONE
      //Get Kode soal
      $loadSoal=$this->Dbs->getdata('tryout_event',array('id'=>$event));
      if($loadSoal->num_rows()>0){
        $get=$loadSoal->row();
        $kode_soal=$get->kode_soal;
        $utbk=$get->utbk;
      }
      //Get Kode
      $dataInsert=array(
        'tanggal'=>$tanggal,
        'waktu_mulai'=>$waktu_mulai,
        // 'pil_1'=>$pil_1,
        // 'pil_2'=>$pil_2,
        'nis'=>$nise,
        'id_event'=>$event,
        'kode_soal'=>$kode_soal,
      );
      $sql=$this->Dbs->insert($dataInsert,'tryout');
      if($sql){

        $insert_id = $this->db->insert_id();
        $dataSession = array(
          'id_try_out'=>$insert_id,
          'continue'=>'false'
        );
        //UPDATE
        if($utbk!=1){
          $dataSession['utbk']='false';
          $this->session->set_userdata($dataSession);
          redirect(base_url('portal/tryout/go'));
        }else{
          $dataSession['utbk']='true';
          $this->session->set_userdata($dataSession);
          redirect(base_url('portal/tryout/utbk'));
        }
        //UPDATE 3 APRIL
      }
    }

  }

  function utbk_test(){
    $json=$this->list_soal_utbk('09708');
    $obj=(object)$json;
    $durasi_mapel=0;
    $id_mapel='';
    $menit=7;
    $i=1;
    $numItem=$obj->total_result;
    foreach ($obj->results as $o) {
      $durasi_mapel=$durasi_mapel+$o->durasi_mapel;
      if($menit<$durasi_mapel){
        echo $menit."<br>";
        echo $durasi_mapel;
        if($i==$numItem){
          echo "LAST";
        }
        $id_mapel=$o->id;//CEK ID MAPEL
        $jsonsoal=$this->soal_by_to('09708',$id_mapel);
        $objsoal=(object)$jsonsoal;
        break;
      }
      $i++;
    }
    // var_dump($obj);
  }
  function utbk_stress(){
    // if($this->session->userdata('id_try_out')){
    //   if($this->session->userdata('utbk')!='true'){
    //     redirect(base_url('tryout/go'));
    //     die;
    //   }
    //   $id_try_out=$this->session->userdata('id_try_out');//Dari Session
    //   $check_try_out = $this->Dbs->check("try_out",array('id'=>$id_try_out));
    //   if($check_try_out->num_rows()>0){
    //   }else{
    //     redirect(base_url('tryout/regist'));
    //   }
    // }else{
    //   redirect(base_url('tryout/regist'));//redirect ke halaman .
    //   die;
    // }
    $id_try_out=$_GET['id'];

    //UPDATE 3 APRIL
    $check_try_out = $this->Dbs->check("try_out",array('id'=>$id_try_out,'waktu_selesai!='=>null));
    if($check_try_out->num_rows()>0){
      redirect(base_url('tryout/finish'));//redirect ke halaman yang menyatakan anda sudah selesai mengerjakan TO Sesi Ini.
    }


    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $jamsekarang=$dt->format('Y-m-d H:i:s');
    //END TIMEZONE
      $json=$this->list_soal_utbk($id_try_out);
    //set session continue
    $dataSession = array(
      'continue' => 'true',
      'id_try_out'=>$id_try_out
    );
    $this->session->set_userdata($dataSession);
    //set session continue
    $obj=(object)$json;


    $data=explode(" ",$obj->waktu_mulai);
    //Cek Durasi
    $date_a = new DateTime($obj->waktu_mulai);
    $date_b = new DateTime($jamsekarang);
    $interval = date_diff($date_a,$date_b);
    $durasi=$interval->format('%H.%i.%s');

    // $durasi="15.70";
    if($durasi=='0'){
      $menit='0';
      $detik='0';
    }else{
      $exDurasi=explode(".",$durasi);
      $menit=$exDurasi[1];
      // echo $exDurasi[0];
      if($exDurasi[0]!='0'){
        $menit=($exDurasi[0]*60)+$menit;
      }
      if(!empty($exDurasi[2])){
        $detik=$exDurasi[2];
      }else{
        $detik="0";
      }

    }

    if($menit>=$obj->durasi){//ambil dari database
      $this->session->set_flashdata('id_try_out', $id_try_out);
      redirect(site_url('tryout/finish'));
      // redirect(base_url());//redirect ke halaman yang menyatakan waktu habis.
    }else{
      $durasi_mapel=0;
      $id_mapel='';
      // $menit=25;
      $i=1;
      $numItem=$obj->total_result;
      foreach ($obj->results as $o) {
        $durasi_mapel=$durasi_mapel+$o->durasi_mapel;
        if($menit<$durasi_mapel){
          $id_mapel=$o->id;//CEK ID MAPEL
          $jsonsoal=$this->soal_by_to($id_try_out,$id_mapel);
          $objsoal=(object)$jsonsoal;
          if($i==$numItem){
            $data['isLast']='true';
          }
          break;
        }
        $i++;
      }
    }
    //Cek Durasi
    $data['soal']=$objsoal;
    $data['id_try_out']=$id_try_out;
    $data['menit']=$menit;
    $data['detik']=$detik;
    $data['durasi']=$obj->durasi;
    $data['durasi_mapel']=$durasi_mapel;
    $data['utbk']='true';
    $this->load->view('soal',$data);//coba buat view yang beda
  }
  function utbk(){
    if($this->session->userdata('id_try_out')){
      if($this->session->userdata('utbk')!='true'){
        redirect(base_url('tryout/go'));
        die;
      }
      $id_try_out=$this->session->userdata('id_try_out');//Dari Session
      $check_try_out = $this->Dbs->check("try_out",array('id'=>$id_try_out));
      if($check_try_out->num_rows()>0){
      }else{
        redirect(base_url('tryout/regist'));
      }
    }else{
      redirect(base_url('tryout/regist'));//redirect ke halaman .
      die;
    }
    //UPDATE 3 APRIL
    $check_try_out = $this->Dbs->check("try_out",array('id'=>$id_try_out,'waktu_selesai!='=>null));
    if($check_try_out->num_rows()>0){
      redirect(base_url('tryout/finish'));//redirect ke halaman yang menyatakan anda sudah selesai mengerjakan TO Sesi Ini.
    }

    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $jamsekarang=$dt->format('Y-m-d H:i:s');
    //END TIMEZONE
      $json=$this->list_soal_utbk($id_try_out);
    //set session continue
    $dataSession = array(
      'continue' => 'true',
      'id_try_out'=>$id_try_out
    );
    $this->session->set_userdata($dataSession);
    //set session continue
    $obj=(object)$json;
    $data=explode(" ",$obj->waktu_mulai);
    //Cek Durasi
    $date_a = new DateTime($obj->waktu_mulai);
    $date_b = new DateTime($jamsekarang);
    $interval = date_diff($date_a,$date_b);
    $durasi=$interval->format('%H.%i.%s');

    // $durasi="15.70";
    if($durasi=='0'){
      $menit='0';
      $detik='0';
    }else{
      $exDurasi=explode(".",$durasi);
      $menit=$exDurasi[1];
      // echo $exDurasi[0];
      if($exDurasi[0]!='0'){
        $menit=($exDurasi[0]*60)+$menit;
      }
      if(!empty($exDurasi[2])){
        $detik=$exDurasi[2];
      }else{
        $detik="0";
      }

    }

    if($menit>=$obj->durasi){//ambil dari database
      $this->session->set_flashdata('id_try_out', $id_try_out);
      redirect(site_url('tryout/finish'));
      // redirect(base_url());//redirect ke halaman yang menyatakan waktu habis.
    }else{
      $durasi_mapel=0;
      $id_mapel='';
      // $menit=25;
      $i=1;
      $numItem=$obj->total_result;
      foreach ($obj->results as $o) {
        $durasi_mapel=$durasi_mapel+$o->durasi_mapel;
        if($menit<$durasi_mapel){
          $id_mapel=$o->id;//CEK ID MAPEL
          $jsonsoal=$this->soal_by_to($id_try_out,$id_mapel);
          $objsoal=(object)$jsonsoal;
          if($i==$numItem){
            $data['isLast']='true';
          }
          break;
        }
        $i++;
      }
    }
    //Cek Durasi
    $data['soal']=$objsoal;
    $data['id_try_out']=$id_try_out;
    $data['menit']=$menit;
    $data['detik']=$detik;
    $data['durasi']=$obj->durasi;
    $data['durasi_mapel']=$durasi_mapel;
    $data['utbk']='true';
    $this->load->view('soal',$data);//coba buat view yang beda
  }
  function go(){
    if($this->session->userdata('id_try_out')){
      if($this->session->userdata('utbk')!='false'){
        redirect(base_url('tryout/utbk'));
        die;
      }
      $id_try_out=$this->session->userdata('id_try_out');//Dari Session
      $check_try_out = $this->Dbs->check("try_out",array('id'=>$id_try_out));
      if($check_try_out->num_rows()>0){
      }else{
        redirect(base_url('tryout/regist'));
      }
    }else{
      redirect(base_url('tryout/regist'));//redirect ke halaman .
      die;
    }
		$check_try_out = $this->Dbs->check("try_out",array('id'=>$id_try_out,'waktu_selesai!='=>null));
    if($check_try_out->num_rows()>0){
      redirect(base_url('tryout/finish'));//redirect ke halaman yang menyatakan anda sudah selesai mengerjakan TO Sesi Ini.
    }

    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $jamsekarang=$dt->format('Y-m-d H:i:s');
    //END TIMEZONE
    $jumlah=$this->Dbs->getJumlahSoal($id_try_out)->num_rows();
    $total_page=ceil($jumlah/20);
    // var_dump($total_page);
    // die;
        //StartPagination
    if(isset($_GET['page'])){//cek parameter page
      $page=$_GET['page'];
    }else{
      $page=1;//default jika parameter page tidak diload
    }
    $limitDb=20;
    $offsetDb=0;
    if($page!=1 and $page!=0){
      $offsetDb=$limitDb*($page-1);
    }
    //End Pagination
    if($page<=$total_page and $page>=1){
      //edit ini
      $this->db->limit($limitDb,$offsetDb);
      $nextPage='true';
      $page=$page+1;
      //sama ini
    }else{
      redirect(site_url('tryout/go'));
    }
      $json=$this->soal_by_to($id_try_out);
    //set session continue
    $dataSession = array(
      'continue' => 'true',
      'id_try_out'=>$id_try_out
    );
    $this->session->set_userdata($dataSession);
    //set session continue
    $obj=(object)$json;
    $data=explode(" ",$obj->waktu_mulai);
    //Cek Durasi
    $date_a = new DateTime($obj->waktu_mulai);
    $date_b = new DateTime($jamsekarang);
    $interval = date_diff($date_a,$date_b);
    $durasi=$interval->format('%H.%i.%s');

    // $durasi="15.70";
    if($durasi=='0'){
      $menit='0';
      $detik='0';
    }else{
      $exDurasi=explode(".",$durasi);
      $menit=$exDurasi[1];
      // echo $exDurasi[0];
      if($exDurasi[0]!='0'){
        $menit=($exDurasi[0]*60)+$menit;
      }
      if(!empty($exDurasi[2])){
        $detik=$exDurasi[2];
      }else{
        $detik="0";
      }
    }

    if($menit>=$obj->durasi){//ambil dari database
      $this->session->set_flashdata('id_try_out', $id_try_out);
      redirect(site_url('tryout/finish'));
      // redirect(base_url());//redirect ke halaman yang menyatakan waktu habis.
    }
    //Cek Durasi
    $data['soal']=$obj;
    $data['id_try_out']=$id_try_out;
    $data['menit']=$menit;
    $data['detik']=$detik;
    $data['durasi']=$obj->durasi;
    $data['utbk']='false';
    $data['durasi_mapel']=$obj->durasi;//karena bukan utbk, jadi durasi mapel disamakan
    //tambah ini
    $data['nextPage']=$nextPage;
    $data['urlNext']=base_url().'tryout/go?page='.$page;
    $data['urlPrev']=base_url().'tryout/go?page='.($page-2);
    $data['page']=$page-1;
    $data['total_page']=$total_page;
    //sampe ini
    $this->load->view('soal',$data);
  }
  function revadmin(){

      $id_try_out=$_GET['id'];//Dari Session

    // if($this->session->userdata('id_try_out')){
      // $id_try_out=$this->session->userdata('id_try_out');//Dari Session
      $check_try_out = $this->Dbs->check("try_out",array('id'=>$id_try_out));
      if($check_try_out->num_rows()>0){
      }else{
        redirect(base_url('tryout/regist'));
      }
    // }else{
    //   redirect(base_url('tryout/regist'));//redirect ke halaman .
    //   die;
    // }

    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $jamsekarang=$dt->format('Y-m-d H:i:s');
    //END TIMEZONE

    if($this->session->userdata('continue')=='true'){
      $json=$this->soal_by_to($id_try_out);
    }else{
      $json=$this->soal_by_to($id_try_out);
    }    //set session continue
    $dataSession = array(
      'continue' => 'true',
      'id_try_out'=>$id_try_out
    );
    $this->session->set_userdata($dataSession);
    //set session continue
    $obj=(object)$json;
    $data=explode(" ",$obj->waktu_mulai);
    //Cek Durasi
    $date_a = new DateTime($obj->waktu_mulai);
    $date_b = new DateTime($jamsekarang);
    $interval = date_diff($date_a,$date_b);
    $durasi=$interval->format('%H.%i.%s');
    if($durasi=='0'){
      $menit='0';
      $detik='0';
    }else{
      $exDurasi=explode(".",$durasi);
      $menit=$exDurasi[1];
      // echo $exDurasi[0];
      if($exDurasi[0]!='0'){
        $menit=($exDurasi[0]*60)+$menit;
      }
      if(!empty($exDurasi[2])){
        $detik=$exDurasi[2];
      }else{
        $detik="0";
      }
    }

    //Cek Durasi
    $data['soal']=$obj;
    $data['id_try_out']=$id_try_out;
    $data['menit']=$menit;
    $data['detik']=$detik;
    $data['durasi']=$obj->durasi;
    $this->load->view('revadmin',$data);
  }

  function xdebug(){

      $id_try_out=$_GET['id'];//Dari Session

    // if($this->session->userdata('id_try_out')){
      // $id_try_out=$this->session->userdata('id_try_out');//Dari Session
      $check_try_out = $this->Dbs->check("try_out",array('id'=>$id_try_out));
      if($check_try_out->num_rows()>0){
      }else{
        redirect(base_url('tryout/regist'));
      }
    // }else{
    //   redirect(base_url('tryout/regist'));//redirect ke halaman .
    //   die;
    // }
    $check_try_out = $this->Dbs->check("try_out",array('id'=>$id_try_out,'waktu_selesai!='=>null));
    if($check_try_out->num_rows()>0){
      redirect(base_url('tryout/finish'));//redirect ke halaman yang menyatakan anda sudah selesai mengerjakan TO Sesi Ini.
    }

    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $jamsekarang=$dt->format('Y-m-d H:i:s');
    //END TIMEZONE
    $jumlah=$this->Dbs->getJumlahSoal($id_try_out)->num_rows();
    $total_page=ceil($jumlah/20);
    // var_dump($total_page);
    // die;
        //StartPagination
    if(isset($_GET['page'])){//cek parameter page
      $page=$_GET['page'];
    }else{
      $page=1;//default jika parameter page tidak diload
    }
    $limitDb=20;
    $offsetDb=0;
    if($page!=1 and $page!=0){
      $offsetDb=$limitDb*($page-1);
    }
    //End Pagination
    if($page<=$total_page and $page>=1){
      //edit ini
      $this->db->limit($limitDb,$offsetDb);
      $nextPage='true';
      $page=$page+1;
      //sama ini
    }else{
      redirect(site_url('tryout/xdebug?id=18431#20'));
    }
      $json=$this->soal_by_to($id_try_out);
    $dataSession = array(
      'continue' => 'true',
      'id_try_out'=>$id_try_out
    );
    $this->session->set_userdata($dataSession);
    //set session continue
    $obj=(object)$json;
    $data=explode(" ",$obj->waktu_mulai);
    //Cek Durasi
    $date_a = new DateTime($obj->waktu_mulai);
    $date_b = new DateTime($jamsekarang);
    $interval = date_diff($date_a,$date_b);
    $durasi=$interval->format('%i.%s');
    if($durasi=='0'){
      $menit='0';
      $detik='0';
    }else{
      $exDurasi=explode(".",$durasi);
      $menit=$exDurasi[0];
      if(!empty($exDurasi[1])){
        $detik=$exDurasi[1];
      }else{
        $detik='0';
      }
    }
    if($menit>=$obj->durasi){//ambil dari database
      $this->session->set_flashdata('id_try_out', $id_try_out);
      redirect(site_url('tryout/finish'));
      // redirect(base_url());//redirect ke halaman yang menyatakan waktu habis.
    }
    //Cek Durasi
    $data['soal']=$obj;
    $data['id_try_out']=$id_try_out;
    $data['menit']=$menit;
    $data['detik']=$detik;
    $data['durasi']=$obj->durasi;
    //tambah ini
    $data['nextPage']=$nextPage;
    $data['urlNext']=base_url().'tryout/xdebug?id=18431&page='.$page;
    $data['urlPrev']=base_url().'tryout/xdebug?id=18431&page='.($page-2);
    $data['utbk']='false';
    $data['page']=$page-1;
    $data['total_page']=$total_page;
    //sampe ini
    $this->load->view('soal',$data);
  }

  function finish(){
    if(isset($_POST['id_try_out']) or $this->session->flashdata('id_try_out') or isset($_POST['id_try_out_js'])){
      $this->session->unset_userdata('id_try_out');
      //TIMEZON php
      // require_once 'class/ClientAPI.php';
      // $api=new ClientAPI();
      $tz = 'Asia/Jakarta';
      $timestamp = time();
      $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
      $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
      $waktu_selesai=$dt->format('Y-m-d H:i:s');
      $tanggal=$dt->format('Y-m-d');
      //END TIMEZONE
      if(isset($_POST['id_try_out'])){
        $id_try_out=$_POST['id_try_out'];
        log_message('error', 'C1POST:id_try_out '.$id_try_out);
      }else if(isset($_POST['id_try_out_js'])){
        $id_try_out= sprintf("%05d", $_POST['id_try_out_js']);
        $submit=$_POST['submit'];
        $totalminute=$_POST['totalminute'];
        $durasi=$_POST['durasi'];
        if($submit=='false'){
          if($totalminute>=$durasi){
            log_message('error', 'C1JS:id_try_out_js '.$id_try_out."total menit ".$totalminute." Durasi ".$durasi);
          }else{
            log_message('error', 'C1JSREDIRECT:id_try_out_js '.$id_try_out."total menit ".$totalminute." Durasi ".$durasi);
            redirect(redirect($_SERVER['HTTP_REFERER']));
          }
        }else{
          log_message('error', 'C1JS_SUBMIT:id_try_out_js '.$id_try_out."total menit ".$totalminute." Durasi ".$durasi);
        }
      }
      else if($this->session->flashdata('id_try_out')){
        $id_try_out=$this->session->flashdata('id_try_out');
        log_message('error', 'C1FLASH:id_try_out '.$id_try_out);
      }
      $loadDb=$this->Dbs->getdata('tryout',array('id'=>$id_try_out));
      if($loadDb->num_rows()>0){
        $data_score =(object) $this->scoring($id_try_out);
        $scoring = $data_score->results->score;
        $dataUpdate=array('waktu_selesai'=>$waktu_selesai,'skor'=>$scoring);
        // $dataUpdate=array('waktu_selesai'=>$waktu_selesai,'skor'=>'0');
        $sql=$this->Dbs->update($dataUpdate,'tryout',array('id'=>$id_try_out));
        if($sql){
          $this->load->view('tryout/finish');

        }
      }
    }else{
      $this->session->unset_userdata('id_try_out');
      $this->load->view('tryout/finish');
    }


  }
  function soal_by_to($id_try_out,$kodematpel=null){
      $res=$this->Dbs->getSoalByTo($id_try_out,$kodematpel);
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'waktu_mulai'=>$result[0]->waktu_mulai,
          'waktu_selesai'=>$result[0]->waktu_selesai,
          'id_event'=>$result[0]->id_event,
          'durasi'=>$result[0]->durasi,
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    return $data;
  }

  function list_soal_utbk($id_try_out){
      $res=$this->Dbs->getUtbkData($id_try_out);
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'waktu_mulai'=>$result[0]->waktu_mulai,
          'waktu_selesai'=>$result[0]->waktu_selesai,
          'id_event'=>$result[0]->id_event,
          'durasi'=>$result[0]->durasi,
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    return $data;
  }

  function scoring($id_try_out){
      $point_benar = 1;
      $point_salah = 0;
      $point_kosong = 0;

      $id = $id_try_out;
      $cek = $this->Dbs->CheckData('id',$id,'tryout');

      if ($cek) {
        $res = $this->Dbs->getScoring($id);
        $res_num = $res->num_rows();
        $res = $res->result();

        //perhitungan
        $benar=0; $salah=0; $kosong=0;
        foreach ($res as $r) {
          if ($r->jawaban_siswa == "") {
            $kosong++;
          }elseif ($r->jawaban_siswa == $r->kunci_jawaban) {
            $benar++;
          }else {
            $salah++;
          }
        }

        //kalkulasi scoring
        $total = $benar*$point_benar + $salah*$point_salah + $kosong*$point_kosong;
        $full_benar = $res_num*$point_benar;
        $score = $total * (100/$full_benar);

        $hasil = array(
          'jmlh_soal' => $res_num,
          'benar' => "$benar",
          'salah' => "$salah",
          'kosong' => "$kosong",
          'score' => "$score",
        );

        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>'1',
          'results'=>$hasil,
        );
      }else {
        $data=array(
          'status'=>'success',
          'total_result'=>'0',
          'message'=>'not found'
        );
      }
    return $data;
  }

}
