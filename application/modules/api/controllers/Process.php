<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs','Soal_model'));
  }

  function index()
  {

  }

  function answer(){//untuk answer Try Out
    if(isset($_POST['kode_list_soal']) and isset($_POST['id_try_out']) and isset($_POST['jawaban'])){
      $id_list_soal=$_POST['kode_list_soal'];
      $id_try_out=$_POST['id_try_out'];
      $jawaban=$_POST['jawaban'];
      $dataReplace=array(
        'id_list_soal'=>$id_list_soal,
        'id_try_out'=>$id_try_out,
        'jawaban'=>$jawaban
      );
      $this->db->replace('jawaban_tryout', $dataReplace);
      $insert_id = $this->db->insert_id();
      $data['status']='inserted';
      $data['id']=$insert_id;
    }else{
        $data['status']='missing parameters';
    }

    $json=json_encode($data);
    echo $json;
  }

  // function answer(){//untuk answer Try Out
  //   if(isset($_POST['kode_list_soal']) and isset($_POST['id_try_out']) and isset($_POST['jawaban'])){
  //     $id_list_soal=$_POST['kode_list_soal'];
  //     $id_try_out=$_POST['id_try_out'];
  //     $jawaban=$_POST['jawaban'];
  //     $where=array(
  //       'id_list_soal'=>$id_list_soal,
  //       'id_try_out'=>$id_try_out
  //     );
  //     $loadDb=$this->Dbs->getdata('jawaban_tryout',$where);
  //     if($loadDb->num_rows()>0){
  //       $getData=$loadDb->row();
  //       $dataUpdate=array('jawaban'=>$jawaban);
  //       $sql=$this->Dbs->update($dataUpdate,'jawaban_tryout',array('id'=>$getData->id));
  //       if($sql){
  //         $data['status']='updated';
  //         $data['id']=$getData->id;
  //       }else{
  //         $data['status']='not updated';
  //         $data['id']=$getData->id;
  //       }
  //     }else{
  //       $dataInsert=array(
  //         'id_list_soal'=>$id_list_soal,
  //         'id_try_out'=>$id_try_out,
  //         'jawaban'=>$jawaban
  //       );
  //       $sql=$this->Dbs->insert($dataInsert,'jawaban_tryout');
  //       if($sql){
  //         $insert_id = $this->db->insert_id();
  //         $data['status']='inserted';
  //         $data['id']=$insert_id;
  //       }else{
  //         $data['status']='not inserted';
  //         $data['id']=$insert_id;
  //       }
  //     }
  //   }else{
  //       $data['status']='missing parameters';
  //   }
  //
  //   $json=json_encode($data);
  //   echo $json;
  // }
  function delete_answer(){//untuk answer Try Out
    if(isset($_POST['kode_list_soal']) and isset($_POST['id_try_out']) and isset($_POST['jawaban'])){
      $id_list_soal=$_POST['kode_list_soal'];
      $id_try_out=$_POST['id_try_out'];
      $jawaban=$_POST['jawaban'];
      $where=array(
        'id_list_soal'=>$id_list_soal,
        'id_try_out'=>$id_try_out
      );
      $loadDb=$this->Dbs->getdata('jawaban_tryout',$where);
      if($loadDb->num_rows()>0){
        $getData=$loadDb->row();
        $dataUpdate=array('jawaban'=>$jawaban);
        $sql=$this->Dbs->delete('jawaban_tryout',array('id'=>$getData->id));
          $data['status']='deleted';
          $data['id']=$getData->id;

      }else{
          $data['status']='not found';
          $data['id']='null';
      }
    }else{
        $data['status']='missing parameters';
    }

    $json=json_encode($data);
    echo $json;
  }
  function answer_quiz(){//untuk answer Quiz
    if(isset($_POST['kode_list_soal']) and isset($_POST['id_quiz']) and isset($_POST['jawaban'])){
      $id_list_soal=$_POST['kode_list_soal'];
      $id_quiz=$_POST['id_quiz'];
      $jawaban=$_POST['jawaban'];
      $where=array(
        'id_list_soal'=>$id_list_soal,
        'id_quiz'=>$id_quiz
      );
      $loadDb=$this->Dbs->getdata('list_jawaban_quiz',$where);
      if($loadDb->num_rows()>0){
        $getData=$loadDb->row();
        $dataUpdate=array('jawaban'=>$jawaban);
        $sql=$this->Dbs->update($dataUpdate,'list_jawaban_quiz',array('id'=>$getData->id));
        if($sql){
          $data['status']='updated';
          $data['id']=$getData->id;
        }else{
          $data['status']='not updated';
          $data['id']=$getData->id;
        }
      }else{
        $dataInsert=array(
          'id_list_soal'=>$id_list_soal,
          'id_quiz'=>$id_quiz,
          'jawaban'=>$jawaban
        );
        $sql=$this->Dbs->insert($dataInsert,'list_jawaban_quiz');
        if($sql){
          $insert_id = $this->db->insert_id();
          $data['status']='inserted';
          $data['id']=$insert_id;        }
      }
    }else{
        $data['status']='missing parameters';
    }

    $json=json_encode($data);
    echo $json;
  }

  function delete_answer_quiz(){//untuk answer Try Out
    if(isset($_POST['kode_list_soal']) and isset($_POST['id_quiz']) and isset($_POST['jawaban'])){
      $id_list_soal=$_POST['kode_list_soal'];
      $id_quiz=$_POST['id_quiz'];
      $jawaban=$_POST['jawaban'];
      $where=array(
        'id_list_soal'=>$id_list_soal,
        'id_quiz'=>$id_quiz
      );
      $loadDb=$this->Dbs->getdata('list_jawaban_quiz',$where);
      if($loadDb->num_rows()>0){
        $getData=$loadDb->row();
        $dataUpdate=array('jawaban'=>$jawaban);
        $sql=$this->Dbs->delete('list_jawaban_quiz',array('id'=>$getData->id));
          $data['status']='deleted';
          $data['id']=$getData->id;

      }else{
          $data['status']='not found';
          $data['id']='null';
      }
    }else{
        $data['status']='missing parameters';
    }

    $json=json_encode($data);
    echo $json;
  }

  function answer_soal(){//untuk answer Try Out
      $levelUser=$this->input->post('level');
      $nise_id=$this->input->post('nise_id');//Bisa nise bisa id tergantung level
      $kode_soal=$this->input->post('kode_soal');
      $jawaban=$this->input->post('jawaban');
      $point_soal=$this->input->post('point_soal');
      $skip=$this->input->post('skip');
      $jurusan='';
      //TIMEZON php
      $tz = 'Asia/Jakarta';
      $timestamp = time();
      $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
      $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
      $waktu_selesai=$dt->format('Y-m-d H:i:s');
      //END TIMEZONE
      if($levelUser!=''){//Jika session level tidak null (sudah login)
        if($levelUser=='users'){
          $where=array(
            'kode_soal'=>$kode_soal,
            'user_id'=>$nise_id
          );
          $table='list_jawaban_soal_users';
        }else if($levelUser=='siswa'){
          $where=array(
            'kode_soal'=>$kode_soal,
            'nise'=>$nise_id
          );
          $table='list_jawaban_soal';
        }

        if($skip==''){//Jika form skip bernilai kosong
          $this->db->join('mata_pelajaran', 'mata_pelajaran.id = soal.mapel', 'left');
          $loadSoal=$this->Dbs->getdata('soal',array('kode'=>$kode_soal));//memanggil data soal dan mengambil data jawaban untuk dicocokan
          if($loadSoal->num_rows()==0){
            $data['status']='Terjadi error,silakan hubungi administrator';
            $data['status_code']=400;
            $json=json_encode($data);
            echo $json;die;
          }else{
            $loadSoal=$loadSoal->row();
            $jurusan=$loadSoal->jurusan;
          }
          $jawaban_soal=$loadSoal->jawaban;//menyimpan jawaban benar dari soal
          if($jawaban_soal==$jawaban){//mengecek kebenaran jawaban
            $point_soal=$point_soal;
            $message="Good Job!";
            $correct=1;
          }else{
            $point_soal=0;
            $message="Ayo, coba lagi!";
            $correct=0;
          }
          $dataUpdate=array(
            'jawaban'=>$jawaban,
            'waktu_selesai'=>$waktu_selesai,
            'point_soal'=>$point_soal
          );
        }else{
          $message=NULL;
          $correct=NULL;
          $loadData=$this->Dbs->getdata($table,$where);
          if($loadData->num_rows()>0){
            $jumlahSkip=$loadData->row()->jumlah_skip;
            $dataUpdate=array(
              'jumlah_skip'=>$jumlahSkip+1,
              'waktu_selesai'=>$waktu_selesai,
            );
          }

        }


        $sql=$this->Dbs->update($dataUpdate,$table,$where);
        if($sql){
          if($skip==''){
            if($jurusan!='' or $jurusan!=NULL){
              if($jurusan=='IPA'){
                $totalPoint=$this->Soal_model->getTotalPoint($nise_id,'IPA')->row();
                $updatePoint['total_point_ipa']=$totalPoint->total_point;
              }else if($jurusan=='IPS'){
                $totalPoint=$this->Soal_model->getTotalPoint($nise_id,'IPS')->row();
                $updatePoint['total_point_ips']=$totalPoint->total_point;
              }else if($jurusan=='IPC'){
                $totalPoint=$this->Soal_model->getTotalPoint($nise_id,'IPC')->row();
                $updatePoint['total_point_tpa']=$totalPoint->total_point;
              }
              $updateSiswa=$this->Dbs->update($updatePoint,'siswa',array('nise'=>$nise_id));
            }
          }

          $data['status']='updated';
          $data['message']=$message;
          $data['correct']=$correct;
          $data['status_code']=200;
        }else{
          $data['status']='not updated';
          $data['message']=$message;
          $data['correct']=$correct;
          $data['status_code']=200;
        }

      }else{
        $data['status']='access denied';
        $data['status_code']=400;
      }

      $json=json_encode($data);
      echo $json;

  }


}
