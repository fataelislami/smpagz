<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temporary_table extends MY_Controller{
  //  function getdata($from,$where=null,$limit=9,$offset=0){
  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Temporary_model'));
    header('Content-Type: application/json');
  }

  //fungsi untuk mengupdate rank siswa nasional
  function update_top_nasional(){
    //mendapatkan dara rangking
    $listRank = $this->Temporary_model->getPointRank('10')->result(); //berdasarkan jumlah point (ipa+ips+tpa)
    // $listRank = $this->Temporary_model->getPointRank('10','IPA')->result(); //berdasarkan jumlah point ipa
    // $listRank = $this->Temporary_model->getPointRank('10','IPS')->result(); //berdasarkan jumlah point ips
    // $listRank = $this->Temporary_model->getPointRank('10','TPA')->result(); //berdasarkan jumlah point tpa

    //mendapatkan tanggal sekarang
    date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('Y-m-d H:i:s');
    $kode = 'top_nasional';

    if ($listRank != NULL) {
      $data = array(
        'kode' => $kode,
        'tanggal' => $tanggal,
        'deskripsi' => json_encode($listRank),
      );
    }else{
      $data = array(
        'kode' => $kode,
        'tanggal' => $tanggal,
        'deskripsi' => NULL,
      );
    }

    if ($this->Temporary_model->checkKode($kode)->num_rows() > 0) {
      $this->Temporary_model->update($kode, $data);
    }else {
      $this->Temporary_model->insert($data);
    }

    $res=array(
      'status'=>'success',
      'message'=>'Update Successful',
      'total_result'=>sizeof($listRank),
    );

    $json=json_encode($res);
    echo $json;
  }

  //fungsi untuk mengupdate rank siswa nasional Hari ini
  function update_top_nasional_now(){
    //mendapatkan tanggal sekarang
    date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('Y-m-d');
    $kode = 'top_nasional_now';

    //mendapatkan data rangking hari ini
    $listRankHariIni = $this->Temporary_model->getPointRankHariIni('10',$tanggal)->result();
    if ($listRankHariIni != NULL) {
      $data = array(
        'kode' => $kode,
        'tanggal' => $tanggal,
        'deskripsi' => json_encode($listRankHariIni),
      );
    }else{
      $data = array(
        'kode' => $kode,
        'tanggal' => $tanggal,
        'deskripsi' => NULL,
      );
    }

    if ($this->Temporary_model->checkKode($kode)->num_rows() > 0) {
      $this->Temporary_model->update($kode, $data);
    }else {
      $this->Temporary_model->insert($data);
    }

    $res=array(
      'status'=>'success',
      'message'=>'Update Successful',
      'total_result'=>sizeof($listRankHariIni),
    );

    $json=json_encode($res);
    echo $json;
  }

  //fungsi untuk mengupdate rank sekolah nasional
  function update_top_sekolah(){
    //mendapatkan tanggal sekarang
    date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('Y-m-d H:i:s');
    $kode = 'top_sekolah';

    //mendapatkan data rangking hari ini
    $topSekolah = $this->Temporary_model->getTopSekolahVer2('6')->result();
    if (sizeof($topSekolah) != 0) {
      $data = array(
        'kode' => $kode,
        'tanggal' => $tanggal,
        'deskripsi' => json_encode($topSekolah),
      );
    }else{
      $data = array(
        'kode' => $kode,
        'tanggal' => $tanggal,
        'deskripsi' => NULL,
      );
    }

    if ($this->Temporary_model->checkKode($kode)->num_rows() > 0) {
      $this->Temporary_model->update($kode, $data);
    }else {
      $this->Temporary_model->insert($data);
    }

    $res=array(
      'status'=>'success',
      'message'=>'Update Successful',
      'total_result'=>sizeof($topSekolah),
    );

    $json=json_encode($res);
    echo $json;
  }

  //fungsi untuk mengupdate rank siswa permapel
  function update_rank_mapel(){
    //mendapatkan tanggal sekarang
    date_default_timezone_set("Asia/Jakarta");
    $tanggal = date('Y-m-d');
    $tanggalWaktu = date('Y-m-d H:i:s');

    $res=array(
      'status'=>'success',
      'message'=>'Update Successful',
      'total_result'=>sizeof($listRank),
    );

    //mendapatkan data semua MataPelajaran
    $mapel = $this->Temporary_model->getAllMapel()->result();
    foreach ($mapel as $mp) {
      //mendapatkan data rangking
      $listRank = $this->Temporary_model->getPointRankByDateAndMapel($tanggal,$mp->slug,5)->result();
      $kode = "mapel_$mp->slug";
      echo "$kode , ";

      if ($listRank != NULL) {
        $data = array(
          'kode' => $kode,
          'tanggal' => $tanggalWaktu,
          'deskripsi' => json_encode($listRank),
        );
      }else{
        $data = array(
          'kode' => $kode,
          'tanggal' => $tanggal,
          'deskripsi' => NULL,
        );
      }

      if ($this->Temporary_model->checkKode($kode)->num_rows() > 0) {
        $this->Temporary_model->update($kode, $data);
      }else {
        $this->Temporary_model->insert($data);
      }

      $res["$kode"] = 'Success';

    }
    $json=json_encode($res);
    echo $json;
  }
}
