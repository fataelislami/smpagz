<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Check extends MY_Controller{
  //  function getdata($from,$where=null,$limit=9,$offset=0){
  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    header('Content-Type: application/json');
  }

  function auth_siswa(){
    if(isset($_POST['username']) && isset($_POST['password'])){
      $username = $this->input->post('username');
      $password = $this->input->post('password');
      $where_siswa = array(
        'nise' => $username,
        'password' => sha1($password)
        );
        $result_siswa = $this->Dbs->check("siswa",$where_siswa);
        if ($result_siswa->num_rows() > 0) {
          $getData = $result_siswa->row();
          $session = array(//Data yang akan disimpan kedalam session
            'nise' => $getData->nise,
            'nama'=>$getData->nama,
            'status' => "login",
            'level'=>'siswa'
            );
          $this->session->set_userdata($session);
          $data['status']='success';
        }else{
          $this->session->sess_destroy();
          $data['status']='failed';
        }
    }else{
      $data['status']='missing parameter';
    }

      $json=json_encode($data);
      echo $json;
  }
  function cabang(){
    if(isset($_GET['kode'])){
      $kode = $this->input->get('kode');
      $cek = $this->Dbs->CheckData('kode',$kode,'cabang');

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }elseif (isset($_GET['cabang'])) {
      $cabang = $this->input->get('cabang');
      $cek = $this->Dbs->CheckData('cabang',$cabang,'cabang');

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }elseif (isset($_GET['email'])) {
      $email = $this->input->get('email');
      $cek = $this->Dbs->CheckData('email',$email,'cabang');

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid',
        'results'=> null,

      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function kelas(){
    if(isset($_GET['kode'])){
      $kode = $this->input->get('kode');
      $cek = $this->Dbs->CheckData('kode',$kode,'kelas');

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid',
        'results'=> null,

      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function siswa(){
    if(isset($_GET['nise'])){
      $nise = $this->input->get('nise');
      $cek = $this->Dbs->CheckData('nise',$nise,'siswa');

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid',
        'results'=> null,

      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function guru(){
    if(isset($_GET['nik'])){
      $nik = $this->input->get('nik');
      $cek = $this->Dbs->CheckData('nik',$nik,'guru');

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid',
        'results'=> null,

      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function univ(){
    if(isset($_GET['id'])){
      $id = $this->input->get('id');
      $cek = $this->Dbs->CheckData('id',$id,'univ');

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid',
        'results'=> null,

      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function pgrade(){
      if(isset($_GET['kode'])){
      $kode = $this->input->get('kode');
      $cek = $this->Dbs->CheckData('kode',$kode,'data_pass_grade');

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid',
        'results'=> null,

      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function soal_to(){
      if(isset($_GET['nomor']) && isset($_GET['kode_soal'])){
      $nomor = $this->input->get('nomor');
      $kode_soal = $this->input->get('kode_soal');
      $cek = $this->Dbs->checkNomotSoal('nomor',$nomor,'list_soal_to',$kode_soal);

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid',
        'results'=> null,

      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function soal_quiz(){
      if(isset($_GET['nomor']) && isset($_GET['kode_soal'])){
      $nomor = $this->input->get('nomor');
      $kode_soal = $this->input->get('kode_soal');
      $cek = $this->Dbs->checkNomotSoal('nomor',$nomor,'list_soal_quiz',$kode_soal);

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid',
        'results'=> null,

      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function admin(){
    if(isset($_GET['username'])){
      $username = $this->input->get('username');
      $cek = $this->Dbs->CheckData('username',$username,'admin');

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }elseif (isset($_GET['email'])) {
      $email = $this->input->get('email');
      $cek = $this->Dbs->CheckData('email',$email,'admin');

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid',
        'results'=> null,

      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function jawaban(){
    header('Content-Type: application/json');
    if(isset($_GET['id_try_out'])){//params yang akan dicek
      //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
      $id_try_out=$_GET['id_try_out'];
      $loadDb=$this->Dbs->getSoalJawaban($id_try_out);//database yang akan di load
      $check=$loadDb->num_rows();
      if($check>0){
        $get=$loadDb->result(); //Uncomment ini untuk contoh
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          // 'results'=>"ISI DARI RESULT DATABASE",
          'results'=>$get //Uncomment ini untuk contoh
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

}
