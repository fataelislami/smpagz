<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tryout extends MY_Controller{
  //  function getdata($from,$where=null,$limit=9,$offset=0){
  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    header('Content-Type: application/json');
  }

  function jurusan_favorit(){
    header('Content-Type: application/json');
      //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
      $loadDb=$this->Dbs->jurusan_fav();//database yang akan di load
      $check=$loadDb->num_rows();
      if($check>0){
        $get=$loadDb->result(); //Uncomment ini untuk contoh
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'result'=>$get
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }

    $json=json_encode($data);
    echo $json;
  }
  function soal_by_to(){
    if(isset($_GET['id_try_out'])){
      if(isset($_GET['continue'])){
        $continue=true;
      }else{
        $continue=false;
      }
      $id_try_out=$this->input->get('id_try_out');
      $res=$this->Dbs->getSoalByTo($id_try_out);
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'waktu_mulai'=>$result[0]->waktu_mulai,
          'waktu_selesai'=>$result[0]->waktu_selesai,
          'id_event'=>$result[0]->id_event,
          'durasi'=>$result[0]->durasi,
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }
  function soal_by_to_test(){
    if(isset($_GET['id_try_out'])){
      if(isset($_GET['continue'])){
        $continue=true;
      }else{
        $continue=false;
      }
      $id_try_out=$this->input->get('id_try_out');
      $res=$this->Dbs->getSoalByTo_test($id_try_out);
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'waktu_mulai'=>$result[0]->waktu_mulai,
          'waktu_selesai'=>$result[0]->waktu_selesai,
          'id_event'=>$result[0]->id_event,
          'durasi'=>$result[0]->durasi,
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }
  function soal(){
    if(isset($_GET['kode_soal'])){
      $kode_soal=$this->input->get('kode_soal');
      $res=$this->Dbs->getSoalTo($kode_soal);
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }
  function info(){
    if(isset($_GET['id'])){
      $id = $this->input->get('id');
      if (isset($_GET['users']) AND $_GET['users']=='true') {
        // mengambil data users mitra sekolah
        $res = $this->Dbs->getInfoTO($id,'users');
      }else { // mengambil data siswa edulab
        $res = $this->Dbs->getInfoTO($id);
      }

      $check = $res->num_rows();
      if($check>0){
        $res = $res->row();
        $result = array(
          'id' => $res->id,
          'tanggal' => $res->tanggal,
          'skor' => $res->skor,
          'waktu_mulai' => $res->waktu_mulai,
          'waktu_selesai' => $res->waktu_selesai,
          'nise' => $res->nise,
          'nama' => $res->nama,
          'sekolah' => $res->sekolah,
          'kelas' => $res->kode_kelas,
          'cabang' => $res->cabang,
          'kode_soal' => $res->kode_soal,
        );
        if ($res->pil_1 != "") {
          $res2 = $this->Dbs->getPassGrade($res->pil_1)->row();
          $result['pil_1'] = $res2;
        }else{
          $result['pil_1'] = NULL;
        }
        if ($res->pil_2 != "") {
          $res2 = $this->Dbs->getPassGrade($res->pil_2)->row();
          $result['pil_2'] = $res2;
        }else{
          $result['pil_2'] = NULL;
        }
        if ($res->pil_3 != "") {
          $res2 = $this->Dbs->getPassGrade($res->pil_3)->row();
          $result['pil_3'] = $res2;
        }else{
          $result['pil_3'] = NULL;
        }

        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function infoUsers(){
    if(isset($_GET['id'])){
      $id = $this->input->get('id');

      $res = $this->Dbs->getInfoTO($id,'users');
      $check = $res->num_rows();
      if($check>0){
        $res = $res->row();
        $result = array(
          'id' => $res->id,
          'tanggal' => $res->tanggal,
          'skor' => $res->skor,
          'waktu_mulai' => $res->waktu_mulai,
          'waktu_selesai' => $res->waktu_selesai,
          'id' => $res->id,
          'nama' => $res->nama,
          'sekolah' => $res->sekolah,
          'kelas' => $res->kode_kelas,
          'cabang' => "mitra Sekolah",
          'kode_soal' => $res->kode_soal,
        );
        if ($res->pil_1 != "") {
          $res2 = $this->Dbs->getPassGrade($res->pil_1)->row();
          $result['pil_1'] = $res2;
        }else{
          $result['pil_1'] = NULL;
        }
        if ($res->pil_2 != "") {
          $res2 = $this->Dbs->getPassGrade($res->pil_2)->row();
          $result['pil_2'] = $res2;
        }else{
          $result['pil_2'] = NULL;
        }
        if ($res->pil_3 != "") {
          $res2 = $this->Dbs->getPassGrade($res->pil_3)->row();
          $result['pil_3'] = $res2;
        }else{
          $result['pil_3'] = NULL;
        }

        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function list_jawaban(){
    if (isset($_GET['id_to'])) {
      $id_to = $this->input->get('id_to');
      $res = $this->Dbs->getJawaban2($id_to,NULL);

      $res_num = $res->num_rows();
      if ($res_num > 0) {
        $res = $res->result();
        $arr = [];

        foreach ($res as $a) {
          array_push($arr,$a);
        }

        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$res_num,
          'results'=>$arr,
        );
      }else {
        $data=array(
          'status'=>'success',
          'total_result'=>$res_num,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function list_mapel(){
    if (isset($_GET['id_to'])) {
      $id_to = $this->input->get('id_to');
      $res = $this->Dbs->getListMapelByTO($id_to);
      $res_num = $res->num_rows();

      if ($res_num > 0) {
        $res = $res->result();
        $arr = [];

        foreach ($res as $r) {
          $res2 = $this->Dbs->getJawaban2($id_to,$r->id)->result();
          $benar = 0; $salah = 0; $null = 0;

          foreach ($res2 as $a) {
            if ($a->jawaban_siswa == NULL) {
              $null++;
            }elseif ($a->jawaban_siswa == $a->kunci_jawaban) {
              $benar++;
            }else {
              $salah++;
            }
          }

          $hasil = array(
            'nama_mapel' => $r->nama,
            'benar' => $benar,
            'salah' => $salah,
            'kosong' => $null,
          );

          array_push($arr,$hasil);
        }

        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$res_num,
          'results'=>$arr,
        );
      }else {
        $data=array(
          'status'=>'success',
          'total_result'=>$res_num,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function detail_soal(){
    if (isset($_GET['kode'])) {
      $kode = $this->input->get('kode');
      $res = $this->Dbs->detailSoalTryOut($kode);

      $res_num = $res->num_rows();
      if ($res_num > 0) {
        $res = $res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$res_num,
          'results'=>$res,
        );
      }else {
        $data=array(
          'status'=>'success',
          'total_result'=>$res_num,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function dataexport(){
    if(isset($_GET['id_tryout'])){
      $id = $this->input->get('id_tryout');

      $res = $this->Dbs->getInfoTO($id);
      $check = $res->num_rows();
      if($check>0){
        $arr = []; //array penampung

        $res = $res->row();
        $info = array(
          'id' => $res->id,
          'tanggal' => $res->tanggal,
          'skor' => $res->skor,
          'waktu_mulai' => $res->waktu_mulai,
          'waktu_selesai' => $res->waktu_selesai,
          'nise' => $res->nise,
          'nama' => $res->nama,
          'sekolah' => $res->sekolah,
          'kelas' => $res->kode_kelas,
          'cabang' => $res->cabang,
          'kode_soal' => $res->kode_soal,
        );
        if ($res->pil_1 != "") {
          $res2 = $this->Dbs->getPassGrade($res->pil_1)->row();
          $info['pil_1'] = $res2;
        }else{
          $info['pil_1'] = NULL;
        }
        if ($res->pil_2 != "") {
          $res2 = $this->Dbs->getPassGrade($res->pil_2)->row();
          $info['pil_2'] = $res2;
        }else{
          $info['pil_2'] = NULL;
        }
        if ($res->pil_3 != "") {
          $res2 = $this->Dbs->getPassGrade($res->pil_3)->row();
          $info['pil_3'] = $res2;
        }else{
          $info['pil_3'] = NULL;
        }

        $siswa = $this->Dbs->getSiswaByTryout($id)->row();
        $jawaban = $this->Dbs->getJawaban2($id,NULL)->result();

        array_push($arr,$info,$siswa,$jawaban);
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'results'=>$arr,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function scoring(){
    if (isset($_GET['id_tryout'])) {
      $point_benar = 1;
      $point_salah = 0;
      $point_kosong = 0;

      $id = $this->input->get('id_tryout');
      $cek = $this->Dbs->CheckData('id',$id,'try_out');

      if ($cek) {
        $res = $this->Dbs->getScoring($id);
        $res_num = $res->num_rows();
        $res = $res->result();

        //perhitungan
        $benar=0; $salah=0; $kosong=0;
        foreach ($res as $r) {
          if ($r->jawaban_siswa == "") {
            $kosong++;
          }elseif ($r->jawaban_siswa == $r->kunci_jawaban) {
            $benar++;
          }else {
            $salah++;
          }
        }

        //kalkulasi scoring
        $total = $benar*$point_benar + $salah*$point_salah + $kosong*$point_kosong;
        $full_benar = $res_num*$point_benar;
        $score = $total * (100/$full_benar);

        $hasil = array(
          'jmlh_soal' => $res_num,
          'benar' => "$benar",
          'salah' => "$salah",
          'kosong' => "$kosong",
          'score' => "$score",
        );

        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>'1',
          'results'=>$hasil,
        );
      }else {
        $data=array(
          'status'=>'success',
          'total_result'=>'0',
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

}
