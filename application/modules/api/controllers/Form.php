<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    header('Content-Type: application/json');
  }

  function index()
  {

  }

  function load_kampus(){
      $res=$this->Dbs->getdata('univ',null,200);
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$res->num_rows(),
          'message'=>'Univ Belum Terdaftar'
        );
      }
    $json=json_encode($data);
    echo $json;
  }


  function identitas(){
    if(isset($_GET['nis'])){
      $nis=$this->input->get('nis');
      $res=$this->Dbs->getdata('siswa',array('nis'=>$nis));
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$res->num_rows(),
          'message'=>'nis Belum Terdaftar'
        );
      }
    }else if(isset($_GET['nik'])){
      $nik=$this->input->get('nik');
      $res=$this->Dbs->getdata('guru',array('nik'=>$nik));
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$res->num_rows(),
          'message'=>'NIK Belum Terdaftar'
        );
      }
    }
    else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

}
