<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quiz extends MY_Controller{
  //  function getdata($from,$where=null,$limit=9,$offset=0){
  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    header('Content-Type: application/json');
  }

  function soal_by_quiz(){
    if(isset($_GET['id_quiz'])){
      if(isset($_GET['continue'])){
        $continue=true;
      }else{
        $continue=false;
      }
      $id_quiz=$this->input->get('id_quiz');
      $res=$this->Dbs->getSoalByQuiz($id_quiz);
      if($res->num_rows()>0){
        $result=$res->result();
        $newResults=[];
        foreach ($result as $obj) {
          $item=array (
                'kode' => $obj->kode,
                'nomor' => $obj->nomor,
                'image' => $obj->image,
                'soal' => $obj->soal,
                'tipe_pil' => $obj->tipe_pil,
                'pil_a' => $obj->pil_a,
                'pil_b' => $obj->pil_b,
                'pil_c' => $obj->pil_c,
                'pil_d' => $obj->pil_d,
                'pil_e' => $obj->pil_e,
                'jawaban' => $obj->jawaban,
                'penjelasan' => $obj->penjelasan,
                'img_penjelasan' => $obj->img_penjelasan,
                'mapel' => $obj->matapel,
                'kode_soal' => $obj->kode_soal,
                'nama_mapel' => $obj->nama_mapel,
              );
              if($continue){
                $where=array(
                  'kode_list_soal'=>$obj->kode,
                  'id_quiz'=>$id_quiz
                );
                $loadDb=$this->Dbs->getdata('list_jawaban_quiz',$where);
                if($loadDb->num_rows()>0){
                  $getData=$loadDb->row();
                  $item['jawaban_guru']=$getData->jawaban;
                }
                else{
                  $item['jawaban_guru']=null;
                }
              }
              array_push($newResults,$item);
        }
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'waktu_mulai'=>$result[0]->waktu_mulai,
          'waktu_selesai'=>$result[0]->waktu_selesai,
          'id_event'=>$result[0]->id_event,
          'durasi'=>$result[0]->durasi,
          'total_result'=>$res->num_rows(),
          'results'=>$newResults,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }
  function info(){
    if(isset($_GET['id'])){
      $id = $this->input->get('id');

      $res = $this->Dbs->getInfoQuiz($id);
      $check = $res->num_rows();
      if($check>0){
        $res = $res->row();

        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'results'=>$res,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function list_jawaban(){
    if (isset($_GET['id_quiz'])) {
      $id_quiz = $this->input->get('id_quiz');
      $res = $this->Dbs->getJawabanQuiz($id_quiz,NULL);

      $res_num = $res->num_rows();
      if ($res_num > 0) {
        $res = $res->result();
        $arr = [];

        foreach ($res as $a) {
          array_push($arr,$a);
        }

        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$res_num,
          'results'=>$arr,
        );
      }else {
        $data=array(
          'status'=>'success',
          'total_result'=>$res_num,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function dataexport(){
    if (isset($_GET['id_quiz'])) {
      $id = $this->input->get('id_quiz');
      $res = $this->Dbs->getGuruByquiz($id);
      $res_num = $res->num_rows();
      if ($res_num > 0) {
        $arr = [];
        $res = $res->row();

        $res2 = $this->Dbs->getJawabanQuiz($id,NULL); //ambil list jawaban
        $res2_num = $res2->num_rows();
        $res2 = $res2->result();

        array_push($arr,$res,$res2);

        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_jawaban'=>$res2_num,
          'results'=>$arr,
        );
      }else {
        $data=array(
          'status'=>'success',
          'total_result'=>$res_num,
          'message'=>'not found'
        );
      }
    }else {
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

}
