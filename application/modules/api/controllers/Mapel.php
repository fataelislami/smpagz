<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel extends MY_Controller{
  //  function getdata($from,$where=null,$limit=9,$offset=0){
  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    header('Content-Type: application/json');
  }

  function subbab(){
    if(isset($_GET['id_mapel'])){
      $id = $this->input->get('id_mapel');
      $cek = $this->Dbs->getSubbabByMapel($id)->result();

      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>1,
        'results'=>$cek,
      );
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid',
        'results'=> null,

      );
    }
    $json=json_encode($data);
    echo $json;
  }

}
