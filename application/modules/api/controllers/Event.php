<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    header('Content-Type: application/json');

  }

  function index()
  {

  }

  function auth(){
    $id=$this->input->post('id');
    $password=$this->input->post('password');
    if($id!='' && $password!=''){
      $where=array('id'=>$id,'password'=>$password);
      $loadDb=$this->Dbs->getdata('tryout_event',$where);
      if($loadDb->num_rows()>0){
        $data=array(
          'status'=>'success',
          'total_result'=>$loadDb->num_rows(),
          'message'=>'Berhasil'
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$loadDb->num_rows(),
          'message'=>'Password Event Salah'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'total_result'=>0,
        'message'=>'Invalid Params'
      );
    }
    $json=json_encode($data);
    echo $json;
  }
//SELECT `tanggal`,COUNT(`id`) as `total_peserta` FROM `try_out` GROUP BY `tanggal` order by `tanggal` DESC LIMIT 7
function graph(){
  header('Content-Type: application/json');
    //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
    $loadDb=$this->Dbs->graph();//database yang akan di load
    $check=$loadDb->num_rows();
    if($check>0){
      $get=$loadDb->result(); //Uncomment ini untuk contoh
      $arr_tanggal=[];
      $arr_peserta=[];
      foreach ($get as $gTgl) {
        array_push($arr_tanggal,$gTgl->tanggal);
        array_push($arr_peserta,$gTgl->total_peserta);
      }
      $data=array(
        'status'=>'success',
        'message'=>'found',
        'total_result'=>$check,
        'tanggal'=>$arr_tanggal, //Uncomment ini untuk contoh
        'total_peserta'=>$arr_peserta
      );
    }else{
      $data=array(
        'status'=>'success',
        'total_result'=>$check,
        'message'=>'not found'
      );
    }

  $json=json_encode($data);
  echo $json;
}

  function recent_list(){
    header('Content-Type: application/json');
          //StartPagination
      if(isset($_GET['page'])){//cek parameter page
        $page=$_GET['page'];
      }else{
        $page=1;//default jika parameter page tidak diload
      }
      $limitDb=5;
      $offsetDb=0;
      if($page!=1 and $page!=0){
        $offsetDb=$limitDb*($page-1);
      }
      //End Pagination
      //TIMEZON php
      $tz = 'Asia/Jakarta';
      $timestamp = time();
      $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
      $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
      $now=$dt->format('Y-m-d H:i:s');
      //END TIMEZONE
      //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
      $table='tryout_event';
      if(isset($_GET['by'])){

        $where=array('tipe'=>$_GET['by']);
        if(isset($_GET['nis'])){
          $nis=$_GET['nis'];
          $kelas=substr($nis,6,1);
          $jurusan=substr($nis,7,1);
          $where['kelas']=$kelas;
          $where['jurusan']=$jurusan;
        }
      }
      else{
        $where=null;
      }
      $this->db->order_by('id','DESC');
      $loadDb=$this->Dbs->getdata($table,$where,$limitDb,$offsetDb);//database yang akan di load
      $check=$loadDb->num_rows();
      if($check>0){
        $now=date('Y-m-d H:i:s', strtotime($now));
        //echo $paymentDate; // echos today!

        $get=$loadDb->result(); //Uncomment ini untuk contoh
        $newResults=[];
        foreach ($get as $obj) {
          $waktu_mulai = date('Y-m-d H:i:s', strtotime($obj->waktu_mulai));
          $waktu_selesai = date('Y-m-d H:i:s', strtotime($obj->waktu_selesai));
          $item=array(
            'id'=>$obj->id,
            'nama'=>$obj->nama,
            'tanggal'=>$obj->tanggal,
            'waktu_mulai'=>$obj->waktu_mulai,
            'waktu_selesai'=>$obj->waktu_selesai,
            'durasi'=>$obj->durasi,
            'kelas'=>$obj->kelas,
          );
          if (($now > $waktu_mulai) && ($now < $waktu_selesai)){
              $item['status']='berlangsung';
          }else if($now<$waktu_mulai){
              $item['status']='belum dimulai';
          }else if($now>$waktu_selesai){
              $item['status']='selesai';
          }
          array_push($newResults,$item);
        }
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'results'=>$newResults
          // 'results'=>$get //Uncomment ini untuk contoh
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }

    $json=json_encode($data);
    echo $json;
  }

  function tryout(){
    if(isset($_GET['nis'])){
      //TIMEZON php
      $tz = 'Asia/Jakarta';
      $timestamp = time();
      $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
      $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
      $now=$dt->format('Y-m-d H:i:s');
      //END TIMEZONE
      $nis=$this->input->get('nis');
      $getKelas=$this->Dbs->getdata('siswa',array('nis'=>$nis))->row();
      $kelas=$getKelas->id_kelas;
      $tipe="TO";
      $res=$this->Dbs->getEvent($tipe,$now,$kelas);
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$res->num_rows(),
          'message'=>'Belum ada event Terdaftar'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function quiz(){
    if(isset($_GET['nik'])){
      //TIMEZON php
      $tz = 'Asia/Jakarta';
      $timestamp = time();
      $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
      $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
      $now=$dt->format('Y-m-d H:i:s');
      //END TIMEZONE
      $nik=$this->input->get('nik');
      $tipe="QUIZ";
      $res=$this->Dbs->getEvent($tipe,$now);
      if($res->num_rows()>0){
        $result=$res->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$res->num_rows(),
          'results'=>$result,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$res->num_rows(),
          'message'=>'Belum ada event Terdaftar'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function getDurationEvent(){
    if (isset($_GET['tipe']) || isset($_GET['kode'])) {
      $tipe = $this->input->get('tipe'); //get tipe event
      $kode = $this->input->get('kode'); //get kode soal

      $list_mapel = $this->Dbs->getMapelBySoalTO($kode); //ambil data dari database
      $list_mapel_num = $list_mapel->num_rows();

      if ($tipe != "TO" && $list_mapel_num > 0) {
        $list_mapel = $list_mapel->result();

        //proses menghitung durasi
        $durasi = 0.0;
        foreach ($list_mapel as $list) {
          $durasi = $durasi + $list->durasi;
        }

        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_mapel'=>$list_mapel_num,
          'duration'=>$durasi,
        );
      }else {
        $data=array(
          'status'=>'success',
          'total_result'=>$list_mapel_num,
          'duration'=>0,
        );
      }
    }else {
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

}
