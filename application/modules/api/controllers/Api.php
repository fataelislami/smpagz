<?php
defined('BASEPATH') OR exit('No direct script access allowed');


// make sure you include this header to your function
// header('Content-Type: application/json');

class Api extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {
    echo "API DOCS!";
  }
  function docs(){
    echo base_url()."api/tryout/info?id={id TO}  <br>";
    echo base_url()."api/tryout/soal?kode_soal={id TO}  <br>";

    echo "<br> API TRYOUT <br>";
    echo base_url()."api/tryout/info?id={id TO}  [menampilkan detail data tryout]<br>";
    echo base_url()."api/tryout/list_jawaban?id_to={id TO}  ['menampilkan seluru jawaban & kuncijawaban tryout']<br>";
    echo base_url()."api/tryout/list_mapel?id_to={id TO}  ['menampilkan list mapel & jumlah benar,salah,kosong berdasarkan id tryout']<br>";
    echo base_url()."api/tryout/detail_soal?kode={kode soal}  ['menampilkan seluru detail lengkap soal']<br>";
    echo base_url()."api/tryout/scoring?id_tryout={id tryout}  ['menampilkan seluru detail lengkap soal']<br>";

    echo "<br> API QUIZ <br>";
    echo base_url()."api/quiz/info?id={id quiz}  [menampilkan detail data Quiz]<br>";
    echo base_url()."api/quiz/list_jawaban?id_quiz={id quiz}  ['menampilkan seluru jawaban & kuncijawaban quiz']<br>";
    echo base_url()."api/quiz/dataexport?id_quiz={id_quiz}  [menampilkan data yang akan di export ke excel]<br>";

    echo "<br> API CHECK (results = true/false) <br>";
    echo base_url()."api/check/cabang?kode={nomor cabang}<br>";
    echo base_url()."api/check/cabang?cabang={kode cabang}<br>";
    echo base_url()."api/check/cabang?email={email cabang}<br>";
    echo base_url()."api/check/kelas?kode={kode kelas}<br>";
    echo base_url()."api/check/siswa?nise={nise siswa}<br>";
    echo base_url()."api/check/guru?nik={nik guru}<br>";
    echo base_url()."api/check/univ?id={id univ}<br>";
    echo base_url()."api/check/pgrade?kode={kode passing grade/ kode jurusan universitas}<br>";
    echo base_url()."api/check/soal_to?nomor={nomor soal}&kode_soal={kode soal}<br>";
    echo base_url()."api/check/soal_quiz?nomor={nomor soal}&kode_soal={kode soal}<br>";
  }
}
