<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends MY_Controller{
  //  function getdata($from,$where=null,$limit=9,$offset=0){
  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs','Siswa_model'));
    header('Content-Type: application/json');
  }

  public function ajax_list()
      {
          $list = $this->Siswa_model->get_datatables();
          $data = array();
          $no = $_POST['start'];
          foreach ($list as $Siswa_model) {
              $no++;
              $row = array();
              $row[] = $no;
              $row[] = $Siswa_model->nise;
              $row[] = $Siswa_model->nama;
              $row[] = $Siswa_model->kode_kelas;
              $row[] = $Siswa_model->cabang;
              $row[] = $Siswa_model->jumlah_point;
              $row[] ="<a href='siswa/tryout?nis=$Siswa_model->nise'>
                  <button class='btn btn-info waves-effect waves-light m-r-10'>List TO</button>
              </a>
              <a href='battlefield/siswa/$Siswa_model->nise'>
                  <button class='btn btn-primary waves-effect waves-light m-r-10'>Battlefield</button>
              </a>
              <a href='siswa/edit/$Siswa_model->nise'>
                  <button class='btn btn-success waves-effect waves-light m-r-10'>Edit</button>
              </a>
              <button  data-toggle='modal' data-target='#responsive-modal' class='btn btn-danger waves-effect waves-light m-r-10 modalDelete' value='$Siswa_model->nise'>Delete</button>";

              $data[] = $row;
          }

          $output = array(
                          "draw" => $_POST['draw'],
                          "recordsTotal" => $this->Siswa_model->count_all(),
                          "recordsFiltered" => $this->Siswa_model->count_filtered(),
                          "data" => $data,
                  );
          //output to json format
          echo json_encode($output);
      }

  function byTryout(){
    if (isset($_GET['id_to'])) {
      $id = $this->input->get('id_to');
      $res = $this->Dbs->getSiswaByTryout($id);
      $res_num = $res->num_rows();
      if ($res_num > 0) {
        $res = $res->row();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$res_num,
          'results'=>$res,
        );
      }else {
        $data=array(
          'status'=>'success',
          'total_result'=>$res_num,
          'message'=>'not found'
        );
      }
    }else {
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

}
