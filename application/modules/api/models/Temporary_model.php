<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temporary_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  var $id = 'kode';
  var $table = 'temporary_table';

  // check kode
  function checkKode($kode){
    $this->db->select('kode');
    $this->db->from($this->table);
    $this->db->where('kode', $kode);
    return $this->db->get();
  }

  // insert data
  function insert($data)
  {
      $this->db->insert($this->table, $data);
  }

  // update data
  function update($id, $data)
  {
      $this->db->where($this->id, $id);
      $this->db->update($this->table, $data);
  }

  function getPointRank($limit,$jurusan = NULL){
    //get table siswa
    $this->db->select('siswa.nise as identity, siswa.nama, siswa.sekolah, sekolah.nama as sekolah_mitra');
    $this->db->select('siswa.total_point_ipa, siswa.total_point_ips, siswa.total_point_tpa');
    $this->db->select('(siswa.total_point_ipa+siswa.total_point_ips+siswa.total_point_tpa)as jumlah_point');
    $this->db->from('siswa');
    $this->db->join('sekolah', 'siswa.id_sekolah_mitra = sekolah.id', 'left');
    if ($jurusan == 'IPS') {
      $this->db->order_by('total_point_ips','DESC');
      $this->db->where('total_point_ips !=','0');
    }elseif ($jurusan == 'IPA') {
      $this->db->order_by('total_point_ipa','DESC');
      $this->db->where('total_point_ipa !=','0');
    }elseif ($jurusan == 'TPA') {
      $this->db->order_by('total_point_tpa','DESC');
      $this->db->where('total_point_tpa !=','0');
    }else {
      $this->db->order_by('jumlah_point','DESC');
      $this->db->where('(siswa.total_point_ipa+siswa.total_point_ips+siswa.total_point_tpa) !=','0');
    }
    $this->db->limit($limit);
    return $this->db->get();
  }

  function getPointRankHariIni($limit,$date,$jurusan = NULL){
    //get data siswa
    $this->db->select('list_jawaban_soal.nise as identity, list_jawaban_soal.waktu_mulai, SUM(list_jawaban_soal.point_soal) as jumlah_point');
    $this->db->select('siswa.nama, siswa.sekolah, sekolah.nama as sekolah_mitra');
    $this->db->from('list_jawaban_soal');
    $this->db->join('siswa', 'list_jawaban_soal.nise = siswa.nise', 'left');
    $this->db->join('sekolah', 'siswa.id_sekolah_mitra = sekolah.id', 'left');
    $this->db->where('DATE(list_jawaban_soal.waktu_mulai)', $date);
    if ($jurusan != NULL) {
      $this->db->select('mata_pelajaran.jurusan');
      $this->db->join('soal', 'list_jawaban_soal.kode_soal = soal.kode', 'left');
      $this->db->join('mata_pelajaran', 'soal.mapel = mata_pelajaran.id', 'left');
      $this->db->where('mata_pelajaran.jurusan', $jurusan);
    }
    $this->db->group_by('list_jawaban_soal.nise');
    $this->db->order_by('jumlah_point','DESC');
    $this->db->having('jumlah_point !=','0');
    $this->db->limit($limit);
    return $this->db->get();;
  }

  function getTopSekolah($limit){
    $this->db->select('sekolah.*, COUNT(siswa.nise) as jumlah_siswa');
    $this->db->from('sekolah');
    $this->db->join('siswa', 'sekolah.id = siswa.id_sekolah_mitra', 'left');
    $this->db->group_by('sekolah.id');
    $this->db->order_by('jumlah_siswa','DESC');
    $this->db->limit($limit);
    return $this->db->get();
  }

  function getTopSekolahVer2($limit){
    $this->db->select('sekolah,SUM(total_point_ipa+total_point_ips+total_point_tpa) AS point');
    $this->db->select('COUNT(nise) as jumlah_siswa');
    $this->db->from('siswa');
    $this->db->where('id_sekolah_mitra IS NULL');
    $this->db->group_by('sekolah');
    $table1 = $this->db->get_compiled_select();

    $this->db->select('sekolah.nama as sekolah,SUM(total_point_ipa+total_point_ips+total_point_tpa) AS point');
    $this->db->select('COUNT(nise) as jumlah_siswa');
    $this->db->from('siswa');
    $this->db->join('sekolah', 'siswa.id_sekolah_mitra = sekolah.id', 'left');
    $this->db->where('id_sekolah_mitra IS NOT NULL');
    $this->db->group_by('id_sekolah_mitra');
    $table2 = $this->db->get_compiled_select();

    $this->db->select("(@rownum:=@rownum+1)as rank,result_table.* FROM (".$table1." UNION ".$table2."ORDER BY `point` DESC LIMIT "."$limit".") AS result_table,(SELECT @rownum := 0) r");
    $this->db->limit($limit);
    return $this->db->get();
  }

  function getAllMapel(){
    $this->db->select('id,slug');
    return $this->db->get('mata_pelajaran');
  }

  /*=============Fungsi untuk mendapatkan ranking by tanggal dan mapel=========*/
  function getPointRankByDateAndMapel($date,$mapel,$limit=5,$result=NULL){
    //get data siswa
    $this->db->select('siswa.nise as identity, siswa.nama, siswa.sekolah');
    $this->db->select('SUM(list_jawaban_soal.point_soal) as jumlah_point');
    $this->db->from('list_jawaban_soal');
    $this->db->join('siswa','list_jawaban_soal.nise = siswa.nise', 'left');
    $this->db->join('soal','list_jawaban_soal.kode_soal = soal.kode', 'left');
    $this->db->join('mata_pelajaran','soal.mapel = mata_pelajaran.id', 'left');
    $this->db->where('DATE(list_jawaban_soal.waktu_mulai)', $date);
    $this->db->where('mata_pelajaran.slug',$mapel);
    $this->db->group_by('list_jawaban_soal.nise');
    $table_siswa = $this->db->get_compiled_select();

    //get table users
    $this->db->select('users.id as identity, users.nama, sekolah.nama');
    $this->db->select('SUM(list_jawaban_soal_users.point_soal) as jumlah_point');
    $this->db->from('list_jawaban_soal_users');
    $this->db->join('users','list_jawaban_soal_users.user_id = users.id', 'left');
    $this->db->join('sekolah', 'users.id_sekolah = sekolah.id', 'left');
    $this->db->join('soal','list_jawaban_soal_users.kode_soal = soal.kode', 'left');
    $this->db->join('mata_pelajaran','soal.mapel = mata_pelajaran.id', 'left');
    $this->db->where('DATE(list_jawaban_soal_users.waktu_mulai)', $date);
    $this->db->where('mata_pelajaran.slug',$mapel);
    $this->db->group_by('list_jawaban_soal_users.user_id');
    $table_users = $this->db->get_compiled_select();

    //union table
    if ($result=="get_compiled_select") {
      $this->db->select("(@rownum:=@rownum+1)as rank,result_table.* FROM (".$table_siswa." UNION ".$table_users."ORDER BY `jumlah_point` DESC) AS result_table,(SELECT @rownum := 0) r");
      return $this->db->get_compiled_select();
    }else {
      $this->db->select("(@rownum:=@rownum+1)as rank,result_table.* FROM (".$table_siswa." UNION ".$table_users."ORDER BY `jumlah_point` DESC LIMIT "."$limit".") AS result_table,(SELECT @rownum := 0) r");
      $this->db->limit($limit);
      return $this->db->get();
    }/*=============Fungsi untuk mendapatkan ranking by tanggal dan mapel=========*/
  function getPointRankByDateAndMapel($date,$mapel,$limit=5,$result=NULL){
    //get data siswa
    $this->db->select('(@rownum:=@rownum+1)as rank');
    $this->db->select('siswa.nise as identity, siswa.nama, siswa.sekolah');
    $this->db->select('SUM(list_jawaban_soal.point_soal) as jumlah_point');
    $this->db->from('list_jawaban_soal');
    $this->db->from('(SELECT @rownum := 0) r');
    $this->db->join('siswa','list_jawaban_soal.nise = siswa.nise', 'left');
    $this->db->join('soal','list_jawaban_soal.kode_soal = soal.kode', 'left');
    $this->db->join('mata_pelajaran','soal.mapel = mata_pelajaran.id', 'left');
    $this->db->where('DATE(list_jawaban_soal.waktu_mulai)', $date);
    $this->db->where('mata_pelajaran.slug',$mapel);
    $this->db->group_by('list_jawaban_soal.nise');
    $this->db->order_by('jumlah_point', 'DESC');
    return $this->db->get();

    $this->db->select("(@rownum:=@rownum+1)as rank,result_table.* FROM (".$table_siswa." UNION ".$table_users."ORDER BY `jumlah_point` DESC) AS result_table,(SELECT @rownum := 0) r");
    return $this->db->get_compiled_select();

    //union table
    // if ($result=="get_compiled_select") {
    //   $this->db->select("(@rownum:=@rownum+1)as rank,result_table.* FROM (".$table_siswa." UNION ".$table_users."ORDER BY `jumlah_point` DESC) AS result_table,(SELECT @rownum := 0) r");
    //   return $this->db->get_compiled_select();
    // }else {
    //   $this->db->select("(@rownum:=@rownum+1)as rank,result_table.* FROM (".$table_siswa." UNION ".$table_users."ORDER BY `jumlah_point` DESC LIMIT "."$limit".") AS result_table,(SELECT @rownum := 0) r");
    //   $this->db->limit($limit);
    //   return $this->db->get();
    // }
  }
  }

}
