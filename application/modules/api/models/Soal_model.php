<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Soal_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function getTotalPoint($nise,$jurusan){
    $this->db->select('sum(point_soal) as total_point');
    $this->db->from('list_jawaban_soal');
    $this->db->join('soal', 'soal.kode = list_jawaban_soal.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'soal.mapel = mata_pelajaran.id', 'left');
    $this->db->where('nise', $nise);
    $this->db->where('mata_pelajaran.jurusan', $jurusan);
    return $this->db->get();
  }

}
