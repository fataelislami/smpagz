<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbs extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function check($table,$where){
		return $this->db->get_where($table,$where);
	}

  function getdata($from,$where=null,$limit=9,$offset=0){
    if($where!=null){
      $this->db->where($where);
    }
    $this->db->limit($limit, $offset);
    $db=$this->db->get($from);
    return $db;
  }

  //get field
  function get_field(){
      $table=$this->table;
      $sql=$this->db->query("SELECT `*` FROM `$table`"); //ganti * untuk custom field yang ditampilkan pada table
      return $sql->list_fields();
  }

  function getwhere($where,$value,$table){
    $this->db->where($where, $value);
    $db=$this->db->get($table);
    return $db;
  }


  function insert($data,$table){
   $insert = $this->db->insert($table, $data);
   if ($this->db->affected_rows()>0) {
     return true;
     }else{
     return false;
     }
 }
 function delete($table,$where){
   $this->db->where($where);
   $this->db->delete($table);
 }

 function update($data,$table,$where){
    $this->db->where($where);
    $db=$this->db->update($table,$data);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
  }

  function getSoalJawaban($id_try_out){
    $this->db->select("list_soal.nomor");
    $this->db->select('tb1.jawaban as jawaban_siswa');
    $this->db->from('tryout');
    $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
    $this->db->join("(SELECT * FROM list_jawaban_to WHERE list_jawaban_to.id_try_out = $id_try_out) AS tb1", 'list_soal.kode = tb1.kode_list_soal', 'left');
    $this->db->where('tryout.id', $id_try_out);
    return $this->db->get();
  }

  function graph(){
    //graph 7 event sebelumnya
    $sql="SELECT `tanggal`,COUNT(`id`) as `total_peserta` FROM `try_out` GROUP BY `tanggal` order by `tanggal` DESC LIMIT 7";
    return $this->db->query($sql);
  }

  function jurusan_fav(){
    $sql="SELECT `kelompok`,`jurusan`,`tahun`,`pil_1` as `jurusan_favorit`,count(`pil_1`) as `total_pilihan` FROM `try_out` JOIN `data_pass_grade` on `try_out`.`pil_1`=`data_pass_grade`.`kode` group by `pil_1` ORDER BY `total_pilihan` DESC LIMIT 4";
    return $this->db->query($sql);
  }

  function getEvent($tipe,$date,$kelas=null,$jurusan=null){
    $this->db->select("*");
    $this->db->from("tryout_event");
    // if($tipe=="QUIZ" && $kelas==null && $jurusan==null){
    //   $this->db->where("'$date' BETWEEN `waktu_mulai` AND `waktu_selesai` AND `tipe`='$tipe'");
    // }
    if($tipe=="TO"){
      $this->db->where("'$date' BETWEEN `waktu_mulai` AND `waktu_selesai`");
    }
      $this->db->where('kelas', $kelas);
    return $this->db->get();
  }

  function getjurusan($jurusan,$id_univ){
    $this->db->select("*");
    $this->db->from("data_pass_grade");
    $this->db->join("univ","univ.id=data_pass_grade.id_univ");
    $this->db->where('data_pass_grade.showing',1);
    $this->db->where('data_pass_grade.id_univ',$id_univ);
    if($jurusan!="ipc"){
      $this->db->where("kelompok",$jurusan);
    }
    return $this->db->get();
  }

  function getSoalTo($kode_soal){
    $this->db->select("list_soal.*");
    $this->db->select("mata_pelajaran.nama as nama_mapel");
    $this->db->from("list_soal");
    $this->db->join("mata_pelajaran","mata_pelajaran.id=list_soal.id_mata_pelajaran");
    $this->db->where('list_soal.kode_soal', $kode_soal);
    return $this->db->get();
  }
  function getSoalByTo($id_try_out){
    $this->db->select("list_soal.*");
    $this->db->select("mata_pelajaran.nama as nama_mapel");
    $this->db->select("try_out.*");
    $this->db->select("tryout_event.durasi as durasi");
    $this->db->select('tb1.jawaban as jawaban_siswa');
    $this->db->from('tryout');
    $this->db->join('event','event.id=try_out.id_event');
    $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal.id_mata_pelajaran = mata_pelajaran.id', 'left');
    $this->db->join("(SELECT * FROM list_jawaban_to WHERE list_jawaban_to.id_try_out = $id_try_out) AS tb1", 'list_soal.kode = tb1.kode_list_soal', 'left');
    $this->db->where('tryout.id', $id_try_out);
    return $this->db->get();

  }
  function getSoalByTo_test($id_try_out){
    $this->db->select("list_soal.kode,list_soal.nomor,list_soal.image,list_soal.soal,list_soal.tipe_pil,list_soal.pil_a,list_soal.pil_b,list_soal.pil_c,list_soal.pil_d,list_soal.pil_e,list_soal.jawaban");
    $this->db->select("mata_pelajaran.nama as nama_mapel");
    $this->db->select("try_out.*");
    $this->db->select("tryout_event.durasi as durasi");
    $this->db->select('tb1.jawaban as jawaban_siswa');
    $this->db->from('tryout');
    $this->db->join('event','event.id=try_out.id_event');
    $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal.id_mata_pelajaran = mata_pelajaran.id', 'left');
    $this->db->join("(SELECT * FROM list_jawaban_to WHERE list_jawaban_to.id_try_out = $id_try_out) AS tb1", 'list_soal.kode = tb1.kode_list_soal', 'left');
    $this->db->where('tryout.id', $id_try_out);
    return $this->db->get();

  }
  function getSoalByQuiz($id_quiz){
    $this->db->select("list_soal_quiz.*");
    $this->db->select("mata_pelajaran.nama as nama_mapel");
    $this->db->select("quiz.*");
    $this->db->select("tryout_event.durasi as durasi");
//
$this->db->select('tb1.jawaban as jawaban_siswa');
$this->db->from('quiz');
$this->db->join("tryout_event","quiz.id_event=event.id");
$this->db->join("list_soal_quiz","list_soal_quiz.kode_soal=quiz.kode_soal");
$this->db->join("mata_pelajaran","mata_pelajaran.id=list_soal_quiz.matapel");
$this->db->join("(SELECT * FROM list_jawaban_quiz WHERE list_jawaban_quiz.id_quiz = $id_quiz) AS tb1", 'list_soal_quiz.kode = tb1.kode_list_soal', 'left');
$this->db->where('quiz.id', $id_quiz);
return $this->db->get();
//
  }

  function getInfoTO($id,$table=null){
    if ($table!=null) { //mendapatkan data mitra sekolah
      $this->db->select('tryout_users.*,users.*,users.kelas as kode_kelas,sekolah.nama as sekolah');
      $this->db->from('tryout_users');
      $this->db->join('users', 'tryout_users.id_user = users.id', 'left');
      $this->db->join('sekolah', 'users.id_sekolah = sekolah.id', 'left');
      $this->db->where('tryout_users.id', $id);
    }else { // mendapatkan data siswa edulab
      $this->db->select('tryout.*,siswa.*,cabang.cabang');
      $this->db->from('tryout');
      $this->db->join('siswa', 'tryout.nis = siswa.nis','LEFT');
      $this->db->join('cabang', 'siswa.kode_cabang = cabang.kode','LEFT');
      $this->db->where('id', $id);
    }

    return $this->db->get();
  }

  function getInfoQuiz($id){
    $this->db->select('quiz.*,guru.nama');
    $this->db->join('guru', 'quiz.nik = guru.nik');
    $this->db->where('id', $id);
    return $this->db->get('quiz');
  }

  function getPassGrade($id_univ){
    $this->db->select('univ.nama,data_pass_grade.jurusan,data_pass_grade.pass_grade');
    $this->db->join('univ', 'data_pass_grade.id_univ = univ.id');
    $this->db->where('kode', $id_univ);
    return $this->db->get('data_pass_grade');
  }

  function CheckData($field,$value,$table){
    $this->db->select('*');
    $this->db->where($field, $value);
    $result = $this->db->get($table,1,0)->num_rows();
    if ($result > 0) {
      return true;
    }else {
      return false;
    }
  }

  function checkNomotSoal($field,$value,$table,$kode_soal){
    $this->db->select('*');
    $this->db->where($field, $value);
    $this->db->where('kode_soal', $kode_soal);
    $result = $this->db->get($table,1,0)->num_rows();
    if ($result > 0) {
      return true;
    }else {
      return false;
    }
  }

  function getJawaban($id_to,$mapel){
    $this->db->select('list_jawaban_to.id, list_soal.nomor, mata_pelajaran.nama');
    $this->db->select('list_jawaban_to.jawaban as jawaban_siswa');
    $this->db->select('list_soal.jawaban as kunci_jawaban');
    $this->db->from('list_jawaban_to');
    $this->db->join('list_soal', 'list_jawaban_to.kode_list_soal = list_soal.kode', 'left');
    $this->db->join('mata_pelajaran', 'list_soal.id_mata_pelajaran = mata_pelajaran.id', 'left');
    if ($mapel != NULL) {
      $this->db->where('mata_pelajaran.id', $mapel);
    }
    $this->db->where('id_try_out', $id_to);
    return $this->db->get();
  }

  function getJawaban2($id_to,$mapel){
    $this->db->select(' tb1.id, list_soal.kode, list_soal.nomor, mata_pelajaran.nama');
    $this->db->select('tb1.jawaban as jawaban_siswa');
    $this->db->select('list_soal.jawaban as kunci_jawaban');
    $this->db->from('tryout');
    $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal.id_mata_pelajaran = mata_pelajaran.id', 'left');
    $this->db->join("(SELECT * FROM list_jawaban_to WHERE list_jawaban_to.id_try_out = $id_to) AS tb1", 'list_soal.kode = tb1.kode_list_soal', 'left');
    if ($mapel != NULL) {
      $this->db->where('mata_pelajaran.id', $mapel);
    }
    $this->db->where('tryout.id', $id_to);
    return $this->db->get();
  }

  function getListMapelByTO($id_to){
    $this->db->select('mata_pelajaran.nama , mata_pelajaran.id');
    $this->db->from('tryout');
    $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal.id_mata_pelajaran = mata_pelajaran.id', 'left');
    $this->db->where('tryout.id', $id_to);
    $this->db->group_by('mata_pelajaran.id');
    return $this->db->get();
  }

  function getJawabanQuiz($id_quiz,$mapel){
    $this->db->select('tb1.id, list_soal_quiz.nomor, mata_pelajaran.nama');
    $this->db->select('tb1.jawaban as jawaban_guru');
    $this->db->select('list_soal_quiz.jawaban as kunci_jawaban');
    $this->db->from('quiz');
    $this->db->join('list_soal_quiz', 'quiz.kode_soal = list_soal_quiz.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal_quiz.matapel = mata_pelajaran.id', 'left');
    $this->db->join("(SELECT * FROM list_jawaban_quiz WHERE list_jawaban_quiz.id_quiz = $id_quiz) AS tb1", 'list_soal_quiz.kode = tb1.kode_list_soal', 'left');
    if ($mapel != NULL) {
      $this->db->where('mata_pelajaran.id', $mapel);
    }
    $this->db->where('quiz.id', $id_quiz);
    return $this->db->get();
  }

  function getSiswaByTryout($id){
    $this->db->select('siswa.nis,siswa.nama,sekolah,kode_kelas,cabang.cabang');
    $this->db->from('tryout');
    $this->db->join('siswa', 'tryout.nis = siswa.nis', 'left');
    $this->db->join('cabang', 'siswa.kode_cabang = cabang.kode', 'left');
    $this->db->where('tryout.id', $id);
    return $this->db->get();
  }

  function getGuruByquiz($id){
    $this->db->select('guru.nik, guru.nama, mata_pelajaran.nama as mapel, guru.email');
    $this->db->join('guru', 'quiz.nik = guru.nik', 'left');
    $this->db->join('mata_pelajaran', 'guru.mapel = mata_pelajaran.id', 'left');
    $this->db->where('quiz.id', $id);
    return $this->db->get('quiz');
  }

  function detailSoalTryOut($kode){
    $this->db->select('list_soal.*,mata_pelajaran.nama');
    $this->db->join('mata_pelajaran', 'list_soal.id_mata_pelajaran = mata_pelajaran.id', 'left');
    $this->db->where('kode', $kode);
    return $this->db->get('list_soal');
  }

  function getScoring($id_to){
    $this->db->select(' tb1.id, tb1.jawaban as jawaban_siswa');
    $this->db->select('list_soal.jawaban as kunci_jawaban');
    $this->db->from('tryout');
    $this->db->join('list_soal', 'tryout.kode_soal = list_soal.kode_soal', 'left');
    $this->db->join("(SELECT * FROM list_jawaban_to WHERE list_jawaban_to.id_try_out = $id_to) AS tb1", 'list_soal.kode = tb1.kode_list_soal', 'left');
    $this->db->where('tryout.id', $id_to);
    return $this->db->get();
  }

  function getMapelBySoalTO($kode){
		$this->db->select('mata_pelajaran.nama,mata_pelajaran.durasi');
    $this->db->from('list_soal');
    $this->db->join('mata_pelajaran', 'list_soal.id_mata_pelajaran = mata_pelajaran.id', 'left');
    $this->db->where('list_soal.kode_soal', $kode);
    $this->db->group_by('list_soal.id_mata_pelajaran');
    return $this->db->get();
	}

  function getSubbabByMapel($id){
    $this->db->select('*');
    $this->db->where('id_mapel', $id);
    return $this->db->get('sub_mapel');
  }

}
