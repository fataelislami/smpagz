<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ortu extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    if($this->session->userdata('status')!='login'){
      redirect(base_url('login'));
    }
    if($this->session->userdata('level')!='ortu'){
      redirect(redirect($_SERVER['HTTP_REFERER']));
    }
  }

  function index()
  {
    $data = array(
      'contain_view' => 'ortu/home_v',
      'sidebar'=>'ortu/sidebar',//Ini buat menu yang ditampilkan di module ortu {DIKIRIM KE TEMPLATE}
      'css'=>'ortu/assets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
      'script'=>'ortu/assets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
      'titlePage'=>'HOME',//Ini Judul Page untuk tiap halaman
    );
    $this->template->load($data);
  }

}
