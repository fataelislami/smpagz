<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              <div class="row">
                  <div class="col-md-6">
                      <h4 class="card-title">Data List_soal_to</h4>
                      <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                  </div>>
              </div>


                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center" width="15%">NOMOR</th>
                                <th class="text-center">MATA PELAJARAN</th>
                                <th class="text-center" width="15%">JAWABAN SISWA</th>
                                <th class="text-center" width="15%">KUNCI JAWABAN</th>
                                <th class="text-center">ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($jawaban as $j): ?>
                            <tr>
                              <td class="text-center"><?php echo $j->nomor; ?></td>
                              <td class="text-center"><?php echo $j->nama; ?></td>
                              <td class="text-center"><?php echo $j->jawaban_siswa; ?></td>
                              <td class="text-center"><?php echo $j->kunci_jawaban; ?></td>
                              <td class="text-center">
                                <a href="<?php echo base_url()."ortu/detailsoal?kode=".$j->kode ?>">
                                  <button class="btn btn-success">Detail Soal</button>
                                </a>
                              </td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="text-center">Nama Mapel</th>
                                <th class="text-center">Benar</th>
                                <th class="text-center">Salah</th>
                                <th class="text-center">Kosong</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($mapel as $j): ?>
                            <tr>
                              <td class="text-center"><?php echo $j['nama_mapel']; ?></td>
                              <td class="text-center"><?php echo $j['benar']; ?></td>
                              <td class="text-center"><?php echo $j['salah']; ?></td>
                              <td class="text-center"><?php echo $j['kosong']; ?></td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
