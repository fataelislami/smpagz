<!-- chartist chart -->
<script src="<?php echo base_url()?>assets/plugins/chartist-js/dist/chartist.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
<!--c3 JavaScript -->
<script src="<?php echo base_url()?>assets/plugins/d3/d3.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/c3-master/c3.min.js"></script>
<!-- Vector map JavaScript -->
<script src="<?php echo base_url()?>assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/vectormap/jquery-jvectormap-us-aea-en.js"></script>
<script src="<?php echo base_url()?>js/dashboard2.js"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!-- chartist chart -->
<script src="<?php echo base_url()?>assets/plugins/chartist-js/dist/chartist.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
<script type="text/javascript">

  var labels_php=<?php echo json_encode(array_reverse($obj_graph->tanggal)) ?>;
  var series_php=<?php echo json_encode(array_reverse($obj_graph->total_peserta)) ?>;
</script>
<script src="<?php echo base_url()?>assets/plugins/chartist-js/dist/chartist-init.js"></script>
<script src="<?php echo base_url()?>assets/plugins/toast-master/js/jquery.toast.js"></script>
<script src="<?php echo base_url()?>js/toastr.js"></script>
<?php if($this->session->flashdata('login_true')) {?>
<script type="text/javascript">
$.toast({
 heading: 'Selamat Datang Admin',
 text: 'lulusnegeri.com - by Edulab',
 position: 'top-right',
 loaderBg:'#fc4b6c',
 icon: 'success',
 hideAfter: 3500,
 stack: 6
});
</script>
<?php } ?>

<!-- Ini untuk semua script yang di load pada page masing masing controller di dalam module -->
<!--
Coba dicek di module template -> folder view -> full.php
cek line ke : 525
 -->
