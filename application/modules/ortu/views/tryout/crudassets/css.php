<link href="<?php echo base_url()?>assets/plugins/summernote/dist/summernote.css" rel="stylesheet" />
<link href="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url()?>assets/plugins/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()?>assets/plugins/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<!-- Date picker plugins css -->
<link href="<?php echo base_url()?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?php echo base_url()?>assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
