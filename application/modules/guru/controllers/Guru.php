<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    if($this->session->userdata('status')!='login'){
      redirect(base_url('login'));
    }
    if($this->session->userdata('level')!='guru'){
      redirect(redirect($_SERVER['HTTP_REFERER']));
    }
  }

  function index()
  {
    $data = array(
      'contain_view' => 'guru/home_v',
      'sidebar'=>'guru/sidebar',//Ini buat menu yang ditampilkan di module guru {DIKIRIM KE TEMPLATE}
      'css'=>'guru/assets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
      'script'=>'guru/assets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
      'titlePage'=>'HOME',//Ini Judul Page untuk tiap halaman
    );
    $this->template->load($data);
  }

}
