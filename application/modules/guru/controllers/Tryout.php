<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    require_once 'class/ClientAPI.php';

class Tryout extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mtryout');
        $this->load->library('form_validation');
        if($this->session->userdata('status')!='login'){
          redirect(base_url('login'));
        }
        if($this->session->userdata('level')!='guru'){
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }
    }

    public function index()
    {
      if (isset($_GET['nis'])) {
        $nis = $this->input->get('nis');
        $this->tryoutSiswa($nis);
      }elseif (isset($_GET['user_id'])) {
        $id = $this->input->get('user_id');
        $this->tryoutUsers($id);
      }else{
        redirect(site_url('guru/siswa'));
      }

    }

    function tryoutSiswa($nis){
      $cek = $this->Mtryout->cekData('nis',$nis,'siswa');
      $tampil = TRUE;

      if ($cek) {
        // $datatryout=$this->Mtryout->get_all();//panggil ke modell
        // $datafield=$this->Mtryout->get_field();//panggil ke modell
        $datatable = $this->Mtryout->getDataTable($nis);

        $data = array(
          'contain_view' => 'guru/tryout/try_out_list',
          'sidebar'=>'guru/sidebar',
          'css'=>'guru/tryout/crudassets/css',
          'script'=>'guru/tryout/crudassets/script',
          // 'datatryout'=>$datatryout,
          // 'datafield'=>$datafield,
          'datatable' => $datatable,
          'tampil' => $tampil,
          'module'=>'guru',
          'titlePage'=>'Try Out',
          'controller'=>'tryout'
         );
        $this->template->load($data);
      }else {
        //redirect(site_url('guru/siswa'));
      }
    }

    function tryoutUsers($id){
      $cek = $this->Mtryout->cekData('id',$id,'users');

      if ($cek) {
        // $datatryout=$this->Mtryout->get_all();//panggil ke modell
        // $datafield=$this->Mtryout->get_field();//panggil ke modell
        $datatable = $this->Mtryout->getDataTable($id,'users');

        $data = array(
          'contain_view' => 'guru/tryout/try_out_list_mitra',
          'sidebar'=>'guru/sidebar',
          'css'=>'guru/tryout/crudassets/css',
          'script'=>'guru/tryout/crudassets/script',
          // 'datatryout'=>$datatryout,
          // 'datafield'=>$datafield,
          'datatable' => $datatable,
          'tampil' => TRUE,
          'module'=>'guru',
          'titlePage'=>'Try Out',
          'controller'=>'tryout'
         );
        $this->template->load($data);
      }else{
        redirect(site_url('guru/siswa'));
      }
    }


    // public function create(){
    //   $data = array(
    //     'contain_view' => 'guru/tryout/try_out_form',
    //     'sidebar'=>'guru/sidebar',//Ini buat menu yang ditampilkan di module guru {DIKIRIM KE TEMPLATE}
    //     'css'=>'guru/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
    //     'script'=>'guru/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
    //     'action'=>'guru/tryout/create_action',
    //     'titlePage'=>'Tambah Try Out'
    //    );
    //   $this->template->load($data);
    // }
    //
    public function edit($id){
      //TIMEZON php
      $tz = 'Asia/Jakarta';
      $timestamp = time();
      $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
      $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
      $jamsekarang=$dt->format('Y-m-d H:i:s');
      //END TIMEZONE
      $dataedit=$this->Mtryout->get_by_id($id);
      $waktu_mulai=$dataedit->waktu_mulai;
      $menit = round(abs(strtotime($jamsekarang) - strtotime($dataedit->waktu_mulai)) / 60,0);
      // //Interval
      // $date_a = new DateTime($waktu_mulai);
      // $date_b = new DateTime($jamsekarang);
      // $interval = date_diff($date_a,$date_b);
      // $durasi=$interval->format('%H.%i.%s');
      // $exDurasi=explode(".",$durasi);
      // $menit=$exDurasi[1];
      // // echo $exDurasi[0];
      // if($exDurasi[0]!='0'){
      //   $menit=($exDurasi[0]*60)+$menit;
      // }
      //Interval End
      $data = array(
        'contain_view' => 'guru/tryout/try_out_edit',
        'sidebar'=>'guru/sidebar',//Ini buat menu yang ditampilkan di module guru {DIKIRIM KE TEMPLATE}
        'css'=>'guru/tryout/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'guru/tryout/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'guru/tryout/update_action',
        'dataedit'=>$dataedit,
        'menit'=>$menit,
        'module'=>'guru',
        'titlePage'=>'try out',
        'controller'=>'tryout'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
          		'tanggal' => $this->input->post('tanggal',TRUE),
          		'skor' => $this->input->post('skor',TRUE),
          		'waktu_mulai' => $this->input->post('waktu_mulai',TRUE),
          		'waktu_selesai' => $this->input->post('waktu_selesai',TRUE),
          		'pil_1' => $this->input->post('pil_1',TRUE),
          		'pil_2' => $this->input->post('pil_2',TRUE),
          		'pil_3' => $this->input->post('pil_3',TRUE),
          		'nis' => $this->input->post('nis',TRUE),
          		'kode_soal' => $this->input->post('kode_soal',TRUE),
          	);

            $this->Mtryout->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('guru/tryout'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id', TRUE));
        } else {
          $nis=$this->input->post('nis');
          $menit_mulai=$this->input->post('menit_mulai');
          //TIMEZON php
          $tz = 'Asia/Jakarta';
          $timestamp = time();
          $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
          $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
          $jamsekarang=$dt->format('Y-m-d H:i:s');
          //END TIMEZONE
          $jamsekarang=$dt->modify("-$menit_mulai minutes");
          $jamsekarang=$dt->format('Y-m-d H:i:s');
          $waktu_mulai=$jamsekarang;

            $data = array(
		'waktu_mulai' => $waktu_mulai,
		'waktu_selesai' => NULL,
	    );


            $this->Mtryout->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Reset Waktu Try Out Berhasil');
            redirect(site_url('guru/siswa/tryout?nis='.$nis));
        }
    }

    public function delete($id)
    {
        $row = $this->Mtryout->get_by_id($id);

        if ($row) {
            $this->Mtryout->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('guru/tryout'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('guru/tryout'));
        }
    }

    public function data_tryout(){
      if (isset($_GET['id_to'])) {
        $id_to = $this->input->get('id_to');
        $cek   = $this->Mtryout->cekData('id',$id_to,'try_out');
        if ($cek) {
          $data_siswa = $this->Mtryout->getSiswa_byTO($id_to);
          // Load plugin PHPExcel nya
          include APPPATH.'third_party/PHPExcel/PHPExcel.php';

          // Panggil class PHPExcel nya
          $excel = new PHPExcel();
          // Settingan awal fil excel
          $excel->getProperties()->setCreator('Kostlab Team')
                 ->setLastModifiedBy('Kostlab Team')
                 ->setTitle("Data Tryout Siswa")
                 ->setSubject("Siswa")
                 ->setDescription("Laporan Tryout Data Siswa")
                 ->setKeywords("Tryout Siswa");
          // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
          $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
              'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
              'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
              'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
              'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            ),
            'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => 'A6A6A6')
            )
          );
          // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
          $style_row = array(
            'alignment' => array(
              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
              'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
              'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
              'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
              'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
          );

          /** Disini untuk membuat judul **/
          $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA Tryout"); // Set kolom A1 dengan tulisan "DATA Tryout"
          $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
          $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
          $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(18); // Set font size 15 untuk kolom A1
          $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
          /** end membuat judul **/

          /**Disini membuat tabel data siswa**/
          // Buat header tabel nya pada baris ke 3
          $excel->setActiveSheetIndex(0)->setCellValue('A3', "nis"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('A4', "Nama"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('A5', "Sekolah"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('A6', "Kelas"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('A7', "Cabang"); // Set kolom
          // Apply style header yang telah kita buat tadi ke masing-masing kolom header
          $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('A4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('A5')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('A6')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('A7')->applyFromArray($style_col);
          // Buat data siswa tabel pada baris ke 4
          $excel->setActiveSheetIndex(0)->setCellValue('B3', "$data_siswa->nis"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('B4', "$data_siswa->nama"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('B5', "$data_siswa->sekolah"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('B6', "$data_siswa->kode_kelas"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('B7', "$data_siswa->cabang"); // Set kolom
          // Apply style
          $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('B4')->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('B5')->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('B6')->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('B7')->applyFromArray($style_row);
          /** end membuat tabel data siswa **/

          /** Disini membuat tabel data TO **/
          // Buat hjudul tabel
          $excel->setActiveSheetIndex(0)->setCellValue('D3', "Tanggal TO");
          $excel->setActiveSheetIndex(0)->setCellValue('D4', "Skor TO");
          $excel->setActiveSheetIndex(0)->setCellValue('D5', "Waktu Mulai");
          $excel->setActiveSheetIndex(0)->setCellValue('D6', "Waktu Selesai");
          $excel->setActiveSheetIndex(0)->setCellValue('D7', "Pilihan 1");
          $excel->setActiveSheetIndex(0)->setCellValue('D8', "Pilihan 1");
          $excel->setActiveSheetIndex(0)->setCellValue('D9', "Pilihan 1");
          // Apply style header
          $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('D4')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('D5')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('D6')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('D7')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('D8')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('D9')->applyFromArray($style_col);
          // Buat data tabel
          $data_to = $this->Mtryout->gettryout_byid($id_to);
          $excel->setActiveSheetIndex(0)->setCellValue('E3', "$data_to->tanggal");
          $excel->setActiveSheetIndex(0)->setCellValue('E4', "$data_to->skor");
          $excel->setActiveSheetIndex(0)->setCellValue('E5', "$data_to->waktu_mulai");
          $excel->setActiveSheetIndex(0)->setCellValue('E6', "$data_to->waktu_selesai");
          // Apply style row
          $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('E4')->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('E5')->applyFromArray($style_row);
          $excel->getActiveSheet()->getStyle('E6')->applyFromArray($style_row);

          if ($data_to->pil_1 != "") {
            $pg_1 = $this->Mtryout->getDataPassGrade($data_to->pil_1);
            $excel->setActiveSheetIndex(0)->setCellValue('E7', "[$pg_1->pass_grade] $pg_1->nama - $pg_1->jurusan $pg_1->tahun");
            $excel->getActiveSheet()->getStyle('E7')->applyFromArray($style_row);
          }else {
            $excel->setActiveSheetIndex(0)->setCellValue('E7', "");
            $excel->getActiveSheet()->getStyle('E7')->applyFromArray($style_row);
          }

          if ($data_to->pil_2 != "") {
            $pg_2 = $this->Mtryout->getDataPassGrade($data_to->pil_2);
            $excel->setActiveSheetIndex(0)->setCellValue('E8', "[$pg_2->pass_grade] $pg_2->nama - $pg_2->jurusan $pg_2->tahun");
            $excel->getActiveSheet()->getStyle('E8')->applyFromArray($style_row);
          }else {
            $excel->setActiveSheetIndex(0)->setCellValue('E8', "");
            $excel->getActiveSheet()->getStyle('E8')->applyFromArray($style_row);
          }

          if ($data_to->pil_3 != "") {
            $pg_3 = $this->Mtryout->getDataPassGrade($data_to->pil_3);
            $excel->setActiveSheetIndex(0)->setCellValue('E9', "[$pg_3->pass_grade] $pg_3->nama - $pg_3->jurusan $pg_3->tahun");
            $excel->getActiveSheet()->getStyle('E9')->applyFromArray($style_row);
          }else {
            $excel->setActiveSheetIndex(0)->setCellValue('E9', "");
            $excel->getActiveSheet()->getStyle('E9')->applyFromArray($style_row);
          }
          /** end membuat tabel data TO **/

          /** Disini membuat tabel jawaban siswa **/
          // Buat header tabel nya
          $excel->setActiveSheetIndex(0)->setCellValue('A14', "Nomor"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('B14', "Mata Pelajaran"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('C14', "Jawaban Siswa"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('D14', "Kunci Jawaban"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('E14', "Hasil"); // Set kolom
          // Apply style header
          $excel->getActiveSheet()->getStyle('A14')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('B14')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('C14')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('D14')->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('E14')->applyFromArray($style_col);

          //membuat tada tabelnya
          $api = new ClientAPI();
          $jawaban = json_decode($api->get(base_url()."api/tryout/list_jawaban?id_to=$id_to"));
          $mapel   = json_decode($api->get(base_url()."api/tryout/list_mapel?id_to=$id_to"));

          $no = 1; // Untuk penomoran tabel, di awal set dengan 1
          $numrow = 15; // Set baris pertama untuk isi tabel adalah baris ke 15
          foreach($jawaban->results as $data){ // Lakukan looping
            $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $data->nomor);
            $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nama);
            $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->jawaban_siswa);
            $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->kunci_jawaban);
            if ($data->jawaban_siswa == "") {
              $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, "B");
            }elseif ($data->jawaban_siswa == $data->kunci_jawaban) {
              $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, "T");
            }else {
              $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, "F");
            }


            // Apply style row
            $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);

            $no++; // Tambah 1 setiap kali looping
            $numrow++; // Tambah 1 setiap kali looping
          }
          /** end membuat tabel jawaban siswa **/

          /** Disini membuat tabel jawaban siswa per mapel **/
          $numrow = $numrow + 3;
          // Buat header tabel nya
          $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, "Nama Mapel"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, "Jumlah Benar"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, "Jumlah Salah"); // Set kolom
          $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, "Jumlah Kosong"); // Set kolom

          // Apply style header yang telah kita buat tadi ke masing-masing kolom header
          $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_col);
          $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_col);

          $numrow++;
          //membuat tada tabelnya
          foreach($mapel->results as $data){ // Lakukan looping pada variabel siswa
            $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $data->nama_mapel);
            $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->benar);
            $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->salah);
            $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->kosong);

            // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
            $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);

            $no++; // Tambah 1 setiap kali looping
            $numrow++; // Tambah 1 setiap kali looping
          }
          /** end membuat tabel jawaban siswa **/

          // Set width kolom
          $excel->getActiveSheet()->getColumnDimension('A')->setWidth(15); // Set width kolom A
          $excel->getActiveSheet()->getColumnDimension('B')->setWidth(30); // Set width kolom B
          $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
          $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
          $excel->getActiveSheet()->getColumnDimension('E')->setWidth(60); // Set width kolom E

          // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
          $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
          // Set orientasi kertas jadi LANDSCAPE
          $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
          // Set judul file excel nya
          $excel->getActiveSheet(0)->setTitle("Laporan Data TO $data_siswa->nama");
          $excel->setActiveSheetIndex(0);
          // Proses file excel
          header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          header('Content-Disposition: attachment; filename="Tryout Siswa - "'.$data_siswa->nama.'".xlsx"'); // Set nama file excel nya
          header('Cache-Control: max-age=0');
          $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
          $write->save('php://output');
        }else {
          echo "Data Tryout tidak ada";
        }
      }else {
        redirect(site_url('guru'));
      }
  }

  public function _rules()
  {
    	// $this->form_validation->set_rules('waktu_mulai', 'waktu mulai', 'trim|required');
    	$this->form_validation->set_rules('nis', 'nis', 'trim|required');

    	$this->form_validation->set_rules('id', 'id', 'trim');
    	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
  }

}
