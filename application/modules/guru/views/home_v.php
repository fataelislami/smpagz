<div class="row">
  <div class="col-md-12">
      <div class="card">
          <div class="card-body">
              <h3>Dashboard Guru</h3>
          </div>
      </div>
  </div>
</div>

<div class="row">
    <!-- Column -->
    <div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-row">
                    <div class="round round-lg align-self-center round-info"><i class="ti-user"></i></div>
                    <div class="m-l-10 align-self-center">
                        <h3 class="m-b-0 font-light"><?php echo 5; ?></h3>
                        <h5 class="text-muted m-b-0">Data Siswa</h5></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-row">
                    <div class="round round-lg align-self-center round-warning"><i class="mdi mdi-account-star-variant"></i></div>
                    <div class="m-l-10 align-self-center">
                        <h3 class="m-b-0 font-lgiht"><?php echo 5; ?></h3>
                        <h5 class="text-muted m-b-0">Data Guru</h5></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <!-- Column -->
    <div class="col-lg-4 col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-row">
                    <div class="round round-lg align-self-center round-danger"><i class="mdi mdi-glassdoor"></i></div>
                    <div class="m-l-10 align-self-center">
                        <h3 class="m-b-0 font-lgiht"><?php echo 5 ?></h3>
                        <h5 class="text-muted m-b-0">Data Kelas</h5></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>

<div class="row">
  <div class="col-md-12">
      <div class="card">
          <div class="card-body">
              <h3>Profil Guru</h3>
          </div>
      </div>
  </div>
</div>
