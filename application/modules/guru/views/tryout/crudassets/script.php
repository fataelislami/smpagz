<script src="<?php echo base_url()?>assets/plugins/summernote/dist/summernote.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
<script>
jQuery(document).ready(function() {

    $('.summernote').summernote({
        height: 350, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: false // set focus to editable area after initializing summernote
    });

    $('.inline-editor').summernote({
        airMode: true
    });

});

window.edit = function() {
        $(".click2edit").summernote()
    },
    window.save = function() {
        $(".click2edit").summernote('destroy');
    }
</script>

<!-- This is data table -->
<script src="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<script>
$(document).ready(function() {
    $('#myTable').DataTable();
    $(document).ready(function() {
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function() {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
});
$('#tabletryout').DataTable({
    dom: 'Bfrtip',
    buttons: [
      'copy'
    ],
    "order": [
        [2, 'asc']
    ]
});
</script>
<script src="<?php echo base_url()?>assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
<script type="text/javascript">
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
  var randomString=makeid();
</script>
<script src="<?php echo base_url()?>assets/plugins/dropzone-master/dist/dropzone.js"></script>
<script type="text/javascript">
$('#aplot').click(function() {
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#tabletryout").on("click", ".modalDelete", function(){//event khusus untuk table datatables setelah pagination suka error
      var id=$(this).val();
      console.log(id);
      $("#modalMsg").html("Apakah Anda Yakin Ingin Reset Try Out Ini ("+id+") ? ");
      $("#modalHref").attr("href", "<?php echo base_url().$module?>/<?php echo $controller; ?>/delete/"+id);
    });
  });
</script>

<script type="text/javascript">

  $(document).ready(function(){
    var id;
      // code to read selected table row cell data (values).
      $("#tabletryout").on('click','.btnDetail',function(){
          // get the current row
           var currentRow=$(this).closest("tr");
           var col1=currentRow.find("td:eq(0)").text();
           id = col1;

           var settings = {
              "async": true,
              "crossDomain": true,
              "url": "<?php echo base_url()?>api/tryout/info?id="+id,
              "method": "GET",
              "headers": {
                "cache-control": "no-cache",
                "Postman-Token": "3cfb89a8-bf7f-48bc-8827-6fcd42933dd0"
              }
            }

            $.ajax(settings).done(function (data) {
              if (data.total_result > 0) {
                $('#d_kode').html(data.results.kode_soal);
                $('#d_nise').html(data.results.nise);
                $('#d_nama').html(data.results.nama);
                $('#d_sekolah').html(data.results.sekolah);
                $('#d_cabang').html(data.results.cabang);
                if (data.results.pil_1 != "") {
                  $('#d_pil1').html("["+data.results.pil_1.pass_grade+"]  "+data.results.pil_1.jurusan+" - "+data.results.pil_1.nama);
                }
                if (data.results.pil_2 != "") {
                  $('#d_pil2').html("["+data.results.pil_2.pass_grade+"]  "+data.results.pil_2.jurusan+" - "+data.results.pil_2.nama);
                }
              }
            });

            $('#mymodal1').modal('show');
          });

          $("#tabletryout").on('click','.btnDetailUsers',function(){
              // get the current row
               var currentRow=$(this).closest("tr");
               var col1=currentRow.find("td:eq(0)").text();
               id = col1;

               var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": "<?php echo base_url()?>api/tryout/infoUsers?id="+id+"users=true",
                  "method": "GET",
                  "headers": {
                    "cache-control": "no-cache",
                    "Postman-Token": "3cfb89a8-bf7f-48bc-8827-6fcd42933dd0"
                  }
                }

                $.ajax(settings).done(function (data) {
                  if (data.total_result > 0) {
                    $('#d_kode').html(data.results.kode_soal);
                    $('#d_nama').html(data.results.nama);
                    $('#d_sekolah').html(data.results.sekolah);
                    $('#d_cabang').html(data.results.cabang);
                    if (data.results.pil_1 != "") {
                      $('#d_pil1').html("["+data.results.pil_1.pass_grade+"]  "+data.results.pil_1.jurusan+" - "+data.results.pil_1.nama);
                    }
                    if (data.results.pil_2 != "") {
                      $('#d_pil2').html("["+data.results.pil_2.pass_grade+"]  "+data.results.pil_2.jurusan+" - "+data.results.pil_2.nama);
                    }
                  }
                });

                $('#mymodal1').modal('show');
              });
    });
</script>

<!-- DATE PICKER -->
<script src="<?php echo base_url()?>assets/plugins/moment/moment.js"></script>
<script src="<?php echo base_url()?>assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<!-- Clock Plugin JavaScript -->
<script src="<?php echo base_url()?>assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="<?php echo base_url()?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Date range Plugin JavaScript -->
<script src="<?php echo base_url()?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
// MAterial Date picker
    $('#waktu_mulai').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm:ss' });
    $('#waktu_selesai').bootstrapMaterialDatePicker({ format : 'YYYY-MM-DD HH:mm:ss' });

if (/mobile/i.test(navigator.userAgent)) {
    $('input').prop('readOnly', true);
}

</script>
