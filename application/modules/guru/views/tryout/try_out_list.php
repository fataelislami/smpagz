<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              <div class="row">
                  <div class="col-md-6">
                      <h4 class="card-title">Data TO</h4>
                      <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                  </div>
                  <!-- <div class="col-md-6 text-right">
                      <?php echo anchor(site_url($module.'/tryout/create'), '+ Tambah Data', 'class="btn btn-primary"'); ?>
      	          </div> -->
              </div>


                <div class="table-responsive m-t-40">
                    <table id="tabletryout" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>TANGGAL</th>
                                <?php if ($tampil): ?>
                                  <th>SKOR</th>
                                <?php endif; ?>
                                <th>WAKTU MULAI</th>
                                <th>WAKTU SELESAI</th>
                                <th>KODE SOAL</th>
                                <th>HASIL</th>
                                <th>EXPORT</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($datatable as $d): ?>
                            <tr>
                              <td><button class="btn btn-success waves-effect waves-light m-r-10 btnDetail"><?php echo $d->id; ?></button></td>
                              <td><?php echo $d->tanggal; ?></td>
                              <?php if ($tampil): ?>
                                <td><?php echo $d->skor; ?></td>
                              <?php endif; ?>
                              <td><?php echo $d->waktu_mulai; ?></td>
                              <td><?php echo $d->waktu_selesai; ?></td>

                              <td><?php echo $d->kode_soal; ?></td>
                              <td>
                                <a href="<?php echo base_url()."guru/jawabantryout?id_to=".$d->id ?>">
                                  <button class="btn btn-success">JAWABAN</button>
                                </a>

                              </td>
                              <td>
                                <a href="<?php echo base_url()."guru/tryout/data_tryout?id_to=".$d->id ?>">
                                  <button class="btn btn-info">Excel Report</button>
                                </a></td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>

                <!-- sample modal content -->
                  <div id="mymodal1" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                      <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <h4 class="modal-title" id="myLargeModalLabel">Detai Data Try Out</h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                              </div>
                              <div class="modal-body">
                                  <h4>Hasil TO siswa</h4>
                                  <table class="tablesaw table-bordered table-hover table" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch>
                                    <div id="info_lengkap">
                                      <tbody>
                                          <tr>
                                              <th class="title" width='30%'>Kode Soal</th>
                                              <td id="d_kode">.....</td>
                                          </tr>
                                          <tr>
                                              <th class="title">NISE</th>
                                              <td id="d_nise">......</td>
                                          </tr>
                                          <tr>
                                              <th class="title">Nama</th>
                                              <td id="d_nama">.....</td>
                                          </tr>
                                          <tr>
                                              <th class="title">Sekolah</th>
                                              <td id="d_sekolah">......</td>
                                          </tr>
                                          <tr>
                                              <th class="title">Cabang</th>
                                              <td id="d_cabang">......</td>
                                          </tr>
                                          <tr>
                                              <th class="title">Pilihan 1</th>
                                              <td id="d_pil1">......</td>
                                          </tr>
                                          <tr>
                                              <th class="title">Pilihan 2</th>
                                              <td id="d_pil2">......</td>
                                          </tr>
                                      </tbody>
                                    </div>
                                </table>
                              </div>
                              <div class="modal-footer">
                                  <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                              </div>
                          </div>
                          <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                  </div>
                  <!-- /.modal -->

            </div>
        </div>
    </div>
</div>
<div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              <p id="modalMsg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                <a id="modalHref" href="#">
                <button type="button" class="btn btn-danger waves-effect waves-light">Ya!</button>
                </a>
            </div>
        </div>
    </div>
</div>
