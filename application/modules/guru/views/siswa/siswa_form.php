<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Siswa</h4>
            <form class="form-material m-t-40" method="post" action="<?php echo base_url().$action ?>">
              <div class="form-group">
                      <label>nis</label>
                      <input type="text" name="nis" class="form-control" placeholder="">
              </div>
	  <div class="form-group">
            <label>nama</label>
            <input type="text" name="nama" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
      <label>jenis kelamin</label>
      <select class="form-control" name="gender">
          <option value="P">Perempuan</option>
          <option value="L">Laki-laki</option>
      </select>
    </div>
	  <div class="form-group">
            <label>alamat</label>
            <input type="text" name="alamat" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
      <label>kelas</label>
      <select class="form-control" name="id_kelas">
        <option value=""> --- pilih ---</option>
        <?php if($kelas->num_rows()>0){ ?>
        <?php foreach ($kelas->result() as $key): ?>
          <option value="<?php echo $key->kode; ?>">kelas <?php echo $key->nama; ?></option>
        <?php endforeach; ?>
        <?php } ?>
      </select>
    </div>
	  <div class="form-group">
            <label>password</label>
            <input type="text" name="password" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>email</label>
            <input type="text" name="email" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
      <label>orang tua</label>
      <select class="form-control" name="id_ortu">
        <option value=""> --- pilih ---</option>
        <?php if($orang_tua->num_rows()>0){ ?>
        <?php foreach ($orang_tua->result() as $key): ?>
          <option value="<?php echo $key->id; ?>"><?php echo $key->nama_ayah." ".$key->nama_ibu; ?></option>
        <?php endforeach; ?>
        <?php } ?>
      </select>
    </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
