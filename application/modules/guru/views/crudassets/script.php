<script src="<?php echo base_url()?>assets/plugins/summernote/dist/summernote.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
<script>
jQuery(document).ready(function() {

    $('.summernote').summernote({
        height: 350, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: false // set focus to editable area after initializing summernote
    });

    $('.inline-editor').summernote({
        airMode: true
    });

});

window.edit = function() {
        $(".click2edit").summernote()
    },
    window.save = function() {
        $(".click2edit").summernote('destroy');
    }
</script>

<!-- This is data table -->
<script src="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<script>
$(document).ready(function() {
    $('#myTable').DataTable();
    $(document).ready(function() {
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="6">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function() {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
});
$('#example23').DataTable({
    dom: 'Bfrtip',
    buttons: [
      'copy'
    ],
});
$('#example24').DataTable({
    dom: 'Bfrtip',
    buttons: [
      'copy'
    ],
    "order": [
        [1, 'desc']
    ]
});
</script>
<script src="<?php echo base_url()?>assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
<script type="text/javascript">
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
  var randomString=makeid();
</script>
<script src="<?php echo base_url()?>assets/plugins/dropzone-master/dist/dropzone.js"></script>
<script type="text/javascript">
$('#aplot').click(function() {
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#example23").on("click", ".modalDelete", function(){//event khusus untuk table datatables setelah pagination suka error
      var id=$(this).val();
      console.log(id);
      $("#modalMsg").html("Apakah Anda Yakin Ingin Menghapus data "+id+" ? ");
      $("#modalHref").attr("href", "<?php echo base_url().$module?>/<?php echo $controller; ?>/delete/"+id);
    });
    $("#example24").on("click", ".modalDelete", function(){//event khusus untuk table datatables setelah pagination suka error
      var id=$(this).val();
      console.log(id);
      $("#modalMsg").html("Apakah Anda Yakin Ingin Menghapus data "+id+" ? ");
      $("#modalHref").attr("href", "<?php echo base_url().$module?>/<?php echo $controller; ?>/delete/"+id);
    });
    $("#example23").on("click", ".modalExport", function(){//event khusus untuk table datatables setelah pagination suka error
      var id=$(this).val();
      console.log(id);
      $("#siswaHref").attr("href", "<?php echo base_url().$module?>/export_v2/excel?event_id="+id+"&edulab=1");
      $("#usersHref").attr("href", "<?php echo base_url().$module?>/export_v2/excel?event_id="+id+"&edulab=0");
      $("#eksternalHref").attr("href", "<?php echo base_url().$module?>/export_v2/excel?event_id="+id+"&edulab=2");
    });
  });
</script>
<!-- jQuery file upload -->
    <script src="<?php echo base_url()?>assets/plugins/dropify/dist/js/dropify.min.js"></script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events Covers
        var drEventEventCover = $('#event_image_dropify').dropify();
        var initImagesCover=null;
        drEventEventCover.on('dropify.beforeClear', function(event, element) {
            console.log(element.file.name);
            initImagesCover=element.file.name;
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEventEventCover.on('dropify.afterClear', function(event, element) {
          if(element.file.name==null){
            $("#deleteImagesCover").val(initImagesCover);
          }else{
            console.log('not deleted');
          }
            alert('File deleted');
            console.log($("#deleteImagesCover").val());
        });

        console.log($("#deleteImagesCover").val());

        drEventEventCover.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        // Used events Covers
        var drEventEventMap = $('#map_layout_dropify').dropify();
        var initImagesMap=null;
        drEventEventMap.on('dropify.beforeClear', function(event, element) {
            console.log(element.file.name);
            initImagesMap=element.file.name;
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEventEventMap.on('dropify.afterClear', function(event, element) {
          if(element.file.name==null){
            $("#deleteImagesMap").val(initImagesMap);
          }else{
            console.log('not deleted');
          }
            alert('File deleted');
            console.log($("#deleteImagesMap").val());
        });

        console.log($("#deleteImagesMap").val());

        drEventEventMap.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        // Used events Covers
        var drEventEventCover = $('#hero_image_dropify').dropify();
        var initImagesCover=null;
        drEventEventCover.on('dropify.beforeClear', function(event, element) {
            console.log(element.file.name);
            initImagesCover=element.file.name;
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEventEventCover.on('dropify.afterClear', function(event, element) {
          if(element.file.name==null){
            $("#deleteImagesHero").val(initImagesCover);
          }else{
            console.log('not deleted');
          }
            alert('File deleted');
            console.log($("#deleteImagesHero").val());
        });

        console.log($("#deleteImagesCover").val());

        drEventEventCover.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        //Used events Logo
        var drEventEventCover = $('#logo_image_dropify').dropify();
        var initImagesCover=null;
        drEventEventCover.on('dropify.beforeClear', function(event, element) {
            console.log(element.file.name);
            initImagesCover=element.file.name;
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEventEventCover.on('dropify.afterClear', function(event, element) {
          if(element.file.name==null){
            $("#deleteImagesLogo").val(initImagesCover);
          }else{
            console.log('not deleted');
          }
            alert('File deleted');
            console.log($("#deleteImagesLogo").val());
        });

        console.log($("#deleteImagesLogo").val());

        drEventEventCover.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        //Used events Sponsor
        var drEventEventCover = $('#sponsor_image_dropify').dropify();
        var initImagesCover=null;
        drEventEventCover.on('dropify.beforeClear', function(event, element) {
            console.log(element.file.name);
            initImagesCover=element.file.name;
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEventEventCover.on('dropify.afterClear', function(event, element) {
          if(element.file.name==null){
            $("#deleteImagesSponsor").val(initImagesCover);
          }else{
            console.log('not deleted');
          }
            alert('File deleted');
            console.log($("#deleteImagesSponsor").val());
        });

        console.log($("#deleteImagesSponsor").val());

        drEventEventCover.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });


        // Used events Covers
        var drEventEventCover = $('#photoUser').dropify();
        var initImagesCover=null;
        drEventEventCover.on('dropify.beforeClear', function(event, element) {
            console.log(element.file.name);
            initImagesCover=element.file.name;
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEventEventCover.on('dropify.afterClear', function(event, element) {
          if(element.file.name==null){
            $("#deletePhoto").val(initImagesCover);
          }else{
            console.log('not deleted');
          }
            alert('File deleted');
            console.log($("#deletePhoto").val());
        });

        console.log($("#deletePhoto").val());

        drEventEventCover.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        });
    });
    </script>
