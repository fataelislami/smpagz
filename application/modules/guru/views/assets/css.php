<link href="<?php echo base_url();?>assets/plugins/c3-master/c3.min.css" rel="stylesheet">
<!-- Vector CSS -->
<link href="<?php echo base_url();?>assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet"/>
<link href="<?php echo base_url();?>assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">

<!-- Ini untuk semua css yang di load pada page masing masing controller di dalam module -->
<!--
Coba dicek di module template -> folder view -> full.php
cek line ke : 22
 -->
