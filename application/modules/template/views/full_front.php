<!DOCTYPE html>
<html class="transition-navbar-scroll top-navbar-xlarge bottom-footer" lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SMPI Al-Ghozali</title>

  <!-- Vendor CSS BUNDLE
    Includes styling for all of the 3rd party libraries used with this module, such as Bootstrap, Font Awesome and others.
    TIP: Using bundles will improve performance by reducing the number of network requests the client needs to make when loading the page. -->
  <link href="<?= base_url() ?>assets-portal/css/vendor/all.css" rel="stylesheet">

  <!-- Vendor CSS Standalone Libraries
        NOTE: Some of these may have been customized (for example, Bootstrap).
        See: src/less/themes/{theme_name}/vendor/ directory -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/bootstrap.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/font-awesome.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/picto.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/material-design-iconic-font.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/datepicker3.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/jquery.minicolors.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/railscasts.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/owl.carousel.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/slick.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/daterangepicker-bs3.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/jquery.bootstrap-touchspin.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/select2.css" rel="stylesheet"> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/vendor/jquery.countdown.css" rel="stylesheet"> -->

  <!-- APP CSS BUNDLE [css/app/app.css]
INCLUDES:
    - The APP CSS CORE styling required by the "html" module, also available with main.css - see below;
    - The APP CSS STANDALONE modules required by the "html" module;
NOTE:
    - This bundle may NOT include ALL of the available APP CSS STANDALONE modules;
      It was optimised to load only what is actually used by the "html" module;
      Other APP CSS STANDALONE modules may be available in addition to what's included with this bundle.
      See src/less/themes/html/app.less
TIP:
    - Using bundles will improve performance by greatly reducing the number of network requests the client needs to make when loading the page. -->
  <link href="<?= base_url() ?>assets-portal/css/app/app.css" rel="stylesheet">

  <!-- App CSS CORE
This variant is to be used when loading the separate styling modules -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/main.css" rel="stylesheet"> -->

  <!-- App CSS Standalone Modules
    As a convenience, we provide the entire UI framework broke down in separate modules
    Some of the standalone modules may have not been used with the current theme/module
    but ALL modules are 100% compatible -->

  <!-- <link href="<?= base_url() ?>assets-portal/css/app/essentials.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/material.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/layout.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/sidebar.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/sidebar-skins.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/navbar.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/messages.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/media.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/charts.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/maps.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/colors-alerts.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/colors-background.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/colors-buttons.css" rel="stylesheet" /> -->
  <!-- <link href="<?= base_url() ?>assets-portal/css/app/colors-text.css" rel="stylesheet" /> -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries
WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!-- If you don't need support for Internet Explorer <= 8 you can safely remove these -->
  <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<?php if(isset($css)){$this->load->view($css);} ?>

</head>

<body>

  <!-- Fixed navbar -->
  <div class="navbar navbar-default navbar-fixed-top navbar-size-large navbar-size-xlarge paper-shadow" data-z="0" data-animated role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-nav">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="navbar-brand navbar-brand-logo">
          <a class="svg" href="<?= base_url() ?>">
            <img src="<?= base_url() ?>xfile/logo.png" alt="" height="65">
          </a>
        </div>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="main-nav">
        <ul class="nav navbar-nav navbar-nav-margin-left">
          <?php
          if($this->session->userdata('id_try_out')){ ?>
            <li class="">
              <a href="#">Dashboard</a>
            </li>
            <?php }else{?>
              <li class="">
                <a href="<?= base_url()?>portal/dashboard">Dashboard</a>
              </li>
            <?php } ?>
          <li class="">
            <a href="<?= base_url()?>portal/tryout/go">Try Out</a>
          </li>
          <!-- <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="index.html">Home page</a></li>
              <li><a href="pricing.html">Pricing</a></li>
              <li><a href="tutors.html">Tutors</a></li>
              <li><a href="survey.html">Survey</a></li>
              <li><a href="website-forum.html">Forum Home</a></li>
              <li><a href="website-forum-category.html">Forum Category</a></li>
              <li><a href="website-forum-thread.html">Forum Thread</a></li>
              <li><a href="website-blog.html">Blog Listing</a></li>
              <li><a href="website-blog-post.html">Blog Post</a></li>
              <li><a href="website-contact.html">Contact</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="website-directory-grid.html">Grid Directory</a></li>
              <li><a href="website-directory-list.html">List Directory</a></li>
              <li><a href="website-course.html">Single Course</a></li>
            </ul>
          </li>
          <li class="dropdown active">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Student <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="active"><a href="website-student-dashboard.html">Dashboard</a></li>
              <li><a href="website-student-courses.html">My Courses</a></li>
              <li><a href="website-take-course.html">Take Course</a></li>
              <li><a href="website-course-forums.html">Course Forums</a></li>
              <li><a href="website-take-quiz.html">Take Quiz</a></li>
              <li><a href="website-student-messages.html">Messages</a></li>
              <li><a href="website-student-profile.html">Private Profile</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Instructor <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="website-instructor-dashboard.html">Dashboard</a></li>
              <li><a href="website-instructor-courses.html">My Courses</a></li>
              <li><a href="website-instructor-course-edit-course.html">Edit Course</a></li>
              <li><a href="website-instructor-earnings.html">Earnings</a></li>
              <li><a href="website-instructor-statement.html">Statement</a></li>
              <li><a href="website-instructor-messages.html">Messages</a></li>
              <li><a href="website-instructor-profile.html">Private Profile</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">UI <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="essential-buttons.html">Buttons</a></li>
              <li><a href="essential-icons.html">Icons</a></li>
              <li><a href="essential-progress.html">Progress</a></li>
              <li><a href="essential-grid.html">Grid</a></li>
              <li><a href="essential-forms.html">Forms</a></li>
              <li><a href="essential-tables.html">Tables</a></li>
              <li><a href="essential-tabs.html">Tabs</a></li>
            </ul>
          </li> -->
        </ul>
        <div class="navbar-right">
          <?php if(($this->session->userdata('status')=='login' && $this->session->userdata('level')=='users') OR ($this->session->userdata('status')=='login' && $this->session->userdata('level')=='siswa')){ ?>
          <ul class="nav navbar-nav navbar-nav-bordered navbar-nav-margin-right">
            <!-- user -->
            <li class="dropdown user active">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <?php if ($this->session->userdata('level')=='users'): ?>
                  <img src="<?= base_url() ?>assets-portal/images/people/110/guy-6.jpg" alt="" class="img-circle" /> <?= $this->session->userdata('username'); ?><span class="caret"></span>
                <?php else: ?>
                  <img src="<?= base_url() ?>assets-portal/images/people/110/guy-6.jpg" alt="" class="img-circle" /> <?= $this->session->userdata('nama'); ?><span class="caret"></span>
                <?php endif; ?>
              </a>
              <ul class="dropdown-menu" role="menu">
                <?php
                if($this->session->userdata('id_try_out')){ ?>
                  <li><a href="#"><i class="fa fa-bar-chart-o"></i> Dashboard</a></li>
                  <li><a href="#"><i class="fa fa-mortar-board"></i> Aktivitas Tryout</a></li>
                  <li><a href="#"><i class="fa fa-user"></i> Profil</a></li>
                  <?php }else{?>
                    <li><a href="<?php echo base_url('portal/dashboard') ?>"><i class="fa fa-bar-chart-o"></i> Dashboard</a></li>
                    <li><a href="<?php echo base_url('portal/mycourses') ?>"><i class="fa fa-mortar-board"></i> Aktivitas Tryout</a></li>
                    <li><a href="<?php echo base_url('portal/profile') ?>"><i class="fa fa-user"></i> Profil</a></li>
                  <?php } ?>
                <li><a href="<?php echo base_url('login/logout') ?>"><i class="fa fa-sign-out"></i> Keluar</a></li>
              </ul>
            </li>
            <!-- // END user -->
          </ul>
          <a href="<?= base_url() ?>login/logout" class="navbar-btn btn btn-primary">Keluar</a>
        <?php }else{ ?>
          <a href="<?= base_url() ?>login" class="navbar-btn btn btn-primary">Masuk</a>
        <?php } ?>
        </div>
      </div>
      <!-- /.navbar-collapse -->

    </div>
  </div>
  <?php if($this->session->userdata('status')=='login' && $this->session->userdata('level')=='users'){ ?>
  <div class="parallax overflow-hidden bg-blue-400 page-section third">
    <div class="container parallax-layer" data-opacity="true">
      <div class="media v-middle">
        <div class="media-left text-center">
          <a href="#">
            <img src="<?= base_url() ?>assets-portal/images/people/110/guy-6.jpg" alt="people" class="img-circle width-80" />
          </a>
        </div>
        <div class="media-body">
          <h1 class="text-white text-display-1 margin-v-0"><?= $this->session->userdata('nama'); ?></h1>
          <p class="text-subhead"><a class="link-white text-underline" href="#"><?= $this->session->userdata('email'); ?></a></p>
        </div>
        <div class="media-right">
          <span class="label bg-blue-500">Siswa</span>
        </div>
      </div>
    </div>
  </div>
<?php } ?>

<?php if($this->session->userdata('status')=='login' && $this->session->userdata('level')=='siswa'){ ?>
<div class="parallax overflow-hidden bg-blue-400 page-section third">
  <div class="container parallax-layer" data-opacity="true">
    <div class="media v-middle">
      <div class="media-left text-center">
        <a href="#">
          <img src="<?= base_url() ?>assets-portal/images/people/110/guy-6.jpg" alt="people" class="img-circle width-80" />
        </a>
      </div>
      <div class="media-body">
        <h1 class="text-white text-display-1 margin-v-0"><?= $this->session->userdata('nama'); ?></h1>
        <p class="text-subhead"><a class="link-white text-underline" href="#"><?= $this->session->userdata('nise'); ?></a></p>
      </div>
      <div class="media-right">
        <span class="label bg-blue-500">Siswa</span>
      </div>
    </div>
  </div>
</div>
<?php } ?>

  <div class="container">

    <div class="page-section">
      <?php if(isset($content)){$this->load->view($content);} ?>
    </div>

  </div>

  <!-- Footer -->
  <footer class="footer">
    <strong>KP UNIKOM</strong> &copy; Copyright 2019
  </footer>
  <!-- // Footer -->

  <!-- Inline Script for colors and config objects; used by various external scripts; -->
  <script>
    var colors = {
      "danger-color": "#e74c3c",
      "success-color": "#81b53e",
      "warning-color": "#f0ad4e",
      "inverse-color": "#2c3e50",
      "info-color": "#2d7cb5",
      "default-color": "#6e7882",
      "default-light-color": "#cfd9db",
      "purple-color": "#9D8AC7",
      "mustard-color": "#d4d171",
      "lightred-color": "#e15258",
      "body-bg": "#f6f6f6"
    };
    var config = {
      theme: "html",
      skins: {
        "default": {
          "primary-color": "#ffcc00"
        }
      }
    };
  </script>

  <!-- Vendor Scripts Bundle
    Includes all of the 3rd party JavaScript libraries above.
    The bundle was generated using modern frontend development tools that are provided with the package
    To learn more about the development process, please refer to the documentation.
    Do not use it simultaneously with the separate bundles above. -->
  <script src="<?= base_url() ?>assets-portal/js/vendor/all.js"></script>

  <!-- Vendor Scripts Standalone Libraries -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/all.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/jquery.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/bootstrap.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/breakpoints.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/jquery.nicescroll.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/isotope.pkgd.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/packery-mode.pkgd.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/jquery.grid-a-licious.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/jquery.cookie.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/jquery-ui.custom.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/jquery.hotkeys.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/handlebars.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/jquery.hotkeys.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/load_image.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/jquery.debouncedresize.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/modernizr.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/core/velocity.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/tables/all.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/forms/all.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/media/slick.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/charts/flot/all.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/nestable/jquery.nestable.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/vendor/angular/all.js"></script> -->

  <!-- App Scripts Bundle
    Includes Custom Application JavaScript used for the current theme/module;
    Do not use it simultaneously with the standalone modules below. -->
  <script src="<?= base_url() ?>assets-portal/js/app/app.js"></script>

  <!-- App Scripts Standalone Modules
    As a convenience, we provide the entire UI framework broke down in separate modules
    Some of the standalone modules may have not been used with the current theme/module
    but ALL the modules are 100% compatible -->

  <!-- <script src="<?= base_url() ?>assets-portal/js/app/essentials.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/app/material.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/app/layout.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/app/sidebar.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/app/media.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/app/messages.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/app/maps.js"></script> -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/app/charts.js"></script> -->

  <!-- App Scripts CORE [html]:
        Includes the custom JavaScript for this theme/module;
        The file has to be loaded in addition to the UI modules above;
        app.js already includes main.js so this should be loaded
        ONLY when using the standalone modules; -->
  <!-- <script src="<?= base_url() ?>assets-portal/js/app/main.js"></script> -->
  <script src="<?= base_url() ?>assets-portal/js/swal.js" charset="utf-8"></script>
  <?php if(isset($js)){$this->load->view($js);} ?>
  <script type="text/javascript">//list-group
  $(function(){
      var current = window.location.href;
      $('.nav li a').each(function(){
          var $this = $(this);
          // if the current path is like this link, make it active
          if($this.attr('href')== current){
            // alert($this[1]);
            // if($this.parents('.sub-item').length==1){
            //   $this.parents('.sub-item').addClass('active');
            // }else{
              $this.parents('li').addClass('active');
            // }
          }
      })
  })
  $(function(){
      var current = window.location.href;
      $('.list-group li a').each(function(){
          var $this = $(this);
          // if the current path is like this link, make it active
          if($this.attr('href')== current){
            // alert($this[1]);
            // if($this.parents('.sub-item').length==1){
            //   $this.parents('.sub-item').addClass('active');
            // }else{
              $this.parents('.list-group-item').addClass('active');
            // }
          }
      })
  })
  </script>
  <script type="text/javascript">
  <?php if($this->session->flashdata('message')) {?>
    Swal.fire({
  type: 'error',
  title: 'Oops...',
  showConfirmButton: false,
  text: '<?= $this->session->flashdata('message') ?>',
  timer: 1200
  })
  <?php } ?>

  </script>
</body>

</html>
