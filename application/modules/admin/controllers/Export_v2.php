<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
  require_once 'class/ClientAPI.php';

class Export_v2 extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mexport'));
        if($this->session->userdata('status')!='login'){
          redirect(base_url('login'));
        }
        if($this->session->userdata('level')!='admin'){
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }
    }

    public function index()
    {
      echo "Export data :<br>";
      echo "- Data Siswa <br>";
      echo "- Output Jawaban <br>";
      echo "- Respon Jawaban <br>";
      echo "- Data Setelah TO <br>";
      echo "<a href='".base_url()."admin/export/excel?event_id=1'>link</a>";
    }

    public function excel(){
      if (isset($_GET['event_id'])) {
        $id_event = $this->input->get('event_id');  //get id event

        //get limit
        if (isset($_GET['limit'])) {
          $limit = $this->input->get('limit');
        }else {
          $limit = null;
        }

        //get offset
        if (isset($_GET['offset'])) {
          $offset = $this->input->get('offset');
        }else {
          $offset = null;
        }

        //get limit
        if (isset($_GET['edulab'])) {
          $edulab = $this->input->get('edulab');
        }else {
          $edulab = NULL;
        }


        $this->tryoutSiswa($id_event,$limit,$offset,$edulab);
        // if (isset($_GET['users']) AND $_GET['users']=='mitra') {
        //   $this->tryoutMitra($id_event,$limit,$offset);
        // }else {
        //   $this->tryoutSiswa($id_event,$limit,$offset);
        // }

      }else {
        redirect(base_url('Admin'));
      }
    }

    function tryoutSiswa($id_event,$limit,$offset,$edulab){
      //mendapatkan data event
      $where = array('id' => $id_event);
      $event = $this->Mexport->getdata($where,'event');

      if ($event->num_rows() > 0) {
          // get data event
          $event = $event->row();
          $event_nama = $event->nama;
          $event_tanggal = $event->tanggal;
          $event_soal = $event->soal_to;
          $filename = "$event_nama - $event_tanggal.xlsx";

          //membuat header data
          $header = array(
            'NO'=>'integer',
            'Kode_soal'=>'string',
            'NISE'=>'string',
            'Nama'=>'string',
            'Sekolah'=>'string',
            'Kelas'=>'string',
            'Cabang'=>'string',
            'Waktu Mulai' => 'string',
            'Kode Jurusan 1'=> 'string',
            'Nama PTN 1'=>'string',
            'Jurusan 1'=>'string',
            'PG 1'=>'string',
            'Kode Jurusan 2'=> 'string',
            'Nama PTN 2'=>'string',
            'Jurusan 2'=>'string',
            'PG 2'=>'string',
          );

          $list_soal = $this->Mexport->getJmlhSoal_v2($event_soal)->result();
          $list_mapel = $this->Mexport->getMatkul($event_soal);
          //header jawaban siswa
          foreach ($list_soal as $res) {
            $header["JWB $res->nomor"] = 'string';
          }
          //header respon jawaban
          foreach ($list_soal as $res) {
            $header["RES $res->nomor"] = 'string';
          }
          //header matkul
          foreach ($list_mapel as $res) {
            $header["$res->nama [Benar]"] = 'string';
            $header["$res->nama [Salah]"] = 'string';
            $header["$res->nama [Kosong]"] = 'string';
          }


          //membuat isi tabel
          $data = [];
          $number = 1;
          $data_tryout = $this->Mexport->getTo_byEvent_v2($id_event,$edulab)->result();
          //var_dump($data_tryout);die;
          foreach ($data_tryout as $res) {
            $temp = array(
              'NO'=> $number,
              'Kode_soal'=> $event_soal,
              'NISE'=> $res->nise,
              'Nama'=> $res->nama,
              'Sekolah'=> $res->sekolah,
              'Kelas'=> $res->kode_kelas,
              'cabang'=> $res->kode_cabang,
              'Waktu Mulai' => $res->waktu_mulai,
              'Kode Jurusan 1'=> $res->kode_jurusan1,
              'Nama PTN 1'=>$res->nama_univ1,
              'Jurusan 1'=> $res->jurusan_univ1,
              'PG 1'=> $res->pg_univ1,
              'Kode Jurusan 2'=> $res->kode_jurusan2,
              'Nama PTN 2'=> $res->nama_univ2,
              'Jurusan 2'=> $res->jurusan_univ2,
              'PG 2'=> $res->pg_univ2,
            );

            $jawaban = $this->Mexport->getJawaban2($res->id,NULL)->result();
            //mengisi jawaban siswa
            foreach ($jawaban as $j) {
              $temp["JWB $j->nomor"] = $j->jawaban_siswa;
            }
            //mengisi respon jawaban
            foreach ($jawaban as $j) {
              if ($j->jawaban_siswa == "") {
                $jwb = 'B';
              }elseif ($j->jawaban_siswa == $j->kunci_jawaban) {
                $jwb = 'T';
              }else {
                $jwb = 'F';
              }
              $temp["RES $j->nomor"] = "$jwb";
            }
            //mengisi jmlh benar,salah,kosong permatkul
            foreach ($list_mapel as $list) {
              $id = $list->id;
              $result = $this->Mexport->getJawaban2($res->id,$id)->result();
              $benar = 0; $salah = 0; $kosong = 0;

              //menghitung benar,salah,kosong
              foreach ($result as $a) {
                if ($a->jawaban_siswa == NULL) {
                  $kosong++;
                }elseif ($a->jawaban_siswa == $a->kunci_jawaban) {
                  $benar++;
                }else {
                  $salah++;
                }
              }

              $temp["Benar $id"] = "$benar";
              $temp["Salah $id"] = "$salah";
              $temp["Kosong $id"] = "$kosong";
            }


            array_push($data,$temp);
            $number++;
          }

          $this->export($header,$data,$filename);
      }else {
        echo "Event tidak ada";
      }
    }

    function tryoutMitra($id_event,$limit,$offset){
      //mendapatkan data event
      $where = array('id' => $id_event);
      $event = $this->Mexport->getdata($where,'event');

      if ($event->num_rows() > 0) {
          $event = $event->row();
          $event_nama = $event->nama;
          $event_tanggal = $event->tanggal;
          $event_soal = $event->soal_to;
          $filename = "$event_nama - $event_tanggal.xlsx";

          //membuat header data
          $header = array(
            'NO'=>'integer',
            'Kode_soal'=>'string',
            'NISE'=>'string',
            'Nama'=>'string',
            'Sekolah'=>'string',
            'Kelas'=>'string',
            'Cabang'=>'string',
            'Waktu Mulai' => 'string',
            'Kode Jurusan 1'=> 'string',
            'Nama PTN 1'=>'string',
            'Jurusan 1'=>'string',
            'PG 1'=>'string',
            'Kode Jurusan 2'=> 'string',
            'Nama PTN 2'=>'string',
            'Jurusan 2'=>'string',
            'PG 2'=>'string',
          );

          $list_soal = $this->Mexport->getJmlhSoal_v2($event_soal)->result();
          $list_mapel = $this->Mexport->getMatkul($event_soal);

          //header jawaban siswa
          foreach ($list_soal as $res) {
            $header["JWB $res->nomor"] = 'string';
          }
          //header respon jawaban
          foreach ($list_soal as $res) {
            $header["RES $res->nomor"] = 'string';
          }
          //header matkul
          foreach ($list_mapel as $res) {
            $header["$res->nama [Benar]"] = 'string';
            $header["$res->nama [Salah]"] = 'string';
            $header["$res->nama [Kosong]"] = 'string';
          }


          //membuat isi tabel
          $data = [];
          $number = 1;
          $data_tryout = $this->Mexport->getToMitra_byEvent_v2($id_event)->result();

          //var_dump($data_tryout);die;
          foreach ($data_tryout as $res) {
            $temp = array(
              'NO'=> $number,
              'Kode_soal'=> $event_soal,
              'NISE'=> "",
              'Nama'=> $res->nama,
              'Sekolah'=> $res->sekolah,
              'Kelas'=> $res->kelas,
              'cabang'=> "",
              'Waktu Mulai' => $res->waktu_mulai,
              'Nama PTN 1'=>$res->nama_univ1,
              'Jurusan 1'=> $res->jurusan_univ1,
              'PG 1'=> $res->pg_univ1,
              'Nama PTN 2'=> $res->nama_univ2,
              'Jurusan 2'=> $res->jurusan_univ2,
              'PG 2'=> $res->pg_univ2,
            );

            $jawaban = $this->Mexport->getJawabanMitra($res->id,NULL)->result();

            //mengisi jawaban siswa
            foreach ($jawaban as $j) {
              $temp["JWB $j->nomor"] = $j->jawaban_siswa;
            }
            //mengisi respon jawaban
            foreach ($jawaban as $j) {
              if ($j->jawaban_siswa == "") {
                $jwb = 'B';
              }elseif ($j->jawaban_siswa == $j->kunci_jawaban) {
                $jwb = 'T';
              }else {
                $jwb = 'F';
              }
              $temp["RES $j->nomor"] = "$jwb";
            }
            //mengisi jmlh benar,salah,kosong permatkul
            foreach ($list_mapel as $list) {
              $id = $list->id;
              $result = $this->Mexport->getJawabanMitra($res->id,$id)->result();
              $benar = 0; $salah = 0; $kosong = 0;

              //menghitung benar,salah,kosong
              foreach ($result as $a) {
                if ($a->jawaban_siswa == NULL) {
                  $kosong++;
                }elseif ($a->jawaban_siswa == $a->kunci_jawaban) {
                  $benar++;
                }else {
                  $salah++;
                }
              }

              $temp["Benar $id"] = "$benar";
              $temp["Salah $id"] = "$salah";
              $temp["Kosong $id"] = "$kosong";
            }


            array_push($data,$temp);
            $number++;
          }

          //var_dump($data);die;

          $this->export($header,$data,$filename);
      }else {
        echo "Event tidak ada";
      }
    }

    function export($header,$data,$filename){
      include_once("PHP_XLSX/xlsxwriter.class.php");

      header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
      header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
      header('Content-Transfer-Encoding: binary');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');

      $style_header = array( 'font'=>'Arial','font-size'=>10,'font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center', 'border'=>'left,right,top,bottom' ,'widths'=>[5,10,12,30,30,10,10,18,20,30,40,5,20,30,40,5] );
      $style_row = array( 'font'=>'Arial','font-size'=>10, 'halign'=>'center', 'border'=>'left,right,top,bottom');

      $writer = new XLSXWriter();
      $writer->setAuthor('Kostlab');
      $writer->writeSheetHeader('Sheet1', $header, $style_header);
      foreach($data as $row)
      	$writer->writeSheetRow('Sheet1', $row , $style_row);
      $writer->writeToStdOut();
    }

}
