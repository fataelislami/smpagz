<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Orang_tua extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Orang_tua_model');
        $this->load->library('form_validation');
    }

    public function index()
    {

      $dataorang_tua=$this->Orang_tua_model->get_all();//panggil ke modell
      $datafield=$this->Orang_tua_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => 'admin/orang_tua/orang_tua_list',
        'sidebar'=>'admin/sidebar',
        'css'=>'admin/crudassets/css',
        'script'=>'admin/crudassets/script',
        'dataorang_tua'=>$dataorang_tua,
        'datafield'=>$datafield,
        'module'=>'admin',
        'titlePage'=>'orang_tua',
        'controller'=>'orang_tua'
       );
      $this->template->load($data);
    }


    public function create(){
      $data = array(
        'contain_view' => 'admin/orang_tua/orang_tua_form',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/orang_tua/create_action',
        'module'=>'admin',
        'titlePage'=>'orang_tua',
        'controller'=>'orang_tua'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Orang_tua_model->get_by_id($id);
      $data = array(
        'contain_view' => 'admin/orang_tua/orang_tua_edit',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/orang_tua/update_action',
        'dataedit'=>$dataedit,
        'module'=>'admin',
        'titlePage'=>'orang_tua',
        'controller'=>'orang_tua'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
              'username' => $this->input->post('username',TRUE),
          		'password' => sha1($this->input->post('password',TRUE)),
          		'nama_ayah' => $this->input->post('nama_ayah',TRUE),
          		'nama_ibu' => $this->input->post('nama_ibu',TRUE),
          		'alamat_ayah' => $this->input->post('alamat_ayah',TRUE),
          		'alamat_ibu' => $this->input->post('alamat_ibu',TRUE),
          		'tlp_ayah' => $this->input->post('tlp_ayah',TRUE),
          		'tlp_ibu' => $this->input->post('tlp_ibu',TRUE),
          		'email_ayah' => $this->input->post('email_ayah',TRUE),
          		'email_ibu' => $this->input->post('email_ibu',TRUE),
          	    );

            $this->Orang_tua_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/orang_tua'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id', TRUE));
        } else {
            $data = array(
              'username' => $this->input->post('username',TRUE),
          		'nama_ayah' => $this->input->post('nama_ayah',TRUE),
          		'nama_ibu' => $this->input->post('nama_ibu',TRUE),
          		'alamat_ayah' => $this->input->post('alamat_ayah',TRUE),
          		'alamat_ibu' => $this->input->post('alamat_ibu',TRUE),
          		'tlp_ayah' => $this->input->post('tlp_ayah',TRUE),
          		'tlp_ibu' => $this->input->post('tlp_ibu',TRUE),
          		'email_ayah' => $this->input->post('email_ayah',TRUE),
          		'email_ibu' => $this->input->post('email_ibu',TRUE),
          	);

            if ($this->input->post('password')!="") {
              $data['password'] = sha1($this->input->post('password',TRUE));
            }

            $this->Orang_tua_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/orang_tua'));
        }
    }

    public function delete($id)
    {
        $row = $this->Orang_tua_model->get_by_id($id);

        if ($row) {
            $this->Orang_tua_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/orang_tua'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/orang_tua'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama_ayah', 'nama ayah', 'trim|required');
	$this->form_validation->set_rules('nama_ibu', 'nama ibu', 'trim|required');
	$this->form_validation->set_rules('alamat_ayah', 'alamat ayah', 'trim|required');
	$this->form_validation->set_rules('alamat_ibu', 'alamat ibu', 'trim|required');
	$this->form_validation->set_rules('tlp_ayah', 'tlp ayah', 'trim|required');
	$this->form_validation->set_rules('tlp_ibu', 'tlp ibu', 'trim|required');
	$this->form_validation->set_rules('email_ayah', 'email ayah', 'trim|required');
	$this->form_validation->set_rules('email_ibu', 'email ibu', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
