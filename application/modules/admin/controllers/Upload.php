<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {
    $this->load->view('admin/upload');
    if($this->session->userdata('status')!='login'){
      redirect(base_url('login'));
    }
    if($this->session->userdata('level')!='admin'){
      redirect(redirect($_SERVER['HTTP_REFERER']));
    }
  }

  function proses(){
    $upload=$this->upload_foto('file');
    if($upload['is_image']){
      echo "gagal";
    }

    // var_dump($upload);
  }

  public function upload_foto($formname){
    $config['upload_path']          = './xfile/images/user';
    $config['allowed_types']        = 'gif|jpg|png|jpeg';
    $config['overwrite'] = FALSE;
    $config['encrypt_name'] = FALSE;
    //$config['max_size']             = 100;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;
    $this->load->library('upload', $config);
    $this->upload->do_upload($formname);
    return $this->upload->data();
    /*Cara penggunaan
    PASTIKAN FOLDER TUJUAN PERMISSION NYA SUDAH BISA WRITE

    $this->upload_foto({ubah dengan name dari input type dari view});
    contoh
    jika <input type="file" name="file">
    maka
    $file=$this->upload_foto('file');
    untuk mengambil nama file gunakan $file['file_name'];
    untuk mengecek berhasil atau tidak gunakan kondisi
    if($file['is_image']!=1){
      echo "gagal";
    }else{
      echo "File Name : ".$file['file_name'];
      echo "Is Image ".$file['is_image'];
    }

    */
    }
    public function summernote(){
      $config['upload_path']          = './xfile/images';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['overwrite'] = FALSE;
      $config['encrypt_name'] = TRUE;
      //$config['max_size']             = 100;
      //$config['max_width']            = 1024;
      //$config['max_height']           = 768;
      $this->load->library('upload', $config);
      $this->upload->do_upload('file');
      $json=json_encode($this->upload->data());
      echo $json;
      /*Cara penggunaan
      PASTIKAN FOLDER TUJUAN PERMISSION NYA SUDAH BISA WRITE

      $this->upload_foto({ubah dengan name dari input type dari view});
      contoh
      jika <input type="file" name="file">
      maka
      $file=$this->upload_foto('file');
      untuk mengambil nama file gunakan $file['file_name'];
      untuk mengecek berhasil atau tidak gunakan kondisi
      if($file['is_image']!=1){
        echo "gagal";
      }else{
        echo "File Name : ".$file['file_name'];
        echo "Is Image ".$file['is_image'];
      }

      */
      }

}
