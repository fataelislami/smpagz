<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
  require_once 'class/ClientAPI.php';

class Export extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mexport'));
        if($this->session->userdata('status')!='login'){
          redirect(base_url('login'));
        }
        if($this->session->userdata('level')!='admin'){
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }
    }

    public function index()
    {
      echo "Export data :<br>";
      echo "- Data Siswa <br>";
      echo "- Output Jawaban <br>";
      echo "- Respon Jawaban <br>";
      echo "- Data Setelah TO <br>";
      echo "<a href='".base_url()."admin/export/excel?event_id=1'>link</a>";
    }
    function dir(){
      echo sys_get_temp_dir();
      die;
    }
    function excel(){
      if (isset($_GET['event_id'])) {
        $id    = $this->input->get('event_id');
        $jenis = $this->Mexport->getJenisEvent($id);
        $jenis_num = $jenis->num_rows();

        if ($jenis_num > 0) {
          $jenis = $jenis->row();
          if ($jenis->tipe == "TO") {
            $this->tryout($id);
          }elseif ($jenis->tipe == "QUIZ") {
            $this->quiz($id);
          }
        }else {
          redirect(base_url('admin/event'));
        }
      }else {
        redirect(base_url('admin/event'));
      }
    }

    function tryout($id){
      //mendapatkan list tryout yang dilaksanakan pada id event = $id
      //mengecek terlebih dahulu apakah to untuk kelas 12
      $where = array('id' => $id);
      $event = $this->Mexport->getdata($where,'event')->row();
      if ($event->kelas == '3') {
        $this->db->order_by('univ','ASC');
        $this->db->order_by('jurusan','ASC');
        $this->db->order_by('try_out.id','ASC');
      }
      $list_to = $this->Mexport->getTo_byEvent($id);
      $list_to_num = $list_to->num_rows();
      $data_event = $this->Mexport->getwhere('id',$id,'event')->row();

      if ($list_to_num > 0) {
        $list_to = $list_to->result(); //mendapatkan list quiz pada event = $id


            /** Memulai Excel **/
            // Load plugin PHPExcel nya
            include APPPATH.'third_party/PHPExcel/PHPExcel.php';

            // Panggil class PHPExcel nya
            $excel = new PHPExcel();
            // Settingan awal fil excel
            $excel->getProperties()->setCreator('Kostlab')
                         ->setLastModifiedBy('Kostlab')
                         ->setTitle("Data Tryout")
                         ->setSubject("Tryout")
                         ->setDescription("Laporan Tryout")
                         ->setKeywords("Data Hasil Tryout");
            // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
            $style_col = array(
              'font' => array('bold' => true), // Set font nya jadi bold
              'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
              ),
              'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
              ),
              'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'A6A6A6')
              )
            );
            // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
            $style_row = array(
              'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
              ),
              'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
              )
            );

            /* Disini proses membuat sheet **/
            $excel = new PHPExcel();
            $excel->createSheet(0);
            $excel->getSheet(0)->setTitle("Data Tryout id=$id");

            /** proses membuat tabel header **/
            $row_1 = 2;  $col_1 = 1; //inisialisasi baris dan kolommnya
            //sheet 1
            $excel->getSheet(0)->SetCellValue('A'.$row_1, 'No');
            $excel->getSheet(0)->SetCellValue('B'.$row_1, 'Kode Soal');
            $excel->getSheet(0)->SetCellValue('C'.$row_1, 'NISE');
            $excel->getSheet(0)->SetCellValue('D'.$row_1, 'Nama');
            $excel->getSheet(0)->SetCellValue('E'.$row_1, 'Sekolah');
            $excel->getSheet(0)->SetCellValue('F'.$row_1, 'Kelas');
            $excel->getSheet(0)->SetCellValue('G'.$row_1, 'Cabang');
            $excel->getSheet(0)->SetCellValue('H'.$row_1, 'Nama PTN');
            $excel->getSheet(0)->SetCellValue('I'.$row_1, 'Jurusan');
            $excel->getSheet(0)->SetCellValue('J'.$row_1, 'PG');
            $excel->getSheet(0)->SetCellValue('K'.$row_1, 'Nama PTN');
            $excel->getSheet(0)->SetCellValue('L'.$row_1, 'Jurusan');
            $excel->getSheet(0)->SetCellValue('M'.$row_1, 'PG');
            $excel->getSheet(0)->SetCellValue('N'.$row_1, 'Nama PTN');
            $excel->getSheet(0)->SetCellValue('O'.$row_1, 'Jurusan');
            $excel->getSheet(0)->SetCellValue('P'.$row_1, 'PG');
            $excel->getSheet(0)->SetCellValue('Q'.$row_1, 'Waktu Mulai');
            //merge cell header
            for ($i=0; $i < 17 ; $i++) {
              $row = $row_1;
              $row2= $row_1+1;
              $start = PHPExcel_Cell::stringFromColumnIndex($i);
              $merge = "$start{$row}:$start{$row2}";

              $excel->getSheet(0)->mergeCells($merge);
            }

            //header jawaban soal
            $col_1 = 17; //awal kolom jawaban to
            $jmlh_soal = $this->Mexport->getJmlhSoal($list_to[0]->kode_soal,'list_soal_to')->result(); //mendapatkan jmlh soal
            foreach ($jmlh_soal as $j) {
              $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1 , $row_1+1, "$j->nomor");
              $col_1++;
            }
            $col_respon = $col_1; //awal colomn respon jawaban
            //header respon soal
            foreach ($jmlh_soal as $j) {
              $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1 , $row_1+1, "$j->nomor");
              $col_1++;
            }
            $col_mapel = $col_1; //awal colomn mapel

            //header table input jawaban
            $excel->getSheet(0)->setCellValueByColumnAndRow( 17 , $row_1, "Jawaban Siswa");
            $start = PHPExcel_Cell::stringFromColumnIndex(17);
            $end = PHPExcel_Cell::stringFromColumnIndex($col_respon-1);
            $merge = "$start{$row_1}:$end{$row_1}";
            $excel->getSheet(0)->mergeCells($merge);

            //header table respon jawaban
            $excel->getSheet(0)->setCellValueByColumnAndRow( $col_respon , $row_1, "Respon Jawaban");
            $start = PHPExcel_Cell::stringFromColumnIndex($col_respon);
            $end = PHPExcel_Cell::stringFromColumnIndex($col_mapel-1);
            $merge = "$start{$row_1}:$end{$row_1}";
            $excel->getSheet(0)->mergeCells($merge);

            //header mata pelajaran
            $list_mapel = $this->Mexport->getMatkul($list_to[0]->kode_soal);
            foreach ($list_mapel as $mapel) {
              $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1 , $row_1, "$mapel->nama"); // header jumlah soal
              $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1 , $row_1+1, "Benar");
              $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1+1 , $row_1+1, "Salah");
              $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1+2 , $row_1+1, "Kosong");

              $row = $row_1;
              $start = PHPExcel_Cell::stringFromColumnIndex($col_1);
              $end = PHPExcel_Cell::stringFromColumnIndex($col_1+2);
              $merge = "$start{$row}:$end{$row}";

              $excel->getSheet(0)->mergeCells($merge);
              $col_1 = $col_1 + 3;
            }

            //membuat style header
            for ($i=0; $i <= $col_1-1 ; $i++) {
                $excel->getSheet(0)->getStyleByColumnAndRow($i , $row_1)->applyFromArray($style_col);
                $excel->getSheet(0)->getStyleByColumnAndRow($i , $row_1+1)->applyFromArray($style_col);
            }

            /** proses membuat isi tabel **/
            $row_1 = $row_1+2; $number=1;
            foreach ($list_to as $data) {
              set_time_limit(20);//Menambah ini untuk handle big request
              $id_to = $data->id;

              //mengisi data siswa
              $siswa = $this->Mexport->getSiswaByTryout($id_to)->row();
              $excel->getSheet(0)->SetCellValue('A'.$row_1, "$number");
              $excel->getSheet(0)->SetCellValue('B'.$row_1, "$data->kode_soal");
              $excel->getSheet(0)->SetCellValue('C'.$row_1, "$siswa->nise");
              $excel->getSheet(0)->SetCellValue('D'.$row_1, "$siswa->nama");
              $excel->getSheet(0)->SetCellValue('E'.$row_1, "$siswa->sekolah");
              $excel->getSheet(0)->SetCellValue('F'.$row_1, "$siswa->kode_kelas");
              $excel->getSheet(0)->SetCellValue('G'.$row_1, "$siswa->cabang");

              //mengisi data universitas
              $pilihan = $this->Mexport->getPilihanTo($id_to)->row();
              if ($pilihan->pil_1 != "") {
                $pg = $this->Mexport->getPassGrade($pilihan->pil_1)->row();
                $excel->getSheet(0)->SetCellValue('H'.$row_1, "$pg->nama");
                $excel->getSheet(0)->SetCellValue('I'.$row_1, "$pg->jurusan");
                $excel->getSheet(0)->SetCellValue('J'.$row_1, "$pg->pass_grade");
              }
              if ($pilihan->pil_2 != "") {
                $pg = $this->Mexport->getPassGrade($pilihan->pil_2)->row();
                $excel->getSheet(0)->SetCellValue('K'.$row_1, "$pg->nama");
                $excel->getSheet(0)->SetCellValue('L'.$row_1, "$pg->jurusan");
                $excel->getSheet(0)->SetCellValue('M'.$row_1, "$pg->pass_grade");
              }
              if ($pilihan->pil_3 != "") {
                $pg = $this->Mexport->getPassGrade($pilihan->pil_3)->row();
                $excel->getSheet(0)->SetCellValue('N'.$row_1, "$pg->nama");
                $excel->getSheet(0)->SetCellValue('O'.$row_1, "$pg->jurusan");
                $excel->getSheet(0)->SetCellValue('P'.$row_1, "$pg->pass_grade");
              }


              //mengisi waktu mulai
              $excel->getSheet(0)->SetCellValue('Q'.$row_1, "$data->waktu_mulai");

              //mengisi jawaban dan respon jawaban siswa
              $jawaban = $this->Mexport->getJawaban2($id_to,NULL)->result();
              $col_1 = 17; $col_3 = $col_respon;
              foreach ($jawaban as $jb) {
                $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1 , $row_1, "$jb->jawaban_siswa");
                if ($jb->jawaban_siswa == "") {
                  $val = 'B';
                }elseif ($jb->jawaban_siswa == $jb->kunci_jawaban) {
                  $val = 'T';
                }else {
                  $val = 'F';
                }
                $excel->getSheet(0)->setCellValueByColumnAndRow( $col_3 , $row_1, "$val");

                $col_1++;$col_3++;
              }

              //mengisi jawaban per matkul
              $col_1 = $col_mapel;
              $res = $this->Mexport->getListMapelByTO($id_to)->result();
              foreach ($res as $r) {
                $res2 = $this->Mexport->getJawaban2($id_to,$r->id)->result();
                $benar = 0; $salah = 0; $null = 0;

                foreach ($res2 as $a) {
                  if ($a->jawaban_siswa == NULL) {
                    $null++;
                  }elseif ($a->jawaban_siswa == $a->kunci_jawaban) {
                    $benar++;
                  }else {
                    $salah++;
                  }
                }
                $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1 , $row_1, "$benar");
                $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1+1 , $row_1, "$salah");
                $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1+2 , $row_1, "$null");

                //membuat style header
                for ($i=0; $i <= $col_1+2 ; $i++) {
                    $excel->getSheet(0)->getStyleByColumnAndRow($i , $row_1)->applyFromArray($style_row);
                }

                $col_1 = $col_1+3;
              }


              $row_1++;$number++;
            }
            // /** end membuat isi tabel **/
            //die;
            /** setting width kolom **/
            $excel->getSheet(0)->getColumnDimension('A')->setWidth(7);
            $excel->getSheet(0)->getColumnDimension('B')->setWidth(12);
            $excel->getSheet(0)->getColumnDimension('C')->setWidth(15);
            $excel->getSheet(0)->getColumnDimension('D')->setWidth(25);
            $excel->getSheet(0)->getColumnDimension('E')->setWidth(25);
            $excel->getSheet(0)->getColumnDimension('F')->setWidth(15);
            $excel->getSheet(0)->getColumnDimension('G')->setWidth(12);
            $excel->getSheet(0)->getColumnDimension('H')->setWidth(33);
            $excel->getSheet(0)->getColumnDimension('I')->setWidth(30);
            $excel->getSheet(0)->getColumnDimension('J')->setWidth(15);
            $excel->getSheet(0)->getColumnDimension('K')->setWidth(33);
            $excel->getSheet(0)->getColumnDimension('L')->setWidth(30);
            $excel->getSheet(0)->getColumnDimension('M')->setWidth(15);
            $excel->getSheet(0)->getColumnDimension('N')->setWidth(33);
            $excel->getSheet(0)->getColumnDimension('O')->setWidth(30);
            $excel->getSheet(0)->getColumnDimension('P')->setWidth(15);
            $excel->getSheet(0)->getColumnDimension('Q')->setWidth(19);
            /** end setting width kolom **/

            $excel->setActiveSheetIndex(0);
            // Proses file excel
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment; filename=$data_event->nama - $data_event->tanggal.xlsx"); // Set nama file excel nya
            header('Cache-Control: max-age=0');
            $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            ob_clean();
            $write->save('php://output');
      }else {
        echo "Tidak ada Data";
      }
    }

    function quiz($id){
      //mendapatkan list quiz yang dilaksanakan pada id event = $id
      $list_quiz = $this->Mexport->getQuiz_byEvent($id);
      $list_quiz_num = $list_quiz->num_rows();

      if ($list_quiz_num > 0) {
        $list_quiz = $list_quiz->result(); //mendapatkan list quiz pada event = $id

            /** Memulai Excel **/
            // Load plugin PHPExcel nya
            include APPPATH.'third_party/PHPExcel/PHPExcel.php';

            //panggil class api
            $api = new ClientAPI();
            // Panggil class PHPExcel nya
            $excel = new PHPExcel();
            // Settingan awal fil excel
            $excel->getProperties()->setCreator('Kostlab')
                         ->setLastModifiedBy('Kostlab')
                         ->setTitle("Data Quiz")
                         ->setSubject("Quiz")
                         ->setDescription("Laporan Quiz")
                         ->setKeywords("Data Hasil Quiz");
            // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
            $style_col = array(
              'font' => array('bold' => true), // Set font nya jadi bold
              'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
              ),
              'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
              ),
              'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'A6A6A6')
              )
            );
            // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
            $style_row = array(
              'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
              ),
              'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
              )
            );

            /* Disini proses membuat sheet **/
            $excel = new PHPExcel();
            $excel->createSheet(0);
            $excel->getSheet(0)->setTitle('Data Quiz Guru');
            $excel->createSheet(1);
            $excel->getSheet(1)->setTitle('Respon Jawaban');

            /** proses membuat tabel header **/
            $row_1 = 1;  $col_1 = 1; //inisialisasi baris dan kolommnya
            //sheet 1
            $excel->getSheet(0)->SetCellValue('A'.$row_1, 'Kode Soal');
            $excel->getSheet(0)->SetCellValue('B'.$row_1, 'NIK');
            $excel->getSheet(0)->SetCellValue('C'.$row_1, 'Nama');
            $excel->getSheet(0)->SetCellValue('D'.$row_1, 'Mapel');
            $excel->getSheet(0)->SetCellValue('E'.$row_1, 'Email');
            $excel->getSheet(0)->getStyle('A'.$row_1)->applyFromArray($style_col);
            $excel->getSheet(0)->getStyle('B'.$row_1)->applyFromArray($style_col);
            $excel->getSheet(0)->getStyle('C'.$row_1)->applyFromArray($style_col);
            $excel->getSheet(0)->getStyle('D'.$row_1)->applyFromArray($style_col);
            $excel->getSheet(0)->getStyle('E'.$row_1)->applyFromArray($style_col);
            //sheet 2
            $excel->getSheet(1)->SetCellValue('A'.$row_1, 'Kode Soal');
            $excel->getSheet(1)->SetCellValue('B'.$row_1, 'NIK');
            $excel->getSheet(1)->getStyle('A'.$row_1)->applyFromArray($style_col);
            $excel->getSheet(1)->getStyle('B'.$row_1)->applyFromArray($style_col);

            //membut header nomor Soal
            $contoh_kodeSoal = $list_quiz[0]->kode_soal;
            $nomor_quiz = $this->Mexport->getJmlhSoal($contoh_kodeSoal,'list_soal_quiz')->result();

            $col_1 = 5;
            foreach ($nomor_quiz as $n) {
              $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1 , $row_1, $n->nomor); //judul kolom sheet 1
              $excel->getSheet(0)->getStyleByColumnAndRow($col_1 , $row_1)->applyFromArray($style_col); //stye kolom sheet 1
              $excel->getSheet(1)->setCellValueByColumnAndRow( $col_1-3 , $row_1, $n->nomor); //judul kolom sheet 2
              $excel->getSheet(1)->getStyleByColumnAndRow($col_1-3 , $row_1)->applyFromArray($style_col); //stye kolom sheet 2
              $col_1++;
            }
            /** end membuat tabel header **/

            /** proses membuat isi tabel **/
            $api = new ClientAPI(); //panggil class api
            $row_1++; $number=1;
            foreach ($list_quiz as $d) {
              $data = json_decode($api->get(base_url()."api/quiz/dataexport?id_quiz=$d->id"));
              $data1 = $data->results[0];
              $excel->getSheet(0)->SetCellValue('A'.$row_1, "$d->kode_soal");
              $excel->getSheet(0)->SetCellValue('B'.$row_1, "$data1->nik");
              $excel->getSheet(1)->SetCellValue('A'.$row_1, "$d->kode_soal"); //sheet 2
              $excel->getSheet(1)->SetCellValue('B'.$row_1, "$data1->nik"); //sheet 2
              $excel->getSheet(0)->SetCellValue('C'.$row_1, "$data1->nama");
              $excel->getSheet(0)->SetCellValue('D'.$row_1, "$data1->mapel");
              $excel->getSheet(0)->SetCellValue('E'.$row_1, "$data1->email");
              //apply style header
              $excel->getSheet(0)->getStyle('A'.$row_1)->applyFromArray($style_row);
              $excel->getSheet(0)->getStyle('B'.$row_1)->applyFromArray($style_row);
              $excel->getSheet(1)->getStyle('A'.$row_1)->applyFromArray($style_row); //sheet 2
              $excel->getSheet(1)->getStyle('B'.$row_1)->applyFromArray($style_row); //sheet 2
              $excel->getSheet(0)->getStyle('C'.$row_1)->applyFromArray($style_row);
              $excel->getSheet(0)->getStyle('D'.$row_1)->applyFromArray($style_row);
              $excel->getSheet(0)->getStyle('E'.$row_1)->applyFromArray($style_row);

              $col_1 = 5;
              foreach ($data->results[1] as $data2) {
                //sheet 1
                $excel->getSheet(0)->setCellValueByColumnAndRow( $col_1 , $row_1, "$data2->jawaban_guru"); //judul kolom
                $excel->getSheet(0)->getStyleByColumnAndRow($col_1 , $row_1)->applyFromArray($style_row); //stye kolom
                //sheet 2
                if ($data2->jawaban_guru == "") {
                  $hasil = "B";
                }elseif ($data2->jawaban_guru == $data2->kunci_jawaban) {
                  $hasil = "T";
                }else {
                  $hasil = "F";
                }
                $excel->getSheet(1)->setCellValueByColumnAndRow( $col_1-3 , $row_1, $hasil); //judul kolom
                $excel->getSheet(1)->getStyleByColumnAndRow($col_1-3 , $row_1)->applyFromArray($style_row); //stye kolom

                $col_1++;
              }

              $row_1++; $number++;
            }
            /** end membuat isi tabel **/

            /** setting width kolom **/
            $excel->getSheet(0)->getColumnDimension('A')->setWidth(10); // Set width kolom A
            $excel->getSheet(0)->getColumnDimension('B')->setWidth(15); // Set width kolom B
            $excel->getSheet(0)->getColumnDimension('C')->setWidth(25); // Set width kolom C
            $excel->getSheet(0)->getColumnDimension('D')->setWidth(15); // Set width kolom D
            $excel->getSheet(0)->getColumnDimension('E')->setWidth(30); // Set width kolom E
            $excel->getSheet(1)->getColumnDimension('A')->setWidth(10); // Set width kolom A
            $excel->getSheet(1)->getColumnDimension('B')->setWidth(15); // Set width kolom B
            /** end setting width kolom **/

            $excel->setActiveSheetIndex(0);
            // Proses file excel
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="Data Tryout.xlsx"'); // Set nama file excel nya
            header('Cache-Control: max-age=0');
            $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            ob_clean();
            $write->save('php://output');
      }else {
        echo "Tidak ada Data";
      }
    }

    function tryout2($id){
      if ($id != "") {
        $cek  =$this->Mexport->cekData('id',$id,'event');
        if ($cek) {
          $tryout = $this->Mexport->getTo_byEvent($id)->result();
          // Load plugin PHPExcel nya
          include APPPATH.'third_party/PHPExcel/PHPExcel.php';

          //panggil class api
          $api = new ClientAPI();
          // Panggil class PHPExcel nya
          $excel = new PHPExcel();
          // Settingan awal fil excel
          $excel->getProperties()->setCreator('Kostlab')
                       ->setLastModifiedBy('Kostlab')
                       ->setTitle("Data TO")
                       ->setSubject("Tryout")
                       ->setDescription("Laporan Tryout Siswa")
                       ->setKeywords("Data Tryout Siswa");
          // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
          $style_col = array(
            'font' => array('bold' => true), // Set font nya jadi bold
            'alignment' => array(
              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
              'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
              'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
              'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
              'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            ),
            'fill' => array(
              'type' => PHPExcel_Style_Fill::FILL_SOLID,
              'color' => array('rgb' => 'A6A6A6')
            )
          );
          // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
          $style_row = array(
            'alignment' => array(
              'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
              'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
            ),
            'borders' => array(
              'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
              'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
              'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
              'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
            )
          );

          /* Disini proses membuat sheet **/
          $excel = new PHPExcel();
          $excel->createSheet(0);
          $excel->createSheet(1);
          $excel->createSheet(2);
          $excel->createSheet(3);
          $excel->getSheet(0)->setTitle('Data Siswa');
          $excel->getSheet(1)->setTitle('Output Jawaban');
          $excel->getSheet(2)->setTitle('Respon Jawaban');
          $excel->getSheet(3)->setTitle('Data Setelah TO');
          /** End membuat sheet **/

          /** Disini proses mengisi data siswa **/
          //membuat header sheet 1
          $row_1 = 1;
          $col_1 = 1;
          $excel->getSheet(0)->SetCellValue('A'.$row_1, 'N0');
          $excel->getSheet(0)->SetCellValue('B'.$row_1, 'NISE');
          $excel->getSheet(0)->SetCellValue('C'.$row_1, 'Nama');
          $excel->getSheet(0)->SetCellValue('D'.$row_1, 'Sekolah');
          $excel->getSheet(0)->SetCellValue('E'.$row_1, 'Kelas');
          $excel->getSheet(0)->SetCellValue('F'.$row_1, 'Cabang');
          //apply style header
          $excel->getSheet(0)->getStyle('A'.$row_1)->applyFromArray($style_col);
          $excel->getSheet(0)->getStyle('B'.$row_1)->applyFromArray($style_col);
          $excel->getSheet(0)->getStyle('C'.$row_1)->applyFromArray($style_col);
          $excel->getSheet(0)->getStyle('D'.$row_1)->applyFromArray($style_col);
          $excel->getSheet(0)->getStyle('E'.$row_1)->applyFromArray($style_col);
          $excel->getSheet(0)->getStyle('F'.$row_1)->applyFromArray($style_col);

          //membuat header sheet 2 dan sheet 3
          $excel->getSheet(1)->SetCellValue('A'.$row_1, 'Kode Soal'); // untuk sheet 2
          $excel->getSheet(1)->SetCellValue('B'.$row_1, 'No Perserta'); // untuk sheet 2
          $excel->getSheet(1)->SetCellValue('C'.$row_1, 'Nama Peserta '); // untuk sheet 2
          $excel->getSheet(1)->getStyle('A'.$row_1)->applyFromArray($style_col); // untuk sheet 2
          $excel->getSheet(1)->getStyle('B'.$row_1)->applyFromArray($style_col); // untuk sheet 2
          $excel->getSheet(1)->getStyle('C'.$row_1)->applyFromArray($style_col); // untuk sheet 2
          $excel->getSheet(2)->SetCellValue('A'.$row_1, 'Kode Soal'); // untuk sheet 3
          $excel->getSheet(2)->SetCellValue('B'.$row_1, 'No Perserta'); // untuk sheet 3
          $excel->getSheet(2)->SetCellValue('C'.$row_1, 'Nama Peserta '); // untuk sheet 3
          $excel->getSheet(2)->getStyle('A'.$row_1)->applyFromArray($style_col); // untuk sheet 3
          $excel->getSheet(2)->getStyle('B'.$row_1)->applyFromArray($style_col); // untuk sheet 3
          $excel->getSheet(2)->getStyle('C'.$row_1)->applyFromArray($style_col); // untuk sheet 3
          $contoh_id_to  = $tryout[0]->id;//ambil 1 sample untuk melihat nomor sial apa saja disoal
          $obj_jawaban = json_decode($api->get(base_url()."api/tryout/list_jawaban?id_to=$contoh_id_to"));
          foreach ($obj_jawaban->results as $obj) {
            $excel->getSheet(1)->setCellValueByColumnAndRow( 2+$obj->nomor , $row_1, $obj->nomor); // header jumlah soal
            $excel->getSheet(2)->setCellValueByColumnAndRow( 2+$obj->nomor , $row_1, $obj->nomor); // header jumlah soal
            $excel->getSheet(1)->getStyleByColumnAndRow(2+$obj->nomor , $row_1)->applyFromArray($style_col); // style jumlah soal
            $excel->getSheet(2)->getStyleByColumnAndRow(2+$obj->nomor , $row_1)->applyFromArray($style_col); // style jumlah soal
          }

          //membuat header untuk sheet 4
          $excel->getSheet(3)->SetCellValue('A'.$row_1, 'Kode Soal'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('B'.$row_1, 'No Perserta'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('C'.$row_1, 'Nama Peserta '); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('D'.$row_1, 'Sekolah'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('E'.$row_1, 'Cabang'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('F'.$row_1, 'Nama PTN'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('G'.$row_1, 'Jurusan'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('H'.$row_1, 'PG'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('I'.$row_1, 'Nama PTN'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('J'.$row_1, 'Jurusan'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('K'.$row_1, 'PG'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('L'.$row_1, 'Nama PTN'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('M'.$row_1, 'Jurusan'); // untuk sheet 4
          $excel->getSheet(3)->SetCellValue('N'.$row_1, 'PG'); // untuk sheet 4
          $excel->getSheet(3)->getStyle('A'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('B'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('C'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('D'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('E'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('F'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('G'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('H'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('I'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('J'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('K'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('L'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('M'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          $excel->getSheet(3)->getStyle('N'.$row_1)->applyFromArray($style_col); // untuk sheet 4
          //buat header dinamis untuk skor per mapel
          $contoh_kode_soal = $tryout[0]->kode_soal;//ambil 1 sample untuk melihat matkul apa saja disoal
          $list_mapel = $this->Mexport->getMatkul($contoh_kode_soal);

          $col_1 = 14;
          foreach ($list_mapel as $mapel) {
            $excel->getSheet(3)->setCellValueByColumnAndRow( $col_1 , $row_1, "$mapel->nama"); // header jumlah soal
            $excel->getSheet(3)->getStyleByColumnAndRow($col_1 , $row_1)->applyFromArray($style_col); // style jumlah soal
            $excel->getSheet(3)->getStyleByColumnAndRow($col_1 + 1 , $row_1)->applyFromArray($style_col); // style jumlah soal
            $excel->getSheet(3)->getStyleByColumnAndRow($col_1 + 2, $row_1)->applyFromArray($style_col); // style jumlah soal

            $row = $row_1;
            $start = PHPExcel_Cell::stringFromColumnIndex($col_1);
            $end = PHPExcel_Cell::stringFromColumnIndex($col_1+2);
            $merge = "$start{$row}:$end{$row}";

            $excel->getSheet(3)->mergeCells($merge);
            $col_1 = $col_1 + 3;
          }
          //end heder


          $row_1++;
          $number = 1;
          //membuat isi tabel
          foreach ($tryout as $to) {
            $id_to = $to->id;
            $kode_soal = $to->kode_soal;

              /** mengisi data sheet 1 **/
              $obj_siswa = json_decode($api->get(base_url()."api/siswa/bytryout?id_to=$id_to"));
              $obj_jawaban = json_decode($api->get(base_url()."api/tryout/list_jawaban?id_to=$id_to"));
              $info_to = json_decode($api->get(base_url()."api/tryout/info?id=$id_to"));
              $obj_mapel = json_decode($api->get(base_url()."api/tryout/list_mapel?id_to=$id_to"));
              $siswa = $obj_siswa->results;

              $excel->getSheet(0)->SetCellValue('A'.$row_1, "$number");
              $excel->getSheet(0)->SetCellValue('B'.$row_1, "$siswa->nise");
              $excel->getSheet(0)->SetCellValue('C'.$row_1, "$siswa->nama");
              $excel->getSheet(0)->SetCellValue('D'.$row_1, "$siswa->sekolah");
              $excel->getSheet(0)->SetCellValue('E'.$row_1, "$siswa->kode_kelas");
              $excel->getSheet(0)->SetCellValue('F'.$row_1, "$siswa->cabang");
              //apply style header
              $excel->getSheet(0)->getStyle('A'.$row_1)->applyFromArray($style_row);
              $excel->getSheet(0)->getStyle('B'.$row_1)->applyFromArray($style_row);
              $excel->getSheet(0)->getStyle('C'.$row_1)->applyFromArray($style_row);
              $excel->getSheet(0)->getStyle('D'.$row_1)->applyFromArray($style_row);
              $excel->getSheet(0)->getStyle('E'.$row_1)->applyFromArray($style_row);
              $excel->getSheet(0)->getStyle('F'.$row_1)->applyFromArray($style_row);
              /** end mengisi sheet 1 **/

              /** mengisi data untuk sheet 2 dan sheet 3 **/
              //membuat header sheet 2 dan sheet 3
              $excel->getSheet(1)->SetCellValue('A'.$row_1, "$kode_soal"); // untuk sheet 2
              $excel->getSheet(1)->SetCellValue('B'.$row_1, "$siswa->nise"); // untuk sheet 2
              $excel->getSheet(1)->SetCellValue('C'.$row_1, "$siswa->nama"); // untuk sheet 2
              $excel->getSheet(1)->getStyle('A'.$row_1)->applyFromArray($style_row); // untuk sheet 2
              $excel->getSheet(1)->getStyle('B'.$row_1)->applyFromArray($style_row); // untuk sheet 2
              $excel->getSheet(1)->getStyle('C'.$row_1)->applyFromArray($style_row); // untuk sheet 2
              $excel->getSheet(2)->SetCellValue('A'.$row_1, "$kode_soal"); // untuk sheet 3
              $excel->getSheet(2)->SetCellValue('B'.$row_1, "$siswa->nise"); // untuk sheet 3
              $excel->getSheet(2)->SetCellValue('C'.$row_1, "$siswa->nama"); // untuk sheet 3
              $excel->getSheet(2)->getStyle('A'.$row_1)->applyFromArray($style_row); // untuk sheet 3
              $excel->getSheet(2)->getStyle('B'.$row_1)->applyFromArray($style_row); // untuk sheet 3
              $excel->getSheet(2)->getStyle('C'.$row_1)->applyFromArray($style_row); // untuk sheet 3
              $col_1 = 3;
              foreach ($obj_jawaban->results as $jw) {
                $excel->getSheet(1)->setCellValueByColumnAndRow( $col_1 , $row_1, "$jw->jawaban_siswa"); // header jumlah soal
                if ($jw->jawaban_siswa == "") {
                  $val = 'B';
                }elseif ($jw->jawaban_siswa == $jw->kunci_jawaban) {
                  $val = 'T';
                }else {
                  $val = 'F';
                }
                $excel->getSheet(2)->setCellValueByColumnAndRow( $col_1 , $row_1, $val); // header jumlah soal
                $excel->getSheet(1)->getStyleByColumnAndRow($col_1 , $row_1)->applyFromArray($style_row); // style jumlah soal
                $excel->getSheet(2)->getStyleByColumnAndRow($col_1 , $row_1)->applyFromArray($style_row); // style jumlah soal
                $col_1++;
              }
              /** end mengisi data sheet 2 dan sheet 3 **/

              /** mengisi data sheet ke 4 **/
              $info = $info_to->results;
              $excel->getSheet(3)->SetCellValue('A'.$row_1, "$info->kode_soal"); // untuk sheet 4
              $excel->getSheet(3)->SetCellValue('B'.$row_1, "$info->nise"); // untuk sheet 4
              $excel->getSheet(3)->SetCellValue('C'.$row_1, "$info->nama"); // untuk sheet 4
              $excel->getSheet(3)->SetCellValue('D'.$row_1, "$info->sekolah"); // untuk sheet 4
              $excel->getSheet(3)->SetCellValue('E'.$row_1, "$info->cabang"); // untuk sheet 4
              if ($info->pil_1 != "") {
                $pil1 = $info->pil_1;
                $excel->getSheet(3)->SetCellValue('F'.$row_1, "$pil1->nama"); // untuk sheet 4
                $excel->getSheet(3)->SetCellValue('G'.$row_1, "$pil1->jurusan"); // untuk sheet 4
                $excel->getSheet(3)->SetCellValue('H'.$row_1, "$pil1->pass_grade"); // untuk sheet 4
              }
              if ($info->pil_2 != "") {
                $pil2 = $info->pil_2;
                $excel->getSheet(3)->SetCellValue('I'.$row_1, "$pil2->nama"); // untuk sheet 4
                $excel->getSheet(3)->SetCellValue('J'.$row_1, "$pil2->jurusan"); // untuk sheet 4
                $excel->getSheet(3)->SetCellValue('K'.$row_1, "$pil2->pass_grade"); // untuk sheet 4
              }
              if ($info->pil_3 != "") {
                $pil3 = $info->pil_3;
                $excel->getSheet(3)->SetCellValue('L'.$row_1, "$pil3->nama"); // untuk sheet 4
                $excel->getSheet(3)->SetCellValue('M'.$row_1, "$pil3->jurusan"); // untuk sheet 4
                $excel->getSheet(3)->SetCellValue('N'.$row_1, "$pil3->pass_grade"); // untuk sheet 4
              }
              $excel->getSheet(3)->getStyle('A'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('B'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('C'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('D'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('E'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('F'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('G'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('H'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('I'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('J'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('K'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('L'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('M'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              $excel->getSheet(3)->getStyle('N'.$row_1)->applyFromArray($style_row); // untuk sheet 4
              //mengisi benar salah dan kosong tiap mapel
              $col_1 = 14;
              foreach ($obj_mapel->results as $mapel) {
                $excel->getSheet(3)->setCellValueByColumnAndRow( $col_1 , $row_1, "$mapel->benar"); // isi jumlah soal
                $excel->getSheet(3)->setCellValueByColumnAndRow( $col_1 + 1, $row_1, "$mapel->salah"); // isi jumlah soal
                $excel->getSheet(3)->setCellValueByColumnAndRow( $col_1 + 2 , $row_1, "$mapel->kosong"); // isi jumlah soal
                $excel->getSheet(3)->getStyleByColumnAndRow($col_1 , $row_1)->applyFromArray($style_row); // style jumlah soal
                $excel->getSheet(3)->getStyleByColumnAndRow($col_1 + 1 , $row_1)->applyFromArray($style_row); // style jumlah soal
                $excel->getSheet(3)->getStyleByColumnAndRow($col_1 + 2, $row_1)->applyFromArray($style_row); // style jumlah soal
                $col_1 = $col_1 + 3;
              }
              /** end mengisi data sheet 4 **/
              $row_1++;
          }
          //end membuat isi tabel

          // Set width kolom sheet 1
          $excel->getSheet(0)->getColumnDimension('A')->setWidth(5); // Set width kolom A
          $excel->getSheet(0)->getColumnDimension('B')->setWidth(15); // Set width kolom B
          $excel->getSheet(0)->getColumnDimension('C')->setWidth(25); // Set width kolom C
          $excel->getSheet(0)->getColumnDimension('D')->setWidth(25); // Set width kolom D
          $excel->getSheet(0)->getColumnDimension('E')->setWidth(10); // Set width kolom E
          $excel->getSheet(0)->getColumnDimension('F')->setWidth(10); // Set width kolom F
          // set width kolom sheet 2
          $excel->getSheet(1)->getColumnDimension('A')->setWidth(10); // Set width kolom A
          $excel->getSheet(1)->getColumnDimension('B')->setWidth(15); // Set width kolom B
          $excel->getSheet(1)->getColumnDimension('C')->setWidth(25); // Set width kolom C
          // set width kolom sheet 3
          $excel->getSheet(2)->getColumnDimension('A')->setWidth(10); // Set width kolom A
          $excel->getSheet(2)->getColumnDimension('B')->setWidth(15); // Set width kolom B
          $excel->getSheet(2)->getColumnDimension('C')->setWidth(25); // Set width kolom C
          // Set width kolom sheet 4
          $excel->getSheet(3)->getColumnDimension('A')->setWidth(10); // Set width kolom A
          $excel->getSheet(3)->getColumnDimension('B')->setWidth(15); // Set width kolom B
          $excel->getSheet(3)->getColumnDimension('C')->setWidth(25); // Set width kolom C
          $excel->getSheet(3)->getColumnDimension('D')->setWidth(25); // Set width kolom D
          $excel->getSheet(3)->getColumnDimension('E')->setWidth(8); // Set width kolom E
          $excel->getSheet(3)->getColumnDimension('F')->setWidth(45); // Set width kolom F
          $excel->getSheet(3)->getColumnDimension('G')->setWidth(25); // Set width kolom G
          $excel->getSheet(3)->getColumnDimension('H')->setWidth(10); // Set width kolom H
          $excel->getSheet(3)->getColumnDimension('I')->setWidth(45); // Set width kolom I
          $excel->getSheet(3)->getColumnDimension('J')->setWidth(25); // Set width kolom J
          $excel->getSheet(3)->getColumnDimension('K')->setWidth(10); // Set width kolom K
          $excel->getSheet(3)->getColumnDimension('L')->setWidth(45); // Set width kolom L
          $excel->getSheet(3)->getColumnDimension('M')->setWidth(25); // Set width kolom M
          $excel->getSheet(3)->getColumnDimension('n')->setWidth(10); // Set width kolom N

          $excel->setActiveSheetIndex(0);
          // Proses file excel
          header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          header('Content-Disposition: attachment; filename="Data Tryout.xlsx"'); // Set nama file excel nya
          header('Cache-Control: max-age=0');
          $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
          $write->save('php://output');

        }else {
          echo "Tidak Ada Data";
        }
      }else {
        redirect(base_url('admin'));
      }
    }

    function backup(){
      // Load plugin PHPExcel nya
      include APPPATH.'third_party/PHPExcel/PHPExcel.php';

      // Panggil class PHPExcel nya
      $excel = new PHPExcel();
      // Settingan awal fil excel
      $excel->getProperties()->setCreator('My Notes Code')
                   ->setLastModifiedBy('My Notes Code')
                   ->setTitle("Data Siswa")
                   ->setSubject("Siswa")
                   ->setDescription("Laporan Semua Data Siswa")
                   ->setKeywords("Data Siswa");
      // Buat sebuah variabel untuk menampung pengaturan style dari header tabel
      $style_col = array(
        'font' => array('bold' => true), // Set font nya jadi bold
        'alignment' => array(
          'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      );
      // Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
      $style_row = array(
        'alignment' => array(
          'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
        ),
        'borders' => array(
          'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
          'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  // Set border right dengan garis tipis
          'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
          'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
        )
      );
      $excel->setActiveSheetIndex(0)->setCellValue('A1', "DATA SISWA"); // Set kolom A1 dengan tulisan "DATA SISWA"
      $excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
      $excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
      $excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
      $excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
      // Buat header tabel nya pada baris ke 3
      $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); // Set kolom A3 dengan tulisan "NO"
      $excel->setActiveSheetIndex(0)->setCellValue('B3', "NIS"); // Set kolom B3 dengan tulisan "NIS"
      $excel->setActiveSheetIndex(0)->setCellValue('C3', "NAMA"); // Set kolom C3 dengan tulisan "NAMA"
      $excel->setActiveSheetIndex(0)->setCellValue('D3', "JENIS KELAMIN"); // Set kolom D3 dengan tulisan "JENIS KELAMIN"
      $excel->setActiveSheetIndex(0)->setCellValue('E3', "ALAMAT"); // Set kolom E3 dengan tulisan "ALAMAT"
      // Apply style header yang telah kita buat tadi ke masing-masing kolom header
      $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
      $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
      // Panggil function view yang ada di SiswaModel untuk menampilkan semua data siswanya
      $siswa = $this->SiswaModel->view();
      $no = 1; // Untuk penomoran tabel, di awal set dengan 1
      $numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4
      foreach($siswa as $data){ // Lakukan looping pada variabel siswa
        $excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
        $excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $data->nis);
        $excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $data->nama);
        $excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $data->jenis_kelamin);
        $excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $data->alamat);

        // Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
        $excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
        $excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);

        $no++; // Tambah 1 setiap kali looping
        $numrow++; // Tambah 1 setiap kali looping
      }
      // Set width kolom
      $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
      $excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); // Set width kolom B
      $excel->getActiveSheet()->getColumnDimension('C')->setWidth(25); // Set width kolom C
      $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20); // Set width kolom D
      $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30); // Set width kolom E

      // Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
      $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
      // Set orientasi kertas jadi LANDSCAPE
      $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
      // Set judul file excel nya
      $excel->getActiveSheet(0)->setTitle("Laporan Data Siswa");
      $excel->setActiveSheetIndex(0);
      // Proses file excel
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment; filename="Data Siswa.xlsx"'); // Set nama file excel nya
      header('Cache-Control: max-age=0');
      $write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
      $write->save('php://output');
    }
}
