<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Soaltryout extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Msoaltryout');
        $this->load->model('Mlistsoaltryout');
        $this->load->model('Dbs');
        $this->load->library('form_validation');
        if($this->session->userdata('status')!='login'){
          redirect(base_url('login'));
        }
        if($this->session->userdata('level')!='admin'){
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }
    }

    public function index()
    {
      $datatable = $this->Msoaltryout->getDataTable();

      $data = array(
        'contain_view' => 'admin/soaltryout/soal_to_list',
        'sidebar'=>'admin/sidebar',
        'css'=>'admin/crudassets/css',
        'script'=>'admin/crudassets/script',
        'datatable' => $datatable,
        'module'=>'admin',
        'titlePage'=>'soaltryout',
        'controller'=>'soaltryout'
       );
      $this->template->load($data);
    }


    public function create(){
      $data = array(
        'contain_view' => 'admin/soaltryout/soal_to_form',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/soaltryout/create_action',
        'titlePage'=>'Tambah Data Soal'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Msoaltryout->get_by_id($id);
      $data = array(
        'contain_view' => 'admin/soaltryout/soal_to_edit',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/soaltryout/update_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'Ubah Data Soal'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $cek = $this->Msoaltryout->cekData('kode',$this->input->post('kode'));
            if ($cek) {
              //Create Slug
              $kode = url_title($this->input->post('kode', TRUE), 'dash', true);
              $check_slug=$this->Dbs->getdata('paket_soal',array('kode'=>$kode));
              if($check_slug->num_rows()>0){
                if($id_post!=$check_slug->row()->id){
                  $number=$check_slug+1;
                  $slug=$slug."$number";
                }else {
                  $slug=$slug;
                }
              }
              //End Create Slug
              $id_admin = $this->session->userdata('id'); //masukan id admin disini
              $kode_soal = $kode;
              $data = array(
                'kode' => $kode_soal,
            		'tanggal' => date('Y-m-d'),
                'kelas' => $this->input->post('kelas',TRUE),
            		'id_admin' => $id_admin,
            	);

              $this->Msoaltryout->insert($data);

              //buat soal sejumlah inputan unser
              $jmlh_soal = $this->input->post('jmlh_soal'); //banyaknya soal
              if ($jmlh_soal > 250) {
                $jmlh_soal = 250;
              }
              for ($i=1; $i <= $jmlh_soal ; $i++) {
                $soal = array(
                  'nomor' => $i,
                  'kode_soal' => $kode_soal,
                );
                $this->Msoaltryout->createSoal($soal);
              }
              $this->session->set_flashdata('message', 'Create Record Success');
              redirect(site_url("admin/listsoaltryout?kode=$kode_soal"));
            }else {
              $this->session->set_flashdata('message', 'Kode sudah ada \nGagal menyimpan data');
              redirect(site_url('admin/soaltryout'));
            }

        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('kode', TRUE));
        } else {
          if ($this->Msoaltryout->cekData('kode',$this->input->post('kode')) ||
              $this->input->post('kode') == $this->input->post('kode_lama')) {

                $id_admin = $this->session->userdata('id'); //masukan id_admin disini
                //Create Slug
                $kode = url_title($this->input->post('kode', TRUE), 'dash', true);
                $check_slug=$this->Dbs->getdata('paket_soal',array('kode'=>$kode));
                if($check_slug->num_rows()>0){
                  if($id_post!=$check_slug->row()->id){
                    $number=$check_slug+1;
                    $slug=$slug."$number";
                  }else {
                    $slug=$slug;
                  }
                }
                //End Create Slug
                $data = array(
              		'kode' => $kode,
                  'kelas' => $this->input->post('kelas',TRUE),
              		'id_admin' => $id_admin,
              	);

                $this->Msoaltryout->update($this->input->post('kode_lama', TRUE), $data);
                $this->session->set_flashdata('message', 'Update Record Success');
                redirect(site_url('admin/soaltryout'));
          }else {
            $this->session->set_flashdata('message', 'Kode sudah ada \nGagal mengupdate data');
            redirect(site_url('admin/soaltryout'));
          }

        }
    }

    public function delete($id)
    {
        $row = $this->Msoaltryout->get_by_id($id);

        if ($row) {
            $this->Msoaltryout->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/soaltryout'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/soaltryout'));
        }
    }
    //Duplikat soal
    public function duplicate($id){
      $dataedit=$this->Msoaltryout->get_by_id($id);
      $data = array(
        'contain_view' => 'admin/soaltryout/soal_to_duplicate',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/soaltryout/duplicate_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'Duplikasi Data Soal'
       );
      $this->template->load($data);
    }

    public function duplicate_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('kode', TRUE));
        } else {
          $kode_baru_post = url_title($this->input->post('kode_baru', TRUE), 'dash', true);
          if ($this->Msoaltryout->cekData('kode',$kode_baru_post)) {
            //Create Slug
            $kode = url_title($this->input->post('kode_baru', TRUE), 'dash', true);
            $check_slug=$this->Dbs->getdata('paket_soal',array('kode'=>$kode));
            if($check_slug->num_rows()>0){
              if($id_post!=$check_slug->row()->id){
                $number=$check_slug+1;
                $slug=$slug."$number";
              }else {
                $slug=$slug;
              }
            }
            //End Create Slug
                $id_admin = $this->session->userdata('id'); //masukan id_admin disini
                $kode_soal_lama=$this->input->post('kode');
                $kode_soal_baru=$kode;
                //Buat Table Soal
                $dataInsertSoal = array(
                  'kode' => $kode_soal_baru,
                  'tanggal' => date('Y-m-d'),
                  'kelas' => $this->input->post('kelas',TRUE),
                  'id_admin' => $id_admin,
                );

                $this->Msoaltryout->insert($dataInsertSoal);
                //Buat Table Soal ENd
                $loadDbSoal=$this->Dbs->getdata('list_soal_to',array('kode_soal'=>$kode_soal_lama),1000);

                foreach ($loadDbSoal->result() as $obj) {
                  $dataInsertListSoal = array(
                      'nomor' => $obj->nomor,
                      'image'=>$obj->image,
                      'soal' => $obj->soal,
                      'tipe_pil' => $obj->tipe_pil,
                      'pil_a'=>$obj->pil_a,
                      'pil_b'=>$obj->pil_b,
                      'pil_c'=>$obj->pil_c,
                      'pil_d'=>$obj->pil_d,
                      'pil_e'=>$obj->pil_e,
                      'jawaban' => $obj->jawaban,
                      'penjelasan' => $obj->penjelasan,
                      'img_penjelasan' => $obj->img_penjelasan,
                      'mapel' => $obj->mapel,
                      'sub_bab' => $obj->sub_bab,
                      'kode_soal' =>$kode_soal_baru,
                  );

                  $this->Mlistsoaltryout->insert($dataInsertListSoal);
                }
                $this->session->set_flashdata('message', 'Update Record Success');
                redirect(site_url('admin/soaltryout'));
          }else {
            $this->session->set_flashdata('message', 'Kode sudah ada \nGagal mengupdate data');
            redirect(site_url('admin/soaltryout'));
          }

        }
    }


    public function _rules()
    {

      	$this->form_validation->set_rules('kode', 'kode', 'trim|required');
      	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
