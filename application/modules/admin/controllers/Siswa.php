<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Siswa extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Siswa_model');
        $this->load->model(array('Dbs'));
        $this->load->library('form_validation');
    }

    public function index()
    {

      $datasiswa=$this->Siswa_model->get_all();//panggil ke modell
      $datafield=$this->Siswa_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => 'admin/siswa/siswa_list',
        'sidebar'=>'admin/sidebar',
        'css'=>'admin/crudassets/css',
        'script'=>'admin/crudassets/script',
        'datasiswa'=>$datasiswa,
        'datafield'=>$datafield,
        'module'=>'admin',
        'titlePage'=>'siswa',
        'controller'=>'siswa'
       );
      $this->template->load($data);
    }


    public function create(){
      $orang_tua=$this->Dbs->getdata('orang_tua',null,1000);
      $kelas=$this->Dbs->getdata('kelas',null,1000);
      $data = array(
        'contain_view' => 'admin/siswa/siswa_form',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/siswa/create_action',
        'module'=>'admin',
        'titlePage'=>'siswa',
        'controller'=>'siswa'
       );

       //initData
       $data['orang_tua']=$orang_tua;
       $data['kelas']=$kelas;
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Siswa_model->get_by_id($id);
      $orang_tua=$this->Dbs->getdata('orang_tua',null,1000);
      $kelas=$this->Dbs->getdata('kelas',null,1000);
      $data = array(
        'contain_view' => 'admin/siswa/siswa_edit',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/siswa/update_action',
        'dataedit'=>$dataedit,
        'module'=>'admin',
        'titlePage'=>'siswa',
        'controller'=>'siswa'
       );
       //initData
       $data['orang_tua']=$orang_tua;
       $data['kelas']=$kelas;
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
    'nis' => $this->input->post('nis',TRUE),
		'nama' => $this->input->post('nama',TRUE),
		'gender' => $this->input->post('gender',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'id_kelas' => $this->input->post('id_kelas',TRUE),
		'password' => sha1($this->input->post('password',TRUE)),
		'email' => $this->input->post('email',TRUE),
		'id_admin' => $this->session->userdata('id'),
	    );

      if($this->input->post('id_ortu')!=''){
        $data['id_ortu'] = $this->input->post('id_ortu',TRUE);
      }
            $this->Siswa_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/siswa'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('nis', TRUE));
        } else {
            $data = array(
          		'nama' => $this->input->post('nama',TRUE),
          		'gender' => $this->input->post('gender',TRUE),
          		'alamat' => $this->input->post('alamat',TRUE),
          		'id_kelas' => $this->input->post('id_kelas',TRUE),
          		'email' => $this->input->post('email',TRUE),
              'id_admin' => $this->session->userdata('id'),
          	 );

             if ($this->input->post('id_ortu')!=NULL) {
               $data['id_ortu']= $this->input->post('id_ortu',TRUE);
             }

      if($this->input->post('password')!=''){
        $data['password']=sha1($this->input->post('password',TRUE));
      }

            $this->Siswa_model->update($this->input->post('nis', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/siswa'));
        }
    }

    public function delete($id)
    {
        $row = $this->Siswa_model->get_by_id($id);

        if ($row) {
            $this->Siswa_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/siswa'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/siswa'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('gender', 'gender', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('id_kelas', 'id kelas', 'trim|required');

	$this->form_validation->set_rules('nis', 'nis', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
