<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'class/ClientAPI.php';
class Admin extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    if($this->session->userdata('status')!='login'){
      redirect(base_url('login'));
    }
    if($this->session->userdata('level')!='admin'){
      $this->session->set_flashdata('message', 'Silahkan Login Menggunakan Akun Admin');
      redirect(redirect($_SERVER['HTTP_REFERER']));
    }
  }

  function index()
  {
    $api=new ClientAPI();
    $json_event=$api->get(base_url().'api/event/recent_list');
    $obj=json_decode($json_event);
    $obj_graph=(object)$this->graph();
    $listevent=$this->Dbs->getEventNow();
    //$listevent = $this->Dbs->listEventNow();
    $total_siswa=$this->Dbs->getdata('siswa',null,10000)->num_rows();
    $total_guru=0;
    $total_cabang=0;
    $total_kelas=$this->Dbs->getdata('kelas',null,10000)->num_rows();
    $data = array(
      'contain_view' => 'admin/home_v',
      'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
      'css'=>'admin/assets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
      'script'=>'admin/assets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
      'titlePage'=>'HOME',//Ini Judul Page untuk tiap halaman
     );
    //Data Variable
    $data['total_siswa']=$total_siswa;
    $data['total_guru']=$total_guru;
    $data['total_cabang']=$total_cabang;
    $data['total_kelas']=$total_kelas;
    $data['obj_graph']=$obj_graph;
    //Data List Event
    if($obj->total_result!=0){
      $data['event_list']=$obj->results;
    }
    if ($listevent->num_rows()>0) {
      $data['event_now'] = $listevent->result();
    }
    // $this->load->view('home_v', $data);
    // var_dump($data);die;
    $this->template->load($data);//pake sistem template, semua view yang di module berupa body saja
  }

  function graph(){
      //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
      $loadDb=$this->Dbs->graph();//database yang akan di load
      $check=$loadDb->num_rows();
      if($check>0){
        $get=$loadDb->result(); //Uncomment ini untuk contoh
        $arr_tanggal=[];
        $arr_peserta=[];
        foreach ($get as $gTgl) {
          array_push($arr_tanggal,$gTgl->tanggal);
          array_push($arr_peserta,$gTgl->total_peserta);
        }
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'tanggal'=>$arr_tanggal, //Uncomment ini untuk contoh
          'total_peserta'=>$arr_peserta
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    return $data;
  }
  function jurusan_favorit(){
      //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
      $loadDb=$this->Dbs->jurusan_fav();//database yang akan di load
      $check=$loadDb->num_rows();
      if($check>0){
        $get=$loadDb->result(); //Uncomment ini untuk contoh
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'result'=>$get
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    return $data;
  }
  function queryRadarJs(){

    //query untuk radasJS : semua mata pelajaran
    // $data = $this->Dbs->radarAllMapel()->result();
    // var_dump($data);

    //query untuk radasJS : semua mata pelajaran berdasarkan nise/ id_users
    // $data = $this->Dbs->radarAllMapel('19213231017')->result();
    // var_dump($data);

  }

}
