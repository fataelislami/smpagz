<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Event extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mevent','Kelas_model'));
        $this->load->library('form_validation');
        if($this->session->userdata('status')!='login'){
          redirect(base_url('login'));
        }
        if($this->session->userdata('level')!='admin'){
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }
    }

    public function index()
    {

      // $dataevent=$this->Mevent->get_all();//panggil ke modell
      // $datafield=$this->Mevent->get_field();//panggil ke modell
      $datatable = $this->Mevent->getDataTable();

      $data = array(
        'contain_view' => 'admin/event/event_list',
        'sidebar'=>'admin/sidebar',
        'css'=>'admin/crudassets/css',
        'script'=>'admin/crudassets/script',
        // 'dataevent'=>$dataevent,
        // 'datafield'=>$datafield,
        'datatable' => $datatable,
        'module'=>'admin',
        'titlePage'=>'event',
        'controller'=>'event'
       );
      $this->template->load($data);
    }


    public function create(){
      $soal_to = $this->Mevent->getSoal('paket_soal');
      $kelas = $this->Kelas_model->get_all();

      $data = array(
        'contain_view' => 'admin/event/event_form',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/event/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/event/create_action',
        'titlePage'=>'Tambah Event',
        'tryout' => $soal_to,
        'kelas'=>$kelas
       );
      $this->template->load($data);
    }

    public function edit($id){
      $soal_to = $this->Mevent->getSoal('paket_soal');
      $kelas = $this->Kelas_model->get_all();
      $dataedit=$this->Mevent->get_by_id($id);
      $data = array(
        'contain_view' => 'admin/event/event_edit',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/event/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/event/update_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'Edit Event',
        'tryout' => $soal_to,
        'kelas'=>$kelas
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $id_admin = $this->session->userdata('id'); //masukan id admin disini

            $data = array(
          		'nama' => $this->input->post('nama',TRUE),
          		'tanggal' => date('Y-m-d'),
              'password' => $this->input->post('password',TRUE),
          		'waktu_mulai' => $this->input->post('waktu_mulai',TRUE),
          		'waktu_selesai' => $this->input->post('waktu_selesai',TRUE),
              'durasi'=>$this->input->post('durasi'),
          		'id_admin' => $id_admin,
          	);
              $data['kode_soal'] = $this->input->post('soal_to',TRUE);
              $data['kelas'] = $this->input->post('kelas',TRUE);

            $this->Mevent->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/event'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id', TRUE));
        } else {
            $data = array(
          		'nama' => $this->input->post('nama',TRUE),
          		'tanggal' => $this->input->post('tanggal',TRUE),
              'password' => $this->input->post('password',TRUE),
          		'waktu_mulai' => $this->input->post('waktu_mulai',TRUE),
          		'waktu_selesai' => $this->input->post('waktu_selesai',TRUE),
              'durasi'=>$this->input->post('durasi'),
          	);
              $data['kode_soal'] = $this->input->post('soal_to',TRUE);
              $data['kelas'] = $this->input->post('kelas',TRUE);

            $this->Mevent->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/event'));
        }
    }

    public function delete($id)
    {
        $row = $this->Mevent->get_by_id($id);

        if ($row) {
            $this->Mevent->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/event'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/event'));
        }
    }

    public function _rules()
    {
      	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
      	$this->form_validation->set_rules('waktu_mulai', 'waktu mulai', 'trim|required');
      	$this->form_validation->set_rules('waktu_selesai', 'waktu selesai', 'trim|required');
        //$this->form_validation->set_rules('tanggal', 'tanggal', 'trim|required');
      	//$this->form_validation->set_rules('id_admin', 'id admin', 'trim|required');

      	$this->form_validation->set_rules('id', 'id', 'trim');
      	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
