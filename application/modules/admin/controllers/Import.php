<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Import extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Dbs');
        $this->load->library('form_validation');
        if($this->session->userdata('status')!='login'){
          redirect(base_url('login'));
        }
        if($this->session->userdata('level')!='admin'){
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }
    }

    public function index()
    {
      echo "Import";
    }

    function excel(){
      include APPPATH.'third_party/PHPExcel/PHPExcel.php';

      date_default_timezone_set("Asia/Jakarta");
      $tanggal = date('Y-m-d H:i:s');

      //cek data sekolah
      $id_sekolah = $this->input->post('id_sekolah');
      if ($this->Dbs->getdata("sekolah",array('id' => $id_sekolah))->num_rows() == 0) {
        $this->session->set_flashdata('message', 'Data sekolah tidak ada');
        redirect(site_url('admin/sekolah'));
      }

      if(isset($_FILES["import"]["name"]))
      {
          $path = $_FILES["import"]["tmp_name"];
          $object = PHPExcel_IOFactory::load($path);
          foreach($object->getWorksheetIterator() as $worksheet)
          {
              $highestRow = $worksheet->getHighestRow();
              $highestColumn = $worksheet->getHighestColumn();
              for($row=1; $row<=$highestRow; $row++)
              {
                  $nise         = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                  $nama         = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                  $kelas_mitra  = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                  $telephone    = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                  $email        = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                  $id_line      = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                  $password     = $worksheet->getCellByColumnAndRow(6, $row)->getValue();

                  if ($nise != "") { //jika nise tidak kosong
                    //cek apakah ada nise yang sama
                    $check = $this->Dbs->check("siswa",array('nise' => $nise))->num_rows();
                    if ($check > 0) {
                      echo "Eror:Duplikat NISE, $nise-$nama : row=$row <br />";
                    }else {
                      $list_siswa[] = array(
                        'nise' => "$nise",
                        'nama' => $nama,
                        'id_sekolah_mitra' => $id_sekolah,
                        'kelas_mitra' => $kelas_mitra,
                        'password' => sha1($password),
                        'telephone' => "$telephone",
                        'id_line' => $id_line,
                        'email' => $email,
                        'edulab' => '0',
                        'import_time' => $tanggal,
                      );
                    }
                  }
              }
          }
          // var_dump($list_siswa);die;
          $this->Dbs->insertBatch("siswa",$list_siswa);

          $this->session->set_flashdata('message', 'Import data success');
          redirect(site_url('admin/mitra?id_sekolah='.$id_sekolah));
      }
    }

    function eksternal(){
      include APPPATH.'third_party/PHPExcel/PHPExcel.php';

      date_default_timezone_set("Asia/Jakarta");
      $tanggal = date('Y-m-d H:i:s');

      if(isset($_FILES["import"]["name"]) AND isset($_POST['id_batch']))
      {
          $path = $_FILES["import"]["tmp_name"];
          $object = PHPExcel_IOFactory::load($path);
          $id_batch = $_POST['id_batch'];
          foreach($object->getWorksheetIterator() as $worksheet)
          {
              $highestRow = $worksheet->getHighestRow();
              $highestColumn = $worksheet->getHighestColumn();
              for($row=1; $row<=$highestRow; $row++)
              {
                  $nise         = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                  $nama         = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                  $kelas_mitra  = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                  $sekolah      = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                  $telephone    = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                  $email        = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                  $id_line      = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                  $password     = $worksheet->getCellByColumnAndRow(7, $row)->getValue();

                  if ($nise != "") { //jika nise tidak kosong
                    //cek apakah ada nise yang sama
                    $check = $this->Dbs->check("siswa",array('nise' => $nise))->num_rows();
                    if ($check > 0) {
                      echo "Eror:Duplikat NISE, $nise-$nama : row=$row <br />";
                    }else {
                      $list_siswa[] = array(
                        'nise' => "$nise",
                        'nama' => $nama,
                        'sekolah' => $sekolah,
                        'kelas_mitra' => $kelas_mitra,
                        'id_batch' => $id_batch,
                        'password' => sha1($password),
                        'telephone' => "$telephone",
                        'id_line' => $id_line,
                        'email' => $email,
                        'edulab' => '2',
                        'import_time' => $tanggal,
                      );
                    }
                  }
              }
          }
          // var_dump($list_siswa);die;
          $this->Dbs->insertBatch("siswa",$list_siswa);

          $this->session->set_flashdata('message', 'Import data success');
          redirect(site_url('admin/siswa_eksternal?id='.$id_batch));
      }
    }

    function pass_grade(){
      include APPPATH.'third_party/PHPExcel/PHPExcel.php';

      date_default_timezone_set("Asia/Jakarta");
      $tanggal = date('Y-m-d H:i:s');

      $idUniv = $this->input->post('idUniv');
      $kelompok = $this->input->post('kelompok');

      //cek apakah id univ ada atau tidak
      $numRowUniv = $this->Dbs->getdata("univ",array('id' => $idUniv))->num_rows();
      if (($kelompok!="saintek" AND $kelompok!="soshum") OR $numRowUniv < 1) {
        $this->session->set_flashdata('message', 'Kesalahan Data');
        redirect(site_url('admin/univ'));
      }

      if(isset($_FILES["import"]["name"]))
      {
          $path = $_FILES["import"]["tmp_name"];
          $object = PHPExcel_IOFactory::load($path);
          foreach($object->getWorksheetIterator() as $worksheet)
          {
              $highestRow = $worksheet->getHighestRow();
              $highestColumn = $worksheet->getHighestColumn();
              for($row=1; $row<=$highestRow; $row++)
              {
                  $kode = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                  $nama = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                  $pass_grade = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                  $tahun = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                  $peminat = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                  $daya_tampung = $worksheet->getCellByColumnAndRow(5, $row)->getValue();

                  //cek apakah ada kode yang sama, jika ada echo kode yang sama
                  $duplikat=0;
                  $numRowPassGrade = $this->Dbs->getdata("data_pass_grade",array('kode' => $kode))->num_rows();
                  if ($numRowPassGrade > 0) {
                    $duplikat=1;
                    echo "Duplikat Kode : kode=$kode - jurusan=$nama - PG=$pass_grade - tahun=$tahun (row $row) <br>";
                  }

                  if ($numRowPassGrade==0 AND $kode!="") {
                    $listJurusan[] = array(
                      'kode' => $kode,
                      'jurusan' => $nama,
                      'pass_grade' => "$pass_grade",
                      'tahun' => "$tahun",
                      'kelompok' => $kelompok,
                      'peminat' => "$peminat",
                      'daya_tampung' => "$daya_tampung",
                      'id_univ' => $idUniv,
                      'showing' => '0',);
                  }
              }
          }
          if ($duplikat!=1) {
            //var_dump($listJurusan);
            $this->Dbs->insertBatch("data_pass_grade",$listJurusan);

            $this->session->set_flashdata('message', 'Import data success');
            redirect(site_url('admin/univ/pgrade?id='.$idUniv.'&kel='.$kelompok));
          }else {
            echo "<br>*Gagal Impor Data";
            echo "$duplikat";
          }
      }
    }

    function imam(){
      $this->All();
    }

    function All(){
      include APPPATH.'third_party/PHPExcel/PHPExcel.php';

      if(isset($_FILES["import"]["name"]))
      {
          $path = $_FILES["import"]["tmp_name"];
          $object = PHPExcel_IOFactory::load($path);
          foreach($object->getWorksheetIterator() as $worksheet)
          {
              $temp = [];
              $highestRow = $worksheet->getHighestRow();
              $highestColumn = $worksheet->getHighestColumn();
              for($row=5; $row<=$highestRow; $row++)
              {
                  $univ = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
                  $kode = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                  $jurusan = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                  $pass_grade = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
                  $tahun = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
                  $kelompok = $worksheet->getCellByColumnAndRow(5, $row)->getValue();
                  $peminat = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
                  $daya_tampung = $worksheet->getCellByColumnAndRow(7, $row)->getValue();

                  $insert[] = array(
                    'kode' => $kode."".$tahun,
                    'jurusan' => $jurusan,
                    'pass_grade' => $pass_grade,
                    'tahun' => $tahun,
                    'kelompok' => $kelompok,
                    'peminat' => $peminat,
                    'daya_tampung' => $daya_tampung,
                    'id_univ' => $univ,
                    'showing' => '0',
                  );
              }
          }

          var_dump($insert);
      }
    }

    function checkUniv(){
      include APPPATH.'third_party/PHPExcel/PHPExcel.php';

      if(isset($_FILES["import"]["name"]))
      {
          $path = $_FILES["import"]["tmp_name"];
          $object = PHPExcel_IOFactory::load($path);
          foreach($object->getWorksheetIterator() as $worksheet)
          {
              //grup by kode univ
              $temp = [];
              $highestRow = $worksheet->getHighestRow();
              $highestColumn = $worksheet->getHighestColumn();
              for($row=5; $row<=$highestRow; $row++)
              {
                  $insert = 1;
                  $kode = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                  foreach ($temp as $k) {
                    if ($k == $kode) {
                      $insert = 0;
                    }
                  }

                  if ($insert == 1) {
                    $temp[] = $kode;
                  }
              }
              //end grub by univ
          }

          foreach ($temp as $k) {
            $cek = $this->Dbs->getdata("univ",array('id' => $kode))->num_rows();
            if ($cek > 0) {
              echo "$k = ada <br>";
            }else {
              echo "$k = tidak ada <br>";
            }
          }
      }
    }

    function checkJurusan(){
      include APPPATH.'third_party/PHPExcel/PHPExcel.php';

      if(isset($_FILES["import"]["name"]))
      {
          $path = $_FILES["import"]["tmp_name"];
          $object = PHPExcel_IOFactory::load($path);
          foreach($object->getWorksheetIterator() as $worksheet)
          {
              //grup by kode univ
              $temp = [];
              $highestRow = $worksheet->getHighestRow();
              $highestColumn = $worksheet->getHighestColumn();
              for($row=5; $row<=$highestRow; $row++)
              {
                  $insert = 1;
                  $kode = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                  foreach ($temp as $k) {
                    if ($k == $kode) {
                      $insert = 0;
                    }
                  }

                  if ($insert == 1) {
                    $temp[] = $kode;
                  }
              }
              //end grub by univ
          }

          foreach ($temp as $k) {
            $cek = $this->Dbs->getdata("data_pass_grade",array('kode' => $kode))->num_rows();
            if ($cek > 0) {
              echo "$k = ada <br>";
            }else {
              echo "$k = tidak ada <br>";
            }
          }
      }
    }

}
