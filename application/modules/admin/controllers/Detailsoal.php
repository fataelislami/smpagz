<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
  require_once 'class/ClientAPI.php';

class Detailsoal extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if($this->session->userdata('status')!='login'){
          redirect(base_url('login'));
        }
        if($this->session->userdata('level')!='admin'){
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }
    }

    public function index()
    {
      if (isset($_GET['kode']) AND !isset($_GET['target'])) { //untuk soal Tryout
        $kode = $this->input->get('kode');

        $api = new ClientAPI();
        $soal = json_decode($api->get(base_url()."api/tryout/detail_soal?kode=$kode"));

        if ($soal->total_result > 0) {
          $data = array(
            'contain_view' => 'admin/detailsoal/soal',
            'sidebar'=>'admin/sidebar',
            'css'=>'admin/crudassets/css',
            'script'=>'admin/detailsoal/crudassets/script',
            'module'=>'admin',
            'soal' => $soal->results[0],
            'titlePage'=>'detailsoal',
            'controller'=>'detailsoal'
           );
          $this->template->load($data);
        }else {
          echo "Soal Tidak Ada..!!! <br>";
          echo "<a href=\"javascript:history.go(-1)\">Kembali Ke Halaman Sebelumnya</a>";
        }
      }elseif (isset($_GET['kode']) AND isset($_GET['target'])){  //untuk Soal biasa
        $kode = $this->input->get('kode');

        $this->load->model(array('Dbs'));
        $soal = $this->Dbs->getSoalByKode($kode);
        $soal_num = $soal->num_rows();

        if ($soal_num > 0) {
          $soal = $soal->row();
          $soal->sub_bab = $soal->nama_sub;
          $data = array(
            'contain_view' => 'admin/detailsoal/soal',
            'sidebar'=>'admin/sidebar',
            'css'=>'admin/crudassets/css',
            'script'=>'admin/detailsoal/crudassets/script',
            'module'=>'admin',
            'soal' => $soal,
            'titlePage'=>'detailsoal',
            'controller'=>'detailsoal'
           );
          $this->template->load($data);
        }else {
          echo "Soal Tidak Ada..!!! <br>";
          echo "<a href=\"javascript:history.go(-1)\">Kembali Ke Halaman Sebelumnya</a>";
        }
      }else{
        redirect(site_url('admin'));
      }
    }
}
