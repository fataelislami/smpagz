<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Guru extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Guru_model');
        $this->load->model(array('Dbs'));
        $this->load->library('form_validation');
    }

    public function index()
    {

      $dataguru=$this->Guru_model->get_all();//panggil ke modell
      $datafield=$this->Guru_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => 'admin/guru/guru_list',
        'sidebar'=>'admin/sidebar',
        'css'=>'admin/crudassets/css',
        'script'=>'admin/crudassets/script',
        'dataguru'=>$dataguru,
        'datafield'=>$datafield,
        'module'=>'admin',
        'titlePage'=>'guru',
        'controller'=>'guru'
       );
      $this->template->load($data);
    }


    public function create(){
      $mata_pelajaran=$this->Dbs->getdata('mata_pelajaran',null,1000);
      $kelas=$this->Dbs->getdata('kelas',null,1000);
      $data = array(
        'contain_view' => 'admin/guru/guru_form',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/guru/create_action',
        'module'=>'admin',
        'titlePage'=>'guru',
        'controller'=>'guru'
       );
       $data['mata_pelajaran']=$mata_pelajaran;
       $data['kelas']=$kelas;
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Guru_model->get_by_id($id);
      $mata_pelajaran=$this->Dbs->getdata('mata_pelajaran',null,1000);
      $kelas=$this->Dbs->getdata('kelas',null,1000);

      $data = array(
        'contain_view' => 'admin/guru/guru_edit',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/guru/update_action',
        'dataedit'=>$dataedit,
        'module'=>'admin',
        'titlePage'=>'guru',
        'controller'=>'guru'
       );
       $data['mata_pelajaran']=$mata_pelajaran;
       $data['kelas']=$kelas;
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
              'nik' => $this->input->post('nik',TRUE),
          		'nama' => $this->input->post('nama',TRUE),
          		'gender' => $this->input->post('gender',TRUE),
          		'alamat' => $this->input->post('alamat',TRUE),
          		'tlp' => $this->input->post('tlp',TRUE),
          		'email' => $this->input->post('email',TRUE),
          		'mapel' => $this->input->post('mapel',TRUE),
          		'password' => sha1($this->input->post('password',TRUE)),
          		'id_admin' => $this->session->userdata('id'),
          	);
            $this->Guru_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/guru'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('nik', TRUE));
        } else {
            $data = array(
          		'nama' => $this->input->post('nama',TRUE),
          		'gender' => $this->input->post('gender',TRUE),
          		'alamat' => $this->input->post('alamat',TRUE),
          		'tlp' => $this->input->post('tlp',TRUE),
          		'email' => $this->input->post('email',TRUE),
          		'mapel' => $this->input->post('mapel',TRUE),
              'id_kelas' => $this->input->post('id_kelas',TRUE),
          		'id_admin' => $this->session->userdata('id'),
          	);
            if($this->input->post('password')!=''){
              $data['password']=sha1($this->input->post('password',TRUE));
            }
            $this->Guru_model->update($this->input->post('nik', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/guru'));
        }
    }

    public function delete($id)
    {
        $row = $this->Guru_model->get_by_id($id);

        if ($row) {
            $this->Guru_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/guru'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/guru'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('gender', 'gender', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('tlp', 'tlp', 'trim|required');
	$this->form_validation->set_rules('email', 'email', 'trim|required');
	$this->form_validation->set_rules('mapel', 'mapel', 'trim|required');

	$this->form_validation->set_rules('nik', 'nik', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
