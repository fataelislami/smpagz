<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Listsoaltryout extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mlistsoaltryout');
        $this->load->library('form_validation');
        if($this->session->userdata('status')!='login'){
          redirect(base_url('login'));
        }
        if($this->session->userdata('level')!='admin'){
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }
    }

    public function index()
    {
      if (isset($_GET['kode'])) {
        $kode = $this->input->get('kode');
        $cek = $this->Mlistsoaltryout->cekData('kode_soal',$kode,'list_soal');
        if ($cek) {
          // $datalistsoaltryout=$this->Mlistsoaltryout->get_all();//panggil ke modell
          // $datafield=$this->Mlistsoaltryout->get_field();//panggil ke modell
          $datatable = $this->Mlistsoaltryout->getDataTable($kode);

          $data = array(
            'contain_view' => 'admin/listsoaltryout/list_soal_to_list',
            'sidebar'=>'admin/sidebar',
            'css'=>'admin/crudassets/css',
            'script'=>'admin/listsoaltryout/crudassets/script',
            // 'datalistsoaltryout'=>$datalistsoaltryout,
            // 'datafield'=>$datafield,
            'datatable'=> $datatable,
            'kode' => $kode,
            'module'=>'admin',
            'titlePage'=>'listsoaltryout',
            'controller'=>'listsoaltryout'
           );
          $this->template->load($data);
        }else {
          redirect(site_url('admin/soaltryout'));
        }

      }else {
        redirect(site_url('admin/soaltryout'));
      }
    }


    public function create(){
      if (isset($_GET['kode'])) {
        $kode = $this->input->get('kode');
        $cek  = $this->Mlistsoaltryout->cekData('kode_soal',$kode,'list_soal');

        if ($cek) { //cek apakah kode soal ada didatabase
          $mapel = $this->Mlistsoaltryout->getMapel();
          $last_number = $this->Mlistsoaltryout->lastNumber($kode)->row();
          $data = array(
            'kode' => $kode,
            'mapel' => $mapel,
            'last_num' => $last_number->nomor + 1,
            'contain_view' => 'admin/listsoaltryout/list_soal_to_form',
            'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
            'css'=>'admin/listsoaltryout/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
            'script'=>'admin/listsoaltryout/crudassets/script2',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
            'action'=>'admin/listsoaltryout/create_action',
            'titlePage'=>'Tambah Soal',
            'module'=>'admin',
            'controller'=>'listsoaltryout',
           );
          $this->template->load($data);
        }else {
          if (isset($_SERVER["HTTP_REFERER"])) {
              header("Location: " . $_SERVER["HTTP_REFERER"]);
          }else {
              redirect(site_url('admin/soaltryout'));
          }
        }
      }else {
        if (isset($_SERVER["HTTP_REFERER"])) {
            header("Location: " . $_SERVER["HTTP_REFERER"]);
        }else {
            redirect(site_url('admin/soaltryout'));
        }
      }
    }

    public function edit($id){
      $dataedit = $this->Mlistsoaltryout->get_by_id($id);

      $soal = array(
        'kode' => $dataedit->kode,
        'nomor' => $dataedit->nomor,
        'soal' => $dataedit->soal,
        'jawaban' => $dataedit->jawaban,
        'penjelasan' => $dataedit->penjelasan,
        'id_mata_pelajaran' => $dataedit->id_mata_pelajaran,
        'kode_soal' => $dataedit->kode_soal,
      );


        $soal['pil_a'] = $dataedit->pil_a;
        $soal['pil_b'] = $dataedit->pil_b;
        $soal['pil_c'] = $dataedit->pil_c;
        $soal['pil_d'] = $dataedit->pil_d;
        $soal['pil_e'] = $dataedit->pil_e;
      $mapel = $this->Mlistsoaltryout->getMapel();
      $data = array(
        'contain_view' => 'admin/listsoaltryout/list_soal_to_edit',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/listsoaltryout/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/listsoaltryout/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/listsoaltryout/update_action',
        'dataedit'=>$soal,
        'mapel' =>$mapel,
        'module'=>'admin',
        'controller'=>'listsoaltryout',
        'titlePage'=>'Ubah Soal'
       );
      $this->template->load($data);
    }


    public function create_action(){
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
          $data = array(
              'nomor' => $this->input->post('nomor',TRUE),
              'soal' => $this->input->post('soal',FALSE),
              'jawaban' => $this->input->post('jawaban',TRUE),
              'penjelasan' => $this->input->post('penjelasan',FALSE),
              'id_mata_pelajaran' => $this->input->post('id_mata_pelajaran',TRUE),
              'kode_soal' => $this->input->post('kode_soal',TRUE),
              //'img_penjelasan' => $this->input->post('img_penjelasan',TRUE),
          );
          //upload gambar soal
          if ($_FILES['image']['name'] != "") {
            $img_soal = $this->upload_foto('image');
            if($img_soal['is_image']){
              $data['image']=$img_soal['file_name'];
            }else {
              $this->session->set_flashdata('Gagal 1', 'Gambar gagal diupload');
            }
          }

          //upload gambar penjelasan soal
          if ($_FILES['img_penjelasan']['name'] != "") {
            $P_soal = $this->upload_foto('img_penjelasan');
            if($P_soal['is_image']){
              $data['img_penjelasan']=$P_soal['file_name'];
            }else {
              $this->session->set_flashdata('Gagal 1', 'Gambar gagal diupload');
            }
          }
            $data['pil_a'] = $this->input->post('pil_a',FALSE);
            $data['pil_b'] = $this->input->post('pil_b',FALSE);
            $data['pil_c'] = $this->input->post('pil_c',FALSE);
            $data['pil_d'] = $this->input->post('pil_d',FALSE);
            $data['pil_e'] = $this->input->post('pil_e',FALSE);


            $this->Mlistsoaltryout->insert($data);

            $kode_soal = $this->input->post('kode_soal');
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url("admin/listsoaltryout?kode=$kode_soal"));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('kode', TRUE));
        } else {
            $data = array(
            		'nomor' => $this->input->post('nomor',TRUE),
            		'soal' => $this->input->post('soal',FALSE),
            		'jawaban' => $this->input->post('jawaban',TRUE),
            		'penjelasan' => $this->input->post('penjelasan',FALSE),
            		'id_mata_pelajaran' => $this->input->post('id_mata_pelajaran',TRUE),
                //'img_penjelasan' => $this->input->post('img_penjelasan',TRUE),
            );
            //upload gambar soal
            $img_soal = $this->upload_foto('image');
            if($img_soal['is_image']){
              $data['image']=$img_soal['file_name'];
            }else {
              $this->session->set_flashdata('Gagal 1', 'Gambar pilihan A gagal diupload');
            }
            //upload gambar penjelasan soal
            $P_soal = $this->upload_foto('img_penjelasan');
            if($P_soal['is_image']){
              $data['img_penjelasan']=$P_soal['file_name'];
            }else {
              $this->session->set_flashdata('Gagal 1', 'Gambar pilihan A gagal diupload');
            }


              $data['pil_a'] = $this->input->post('pil_a',FALSE);
              $data['pil_b'] = $this->input->post('pil_b',FALSE);
              $data['pil_c'] = $this->input->post('pil_c',FALSE);
              $data['pil_d'] = $this->input->post('pil_d',FALSE);
              $data['pil_e'] = $this->input->post('pil_e',FALSE);

            $this->Mlistsoaltryout->update($this->input->post('kode', TRUE), $data);

            $kode_soal = $this->input->post('kode_soal');
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url("admin/listsoaltryout?kode=$kode_soal"));
        }
    }

    public function delete($id)
    {
        $row = $this->Mlistsoaltryout->get_by_id($id);

        if ($row) {
            $this->Mlistsoaltryout->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            if (isset($_SERVER["HTTP_REFERER"])) {
                header("Location: " . $_SERVER["HTTP_REFERER"]);
            }else {
                redirect(site_url('admin/listsoaltryout'));
            }
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            if (isset($_SERVER["HTTP_REFERER"])) {
                header("Location: " . $_SERVER["HTTP_REFERER"]);
            }else {
                redirect(site_url('admin/listsoaltryout'));
            }
        }
    }

    public function upload_foto($formname){
      $config['upload_path']          = './xfile/images/soal';
      $config['allowed_types']        = 'gif|jpg|png|jpeg';
      $config['overwrite'] = FALSE;
      $config['encrypt_name'] = FALSE;
      //$config['max_size']             = 100;
      //$config['max_width']            = 1024;
      //$config['max_height']           = 768;
      $this->load->library('upload', $config);
      $this->upload->do_upload($formname);
      return $this->upload->data();
      /*Cara penggunaan
      PASTIKAN FOLDER TUJUAN PERMISSION NYA SUDAH BISA WRITE
      $this->upload_foto({ubah dengan name dari input type dari view});
      contoh
      jika <input type="file" name="file">
      maka
      $file=$this->upload_foto('file');
      untuk mengambil nama file gunakan $file['file_name'];
      untuk mengecek berhasil atau tidak gunakan kondisi
      if($file['is_image']!=1){
        echo "gagal";
      }else{
        echo "File Name : ".$file['file_name'];
        echo "Is Image ".$file['is_image'];
      }

      */
    }

    public function _rules()
    {
      	$this->form_validation->set_rules('nomor', 'nomor', 'trim|required');
      	//$this->form_validation->set_rules('image', 'image', 'trim|required');
      	// $this->form_validation->set_rules('soal', 'soal', 'trim|required');
      	// $this->form_validation->set_rules('tipe_pil', 'tipe pil', 'trim|required');
          // $this->form_validation->set_rules('file-a', 'pil a', 'trim|required');
        	// $this->form_validation->set_rules('file-b', 'pil b', 'trim|required');
        	// $this->form_validation->set_rules('file-c', 'pil c', 'trim|required');
        	// $this->form_validation->set_rules('pil_d', 'pil d', 'trim|required');
        	// $this->form_validation->set_rules('pil_e', 'pil e', 'trim|required');

          // $this->form_validation->set_rules('pil_a', 'pil a', 'trim|required');
        	// $this->form_validation->set_rules('pil_b', 'pil b', 'trim|required');
        	// $this->form_validation->set_rules('pil_c', 'pil c', 'trim|required');
        	// $this->form_validation->set_rules('pil_d', 'pil d', 'trim|required');
        	// $this->form_validation->set_rules('pil_e', 'pil e', 'trim|required');

      	// $this->form_validation->set_rules('jawaban', 'jawaban', 'trim|required');
      	//$this->form_validation->set_rules('penjelasan', 'penjelasan', 'trim|required');
      	//$this->form_validation->set_rules('img_penjelasan', 'img penjelasan', 'trim|required');
      	//$this->form_validation->set_rules('kode_soal', 'kode soal', 'trim|required');

      	$this->form_validation->set_rules('kode', 'kode', 'trim|required');
      	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
