<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mata_pelajaran extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Mata_pelajaran_model');
        $this->load->library('form_validation');
    }

    public function index()
    {

      $datamata_pelajaran=$this->Mata_pelajaran_model->get_all();//panggil ke modell
      $datafield=$this->Mata_pelajaran_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => 'admin/mata_pelajaran/mata_pelajaran_list',
        'sidebar'=>'admin/sidebar',
        'css'=>'admin/crudassets/css',
        'script'=>'admin/crudassets/script',
        'datamata_pelajaran'=>$datamata_pelajaran,
        'datafield'=>$datafield,
        'module'=>'admin',
        'titlePage'=>'mata_pelajaran',
        'controller'=>'mata_pelajaran'
       );
      $this->template->load($data);
    }


    public function create(){
      $data = array(
        'contain_view' => 'admin/mata_pelajaran/mata_pelajaran_form',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/mata_pelajaran/create_action',
        'module'=>'admin',
        'titlePage'=>'mata_pelajaran',
        'controller'=>'mata_pelajaran'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Mata_pelajaran_model->get_by_id($id);
      $data = array(
        'contain_view' => 'admin/mata_pelajaran/mata_pelajaran_edit',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/mata_pelajaran/update_action',
        'dataedit'=>$dataedit,
        'module'=>'admin',
        'titlePage'=>'mata_pelajaran',
        'controller'=>'mata_pelajaran'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
	    );

            $this->Mata_pelajaran_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/mata_pelajaran'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id', TRUE));
        } else {
            $data = array(
		'nama' => $this->input->post('nama',TRUE),
	    );

            $this->Mata_pelajaran_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/mata_pelajaran'));
        }
    }

    public function delete($id)
    {
        $row = $this->Mata_pelajaran_model->get_by_id($id);

        if ($row) {
            $this->Mata_pelajaran_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/mata_pelajaran'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/mata_pelajaran'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
