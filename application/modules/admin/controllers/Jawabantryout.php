<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
  require_once 'class/ClientAPI.php';

class Jawabantryout extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('MJawabanToSiswa','Mtryout_siswa');
        $this->load->model('MJawabanToUsers','Mtryout_users');
        if($this->session->userdata('status')!='login'){
          redirect(base_url('login'));
        }
        if($this->session->userdata('level')!='admin'){
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }
    }

    public function index()
    {
      if (isset($_GET['id_to'])) {
        $id_to = $this->input->get('id_to');

        if (isset($_GET['users']) AND $_GET['users'] == 'mitra') {
          // mengambil data TO users mitra sekolah
          $this->jawabanUsers($id_to);
        }else{
          // mengambil data TO users siswa edulab
          $this->jawabanSiswa($id_to);
        }
      }else {
        redirect(site_url('admin'));
      }
    }

    function jawabanSiswa($id){
      if ($this->Mtryout_siswa->get_tryout_by_id($id)->num_rows() > 0) {
        $dataTryout = $this->Mtryout_siswa->get_tryout_by_id($id)->row(); // mengambil data tryout
        $dataJawaban = $this->Mtryout_siswa->get_Jawaban_To($id,NULL)->result(); // mengambil data jawaban tryout

        //mengambil data jawaban tryout per mapel
        $listMapel = $this->Mtryout_siswa->getListMapelByTO($id)->result(); //mengambil list mapel
        $dataJawabanMapel = []; // array penampung

        foreach ($listMapel as $r) { // ulangi sebanyak banyaknya mapel
          $listJawabanPerMapel = $this->Mtryout_siswa->get_Jawaban_To($id,$r->id)->result();
          $benar = 0; $salah = 0; $null = 0;
          //menghitung jumlah benar salah kosong
          foreach ($listJawabanPerMapel as $a) {
            if ($a->jawaban_siswa == NULL) {
              $null++;
            }elseif ($a->jawaban_siswa == $a->kunci_jawaban) {
              $benar++;
            }else {
              $salah++;
            }
          }
          //mentimpan hasil perhitungan
          $hasil = array(
            'nama_mapel' => $r->nama,
            'benar' => $benar,
            'salah' => $salah,
            'kosong' => $null,
          );
          array_push($dataJawabanMapel,$hasil);
        }
        //end mengambil data jawaban tryout per mapel

        $data = array(
          'contain_view' => 'admin/jawabantryout/jawabantryout_list',
          'sidebar'=>'admin/sidebar',
          'css'=>'admin/crudassets/css',
          'script'=>'admin/jawabantryout/crudassets/script',
          'module'=>'admin',
          'jawaban' => $dataJawaban,
          'mapel' => $dataJawabanMapel,
          'titlePage'=>'jawabantryout',
          'controller'=>'jawabantryout'
         );
        $this->template->load($data);
      }else {
        redirect(base_url('admin'));
      }
    }

    function jawabanUsers($id){
      if ($this->Mtryout_users->get_tryout_by_id($id)->num_rows() > 0) {
        $dataTryout = $this->Mtryout_users->get_tryout_by_id($id)->row(); // mengambil data tryout
        $dataJawaban = $this->Mtryout_users->get_Jawaban_To($id,NULL)->result(); // mengambil data jawaban tryout

        //mengambil data jawaban tryout per mapel
        $listMapel = $this->Mtryout_users->getListMapelByTO($id)->result(); //mengambil list mapel
        $dataJawabanMapel = []; // array penampung

        foreach ($listMapel as $r) { // ulangi sebanyak banyaknya mapel
          $listJawabanPerMapel = $this->Mtryout_users->get_Jawaban_To($id,$r->id)->result();
          $benar = 0; $salah = 0; $null = 0;
          //menghitung jumlah benar salah kosong
          foreach ($listJawabanPerMapel as $a) {
            if ($a->jawaban_siswa == NULL) {
              $null++;
            }elseif ($a->jawaban_siswa == $a->kunci_jawaban) {
              $benar++;
            }else {
              $salah++;
            }
          }
          //mentimpan hasil perhitungan
          $hasil = array(
            'nama_mapel' => $r->nama,
            'benar' => $benar,
            'salah' => $salah,
            'kosong' => $null,
          );
          array_push($dataJawabanMapel,$hasil);
        }
        //end mengambil data jawaban tryout per mapel

        $data=array(
          'contain_view' => 'admin/jawabantryout/jawabantryout_list',
          'sidebar'=>'admin/sidebar',
          'css'=>'admin/crudassets/css',
          'script'=>'admin/jawabantryout/crudassets/script',
          'module'=>'admin',
          'jawaban'=>$dataJawaban,
          'mapel'=>$dataJawabanMapel,
          'idTryout'=>$id,
        );
        $this->template->load($data);
      }else {
        redirect(base_url('admin'));
      }
    }
}
