<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mexport extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function getdata($where=null,$from){
    if($where!=null){
      $this->db->where($where);
    }
    $db=$this->db->get($from);
    return $db;
  }

  function getwhere($where,$value,$table){
    $this->db->where($where, $value);
    $db=$this->db->get($table);
    return $db;
  }


  function insert($data,$table){
   $insert = $this->db->insert($table, $data);
   if ($this->db->affected_rows()>0) {
     return true;
     }else{
     return false;
     }
 }

 function update($data,$table,$where,$value){
    $this->db->where($where,$value);
    $db=$this->db->update($table,$data);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
  }

  function cekData($field,$Value,$table){
    $this->db->select('id');
    $this->db->where($field, $Value);
    $result = $this->db->get($table)->num_rows();
    if ($result > 0) {
      return true;
    }else {
      return false;
    }
  }

  function getTo_byEvent($id){
    $this->db->select('try_out.id,try_out.kode_soal,try_out.waktu_mulai');
    $this->db->select('data_pass_grade.kode as jurusan, data_pass_grade.id_univ as univ');
    $this->db->join('data_pass_grade', 'try_out.pil_1 = data_pass_grade.kode', 'left');
    $this->db->where('id_event', $id);
    // $this->db->limit(1000,999);
    return $this->db->get('try_out');
  }

  function getQuiz_byEvent($id){
    $this->db->select('quiz.id, quiz.kode_soal');
    $this->db->where('id_event', $id);
    return $this->db->get('quiz');
  }

  function getMatkul($kode){
    $this->db->select('mata_pelajaran.nama,mata_pelajaran.id');
    $this->db->from('list_soal_to');
    $this->db->join('mata_pelajaran', 'list_soal_to.mapel = mata_pelajaran.id', 'left');
    $this->db->where('list_soal_to.kode_soal', $kode);
    $this->db->group_by('list_soal_to.mapel');
    return $this->db->get()->result();
  }

  function getJenisEvent($id){
    $this->db->select('event.tipe');
    $this->db->where('id', $id);
    return $this->db->get('event');
  }

  function getJmlhSoal($kode_soal,$table){
    $this->db->select('nomor');
    $this->db->where('kode_soal', $kode_soal);
    $this->db->order_by('nomor','ASC');
    return $this->db->get($table);
  }

  function getSiswaByTryout($id){
    $this->db->select('siswa.nise,siswa.nama,sekolah,kode_kelas,cabang.cabang');
    $this->db->from('try_out');
    $this->db->join('siswa', 'try_out.nise = siswa.nise', 'left');
    $this->db->join('cabang', 'siswa.kode_cabang = cabang.kode', 'left');
    $this->db->where('try_out.id', $id);
    return $this->db->get();
  }

  function getPilihanTo($id){
    $this->db->select('pil_1,pil_2,pil_3');
    $this->db->where('id', $id);
    return $this->db->get('try_out');
  }

  function getPassGrade($id_univ){
    $this->db->select('univ.nama,data_pass_grade.jurusan,data_pass_grade.pass_grade');
    $this->db->join('univ', 'data_pass_grade.id_univ = univ.id');
    $this->db->where('kode', $id_univ);
    return $this->db->get('data_pass_grade');
  }

  function getJawaban2($id_to,$mapel){
    $this->db->select(' tb1.id, list_soal_to.kode, list_soal_to.nomor, mata_pelajaran.nama');
    $this->db->select('tb1.jawaban as jawaban_siswa');
    $this->db->select('list_soal_to.jawaban as kunci_jawaban');
    $this->db->from('try_out');
    $this->db->join('list_soal_to', 'try_out.kode_soal = list_soal_to.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal_to.mapel = mata_pelajaran.id', 'left');
    $this->db->join("(SELECT * FROM list_jawaban_to WHERE list_jawaban_to.id_try_out = $id_to) AS tb1", 'list_soal_to.kode = tb1.kode_list_soal', 'left');
    if ($mapel != NULL) {
      $this->db->where('mata_pelajaran.id', $mapel);
    }
    $this->db->where('try_out.id', $id_to);
    $this->db->order_by('list_soal_to.nomor','ASC');
    return $this->db->get();
  }

  function getJawabanMitra($id_to,$mapel){
    $this->db->select('tb1.id, list_soal_to.kode, list_soal_to.nomor, mata_pelajaran.nama');
    $this->db->select('tb1.jawaban as jawaban_siswa');
    $this->db->select('list_soal_to.jawaban as kunci_jawaban');
    $this->db->from('try_out_users');
    $this->db->join('list_soal_to', 'try_out_users.kode_soal = list_soal_to.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal_to.mapel = mata_pelajaran.id', 'left');
    $this->db->join("(SELECT * FROM list_jawaban_to_users WHERE list_jawaban_to_users.id_try_out_users = $id_to) AS tb1", 'list_soal_to.kode = tb1.kode_list_soal', 'left');
    if ($mapel != NULL) {
      $this->db->where('mata_pelajaran.id', $mapel);
    }
    $this->db->where('try_out_users.id', $id_to);
    $this->db->order_by('list_soal_to.nomor','ASC');
    return $this->db->get();
  }

  function getListMapelByTO($id_to){
    $this->db->select('mata_pelajaran.nama , mata_pelajaran.id');
    $this->db->from('try_out');
    $this->db->join('list_soal_to', 'try_out.kode_soal = list_soal_to.kode_soal', 'left');
    $this->db->join('mata_pelajaran', 'list_soal_to.mapel = mata_pelajaran.id', 'left');
    $this->db->where('try_out.id', $id_to);
    $this->db->group_by('mata_pelajaran.id');
    return $this->db->get();
  }

  function getTo_byEvent_v2($id_event,$edulab=1){
    $this->db->select('try_out.id,try_out.nise,try_out.waktu_mulai,');
    $this->db->select('siswa.nise,siswa.nama');
    $this->db->select('cabang.cabang as kode_cabang');
    $this->db->select('univ1.kode as kode_jurusan1, univ1.nama as nama_univ1, univ1.jurusan as jurusan_univ1, univ1.pass_grade as pg_univ1');
    $this->db->select('univ2.kode as kode_jurusan2, univ2.nama as nama_univ2, univ2.jurusan as jurusan_univ2, univ2.pass_grade as pg_univ2');
    $this->db->join('siswa', 'try_out.nise = siswa.nise', 'left');
    $this->db->join('cabang', 'siswa.kode_cabang = cabang.kode', 'left');
    $this->db->join("(SELECT kode,jurusan,nama,pass_grade FROM data_pass_grade LEFT JOIN univ ON data_pass_grade.id_univ = univ.id) as univ1", 'try_out.pil_1 = univ1.kode', 'left');
    $this->db->join("(SELECT kode,jurusan,nama,pass_grade FROM data_pass_grade LEFT JOIN univ ON data_pass_grade.id_univ = univ.id) as univ2", 'try_out.pil_2 = univ2.kode', 'left');
    if ($edulab == 0) { //jika siswa mitra
      $this->db->select('sekolah.nama as sekolah,kelas_mitra as kode_kelas');
      $this->db->join('sekolah', 'siswa.id_sekolah_mitra = sekolah.id', 'left');
      $this->db->where('siswa.edulab', '0');
    }elseif ($edulab == 2) { //jika siswa eksternal
      $this->db->select('siswa.sekolah,siswa.kelas_mitra as kode_kelas');
      $this->db->where('siswa.edulab', '2');
    }else { //jika siswa edulab
      $this->db->select('siswa.sekolah,siswa.kode_kelas');
      $this->db->where('siswa.edulab', '1');
    }
    $this->db->where('try_out.id_event', $id_event);
    $this->db->order_by('univ1.nama','ASC');
    $this->db->order_by('univ1.jurusan','ASC');
    $this->db->order_by('try_out.id','ASC');
    return $this->db->get('try_out');
  }

  function getToMitra_byEvent_v2($id_event){
    $this->db->select('try_out_users.id,try_out_users.id_user,try_out_users.waktu_mulai,');
    //$this->db->select('siswa.nise,siswa.nama,siswa.sekolah,siswa.kode_kelas');
    //$this->db->select('cabang.cabang as kode_cabang');
    $this->db->select('users.id as id_users,users.nama,sekolah.nama as sekolah,users.kelas');
    $this->db->select('univ1.nama as nama_univ1, univ1.jurusan as jurusan_univ1, univ1.pass_grade as pg_univ1');
    $this->db->select('univ2.nama as nama_univ2, univ2.jurusan as jurusan_univ2, univ2.pass_grade as pg_univ2');
    $this->db->join('users', 'try_out_users.id_user = users.id', 'left');
    $this->db->join('sekolah', 'users.id_sekolah = sekolah.id', 'left');
    //$this->db->join('cabang', 'siswa.kode_cabang = cabang.kode', 'left');
    $this->db->join("(SELECT kode,jurusan,nama,pass_grade FROM data_pass_grade LEFT JOIN univ ON data_pass_grade.id_univ = univ.id) as univ1", 'try_out_users.pil_1 = univ1.kode', 'left');
    $this->db->join("(SELECT kode,jurusan,nama,pass_grade FROM data_pass_grade LEFT JOIN univ ON data_pass_grade.id_univ = univ.id) as univ2", 'try_out_users.pil_2 = univ2.kode', 'left');
    $this->db->where('try_out_users.id_event', $id_event);
    $this->db->order_by('univ1.nama','ASC');
    $this->db->order_by('univ1.jurusan','ASC');
    $this->db->order_by('try_out_users.id','ASC');
    return $this->db->get('try_out_users');
  }

  function getJmlhSoal_v2($kode_soal){
    $this->db->select('list_soal_to.nomor');
    $this->db->from('list_soal_to');
    $this->db->where('list_soal_to.kode_soal', $kode_soal);
    $this->db->order_by('list_soal_to.nomor','ASC');
    return $this->db->get();
  }




}
