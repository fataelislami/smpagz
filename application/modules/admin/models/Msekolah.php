<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Msekolah extends CI_Model
{

    public $table = 'sekolah';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    //get field

    function get_field(){
      $table=$this->table;
      $sql=$this->db->query("SELECT id,nama,alamat,waktu_mulai,waktu_selesai FROM `$table`"); //ganti * untuk custom field yang ditampilkan pada table
      return $sql->list_fields();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('nama', $q);
	$this->db->or_like('alamat', $q);
	$this->db->or_like('tlp', $q);
	$this->db->or_like('email', $q);
	$this->db->or_like('deskripsi', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('nama', $q);
	$this->db->or_like('alamat', $q);
	$this->db->or_like('tlp', $q);
	$this->db->or_like('email', $q);
	$this->db->or_like('deskripsi', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Msekolah.php */
/* Location: ./application/models/Msekolah.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2019-08-23 05:22:28 */
/* http://harviacode.com */
