<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbs extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function check($table,$where){
		return $this->db->get_where($table,$where);
	}

  public function insertBatch($table,$data){
      $this->db->insert_batch($table,$data);
      return $this->db->insert_id();
  }

  function getdata($from,$where=null,$limit=9,$offset=0){
    if($where!=null){
      $this->db->where($where);
    }
    $this->db->limit($limit, $offset);
    $db=$this->db->get($from);
    return $db;
  }

  function getDataByIdAndTable($table,$tableId,$id){
    $this->db->select('*');
    $this->db->from($table);
    $this->db->where($tableId, $id);
    return $this->db->get();
  }

  function insert($data,$table){
   $insert = $this->db->insert($table, $data);
   if ($this->db->affected_rows()>0) {
     return true;
     }else{
     return false;
     }
 }
 function update($data,$table,$where){
    $this->db->where($where);
    $db=$this->db->update($table,$data);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
  }

  function graph(){
    //graph 7 event sebelumnya
    $sql="SELECT `tanggal`,COUNT(`id`) as `total_peserta` FROM `tryout` GROUP BY `tanggal` order by `tanggal` DESC LIMIT 7";
    return $this->db->query($sql);
  }

  function jurusan_fav(){
    $sql="SELECT `kelompok`,`jurusan`,`tahun`,`pil_1` as `jurusan_favorit`,count(`pil_1`) as `total_pilihan` FROM `tryout` JOIN `data_pass_grade` on `tryout`.`pil_1`=`data_pass_grade`.`kode` group by `pil_1` ORDER BY `total_pilihan` DESC LIMIT 4";
    return $this->db->query($sql);
  }

  // function getEventNow(){
  //   //TIMEZON php
  //   $tz = 'Asia/Jakarta';
  //   $timestamp = time();
  //   $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  //   $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  //   $now=$dt->format('Y-m-d H:i:s');
  //
  //   $this->db->select('tryout_event.nama,event.tipe,event.waktu_mulai,event.waktu_selesai');
  //   $this->db->select('COUNT(tryout.id) AS jumlah_to');
  //   $this->db->select('COUNT(quiz.id) AS jumlah_quiz');
  //   $this->db->from('tryout_event');
  //   $this->db->join('tryout', 'tryout_event.id = tryout.id_event', 'left');
  //   $this->db->join('quiz', 'tryout_event.id = quiz.id_event', 'left');
  //   $this->db->where("'$now' BETWEEN event.waktu_mulai AND event.waktu_selesai");
  //   $this->db->where("'$now' BETWEEN tryout.waktu_mulai AND DATE_ADD(tryout.waktu_mulai,INTERVAL 1000 MINUTE)");
  //   $this->db->where("quiz.waktu_selesai IS NULL");
  //   $this->db->group_by('tryout_event.id');
  //   return $this->db->get();
  // }

  function getEventNow(){
    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $now=$dt->format('Y-m-d H:i:s');

    $this->db->select('tryout_event.*, COUNT(tb_join.id_to) as jumlah_to');
    $this->db->from('tryout_event');
    $this->db->join("(
         SELECT tryout_event.*, tryout.id as id_to FROM tryout_event
         LEFT JOIN tryout ON tryout_event.id = tryout.id_event
         WHERE '$now' BETWEEN `tryout_event`.`waktu_mulai` AND tryout_event.waktu_selesai
         AND '$now' BETWEEN `tryout`.`waktu_mulai` AND DATE_ADD(tryout.waktu_mulai,INTERVAL tryout_event.durasi MINUTE)
     ) AS tb_join ","tryout_event.id = tb_join.id", "LEFT");
    $this->db->where("'$now' BETWEEN `tryout_event`.`waktu_mulai` AND tryout_event.waktu_selesai");
    $this->db->group_by('tryout_event.id');
    return $this->db->get();
  }

  function listEventNow(){
    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $now=$dt->format('Y-m-d H:i:s');

    $this->db->select('tryout_event.id,event.nama,event.tipe,event.waktu_mulai,event.waktu_selesai,event.durasi');
    $this->db->from('tryout_event');
    $this->db->where("'$now' BETWEEN event.waktu_mulai AND event.waktu_selesai");
    return $this->db->get();
  }

  function countTryoutByEvent($id,$durasi){
    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $now=$dt->format('Y-m-d H:i:s');

    $this->db->select('COUNT(tryout.id) AS res_count');
    $this->db->from('tryout');
    $this->db->where('tryout.id_event', $id);
    $this->db->where("'$now' BETWEEN `tryout`.`waktu_mulai` AND DATE_ADD(tryout.waktu_mulai,INTERVAL $durasi MINUTE) ");
    return $this->db->get();
  }

  function countQuizByEvent($id,$durasi){
    //TIMEZON php
    $tz = 'Asia/Jakarta';
    $timestamp = time();
    $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    $now=$dt->format('Y-m-d H:i:s');

    $this->db->select('COUNT(quiz.id) AS res_count');
    $this->db->from('quiz');
    $this->db->where('quiz.id_event', $id);
    $this->db->where("'$now' BETWEEN `quiz`.`waktu_mulai` AND DATE_ADD(quiz.waktu_mulai,INTERVAL $durasi MINUTE) ");
    return $this->db->get();
  }

  function resetWaktuServer($time, $tanggal){
    $query = "UPDATE `tryout` set `waktu_mulai` = DATE_ADD(`waktu_mulai`,INTERVAL $time minute) where `tanggal` = '$tanggal'";
    $db=$this->db->query($query);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
  }

  function getSoalByKode($id){
    $this->db->select('soal.*, mata_pelajaran.nama, sub_mapel.nama_sub');
    $this->db->join('mata_pelajaran', 'soal.mapel = mata_pelajaran.id', 'left');
    $this->db->join('sub_mapel', 'soal.sub_bab = sub_mapel.id', 'left');
    $this->db->where('soal.kode', $id);
    return $this->db->get('soal');
  }

  function radarAllMapel($id=NULL){
    $this->db->select('mata_pelajaran.id, mata_pelajaran.nama');
    $this->db->select('COALESCE(SUM(soal_jawaban.point),0) as total_point');
    $this->db->from('mata_pelajaran');
    if ($id != "") { //untuk mendapatkan seluru point matapelajaran by siswa
      $this->db->join("(SELECT soal.mapel as id_mapel, SUM(list_jawaban_soal.point_soal) AS point
                       FROM list_jawaban_soal
                       LEFT JOIN soal ON list_jawaban_soal.kode_soal = soal.kode
                       WHERE list_jawaban_soal.nise = $id
                       GROUP BY soal.mapel) as soal_jawaban",
                      'mata_pelajaran.id = soal_jawaban.id_mapel',
                      'left');
    }else{ //untuk menpatkan point seluru mata pelajaran
      $this->db->join('(SELECT soal.mapel as id_mapel, SUM(list_jawaban_soal.point_soal) AS point
                       FROM list_jawaban_soal
                       LEFT JOIN soal ON list_jawaban_soal.kode_soal = soal.kode
                       GROUP BY soal.mapel) as soal_jawaban',
                      'mata_pelajaran.id = soal_jawaban.id_mapel',
                      'left');
    }
    $this->db->group_by('mata_pelajaran.id');
    $table_siswa = $this->db->get_compiled_select();

    $this->db->select('mata_pelajaran.id, mata_pelajaran.nama');
    $this->db->select('COALESCE(SUM(soal_jawaban.point),0) as total_point');
    $this->db->from('mata_pelajaran');
    if ($id != "") { //untuk mendapatkan seluru point matapelajaran by users
      $this->db->join("(SELECT soal.mapel as id_mapel, SUM(list_jawaban_soal_users.point_soal) AS point
                       FROM list_jawaban_soal_users
                       LEFT JOIN soal ON list_jawaban_soal_users.kode_soal = soal.kode
                       WHERE list_jawaban_soal_users.user_id = $id
                       GROUP BY soal.mapel) as soal_jawaban",
                      'mata_pelajaran.id = soal_jawaban.id_mapel',
                      'left');
    }else { //untuk menpatkan point seluru mata pelajaran
      $this->db->join('(SELECT soal.mapel as id_mapel, SUM(list_jawaban_soal_users.point_soal) AS point
                       FROM list_jawaban_soal_users
                       LEFT JOIN soal ON list_jawaban_soal_users.kode_soal = soal.kode
                       GROUP BY soal.mapel) as soal_jawaban',
                      'mata_pelajaran.id = soal_jawaban.id_mapel',
                      'left');
    }
    $this->db->group_by('mata_pelajaran.id');
    $table_users = $this->db->get_compiled_select();

    $this->db->select('id,nama,SUM(total_point) AS total_point');
    $this->db->from("($table_siswa UNION $table_users) as table_result");
    $this->db->order_by('table_result.id');
    $this->db->group_by('table_result.id');
    return $this->db->get();
  }

  function getJawaban($status,$nise=NULL){
    $this->db->select('COUNT(list_jawaban_soal.id) as total');
    $this->db->from('list_jawaban_soal');
    if ($status == 'benar') {
      $this->db->join('soal', 'list_jawaban_soal.kode_soal = soal.kode', 'left');
      $this->db->where('list_jawaban_soal.jawaban = soal.jawaban');
    }elseif ($status == 'salah') {
      $this->db->join('soal', 'list_jawaban_soal.kode_soal = soal.kode', 'left');
      $this->db->where('list_jawaban_soal.jawaban != soal.jawaban');
    }elseif ($status == 'kosong') {
      $this->db->where('list_jawaban_soal.jawaban IS NULL');
    }
    if ($nise!=NULL) {
      $this->db->where('list_jawaban_soal.nise', $nise);
    }
    return $this->db->get();
  }

}
