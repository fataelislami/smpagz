  <nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li class="nav-small-cap">Menu</li>
        <!-- //BATAS SATU MENU -->
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>admin"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>admin/soaltryout"><i class="mdi mdi-file-document-box"></i><span class="hide-menu">Soal</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>admin/siswa"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Siswa</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>admin/mata_pelajaran"><i class="mdi mdi-book-open-page-variant"></i><span class="hide-menu">Data Mata Pelajaran</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>admin/Event"><i class="mdi mdi-bell"></i><span class="hide-menu">Event</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>admin/kelas"><i class="mdi mdi-houzz"></i><span class="hide-menu">Kelola Kelas</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>admin/guru"><i class="mdi mdi-account-star-variant"></i><span class="hide-menu">Kelola Guru</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>admin/orang_tua"><i class="mdi mdi-account-circle"></i><span class="hide-menu">Kelola Orang Tua</span></a>
        </li>

        <!-- //BATAS SATU MENU -->



        <!-- <li>
        CONTOH MENU DENGAN SUBMENU
            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-pencil"></i><span class="hide-menu">Tulisan</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="<?php echo base_url()?>admin/tulisan">Tulisan Saya</a></li>
                <li><a href="<?php echo base_url()?>admin/tulisan/all">Tulisan Creator</a></li>
            </ul>
        </li>
         -->
<!-- REFERENSI IKON UNTUK MENU : https://cdn.materialdesignicons.com/1.1.34/ -->
    </ul>
</nav>
