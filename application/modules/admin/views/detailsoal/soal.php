<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

              <div class="soal">
                <div class="scrollbar" style="height:23%">
                  <h3><strong>Soal No <?= $soal->nomor; ?></strong> </h3>
                  <?php echo $soal->soal; ?>
                  <br>
                    <ol type="A">
                      <li><h4><?php echo $soal->pil_a; ?></h4></li>
                      <li><h4><?php echo $soal->pil_b; ?></h4></li>
                      <li><h4><?php echo $soal->pil_c; ?></h4></li>
                      <li><h4><?php echo $soal->pil_d; ?></h4></li>
                      <li><h4><?php echo $soal->pil_e; ?></h4></li>
                    </ol>
                  <br>
                  <h4><strong> Jawaban : <?php echo $soal->jawaban; ?> </strong></h4>
                  <hr>
                  <br>

                  <h4><strong>Pembahasan Soal No <?= $soal->nomor; ?></strong> </h4>
                  <h5>Mata Pelajaran : <?php echo $soal->nama; ?> </h5>
                  <br>
                  <?php echo $soal->penjelasan; ?>
                </div>
              </div>


            </div>
        </div>
    </div>
</div>
