<script src="<?php echo base_url()?>assets/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/tinymce/plugins/tiny_mce_wiris/integration/WIRISplugins.js?viewer=image"></script>

<!-- Sweet-Alert  -->
<script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
<script>
jQuery(document).ready(function() {
//Contoh Image Upload Summernote
    $('.summernote').summernote({
        height: 350, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: false, // set focus to editable area after initializing summernote
        fontNames: [
        'Arial', 'Arial Black', 'Cambria', 'Comic Sans MS', 'Courier New',
        'Helvetica Neue', 'Helvetica', 'Impact', 'Lucida Grande',
        'Tahoma', 'Times New Roman', 'Verdana',
        ],
        callbacks: {
          onImageUpload: function(image) {
                      uploadImage(image[0],this);
                          // console.log(image[0]);
                      }
    }
    });
    //http://localhost/edulab/admin/upload/summernote
    function uploadImage(image,summernoteID) {
      var form = new FormData();
      form.append("file", image);

      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "<?php echo base_url()?>admin/upload/summernote",
        "method": "POST",
        "headers": {
          "cache-control": "no-cache",
          "Postman-Token": "710b393d-9fe8-4eb5-bdbc-2d53b3c077b9"
        },
        "processData": false,
        "contentType": false,
        "mimeType": "multipart/form-data",
        "data": form
      }

      $.ajax(settings).done(function (response) {
        var obj=JSON.parse(response);
        var url="<?php echo base_url() ?>xfile/images/"+obj.file_name;
        var image = $('<img>').attr('src',url);
        $(summernoteID).summernote("insertNode", image[0]);
        // console.log(obj.file_name);
      });
}

    $('.pilihan_ganda').summernote({
        height: 50, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: false,// set focus to editable area after initializing summernote
        fontNames: [
        'Arial', 'Arial Black', 'Cambria', 'Comic Sans MS', 'Courier New',
        'Helvetica Neue', 'Helvetica', 'Impact', 'Lucida Grande',
        'Tahoma', 'Times New Roman', 'Verdana',
        ],
        toolbar: [
        ['font', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['insert', ['link', 'picture', 'hr']],
        ['view', ['fullscreen', 'codeview']],
        ['help', ['help']]
  ],
  callbacks: {
    onImageUpload: function(image) {
                uploadImage(image[0],this);
                    // console.log(image[0]);
                }
}
    });

    $('.inline-editor').summernote({
        airMode: true
    });
});

window.edit = function() {
        $(".click2edit").summernote()
    },
    window.save = function() {
        $(".click2edit").summernote('destroy');
    }
</script>

<!-- This is data table -->
<script src="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<script>
$(document).ready(function() {
    $('#myTable').DataTable();
    $(document).ready(function() {
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function() {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
});
$('#example23').DataTable({
    dom: 'Bfrtip',
    buttons: [
      'copy'
    ],
    "order": [
        [2, 'asc']
    ]
});
</script>
<script src="<?php echo base_url()?>assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
<script type="text/javascript">
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
  var randomString=makeid();
</script>
<script src="<?php echo base_url()?>assets/plugins/dropzone-master/dist/dropzone.js"></script>
<script type="text/javascript">
$('#aplot').click(function() {
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#example23").on("click", ".modalDelete", function(){//event khusus untuk table datatables setelah pagination suka error
      var id=$(this).val();
      console.log(id);
      $("#modalMsg").html("Apakah Anda Yakin Ingin Menghapus data "+id+" ? ");
      $("#modalHref").attr("href", "<?php echo base_url().$module?>/<?php echo $controller; ?>/delete/"+id);
    });
  });
</script>

<!-- jQuery file upload -->
    <script src="<?php echo base_url()?>assets/plugins/dropify/dist/js/dropify.min.js"></script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>

<!-- ini script untuk mengubah tipe jawaban (image / text) -->
<script type="text/javascript">
  $(document).ready(function(){
    $(".tipepilihan-1").show();
    $(".tipepilihan-2").hide();

    $('input[type=radio][name=tipe_pil]').change(function() {
        if (this.value == 'text') {
            $(".tipepilihan-2").hide();
            $(".tipepilihan-1").show();
            //$(".text-pilihan").attr("required", true);
            //$(".img-pilihan").attr("required", false);
        }
        else if (this.value == 'image') {
            $(".tipepilihan-1").hide();
            $(".tipepilihan-2").show();
          //  $(".text-pilihan").attr("required", false);
          //  $(".img-pilihan").attr("required", true);
        }
    });
  });
</script>

<!-- ini script untuk pengecekan nomor soal -->
<script type="text/javascript">
  $(document).ready(function(){
    //pengecekan form input kode kelas
    $("#nomorform").change(function(){
      var nomor = $("#nomorform").val();
      var kode_soal = $("#kode_soal").val();

      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "<?php echo base_url()?>api/check/soal_to?nomor="+nomor+"&kode_soal="+kode_soal,
        "method": "GET",
        "headers": {
          "cache-control": "no-cache",
          "Postman-Token": "1f7f7a89-0b64-478e-a8dd-26120f422f56"
        }
      }

      $.ajax(settings).done(function (response) {
        if (response.results == false && nomor != "") {
          inputNomorSuccess();
        }else {
          inputNomorFailed();
        }
      });
    });

    function inputNomorSuccess(){
      $("#inputNomor").attr('class', 'form-group col-md-3 has-success');
      $('#info_nomor').text("Tersedia");
      $('#info_nomor').show();
      $(':input[type="submit"]').prop('disabled', false);
    }

    function inputNomorFailed(){
      $("#inputNomor").attr('class', 'form-group col-md-3 has-danger');
      $('#info_nomor').text("nomor sudah ada");
      $('#info_nomor').show();
      $(':input[type="submit"]').prop('disabled', true);
    }

  });
</script>
<!-- wysuhtml5 Plugin JavaScript -->
<script src="<?php echo base_url()?>assets/plugins/tinymce/tinymce.min.js"></script>
<!-- <script>
$(document).ready(function() {

    if ($(".mymce").length > 0) {
        tinymce.init({
            selector: "textarea.mymce",
            theme: "modern",
            height: 300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor tiny_mce_wiris"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview media fullpage | forecolor backcolor | tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry",
            paste_data_images:true

        });
    }
});
</script> -->
<!-- <script>
$(document).ready(function() {

    if ($(".pilihan_ganda").length > 0) {
        tinymce.init({
            selector: "textarea.pilihan_ganda",
            theme: "modern",
            height: 70,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor tiny_mce_wiris"
            ],
            toolbar: "tiny_mce_wiris_formulaEditor tiny_mce_wiris_formulaEditorChemistry",
            paste_data_images:true

        });
    }
});
</script> -->
<!-- ============================================================== -->
