<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah List_soal_to</h4>
            <form class="form-horizontal m-t-40" method="post" action="<?php echo base_url().$action ?>"  enctype="multipart/form-data" >
              <input type="hidden" name="kode_soal" id="kode_soal" value="<?php echo $kode; ?>">
	            <input type="hidden" name="kode" value="null" />
              <div class="form-group col-md-3" id="inputNomor">
                      <label class="form-control-label" for="nomorform">Nomor Soal</label>
                      <input type="Number" name="nomor" min="0" max="250" id="nomorform" class="form-control" value="<?php echo $last_num; ?>" required>
                      <div class="form-control-feedback" id="info_nomor"></div>
              </div>
              <div class="form-group col-md-12">
                      <label>soal</label>
                      <textarea name="soal" class="form-control summernote" rows="8" cols="80"></textarea>
              </div>
              <div class="tipepilihan-1">
                <div class="form-group col-md-6">
                        <label>Pilihan A</label>
                        <textarea name="pil_a" class="form-control text-pilihan pilihan_ganda" rows="2" cols="80"></textarea>
                </div>
                <div class="form-group col-md-6">
                        <label>Pilihan B</label>
                        <textarea name="pil_b" class="form-control  text-pilihan pilihan_ganda" rows="2" cols="80"></textarea>
                </div>
                <div class="form-group col-md-6">
                        <label>Pilihan C</label>
                        <textarea name="pil_c" class="form-control  text-pilihan pilihan_ganda" rows="2" cols="80"></textarea>
                </div>
                <div class="form-group col-md-6">
                        <label>Pilihan D</label>
                        <textarea name="pil_d" class="form-control  text-pilihan pilihan_ganda" rows="2" cols="80"></textarea>
                </div>
                <div class="form-group col-md-6">
                        <label>Pilihan E</label>
                        <textarea name="pil_e" class="form-control  text-pilihan pilihan_ganda" rows="2" cols="80"></textarea>
                </div>
              </div>
              <div class="form-group col-md-12">
                      <label>Jawaban</label>
                      <div class="demo-radio-button">
                        <div class="row">
                          <div class="form-group col-md-1">
                            <input name="jawaban" type="radio" id="radio_3" class="with-gap" value="A" required/>
                            <label for="radio_3">A</label>
                            <input name="jawaban" type="radio" id="radio_4" class="with-gap" value="B"/>
                            <label for="radio_4">B</label>
                            <input name="jawaban" type="radio" id="radio_5" class="with-gap" value="C"/>
                            <label for="radio_5">C</label>
                          </div>
                          <div class="form-group col-md-4">
                            <input name="jawaban" type="radio" id="radio_6" class="with-gap" value="D"/>
                            <label for="radio_6">D</label>
                            <input name="jawaban" type="radio" id="radio_7" class="with-gap" value="E"/>
                            <label for="radio_7">E</label>
                          </div>
                        </div>
                      </div>
              </div>
              <div class="form-group col-md-12">
                      <label>Penjelasan Jawaban</label>
                      <textarea name="penjelasan" class="form-control summernote" rows="5" cols="10"></textarea>
              </div>
              <div class="form-group col-md-4">
                      <label>Mata Pelajaran</label>
                      <select class="form-control" name="id_mata_pelajaran">
                        <?php foreach ($mapel as $m): ?>
                          <option value="<?php echo $m->id; ?>"><?php echo $m->nama ?></option>
                        <?php endforeach; ?>
                      </select>
              </div>
          	  <div class="form-group col-md-2">
                      <label>Kode Soal</label>
                      <input type="text" name="" class="form-control" placeholder="" value="<?php echo $kode; ?>" readonly>
              </div>



                <div class="form-group col-md-12">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
