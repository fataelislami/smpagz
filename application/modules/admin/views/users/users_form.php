<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Users</h4>
            <form class="form-horizontal m-t-40" method="post" action="<?php echo base_url().$action ?>">
            	  <!-- <div class="form-group">
                        <label>nise</label>
                        <input type="text" name="nise" class="form-control" placeholder="">
                </div> -->
                <div class="row">
                  <div class="form-group col-md-12">
                          <label>nama</label>
                          <input type="text" name="nama" class="form-control" placeholder="" required>
                  </div>
                  <div class="form-group col-md-4">
                          <label>username</label>
                          <input type="text" name="username" class="form-control" placeholder="" required>
                  </div>
              	  <div class="form-group col-md-8">
                          <label>email</label>
                          <input type="email" name="email" class="form-control" placeholder="" required>
                  </div>
                </div>

            	  <!-- <div class="form-group">
                        <label>sekolah</label>
                        <input type="text" name="sekolah" class="form-control" placeholder="">
                </div> -->
                <div class="row">
                  <div class="form-group col-md-4">
                          <label>Kelas</label>
                          <input type="text" name="kelas" class="form-control" placeholder="" required>
                  </div>
                  <div class="form-group col-md-8">
                          <label>Jurusan</label>
                          <select class="form-control" name="jurusan">
                            <option value="">Pilih Jurusan</option>
                            <option value="IPA">IPA</option>
                            <option value="IPS">IPS</option>
                            <option value="IPC">IPC</option>
                            <option value="SMP">SMP</option>
                          </select>
                  </div>
                </div>
                <div class="form-group">
                        <label>Sekolah</label>
                        <input type="text" name="school" class="form-control" placeholder="" value="<?php echo $data_sekolah->nama ?>"readonly>
                </div>
                <div class="row">
                  <div class="form-group col-md-6">
                          <label>password</label>
                          <input type="text" name="password" class="form-control" placeholder="masukan password....." required>
                  </div>
                </div>


            	  <!-- <div class="form-group">
                        <label>total_point</label>
                        <input type="text" name="total_point" class="form-control" placeholder="">
                </div> -->
	              <input type="hidden" name="id" />
                <input type="hidden" name="id_sekolah" value="<?php echo $data_sekolah->id ?>">

                <div class="form-group">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
