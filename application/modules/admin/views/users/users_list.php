<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              <div class="row">
                  <div class="col-md-6">
                      <h4 class="card-title">Data Siswa <?php echo $nama_sekolah ?></h4>
                      <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                  </div>


                    <div class="col-md-6 text-right">
                      <div class="">
                        <?php if (isset($id_sekolah) && $id_sekolah != ""): ?>
                          <form class="text-right" action="<?php echo base_url().$module; ?>/users/create" method="post">
                            <input type="hidden" name="id_sekolah" value="<?php echo $id_sekolah; ?>">
                            <button type="submit" class="btn btn-primary">+ Tambah Data</button>
                          </form>
                        <?php else: ?>
                          <?php echo anchor(site_url($module.'/sekolah'), '+ Tambah Data', 'class="btn btn-primary"'); ?>
                        <?php endif; ?>
                        <button type="button" class="btn btn-info" name="button" data-toggle="modal" data-target=".bs-example-modal-lg">+ Import Data</button>
                      </div>
                    </div>


              </div>


                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <?php foreach ($datafield as $d): ?>
                                  <th><?php echo str_replace("_"," ",$d) ?></th>
                                <?php endforeach; ?>
                                <th>aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($datausers as $d): ?>
                            <tr>
                              <?php foreach ($datafield as $df): ?>
                                <td><?php echo $d->$df ?></td>
                              <?php endforeach; ?>
                                <td>
                                    <a href="<?php echo base_url().$module?>/tryout?user_id=<?php echo $d->id ?>">
                                        <button class="btn btn-info waves-effect waves-light m-r-10">List TO</button>
                                    </a>
                                    <a href="<?php echo base_url().$module?>/users/edit/<?php echo $d->id ?>">
                                        <button class="btn btn-success waves-effect waves-light m-r-10">Edit</button>
                                    </a>
                                    <button  data-toggle="modal" data-target="#responsive-modal" class="btn btn-danger waves-effect waves-light m-r-10 modalDelete" value="<?php echo $d->id ?>">Delete</button>
                                </td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              <p id="modalMsg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                <a id="modalHref" href="#">
                <button type="button" class="btn btn-danger waves-effect waves-light">Ya!</button>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- sample modal content -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Import Excel</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <h4>Pastikan file excel sudah sesui dengan format berikut : </h4>
                <ol>
                  <li>Kolom A diisi dengan nama siswa</li>
                  <li>Kolom B diisi dengan username</li>
                  <li>Kolom C diisi dengan email</li>
                  <li>Kolom D diisi dengan Kelas</li>
                  <li>Kolom E diisi dengan Password</li>
                  <li>Kolom F diisi dengan Jurusan(IPA/IPS/IPC/SMP)</li>
                  <li>Data siswa dimulai dari baris pertama</li>
                  <li>Pastikan username siswa tidak ada yang sama</li>
                  <li>Satu kali impor data tidak lebih dari 50 siswa</li>
                </ol>
                <form class="form-horizontal m-t-40" action="<?php echo base_url("admin/import/excel"); ?>" method="post" enctype="multipart/form-data">
                  <div class="form-group col-md-12">
                          <label>Sekolah</label>
                          <input type="text" name="sekolah" class="form-control" placeholder="" value="<?php echo $nama_sekolah ?>" readonly>
                  </div>
                  <div class="form-group col-md-12">
                          <label>File Excel</label>
                          <input type="file" name="import" class="form-control" placeholder="" required>
                  </div>
                  <input type="hidden" name="id_sekolah" value="<?php echo $id_sekolah ?>">
                  <div class="form-group col-md-12">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Impor Data</button>
                  </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
