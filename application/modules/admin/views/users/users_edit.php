<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Users</h4>
            <form class="form-horozontal m-t-40" method="post" action="<?php echo base_url().$action ?>">
            	  <div class="form-group">
                        <label>id</label>
                        <input type="text" name="id" class="form-control" placeholder="" value="<?php echo $dataedit->id?>" readonly>
                </div>
            	  <!-- <div class="form-group">
                        <label>nise</label>
                        <input type="text" name="nise" class="form-control" value="<?php echo $dataedit->nise?>">
                </div> -->
                <div class="row">
                  <div class="form-group col-md-12">
                          <label>nama</label>
                          <input type="text" name="nama" class="form-control" value="<?php echo $dataedit->nama?>" required>
                  </div>
                  <div class="form-group col-md-4">
                          <label>username</label>
                          <input type="text" name="username" class="form-control" value="<?php echo $dataedit->username?>" required>
                  </div>
              	  <div class="form-group col-md-8">
                          <label>email</label>
                          <input type="email" name="email" class="form-control" value="<?php echo $dataedit->email?>" required>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-4">
                          <label>Kelas</label>
                          <input type="text" name="kelas" class="form-control" value="<?php echo $dataedit->kelas?>" required>
                  </div>
                  <div class="form-group col-md-8">
                          <label>Jurusan</label>
                          <select class="form-control" name="jurusan">
                            <option value="">Pilih Jurusan</option>
                            <option value="IPA" <?php echo $dataedit->jurusan=='IPA' ? 'selected':'';?>>IPA</option>
                            <option value="IPS" <?php echo $dataedit->jurusan=='IPS' ? 'selected':'';?>>IPS</option>
                            <option value="IPC" <?php echo $dataedit->jurusan=='IPC' ? 'selected':'';?>>IPC</option>
                            <option value="SMP" <?php echo $dataedit->jurusan=='SMP' ? 'selected':'';?>>SMP</option>
                          </select>
                  </div>
                </div>
                <div class="form-group">
                        <label>sekolah</label>
                        <select class="form-control" name="id_sekolah">
                          <option value="">Pilih sekolah</option>
                          <?php foreach ($list_sekolah as $s): ?>
                            <option value="<?php echo $s->id ?>" <?php echo $s->id==$dataedit->id_sekolah ? 'selected':''; ?> ><?php echo $s->nama ?></option>
                          <?php endforeach; ?>
                        </select>
                </div>
            	  <div class="form-group">
                        <label>password</label>
                        <input type="text" name="password" class="form-control" placeholder="ubah password" value="">
                </div>
            	  <!-- <div class="form-group">
                        <label>edulab</label>
                        <input type="text" name="edulab" class="form-control" value="<?php echo $dataedit->edulab?>">
                </div> -->

                <input type="hidden" name="previous_username" value="<?php echo $dataedit->username?>">

                <div class="form-group">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
