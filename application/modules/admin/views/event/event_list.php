<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              <div class="row">
                  <div class="col-md-6">
                      <h4 class="card-title">Data Event</h4>
                      <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                  </div>
                  <div class="col-md-6 text-right">
                      <?php echo anchor(site_url($module.'/event/create'), '+ Tambah Data', 'class="btn btn-primary"'); ?>
      	    </div>
              </div>


                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>NAMA</th>
                                <th>TANGGAL</th>
                                <th>PESERTA</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($datatable as $d): ?>
                            <tr>
                                <td><?php echo $d->nama ?></td>
                                <td><?php echo $d->tanggal ?></td>
                                <td class="text-center"><?php echo $d->jumlah_to; ?></td>
                                <td>
                                    <a href="<?php echo base_url().$module?>/event/edit/<?php echo $d->id ?>">
                                        <button class="btn btn-success waves-effect waves-light m-r-10">Edit</button>
                                    </a>
                                    <button  data-toggle="modal" data-target="#responsive-modal" class="btn btn-danger waves-effect waves-light m-r-10 modalDelete" value="<?php echo $d->id ?>">Delete</button>
                                    <button class="btn btn-info waves-effect waves-light m-r-10 modalExport" alt="default" data-toggle="modal" data-target="#modal-export" value="<?php echo $d->id ?>">Export Excel</button>
                                </td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              <p id="modalMsg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                <a id="modalHref" href="#">
                <button type="button" class="btn btn-danger waves-effect waves-light">Ya!</button>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- modal export excel -->
<div id="modal-export" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="mySmallModalLabel">Export Excel</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                  <a id="siswaHref" class="" href="#">
                      <button class="btn btn-info waves-effect waves-light m-r-10" id="sa-export">Export Excel Edulab</button>
                  </a>
                </div>
                <hr>
                <div class="text-center">
                  <a id="usersHref" class="" href="#">
                      <button class="btn btn-info waves-effect waves-light m-r-10" id="sa-export">Export Excel Mitra</button>
                  </a>
                </div>
                <hr>
                <div class="text-center">
                  <a id="eksternalHref" class="" href="#">
                      <button class="btn btn-info waves-effect waves-light m-r-10" id="sa-export">Export Excel Eksternal</button>
                  </a>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal export excel -->
