<script src="<?php echo base_url()?>assets/plugins/summernote/dist/summernote.min.js"></script>

<script src="<?php echo base_url()?>assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>

<script>
    jQuery(document).ready(function() {
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
        // For select 2
        $(".select2").select2();
        $('.selectpicker').selectpicker();
        //Bootstrap-TouchSpin
        $(".vertical-spin").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'ti-plus',
            verticaldownclass: 'ti-minus'
        });
        var vspinTrue = $(".vertical-spin").TouchSpin({
            verticalbuttons: true
        });
        if (vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }
        $("input[name='tch1']").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });
        $("input[name='tch2']").TouchSpin({
            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
        $("input[name='tch3']").TouchSpin();
        $("input[name='tch3_22']").TouchSpin({
            initval: 40
        });
        $("input[name='tch5']").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });
        // For multiselect
        $('#pre-selected-options').multiSelect();
        $('#optgroup').multiSelect({
            selectableOptgroup: true
        });
        $('#public-methods').multiSelect();
        $('#select-all').click(function() {
            $('#public-methods').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function() {
            $('#public-methods').multiSelect('deselect_all');
            return false;
        });
        $('#refresh').on('click', function() {
            $('#public-methods').multiSelect('refresh');
            return false;
        });
        $('#add-option').on('click', function() {
            $('#public-methods').multiSelect('addOption', {
                value: 42,
                text: 'test 42',
                index: 0
            });
            return false;
        });
        $(".ajax").select2({
            ajax: {
                url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    });
    </script>

<!-- Sweet-Alert  -->
<script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
<script>
jQuery(document).ready(function() {

    $('.summernote').summernote({
        height: 350, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: false // set focus to editable area after initializing summernote
    });

    $('.inline-editor').summernote({
        airMode: true
    });

});

window.edit = function() {
        $(".click2edit").summernote()
    },
    window.save = function() {
        $(".click2edit").summernote('destroy');
    }
</script>

<!-- This is data table -->
<script src="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<script>
$(document).ready(function() {
    $('#myTable').DataTable();
    $(document).ready(function() {
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function() {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
});
$('#example23').DataTable({
    dom: 'Bfrtip',
    buttons: [
      'copy'
    ],
    "order": [
        [2, 'asc']
    ]
});
</script>
<script src="<?php echo base_url()?>assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
<script type="text/javascript">
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
  var randomString=makeid();
</script>
<script src="<?php echo base_url()?>assets/plugins/dropzone-master/dist/dropzone.js"></script>
<script type="text/javascript">
$('#aplot').click(function() {
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#example23").on("click", ".modalDelete", function(){//event khusus untuk table datatables setelah pagination suka error
      var id=$(this).val();
      console.log(id);
      $("#modalMsg").html("Apakah Anda Yakin Ingin Menghapus data "+id+" ? ");
      $("#modalHref").attr("href", "<?php echo base_url().$module?>/<?php echo $controller; ?>/delete/"+id);
    });

    $("#example23").on("click", ".modalExport", function(){//event khusus untuk table datatables setelah pagination suka error
      var id=$(this).val();
      console.log(id);
      alert('imam');
      $("#siswaHref").attr("href", "<?php echo base_url().$module?>/<?php echo $controller; ?>/delete/"+id);
      $("#usersHref").attr("href", "<?php echo base_url().$module?>/<?php echo $controller; ?>/delete/"+id);
    });
  });
</script>


<!-- ini script untuk pengecekan nik guru -->
<script type="text/javascript">
  $(document).ready(function(){
    $('input[type=radio][name=tipe]').click(function() {
        if (this.value == 'TO' || this.value == 'UTBK') {
            $(".pilihan-1").show();
            $(".pilihan-2").hide();
            $("#soal_to").attr("required", true);
            $("#input_kelas").attr("required", true);
            $("#input_jurusan").attr("required", true);
            $("#soal_quiz").attr("required", false);
        }else if (this.value == 'QUIZ') {
            $(".pilihan-1").hide();
            $(".pilihan-2").show();
            $("#soal_to").attr("required", false);
            $("#input_kelas").attr("required", false);
            $("#input_jurusan").attr("required", false);
            $("#soal_quiz").attr("required", true);
        }
    });

  });
</script>
<?php if(isset($dataedit->jurusan)){ ?>
  <script type="text/javascript">
  if($('input[type=radio][name=tipe][value=TO]').is(':checked')){
    $(".pilihan-1").show();
    $(".pilihan-2").hide();
    $("#soal_to").val("<?php echo $dataedit->soal_to ?>");
    $("#input_jurusan").val("<?php echo $dataedit->jurusan ?>");
    $("#input_kelas").val("<?php echo $dataedit->kelas ?>");
    if (<?php echo $dataedit->utbk; ?> == '1') {
      $("#basic_checkbox_2").prop('checked',true);
    }


  }
  if($('input[type=radio][name=tipe][value=QUIZ]').is(':checked')){
    $(".pilihan-1").hide();
    $(".pilihan-2").show();
    $("#soal_quiz").val("<?php echo $dataedit->soal_quiz ?>");
  }
  </script>
<?php } ?>

<!-- script pengecakan UTBK
juka utbk maka durasi otomatis terinput -->
<script type="text/javascript">
  $(document).ready(function(){
    $("#basic_checkbox_2").click(function() {
      updateWaktu();
    });

    $("#soal_to").change(function() {
      updateWaktu();
    });

    if($('input[type=checkbox][name=utbk]').is(':checked')){
        $("#durasi").prop('readonly',true);
    }

    function updateWaktu(){
      if ($("#basic_checkbox_2").prop("checked") == true) { //jika utbk checked
        var soal_to = $("#soal_to").val(); //ambil value soal to

        if (soal_to != "") { //jika soal to terisi

          var settings = {
            "async": true,
            "crossDomain": true,
            "url": "<?php echo base_url()?>api/event/getdurationevent?tipe=to&kode="+soal_to,
            "method": "GET",
            "headers": {
              "cache-control": "no-cache",
              "Postman-Token": "1f7f7a89-0b64-478e-a8dd-26120f422f56"
            }
          }

          $.ajax(settings).done(function (response) {
            // console.log(response);
            var duration = response.duration; //ambil nilai durasi

            $("#durasi").val(duration);
            $("#durasi").prop('readonly',true);
          });


        }else {  //jka usoal to tidak terisi
          alert("Pilih soal Tryout terlebih dahulu");
          $("#basic_checkbox_2").prop('checked',false);
        }
      }else { //jika utbk tidak checked
        $("#durasi").prop('readonly',false);
      }
    }

  });
</script>
