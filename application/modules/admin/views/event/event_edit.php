<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Event</h4>
            <form class="form-horizontal m-t-40" method="post" action="<?php echo base_url().$action ?>">
                <input type="hidden" name="id" class="form-control" placeholder="" value="<?php echo $dataedit->id?>" readonly>
            	  <div class="form-group col-md-6">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" value="<?php echo $dataedit->nama?>" required>
                </div>
            	  <div class="form-group col-md-4">
                        <label>Tanggal Event Dibuat</label>
                        <input type="date" name="tanggal" class="form-control" value="<?php echo $dataedit->tanggal?>" required>
                </div>
                <div class="row col-md-12">
                  <div class="form-group col-md-4">
                          <label>Waktu Mulai (tanggal & waktu)</label>
                          <input type="text" name="waktu_mulai" class="form-control" value="<?php echo $dataedit->waktu_mulai?>" required>
                  </div>
              	  <div class="form-group col-md-4">
                          <label>Waktu Selesai (tanggal & waktu)</label>
                          <input type="text" name="waktu_selesai" class="form-control" value="<?php echo $dataedit->waktu_selesai?>" required>
                  </div>
                </div>
                <div class="row col-md-12">
                  <div class="form-group col-md-4">
                          <label>Password Event</label>
                          <input type="text" id="password" step="0.1" name="password" class="form-control" value="<?php echo $dataedit->password?>" required>
                  </div>
                  <div class="form-group col-md-4">
                          <label>Durasi Pengerjaan</label>
                          <input type="number" id="durasi" step="0.1" name="durasi" class="form-control" value="<?php echo $dataedit->durasi?>" required>
                  </div>
                </div>
                <div class="pilihan-1">
                  <div class="row col-md-12">
                    <div class="form-group col-md-6">
                      <label>Soal TO</label>
                      <select class="form-control" id="soal_to" name="soal_to">
                        <option value=""> --- Pilih ---</option>
                        <?php foreach ($tryout as $to): ?>
                          <option value="<?php echo $to->kode; ?>" <?php if($dataedit->kode_soal==$to->kode){echo "selected";} ?>><?php echo $to->kode; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>

                    <div class="form-group col-md-6">
                            <label>Kelas</label>
                            <select class="form-control" id="input_kelas" name="kelas">
                              <option value=""> --- Pilih ---</option>
                              <?php foreach ($kelas as $key): ?>
                                <option value="<?= $key->kode ?>" <?php if($dataedit->kelas==$key->kode){echo "selected";} ?>><?= $key->nama ?></option>
                              <?php endforeach; ?>
                            </select>
                    </div>
                  </div>

                </div>


                <div class="form-group col-md-12">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
