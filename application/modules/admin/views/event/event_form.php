<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Event</h4>
            <form class="form-horizontal m-t-40" method="post" action="<?php echo base_url().$action ?>">
            	  <div class="form-group col-md-8">
                        <label>Nama Event</label>
                        <input type="text" name="nama" class="form-control" placeholder="" required>
                </div>
                <div class="row col-md-12">
                  <div class="form-group col-md-4">
                          <label>Waktu Mulai (tanggal & waktu)</label>
                          <input type="datetime-local" name="waktu_mulai" class="form-control" placeholder="" required>
                  </div>
              	  <div class="form-group col-md-4">
                          <label>Waktu Selesai (tanggal & waktu)</label>
                          <input type="datetime-local" name="waktu_selesai" class="form-control" placeholder="" required>
                  </div>
                </div>
                <div class="row col-md-12">
                  <div class="form-group col-md-4">
                          <label>Password Event</label>
                          <input type="text" id="password" name="password" class="form-control" placeholder="" required>
                  </div>
                  <div class="form-group col-md-4">
                          <label>Durasi Pengerjaan</label>
                          <input type="number" id="durasi" name="durasi" class="form-control" placeholder="" required>
                  </div>
                </div>
                <div class="pilihan-1">
                  <div class="row col-md-12">
                    <div class="form-group col-md-4">
                      <label>Soal TO</label>
                      <select class="form-control" id="soal_to" name="soal_to">
                        <option value=""> --- Pilih ---</option>
                        <?php foreach ($tryout as $to): ?>
                          <option value="<?php echo $to->kode; ?>"><?php echo $to->kode; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                            <label>Kelas</label>
                            <select class="form-control" id="input_kelas" name="kelas">
                              <option value=""> --- Pilih ---</option>
                              <?php foreach ($kelas as $key): ?>
                                <option value="<?= $key->kode ?>"><?= $key->nama ?></option>
                              <?php endforeach; ?>
                            </select>
                    </div>
                  </div>
                </div>


            	  <input type="hidden" name="id" />

                <div class="form-group col-md-12">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
