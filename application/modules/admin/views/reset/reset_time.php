<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
<div class="row">
  <div class="col-6">
    <div class="card">
        <div class="card-body">
            <h3 class="card-title col-md-12">Refresh Server Time</h3>
            <p></p>
            <form class="form-horizontal m-t-40" method="post" action="<?php echo base_url().$action ?>">
                <div class="form-group col-md-8" id="inputKode">
                        <label class="form-control-label" for="kodeform">Lama Waktu (menit)</label>
                        <input type="text" name="time" class="form-control" id="kodeform" placeholder="" required>
                        <div class="form-control-feedback" id="info_kode"></div>
                </div>

                <div class="form-group col-md-12">
                  <button type="button" class="btn btn-success waves-effect waves-light m-r-10" data-toggle="modal" data-target="#myModal"> Submit</button>
                </div>

                <!-- sample modal content -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            </div>
                            <div class="modal-body">
                                <h4>Reset waktu server</h4>
                                <p>Apakah anda yakin akan mereset waktu tryout yang sedang berlangsung hari ini?</p>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Ya</button>
                                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Tidak</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </form>
        </div>
    </div>
  </div>
</div>

<div class="col-6">
  <div class="card">
      <div class="card-body">
          <h3 class="card-title col-md-12">INFO :</h3>
          <p>
            Refresh Server Time berfungsi untuk mengembalikan waktu tryout siswa
            ketika terjadi eror pada sistem pada hari ini.

          </p>
      </div>
  </div>
</div>
</div>
</div>
