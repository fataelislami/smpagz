<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Guru</h4>
            <form class="form-material m-t-40" method="post" action="<?php echo base_url().$action ?>">
              <div class="form-group">
                      <label>nik</label>
                      <input type="text" name="nik" class="form-control" placeholder="" value="">
              </div>
          	  <div class="form-group">
                      <label>nama</label>
                      <input type="text" name="nama" class="form-control" placeholder="">
              </div>
              <div class="form-group">
                <label>jenis kelamin</label>
                <select class="form-control" name="gender">
                    <option value="P">Perempuan</option>
                    <option value="L">Laki-laki</option>
                </select>
              </div>
          	  <div class="form-group">
                      <label>alamat</label>
                      <input type="text" name="alamat" class="form-control" placeholder="">
              </div>
          	  <div class="form-group">
                      <label>tlp</label>
                      <input type="text" name="tlp" class="form-control" placeholder="">
              </div>
          	  <div class="form-group">
                      <label>email</label>
                      <input type="text" name="email" class="form-control" placeholder="">
              </div>
              <div class="form-group">
                <label>mata pelajaran</label>
                <select class="form-control" name="mapel">
                  <option value=""> --- pilih ---</option>
                  <?php if($mata_pelajaran->num_rows()>0){ ?>
                  <?php foreach ($mata_pelajaran->result() as $key): ?>
                    <option value="<?php echo $key->id; ?>"><?php echo $key->nama;?></option>
                  <?php endforeach; ?>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label>Kelas</label>
                <select class="form-control" name="id_kelas">
                  <option value=""> --- pilih ---</option>
                  <?php if($kelas->num_rows()>0){ ?>
                  <?php foreach ($kelas->result() as $key): ?>
                    <option value="<?php echo $key->kode; ?>" ?>><?php echo $key->nama;?></option>
                  <?php endforeach; ?>
                  <?php } ?>
                </select>
              </div>
          	  <div class="form-group">
                      <label>password</label>
                      <input type="text" name="password" class="form-control" placeholder="">
              </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
