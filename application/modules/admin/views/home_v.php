

<div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-info"><i class="ti-user"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0 font-light"><?php echo $total_siswa; ?></h3>
                                        <h5 class="text-muted m-b-0">Data Siswa</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-warning"><i class="mdi mdi-account-star-variant"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0 font-lgiht"><?php echo $total_guru; ?></h3>
                                        <h5 class="text-muted m-b-0">Data Guru</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-danger"><i class="mdi mdi-glassdoor"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        <h3 class="m-b-0 font-lgiht"><?php echo $total_kelas ?></h3>
                                        <h5 class="text-muted m-b-0">Data Kelas</h5></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <?php if (isset($event_now)){ ?>
                  <div class="col-lg-12">
                      <div class="card">
                          <div class="card-body">
                              <a href="<?php echo base_url().'admin/Event' ?>" class=" waves-effect waves-dark btn btn-warning btn-rounded pull-right">Kelola Event</a>
                              <h4 class="card-title">Event Ujian yang Sedang Berlangsung</h4>
                              <div class="table-responsive m-t-20">
                                  <table class="table stylish-table">
                                      <thead>
                                          <tr>
                                              <th>Judul Event</th>
                                              <th class="text-center">Waktu Mulai</th>
                                              <th class="text-center">Waktu Selesai</th>
                                              <th class="text-center">Peserta yang mengerjakan</th>
                                          </tr>
                                      </thead>
                                      <tbody>
                                        <?php foreach ($event_now as $d): ?>
                                          <tr>
                                            <td><?php echo $d->nama; ?></td>
                                            <td class="text-center"><?php echo $d->waktu_mulai; ?></td>
                                            <td class="text-center"><?php echo $d->waktu_selesai; ?></td>
                                            <td class="text-center"><?php echo $d->jumlah_to; ?></td>
                                          </tr>
                                        <?php endforeach; ?>
                                      </tbody>
                                  </table>
                              </div>
                          </div>
                      </div>
                  </div>
                <?php }?>
                <div class="row">
                  <div class="col-md-12">
                      <div class="card">
                          <div class="card-body">
                              <h4 class="card-title">Total Peserta Ujian</h4>
                              <h6 class="card-subtitle">Berdasarkan event sebelumnya</h6>
                              <div class="ct-animation-chart" style="height: 400px;"></div>
                          </div>
                      </div>
                  </div>

                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <a href="<?php echo base_url().'admin/Event' ?>" class=" waves-effect waves-dark btn btn-warning btn-rounded pull-right">Kelola Event</a>
                                <h4 class="card-title">Riwayat Event Ujian Terbaru</h4>
                                <div class="table-responsive m-t-20">
                                    <table class="table stylish-table">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Judul Event</th>
                                                <th>Waktu Mulai</th>
                                                <th>Status</th>
                                                <th>Durasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php if (isset($event_list)): ?>
                                            <?php foreach ($event_list as $obj): ?>
                                              <?php $a=array("warning","primary","info");$random_keys=array_rand($a,1); ?>
                                              <?php if($obj->status!='berlangsung'){ ?>
                                                <tr>
                                                    <td><span class="round round-<?php echo $a[$random_keys]; ?>"><?php echo substr($obj->nama,0,1); ?></span></td>
                                                    <td>
                                                        <h6><?php echo $obj->nama ?></h6><small class="text-muted"></small></td>
                                                    <td><?php echo $obj->waktu_mulai ?></td>
                                                    <?php if($obj->status=='selesai'){ ?>
                                                      <td><span class="label label-info">selesai</span></td>
                                                    <?php }else if($obj->status=='belum dimulai'){ ?>
                                                      <td><span class="label label-warning" style="background-color: darkgrey;">belum dimulai</span></td>
                                                    <?php } ?>
                                                    <td><?php echo $obj->durasi." Menit" ?></td>
                                                </tr>
                                              <?php } ?>
                                            <?php endforeach; ?>
                                          <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
