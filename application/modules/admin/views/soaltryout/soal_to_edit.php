<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Ubah Soal Tryout</h4>
            <form class="form-horizontal m-t-40" method="post" action="<?php echo base_url().$action ?>">
            	  <div class="form-group col-md-3">
                        <label>Kode Soal</label>
                        <input type="text" name="kode" class="form-control" placeholder="" value="<?php echo $dataedit->kode?>">
                </div>
                <div class="form-group col-md-3">
                        <label>Kelas</label>
                        <select class="form-control" name="kelas">
                          <option value="7" <?php if($dataedit->kelas=="7"){echo "selected";} ?>>KELAS VII</option>
                          <option value="8" <?php if($dataedit->kelas=="8"){echo "selected";} ?>>KELAS VIII</option>
                          <option value="9" <?php if($dataedit->kelas=="9"){echo "selected";} ?>>KELAS IX</option>
                        </select>
                </div>
	              <input type="hidden" name="kode_lama" value="<?php echo $dataedit->kode?>">
                <div class="form-group col-md-12">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
