<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Soal Tryout</h4>
            <form class="form-horizontal m-t-40" method="post" action="<?php echo base_url().$action ?>">
                <div class="form-group col-md-12">
                  <div class="row">
                    <div class="form-group col-md-3">
                            <label>Kode Soal</label>
                            <input type="text" name="kode" class="form-control" placeholder="">
                    </div>
                    <div class="form-group col-md-3">
                            <label>Kelas</label>
                            <select class="form-control" name="kelas">
                              <option value=""> --- Pilih ---</option>
                                <option value="7">KELAS VII</option>
                                <option value="8">KELAS VIII</option>
                                <option value="9">KELAS IX</option>
                            </select>
                    </div>
                    <div class="form-group col-md-3">
                            <label>Jumlah Soal</label>
                            <input type="number" min="0" max="250" name="jmlh_soal" class="form-control" placeholder="">
                    </div>
                  </div>
                </div>

                <div class="form-group col-md-12">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
