<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
} ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              <div class="row">
                  <div class="col-md-6">
                      <h4 class="card-title">Data Soal</h4>
                      <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                  </div>
                  <div class="col-md-6 text-right">
                      <?php echo anchor(site_url($module.'/soaltryout/create'), '+ Tambah Data', 'class="btn btn-primary"'); ?>
      	    </div>
              </div>


                <div class="table-responsive m-t-40">
                    <table id="example24" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Kode Soal</th>
                                <th>Tanggal Dibuat</th>
                                <th>Jumlah Soal</th>
                                <th class="text-center">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($datatable as $d): ?>
                            <tr>
                                <td><?php echo $d->kode; ?></td>
                                <td><?php echo date_format(date_create($d->tanggal),"d M Y"); ?></td>
                                <td><?php echo $d->jmlh_soal; ?></td>
                                <td class="text-center">
                                  <a href="<?php echo base_url().$module?>/listsoaltryout?kode=<?php echo $d->kode ?>">
                                      <button class="btn btn-info waves-effect waves-light m-r-10">Lihat Soal</button>
                                  </a>
                                  <a href="<?php echo base_url().$module?>/soaltryout/edit/<?php echo $d->kode ?>">
                                      <button class="btn btn-success waves-effect waves-light m-r-10">Edit</button>
                                  </a>
                                  <a href="<?php echo base_url().$module?>/soaltryout/duplicate/<?php echo $d->kode ?>">
                                      <button class="btn btn-primary waves-effect waves-light m-r-10">Copy</button>
                                  </a>
                                  <button  data-toggle="modal" data-target="#responsive-modal" class="btn btn-danger waves-effect waves-light m-r-10 modalDelete" value="<?php echo $d->kode ?>">Delete</button>
                                </td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
              <p id="modalMsg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                <a id="modalHref" href="#">
                <button type="button" class="btn btn-danger waves-effect waves-light">Ya!</button>
                </a>
            </div>
        </div>
    </div>
</div>
