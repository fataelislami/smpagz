<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Duplikat Soal Tryout</h4>
            <form class="form-horizontal m-t-40" method="post" action="<?php echo base_url().$action ?>">
              <div class="row">
                <div class="form-group col-md-6">
                        <label>Kode Soal</label>
                        <input type="text" name="kode" class="form-control" placeholder="" value="<?php echo $dataedit->kode?>" readonly>
                </div>
                <div class="form-group col-md-6">
                        <label>Kode Soal Baru</label>
                        <input type="text" name="kode_baru" class="form-control" placeholder="Masukan kode Soal Baru">
                </div>
              </div>
              <div class="row">
                <input type="hidden" name="p_studi" value="<?= $dataedit->p_studi ?>">
                <input type="hidden" name="kelas" value="<?= $dataedit->kelas ?>">
                <div class="form-group col-md-12">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Proses</button>
                </div>
              </div>

            </form>
        </div>
    </div>
  </div>
</div>
