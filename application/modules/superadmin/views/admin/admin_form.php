<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Admin</h4>
            <form class="form-horizontal m-t-40" method="post" action="<?php echo base_url().$action ?>">
            	  <div class="form-group col-md-6" id="inputUsername">
                        <label class="form-control-label" for="usernameform">Username</label>
                        <input type="text" name="username" class="form-control" id="usernameform" placeholder="" required>
                        <div class="form-control-feedback" id="info_username"></div>
                </div>
            	  <div class="form-group col-md-6">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" placeholder="" required>
                </div>
            	  <div class="form-group col-md-12">
                        <label>Gender</label>
                        <div class="demo-radio-button">
                          <input name="gender" type="radio" id="radio_1" class="with-gap" value="L" required/>
                          <label for="radio_1">Laki-Laki</label>
                          <input name="gender" type="radio" id="radio_2" class="with-gap" value="P" />
                          <label for="radio_2">Perempuan</label>
                        </div>
                </div>
            	  <div class="form-group col-md-8">
                        <label>Alamat</label>
                        <input type="text" name="alamat" class="form-control" placeholder="" required>
                </div>
            	  <div class="form-group col-md-3">
                        <label>Telephone</label>
                        <input type="text" name="tlp" class="form-control" placeholder="" required>
                </div>
            	  <div class="form-group col-md-8" id="inputEmail">
                        <label class="form-control-label" for="emailform">Email</label>
                        <input type="email" name="email" class="form-control" id="emailform" placeholder="" required>
                        <div class="form-control-feedback" id="info_email"></div>
                </div>
                <div class="form-group col-md-6">
                        <label>Password</label>
                        <input type="text" name="password" class="form-control" placeholder="" required>
                </div>
	              <input type="hidden" name="id" />

                <div class="form-group col-md-12">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
