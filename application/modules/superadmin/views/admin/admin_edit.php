<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Admin</h4>
            <form class="form-horizontal m-t-40" method="post" action="<?php echo base_url().$action ?>">

                <input type="hidden" name="id" class="form-control" placeholder="" value="<?php echo $dataedit->id?>" readonly>
                <input type="hidden" name="username_lama" id="username_lama" value="<?php echo $dataedit->username?>">
                <input type="hidden" name="email_lama" id="email_lama" value="<?php echo $dataedit->email?>">

            	  <div class="form-group col-md-6" id="editUsername">
                        <label class="form-control-label" for="usernameformedit">Username</label>
                        <input type="text" name="username" class="form-control" id="usernameformedit" value="<?php echo $dataedit->username?>" required>
                        <div class="form-control-feedback" id="info_username"></div>
                </div>
            	  <div class="form-group col-md-6">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" value="<?php echo $dataedit->nama?>" required>
                </div>
            	  <div class="form-group col-md-12">
                        <label>Gender</label>
                        <div class="demo-radio-button">
                          <input name="gender" type="radio" id="radio_1" class="with-gap" value="L" <?php if ($dataedit->gender=='L'){echo "checked";} ?> required/>
                          <label for="radio_1">Laki-Laki</label>
                          <input name="gender" type="radio" id="radio_2" class="with-gap" value="P" <?php if ($dataedit->gender=='P'){echo "checked";} ?>/>
                          <label for="radio_2">Perempuan</label>
                        </div>
                </div>
            	  <div class="form-group col-md-8">
                        <label>Alamat</label>
                        <input type="text" name="alamat" class="form-control" value="<?php echo $dataedit->alamat?>" required>
                </div>
            	  <div class="form-group col-md-3">
                        <label>Telephone</label>
                        <input type="text" name="tlp" class="form-control" value="<?php echo $dataedit->tlp?>" required>
                </div>
            	  <div class="form-group col-md-8" id="editEmail">
                        <label class="form-control-label" for="emailformedit">Email</label>
                        <input type="email" name="email" class="form-control" id="emailformedit" value="<?php echo $dataedit->email?>" required>
                        <div class="form-control-feedback" id="info_email"></div>
                </div>
                <div class="form-group col-md-4">
                        <label>Password</label>
                        <input type="text" name="password" class="form-control" placeholder="  ubah password">
                </div>

                <div class="form-group col-md-12">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
