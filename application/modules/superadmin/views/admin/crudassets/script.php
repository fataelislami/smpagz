<script src="<?php echo base_url()?>assets/plugins/summernote/dist/summernote.min.js"></script>

<!-- Sweet-Alert  -->
<script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
<script>
jQuery(document).ready(function() {

    $('.summernote').summernote({
        height: 350, // set editor height
        minHeight: null, // set minimum height of editor
        maxHeight: null, // set maximum height of editor
        focus: false // set focus to editable area after initializing summernote
    });

    $('.inline-editor').summernote({
        airMode: true
    });

});

window.edit = function() {
        $(".click2edit").summernote()
    },
    window.save = function() {
        $(".click2edit").summernote('destroy');
    }
</script>

<!-- This is data table -->
<script src="<?php echo base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- end - This is for export functionality only -->
<script>
$(document).ready(function() {
    $('#myTable').DataTable();
    $(document).ready(function() {
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function() {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
});
$('#example23').DataTable({
    dom: 'Bfrtip',
    buttons: [
      'copy'
    ],
    "order": [
        [2, 'asc']
    ]
});
</script>
<script src="<?php echo base_url()?>assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
<script type="text/javascript">
function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
  var randomString=makeid();
</script>
<script src="<?php echo base_url()?>assets/plugins/dropzone-master/dist/dropzone.js"></script>
<script type="text/javascript">
$('#aplot').click(function() {
    var myDropzone = Dropzone.forElement(".dropzone");
    myDropzone.processQueue();
});
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#example23").on("click", ".modalDelete", function(){//event khusus untuk table datatables setelah pagination suka error
      var id=$(this).val();
      console.log(id);
      $("#modalMsg").html("Apakah Anda Yakin Ingin Menghapus data "+id+" ? ");
      $("#modalHref").attr("href", "<?php echo base_url().$module?>/<?php echo $controller; ?>/delete/"+id);
    });
  });
</script>


<!-- ini script untuk pengecekan form admin -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#info_username').hide();
    $('#info_email').hide();

    //pengecekan form input username admin
    $("#usernameform").change(function(){
      var val = $("#usernameform").val();

      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "<?php echo base_url()?>api/check/admin?username="+val,
        "method": "GET",
        "headers": {
          "cache-control": "no-cache",
          "Postman-Token": "1f7f7a89-0b64-478e-a8dd-26120f422f56"
        }
      }

    $.ajax(settings).done(function (response) {
      if (response.results == false && val != "") {
        $("#inputUsername").attr('class', 'form-group col-md-3 has-success');
        $('#info_username').text("Tersedia");
        $('#info_username').show();
        $(':input[type="submit"]').prop('disabled', false);
      }else {
        $("#inputUsername").attr('class', 'form-group col-md-3 has-danger');
        $('#info_username').text("tidak tersedia");
        $('#info_username').show();
        $(':input[type="submit"]').prop('disabled', true);
      }
    });
    });

    //pengecekan form input email admin
    $("#emailform").change(function(){
      var val = $("#emailform").val();

      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "<?php echo base_url()?>api/check/admin?email="+val,
        "method": "GET",
        "headers": {
          "cache-control": "no-cache",
          "Postman-Token": "1f7f7a89-0b64-478e-a8dd-26120f422f56"
        }
      }

    $.ajax(settings).done(function (response) {
      if (response.results == false && val != "") {
        $("#inputEmail").attr('class', 'form-group col-md-3 has-success');
        $('#info_email').text("Tersedia");
        $('#info_email').show();
        $(':input[type="submit"]').prop('disabled', false);
      }else {
        $("#inputEmail").attr('class', 'form-group col-md-3 has-danger');
        $('#info_email').text("Tidak tersedia");
        $('#info_email').show();
        $(':input[type="submit"]').prop('disabled', true);
      }
    });
    });

    //pengecekan form edit username admin
    $("#usernameformedit").change(function(){
      var username = $("#usernameformedit").val();
      var username_lama = $("#username_lama").val();

      if (username_lama != username) {
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "<?php echo base_url()?>api/check/admin?username="+username,
          "method": "GET",
          "headers": {
            "cache-control": "no-cache",
            "Postman-Token": "1f7f7a89-0b64-478e-a8dd-26120f422f56"
          }
        }

        $.ajax(settings).done(function (response) {
          if (response.results == false && username != "") {
            $("#editUsername").attr('class', 'form-group col-md-3 has-success');
            $('#info_username').text("Tersedia");
            $('#info_username').show();
            $(':input[type="submit"]').prop('disabled', false);
          }else {
            $("#editUsername").attr('class', 'form-group col-md-3 has-danger');
            $('#info_username').text("tidak tersedia");
            $('#info_username').show();
            $(':input[type="submit"]').prop('disabled', true);
          }
        });
      }else{
        $("#editUsername").attr('class', 'form-group col-md-3 has-success');
        $('#info_username').text("Tersedia");
        $('#info_username ').show();
        $(':input[type="submit"]').prop('disabled', false);
      }
    });

    //pengecekan form edit email admin
    $("#emailformedit").change(function(){
      var email = $("#emailformedit").val();
      var email_lama = $("#email_lama").val();

      if (email_lama != email) {
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "<?php echo base_url()?>api/check/admin?email="+email,
          "method": "GET",
          "headers": {
            "cache-control": "no-cache",
            "Postman-Token": "1f7f7a89-0b64-478e-a8dd-26120f422f56"
          }
        }

        $.ajax(settings).done(function (response) {
          if (response.results == false && email != "") {
            $("#editEmail").attr('class', 'form-group col-md-8 has-success');
            $('#info_email').text("Tersedia");
            $('#info_email').show();
            $(':input[type="submit"]').prop('disabled', false);
          }else {
            $("#editEmail").attr('class', 'form-group col-md-8 has-danger');
            $('#info_email').text("tidak tersedia");
            $('#info_email').show();
            $(':input[type="submit"]').prop('disabled', true);
          }
        });
      }else {
        $("#editEmail").attr('class', 'form-group col-md-8 has-success');
        $('#info_email').text("Tersedia");
        $('#info_email').show();
        $(':input[type="submit"]').prop('disabled', false);
      }
    });
  });
</script>
