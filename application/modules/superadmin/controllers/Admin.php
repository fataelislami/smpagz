<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Madmin');
        $this->load->library('form_validation');
    }

    public function index()
    {

      // $dataadmin=$this->Madmin->get_all();//panggil ke modell
      // $datafield=$this->Madmin->get_field();//panggil ke modell
      $datatable = $this->Madmin->getDatatable();

      $data = array(
        'contain_view' => 'superadmin/admin/admin_list',
        'sidebar'=>'superadmin/sidebar',
        'css'=>'superadmin/crudassets/css',
        'script'=>'superadmin/admin/crudassets/script',
        // 'dataadmin'=>$dataadmin,
        // 'datafield'=>$datafield,
        'datatable' => $datatable,
        'module'=>'superadmin',
        'titlePage'=>'admin',
        'controller'=>'admin'
       );
      $this->template->load($data);
    }


    public function create(){
      $data = array(
        'contain_view' => 'superadmin/admin/admin_form',
        'sidebar'=>'superadmin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'superadmin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'superadmin/admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'superadmin/admin/create_action',
        'titlePage'=>'Tambah Admin'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Madmin->get_by_id($id);
      $data = array(
        'contain_view' => 'superadmin/admin/admin_edit',
        'sidebar'=>'superadmin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'superadmin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'superadmin/admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'superadmin/admin/update_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'Edit Admin'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $username = $this->input->post('username');
            $email    = $this->input->post('email');

            $cek_username = $this->Madmin->CheckData('username',$username,'admin');
            $cek_email    = $this->Madmin->CheckData('email',$email,'admin');

            if (!$cek_username && !$cek_email) {
              $data = array(
            		'username' => $this->input->post('username',TRUE),
            		'password' => sha1($this->input->post('password',TRUE)),
            		'nama' => $this->input->post('nama',TRUE),
            		'gender' => $this->input->post('gender',TRUE),
            		'alamat' => $this->input->post('alamat',TRUE),
            		'tlp' => $this->input->post('tlp',TRUE),
            		'email' => $this->input->post('email',TRUE),
            	);

              $this->Madmin->insert($data);
              $this->session->set_flashdata('message', 'Create Record Success');
              redirect(site_url('superadmin/admin'));
            }else {
              $this->session->set_flashdata('message', 'username/password sudah ada');
              redirect(site_url('superadmin/admin'));
            }

        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id', TRUE));
        } else {
            $username = $this->input->post('username');
            $email    = $this->input->post('email');
            $username_lama = $this->input->post('username_lama');
            $email_lama = $this->input->post('email_lama');

            $cek_username = $this->Madmin->CheckData('username',$username,'admin');
            $cek_email    = $this->Madmin->CheckData('email',$email,'admin');

            if ((!$cek_username || $username == $username_lama) && (!$cek_email || $email == $email_lama)) {
              $data = array(
            		'username' => $this->input->post('username',TRUE),
            		'nama' => $this->input->post('nama',TRUE),
            		'gender' => $this->input->post('gender',TRUE),
            		'alamat' => $this->input->post('alamat',TRUE),
            		'tlp' => $this->input->post('tlp',TRUE),
            		'email' => $this->input->post('email',TRUE),
            	);
              if ($_POST['password'] != "") {
                $data['password'] = sha1($this->input->post('password',TRUE));
              }

              $this->Madmin->update($this->input->post('id', TRUE), $data);
              $this->session->set_flashdata('message', 'Update Record Success');
              redirect(site_url('superadmin/admin'));
            }else {
              $this->session->set_flashdata('message', 'Username/password sudah ada');
              redirect(site_url('superadmin/admin'));
            }

        }
    }

    public function delete($id)
    {
        $row = $this->Madmin->get_by_id($id);

        if ($row) {
            $this->Madmin->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('superadmin/admin'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('superadmin/admin'));
        }
    }

    public function _rules()
    {
      	$this->form_validation->set_rules('username', 'username', 'trim|required');
      	//$this->form_validation->set_rules('password', 'password', 'trim|required');
      	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
      	$this->form_validation->set_rules('gender', 'gender', 'trim|required');
      	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
      	$this->form_validation->set_rules('tlp', 'tlp', 'trim|required');
      	$this->form_validation->set_rules('email', 'email', 'trim|required');

      	$this->form_validation->set_rules('id', 'id', 'trim');
      	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
