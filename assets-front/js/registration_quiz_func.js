	/*  Wizard */
	jQuery(function ($) {
		"use strict";
		$('form#wrapped').attr('action', 'proses');
		$("#wizard_container").wizard({
			stepsWrapper: "#wrapped",
			submit: ".submit",
			beforeSelect: function (event, state) {
				if ($('input#website').val().length != 0) {
					return false;
				}
				if (!state.isMovingForward)
					return true;
				var inputs = $(this).wizard('state').step.find(':input');
				return !inputs.length || !!inputs.valid();
			}
		}).validate({
			errorPlacement: function (error, element) {
				if (element.is(':radio') || element.is(':checkbox')) {
					error.insertBefore(element.next());
				} else {
					error.insertAfter(element);
				}
			}
		});
		//  progress bar
		$("#progressbar").progressbar();
		$("#wizard_container").wizard({
			afterSelect: function (event, state) {
				$("#progressbar").progressbar("value", state.percentComplete);
				$("#location").text("(" + state.stepsComplete + "/" + state.stepsPossible + ")");
			}
		});
		// Validate select
		$('#wrapped').validate({
			ignore: [],
			rules: {
				select: {
					required: true
				}
			},
			errorPlacement: function (error, element) {
				if (element.is('select:hidden')) {
					error.insertAfter(element.next('.nice-select'));
				} else {
					error.insertAfter(element);
				}
			}
		});
	});

	// Summary
	function getVals(formControl, controlType) {
		switch (controlType) {

			case 'NIK':
				// Get the value for a input text
				var value = $(formControl).val();
				identitas(value);
				break;


		}
	}
	// alert(base_url);
	function identitas(nik){
		var settings = {
  "async": true,
  "crossDomain": true,
  "url": base_url+"api/form/identitas?nik="+nik,
  "method": "GET",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "29f439f6-2d33-480f-9346-1732c801274f"
  }
}

$.ajax(settings).done(function (response) {
	if(response.total_result==0){
		$("#nama").val("NIK Belum Terdaftar");
		$("select.event_option").html('<option>---</option>');
		$('select.event_option').niceSelect('update');
		$(".forward").hide();
	}else{
		getevent(nik);
		$("#nama").val(response.results[0].nama);
		$(".forward").show();
	}
});
	}
	function getevent(nik){
		var settings = {
		  "async": true,
		  "crossDomain": true,
		  "url": base_url+"api/event/quiz?nik="+nik,
		  "method": "GET",
		}

		$.ajax(settings).done(function (response) {
			var total_result=response.total_result;
			if(total_result==0){
				$("#event_false").show();
				$("#select_event").hide();
				$(".forward").hide();
				$("#event").val("Belum Ada Event Terdaftar");
			}else{
				$("#event_false").hide();
				$("#select_event").show();
				$(".forward").show();
				// var obj=JSON.parse(response.results);
				console.log(response.results[0].id);
				$("select.event_option").html('<option value="'+response.results[0].id+'">'+response.results[0].nama+'</option>');
				for (var i = 1; i < total_result; i++) {
					$("select.event_option").append('<option value="'+response.results[i].id+'">'+response.results[i].nama+'</option>');
					// alert("HAI");
				}
				$('select.event_option').niceSelect('update');
				// alert("update");
			}
		});
	}
