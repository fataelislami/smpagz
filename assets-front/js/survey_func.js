	/*  Wizard */
	jQuery(function ($) {
		"use strict";
		$('form#wrapped').attr('action', 'finish');
		$("#wizard_container").wizard({
			stepsWrapper: "#wrapped",
			submit: ".submit",
			beforeSelect: function (event, state) {
				if ($('input#website').val().length != 0) {
					return false;
				}
				if (!state.isMovingForward)
					return true;
				var inputs = $(this).wizard('state').step.find(':input');
				return !inputs.length || !!inputs.valid();
			}
		}).validate({
			errorPlacement: function (error, element) {
				if (element.is(':radio') || element.is(':checkbox')) {
					error.insertBefore(element.next());
				} else {
					error.insertAfter(element);
				}
			}
		});
		// $("#q1").click(function(){
		// 	$("#wizard_container").wizard('select',0); //SELECT KE STEP BERAPA
		// });
		// $("#q2").click(function(){
		// 	$("#wizard_container").wizard('select',1); //SELECT KE STEP BERAPA
		// });
		// $("#wizard_container").wizard('select',1); //SELECT KE STEP BERAPA

		//  progress bar
		$("#progressbar").progressbar();
		$("#wizard_container").wizard({
			afterSelect: function (event, state) {
				$("#progressbar").progressbar("value", state.percentComplete);
				if(state.isLastStep){
					// console.log("Ini Step Terakhir / SUBMIT "+state.isLastStep+id_to);
					checkjawaban(id_to);
				}
				$("#location").text("(" + state.stepsComplete + "/" + state.stepsPossible + ")");
				var thisstep=(state.stepIndex+1);
				if($(".jawaban_"+thisstep).is(":checked")){
					$(".BatalJawaban").show();
				// 	var formControl2=".jawaban_"+thisstep;
				//
			}else{
				$(".BatalJawaban").hide();

			}
				// var jawaban=value;
				// console.log("SELECTED"+(state.stepIndex+1));
				// console.log("jawaban "+value);
				// $("#btn_sel-"+(state.stepIndex+1)).addClass('btn_active');
				$(".btn_active").removeClass('btn_active');
				$("#btn_sel-"+(state.stepIndex+1)).addClass('btn_active');
			}
		});

		// Validate select
		$('#wrapped').validate({
			ignore: [],
			rules: {
				select: {
					required: true
				}
			},
			errorPlacement: function (error, element) {
				if (element.is('select:hidden')) {
					error.insertAfter(element.next('.nice-select'));
				} else {
					error.insertAfter(element);
				}
			}
		});
	});
	//Init Data OnLoad

	 //Init Data OnLoad
	 //Remove Piliha Soal
	 $(".PilihJawaban").click(function(){
		 var stepnow=($("#wizard_container").wizard('stepIndex')+1);
		 if($(".jawaban_"+stepnow).is(":checked")){
			 var formControl2=".jawaban_"+stepnow;
			 var jawaban=value;
			 var extract_value=jawaban.split("#");
			 var jawaban=extract_value[0];
			 var kode_list_soal=extract_value[1];
			 var id_try_out=extract_value[2];
			 answer(kode_list_soal,id_try_out,jawaban,stepnow,formControl2);
			 // alert(value);
		 }
	 });
	 $(".BatalJawaban").click(function(){
		 var stepnow=($("#wizard_container").wizard('stepIndex')+1);
		 if($(".jawaban_"+stepnow).is(":checked")){
			 var formControl2=".jawaban_"+stepnow;
			 var vall=$("input[name='jawaban_"+stepnow+"']:checked").val();
			 var extract_value=vall.split("#");
			 var jawaban=extract_value[0];
			 var kode_list_soal=extract_value[1];
			 var id_try_out=extract_value[2];
			 delete_answer(kode_list_soal,id_try_out,jawaban,stepnow,formControl2);
			 // answer(kode_list_soal,id_try_out,jawaban,stepnow,formControl2);
			 alert("Kamu Baru Saja Menghapus Jawaban "+jawaban);
		 }
	 });
// Summary
var value;
var formControl2;
function getVals(formControl, controlType) {
		value = $(formControl).val();
		// console.log(value);
		// console.log(kode_list_soal);
		// console.log(id_try_out);
		// answer(kode_list_soal,id_try_out,jawaban,stepnow,formControl);
}
function toast() {
  var x = document.getElementById("snackbar");
  x.className = "show";
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 900);
}
function answer(kode_list_soal,id_try_out,jawaban,step,formControl){
	var form = new FormData();
form.append("kode_list_soal", kode_list_soal);
form.append("id_try_out", id_try_out);
form.append("jawaban", jawaban);

var settings = {
  "async": true,
  "crossDomain": true,
  "url": base_url+"api/process/answer",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "99683224-f1e7-455e-b8af-d790b9b274f6"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

$.ajax(settings).done(function (response) {
	var obj=JSON.parse(response);
	var status=obj.status;
	console.log(status);
	if(status=='updated' || status=='inserted'){
		toast();
		$("#btn_sel-"+step).addClass('btn_isi');
		$(".BatalJawaban").show();
	}
	// if(status=='not updated'){
	//
	// }
});
}

function delete_answer(kode_list_soal,id_try_out,jawaban,step,formControl){
	var form = new FormData();
form.append("kode_list_soal", kode_list_soal);
form.append("id_try_out", id_try_out);
form.append("jawaban", jawaban);

var settings = {
  "async": true,
  "crossDomain": true,
  "url": base_url+"api/process/delete_answer",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "99683224-f1e7-455e-b8af-d790b9b274f6"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

$.ajax(settings).done(function (response) {
	$(".BatalJawaban").hide();
	$(formControl).prop('checked',false);
	$("#btn_sel-"+step).removeClass('btn_isi');
// console.log(response);
});
}

function checkjawaban(id_try_out){
	var settings = {
  "async": true,
  "crossDomain": true,
  "url": base_url+"api/check/jawaban?id_try_out="+id_try_out,
  "method": "GET",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "ff22eb9f-0b88-4c46-9515-5666446dbf0f"
  }
}

$.ajax(settings).done(function (response) {
	var sizeArray=response.results.length;
	$(".tdNomor").html("<th>Nomor</th>");
	$(".tdJawaban").html("<th>Jawaban Tersimpan</th>");
	for (var i = 0; i < response.results.length; i++) {

		$(".tdNomor").append("<td>"+response.results[i].nomor+"</td>");
		if(response.results[i].jawaban_siswa==null){
			$(".tdJawaban").append("<td>-</td>");
		}else{
			$(".tdJawaban").append("<td>"+response.results[i].jawaban_siswa+"</td>");

		}
	}
  // console.log(sizeArray);
});
}

// var isChecked=$(formControl).prop('checked');
// if(isChecked){
//
// }else if(!isChecked){
// 	$(formControl).prop('checked',true);
// 	isChecked=$(formControl).prop('checked');
// }
//
// console.log("Statusnya "+isChecked);
