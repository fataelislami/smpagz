/*  Wizard */
jQuery(function ($) {
	"use strict";
	$('form#wrapped').attr('action', 'proses');
	$("#wizard_container").wizard({
		stepsWrapper: "#wrapped",
		submit: ".submit",
		beforeSelect: function (event, state) {
			if ($('input#website').val().length != 0) {
				return false;
			}
			if (!state.isMovingForward)
				return true;
			var inputs = $(this).wizard('state').step.find(':input');
			return !inputs.length || !!inputs.valid();
		}
	}).validate({
		errorPlacement: function (error, element) {
			if (element.is(':radio') || element.is(':checkbox')) {
				error.insertBefore(element.next());
			} else {
				error.insertAfter(element);
			}
		}
	});
	//  progress bar
	$("#progressbar").progressbar();
	$("#wizard_container").wizard({
		afterSelect: function (event, state) {
			$("#progressbar").progressbar("value", state.percentComplete);
			$("#location").text("(" + state.stepsComplete + "/" + state.stepsPossible + ")");
		},
		beforeForward: function (event, state) {
			if(password_event==''){
				alert("password event harus diisi");
				return false;
			}else{
				var event_id=$(".event_option").val();
				auth_event(event_id,password_event);
				if(valid_event){
					return valid_event;
				}else{
					return false;
				}
			}
			// alert('hai');
		},
	});
	// Validate select
	$('#wrapped').validate({
		ignore: [],
		rules: {
			select: {
				required: true
			}
		},
		errorPlacement: function (error, element) {
			if (element.is('select:hidden')) {
				error.insertAfter(element.next('.nice-select'));
			} else {
				error.insertAfter(element);
			}
		}
	});
});
//p4s
var passwordsInfo 	= $('#pass-info'); //id of indicator element
var nise_input;
var password_event='';
var valid_event=false;
$("#form-password").hide();
$("#form-password-event").hide();
$(".forward").show();
$("#password").val();
identitas(nise);
//p4send
// Summary
function getVals(formControl, controlType) {
	switch (controlType) {

		case 'NISE':
			// Get the value for a input text
			var value = $(formControl).val();
			nise_input=$(formControl).val();
			identitas(value);
			break;
			case 'PASSWORD':
				// Get the value for a input text
				var value = $(formControl).val();
				auth_siswa(nise_input,value);
				break;
		 case 'pil_1':
			// Get the value for a select
			var value = $(formControl).val();
			$("#country").text(value);
			break;
		 case 'pil_univ_1':
			 // Get the value for a select
			var value = $(formControl).val();
			// alert(value+"-"+nise_input);
			get_list_jurusan(nise,value);
			break;
			case 'pil_univ_2':
				// Get the value for a select
			 var value = $(formControl).val();
			 // alert(value+"-"+nise_input);
			 get_list_jurusan2(nise,value);
			 break;
			 case 'PASSWORD_EVENT':
				 // Get the value for a input text
				 var value = $(formControl).val();
				 var event_id=$(".event_option").val();
				 password_event=value;
				 auth_event(event_id,password_event);
				 break;

	}
}
// alert(base_url);
function auth_event(id,password){
	var form = new FormData();
form.append("id", id);
form.append("password", password);

var settings = {
"async": true,
"crossDomain": true,
"url": base_url+"api/event/auth",
"method": "POST",
"headers": {
	"cache-control": "no-cache",
	"Postman-Token": "fb446c5c-1d09-4f99-9302-44ea7c2ee5cd"
},
"processData": false,
"contentType": false,
"mimeType": "multipart/form-data",
"data": form
}

$.ajax(settings).done(function (response) {
var obj=JSON.parse(response);
if(obj.total_result==1){
	valid_event=true;
}else{
	alert("Password Event Salah");
	valid_event=false;
}
});
}
// function auth_siswa(username,password){
// 	var form = new FormData();
// form.append("username", username);
// form.append("password", password);
//
// var settings = {
// "async": true,
// "crossDomain": true,
// "url": base_url+"api/check/auth_siswa",
// "method": "POST",
// "headers": {
// 	"cache-control": "no-cache",
// 	"Postman-Token": "a0e771f7-24ba-4b05-bb17-0229156ca978"
// },
// "processData": false,
// "contentType": false,
// "mimeType": "multipart/form-data",
// "data": form
// }
//
// $.ajax(settings).done(function (response) {
// var obj=JSON.parse(response);
// if(obj.status=='success'){
// 	$(".forward").show();
// 	passwordsInfo.removeClass('weakpass').addClass('goodpass').html("Password Terkonfirmasi");
// }else{
// 	$(".forward").hide();
// 	passwordsInfo.removeClass('goodpass').addClass('weakpass').html("Password Salah");
// }
// console.log(obj.status);
// });
// }
function get_kampus(){
	var settings = {
	"async": true,
	"crossDomain": true,
	"url": base_url+"api/form/load_kampus",
	"method": "GET",
	"headers": {
		"cache-control": "no-cache",
		"Postman-Token": "f7dbdf6b-8b4c-4188-a8dc-da46223c0b9b"
	}
}

$.ajax(settings).done(function (response) {
	var total_result=response.total_result;
		if(total_result==0){
			$("select#pil_univ_1").html('<option value="NULL">-----</option>');
			$("select#pil_univ_2").html('<option value="NULL">-----</option>');
			$('select#pil_univ_1').niceSelect('update');
			$('select#pil_univ_2').niceSelect('update');
		}else{
			$("select#pil_univ_1").html('<option value="NULL">Pilih Kampus</option>');
			$("select#pil_univ_2").html('<option value="NULL">Pilih Kampus 2</option>');

				for (var i = 0; i < total_result; i++) {
				$("select#pil_univ_1").append('<option value="'+response.results[i].id+'">'+response.results[i].nama+'</option>');
				$("select#pil_univ_2").append('<option value="'+response.results[i].id+'">'+response.results[i].nama+'</option>');

				// alert("HAI");
			}
			$('select#pil_univ_1').niceSelect('update');
			$('select#pil_univ_2').niceSelect('update');

			// alert("update");
		}


});
}
function get_list_jurusan2(nise,id_univ){
	var settings = {
	"async": true,
	"crossDomain": true,
	"url": base_url+"api/form/pil_jurusan?nis="+nise+"&id_univ="+id_univ,
	"method": "GET",
	"headers": {
		"cache-control": "no-cache",
		"Postman-Token": "f7dbdf6b-8b4c-4188-a8dc-da46223c0b9b"
	}
}

$.ajax(settings).done(function (response) {
	console.log(response);
	var total_result=response.total_result;
	var kelas =nise.substring(6, 7);
	if(kelas!=3 && kelas!=4){
		jurusan("hide");
	}else{
		jurusan("show");
		if(total_result==0){
			$("select#pil_2").html('<option value="NULL">-----</option>');
			$('select#pil_2').niceSelect('update');

		}else{
			$("select#pil_2").html('<option value="NULL">Pilih Jurusan 2</option>');

				for (var i = 0; i < total_result; i++) {
				$("select#pil_2").append('<option value="'+response.results[i].kode+'">'+response.results[i].jurusan+'</option>');
				// alert("HAI");
			}
			$('select#pil_2').niceSelect('update');
			// alert("update");
		}
	}

});
}
function get_list_jurusan(nise,id_univ){
var settings = {
"async": true,
"crossDomain": true,
"url": base_url+"api/form/pil_jurusan?nis="+nise+"&id_univ="+id_univ,
"method": "GET",
"headers": {
	"cache-control": "no-cache",
	"Postman-Token": "f7dbdf6b-8b4c-4188-a8dc-da46223c0b9b"
}
}

$.ajax(settings).done(function (response) {
console.log(response);
var total_result=response.total_result;
// alert(total_result);
var kelas =nise.substring(6, 7);
if(kelas!=3 && kelas!=4){
	jurusan("hide");
}else{
	jurusan("show");
	if(total_result==0){
		$("select#pil_1").html('<option value="NULL">-----</option>');
		$('select#pil_1').niceSelect('update');
	}else{
		$("select#pil_1").html('<option value="NULL">Pilih Jurusan 1</option>');

			for (var i = 0; i < total_result; i++) {
			$("select#pil_1").append('<option value="'+response.results[i].kode+'">'+response.results[i].jurusan+'</option>');
			// alert("HAI");
		}
		$('select#pil_1').niceSelect('update');
		// alert("update");
	}
}

});
}
function jurusan(params){
	if(params=="hide"){
		$("#pil_1").attr('class','wide');
		$("#pil_2").attr('class','wide');
		$("#jurusan").hide();
	}else{
		// $("#pil_1").attr('class','wide required');
		$("#pil_1").attr('class','wide');
		// $("#pil_2").attr('class','wide required');
		$("#pil_2").attr('class','wide');
		$("#jurusan").show();
	}
}
function identitas(nise){
	var settings = {
"async": true,
"crossDomain": true,
"url": base_url+"api/form/identitas?nis="+nise,
"method": "GET",
"headers": {
	"cache-control": "no-cache",
	"Postman-Token": "29f439f6-2d33-480f-9346-1732c801274f"
}
}

$.ajax(settings).done(function (response) {
if(response.total_result==0){
	$("#nama").val("NISE Belum Terdaftar");
	$("select.event_option").html('<option>---</option>');
	$('select.event_option').niceSelect('update');
	// $("#password").val('');
	$("#form-password").hide();
	$("#form-password-event").hide();
	$(".forward").hide();
	$("#jurusan").hide();
	$("#select_event").hide();
	$("#event_false").hide();
	$("#password").val('');
	passwordsInfo.hide();
	passwordsInfo.removeClass('goodpass').addClass('weakpass').html("Masukan Password Untuk Melanjutkan");
}else{
	getevent(nise);
	// alert("NISE TERDAFTAR");
	get_kampus();
	var kelas =nise.substring(6, 7);
	if(kelas!=3 && kelas!=4){
		jurusan("hide");
	}else{
		 jurusan("show");
	}
	$("#form-password").show();
	passwordsInfo.show();
	// getevent(nise);
	$("#nama").val(response.results[0].nama);
}
});
}
function getevent(nise){
	var settings = {
		"async": true,
		"crossDomain": true,
		"url": base_url+"api/event/tryout?nis="+nise,
		"method": "GET",
		"headers": {
			"cache-control": "no-cache",
			"Postman-Token": "039dad8e-a83e-4725-ac41-d66b3efffe78"
		}
	}

	$.ajax(settings).done(function (response) {
		var total_result=response.total_result;
		if(total_result==0){
			$("#event_false").show();
			$("#select_event").hide();
			$("#event").val("Belum Ada Event Terdaftar");
			$("#event_false").attr('class','required');
			$(".forward").hide();
		}else{
			$("#event_false").attr('class','');
			$("#event_false").hide();

			$("#select_event").show();
			// var obj=JSON.parse(response.results);
			console.log(response.results[0].id);
			$("select.event_option").html('<option value="'+response.results[0].id+'">'+response.results[0].nama+'</option>');
			for (var i = 1; i < total_result; i++) {
				$("select.event_option").append('<option value="'+response.results[i].id+'">'+response.results[i].nama+'</option>');
				// alert("HAI");
			}
			$('select.event_option').niceSelect('update');
			$("#form-password-event").show();
			// alert("update");
		}
	});
}
