	/*  Wizard */
	jQuery(function ($) {
		"use strict";
		$('form#wrapped').attr('action', 'finish');
		$("#wizard_container").wizard({
			stepsWrapper: "#wrapped",
			submit: ".submit",
			beforeSelect: function (event, state) {
				if ($('input#website').val().length != 0) {
					return false;
				}
				if (!state.isMovingForward)
					return true;
				var inputs = $(this).wizard('state').step.find(':input');
				return !inputs.length || !!inputs.valid();
			}
		}).validate({
			errorPlacement: function (error, element) {
				if (element.is(':radio') || element.is(':checkbox')) {
					error.insertBefore(element.next());
				} else {
					error.insertAfter(element);
				}
			}
		});
		// $("#q1").click(function(){
		// 	$("#wizard_container").wizard('select',0); //SELECT KE STEP BERAPA
		// });
		// $("#q2").click(function(){
		// 	$("#wizard_container").wizard('select',1); //SELECT KE STEP BERAPA
		// });
		// $("#wizard_container").wizard('select',1); //SELECT KE STEP BERAPA
		 console.log($("#wizard_container").wizard('stepCount')); //SELECT KE STEP BERAPA

		//  progress bar
		$("#progressbar").progressbar();
		$("#wizard_container").wizard({
			afterSelect: function (event, state) {
				$("#progressbar").progressbar("value", state.percentComplete);
				$("#location").text("(" + state.stepsComplete + "/" + state.stepsPossible + ")");
				console.log("SELECTED"+state.stepIndex);
			}
		});

		// Validate select
		$('#wrapped').validate({
			ignore: [],
			rules: {
				select: {
					required: true
				}
			},
			errorPlacement: function (error, element) {
				if (element.is('select:hidden')) {
					error.insertAfter(element.next('.nice-select'));
				} else {
					error.insertAfter(element);
				}
			}
		});
	});
	//Init Data OnLoad

	 //Init Data OnLoad
// Summary
function getVals(formControl, controlType) {
	// if(controlType=='jawaban_1'){
		var value = $(formControl).val();
		var extract_value=value.split("#");
		var jawaban=extract_value[0];
		var kode_list_soal=extract_value[1];
		var id_quiz=extract_value[2];
		console.log(value);
		// console.log(kode_list_soal);
		// console.log(id_quiz);
		answer(kode_list_soal,id_quiz,jawaban);
	// }
	// switch (controlType) {
	//
	// 	case 'jawaban_1':
	// 		// Get the value for a radio
	// 		var value = $(formControl).val();
	// 		$("#jawaban_1").text(value);
	// 		break;
	//
	// 	case 'jawaban_2':
	// 		// Get name for set of checkboxes
	// 		var checkboxName = $(formControl).attr('name');
	//
	// 		// Get all checked checkboxes
	// 		var value = [];
	// 		$("input[name*='" + checkboxName + "']").each(function () {
	// 			// Get all checked checboxes in an array
	// 			if (jQuery(this).is(":checked")) {
	// 				value.push($(this).val());
	// 			}
	// 		});
	// 		$("#jawaban_2").text(value.join(", "));
	// 		break;
	//
	// 	case 'question_3':
	// 		// Get the value for a radio
	// 		var value = $(formControl).val();
	// 		$("#question_3").text(value);
	// 		break;
	//
	// 	case 'additional_message':
	// 		// Get the value for a textarea
	// 		var value = $(formControl).val();
	// 		$("#additional_message").text(value);
	// 		break;
	// }
}

function answer(kode_list_soal,id_quiz,jawaban){
	var form = new FormData();
form.append("kode_list_soal", kode_list_soal);
form.append("id_quiz", id_quiz);
form.append("jawaban", jawaban);

var settings = {
  "async": true,
  "crossDomain": true,
  "url": base_url+"api/process/answer_quiz",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache",
    "Postman-Token": "99683224-f1e7-455e-b8af-d790b9b274f6"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
}
